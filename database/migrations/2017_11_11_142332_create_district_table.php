<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDistrictTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('districts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('Name',250)->unique();
            $table->decimal('long',9,6)->nullable();
            $table->decimal('lat',9,6)->nullable();
            $table->integer('Government_Id')->unsigned();
           $table->foreign('Government_Id')
              ->references('id')->on('governments')
              ->onDelete('cascade')
              ->onupdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::drop('districts');
    }
}
