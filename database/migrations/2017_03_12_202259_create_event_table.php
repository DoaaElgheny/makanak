<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::create('events', function (Blueprint $table) {
             $table->increments('id')->unsigned();
             $table->string('name');
             $table->integer('event_type');
             $table->integer('organizer_id')->unsigned();
             $table->foreign('organizer_id')->references('id')->on('users');
             $table->string('cover_photo')->nullable();
             $table->integer('region_id')->unsigned();
             $table->foreign('region_id')
                ->references('id')
                ->on('districts');
             $table->text('description')->nullable();
             $table->integer('attendance_type');
             $table->integer('subscribe_type');
             $table->float('price')->nullable();
             $table->string('address');
             $table->integer('capacity')->nullable();
             $table->string('fb_link')->nulable();
             $table->decimal('long',9,6);
             $table->decimal('lat',9,6);
             $table->dateTime('date_from');
             $table->dateTime('date_to');
             $table->softDeletes();
             $table->timestamps();

         });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('events');
    }
}
