<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
         Schema::create('advertises', function (Blueprint $table) {
             $table->increments('id')->unsigned();
             $table->string('name', 150);
             $table->text('description');
             $table->string('photo')->nullable();
             $table->boolean('approval');
             $table->integer('k_index')->nullable();
             $table->integer('created_by')->unsigned();
             $table->foreign('created_by')
                ->references('id')
                ->on('users')
                ->onUpdate('cascade')
                ->onDelete('cascade');
             $table->softDeletes();
             $table->timestamps();
        });
    
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('advertises');
    }
}
