<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToCowrkingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('coworking', function(Blueprint $table)
        {
            
            $table->decimal('total_price',8,2)->nullable();
            $table->decimal('paid_price',8,2)->nullable();
            $table->time('total_time')->nullable();
            $table->time('calculated_time')->nullable();
            $table->integer('Place_ID')->nullable();
        
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('coworking', function(Blueprint $table)
        {
            
            $table->dropColumn('total_price');
            $table->dropColumn('paid_price');
            $table->dropColumn('total_time');
            $table->integer('Place_ID')->nullable();
        });
    }
}
