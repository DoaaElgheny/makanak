<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventsTargetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('events_targets', function (Blueprint $table) {
             $table->increments('id')->unsigned();
             $table->integer('event_id');
             $table->integer('target_id');
             $table->softDeletes();
             $table->timestamps();
             });
             
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('events_targets');
    }
}
