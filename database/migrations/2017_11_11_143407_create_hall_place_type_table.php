<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHallPlaceTypeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('halles_types', function (Blueprint $table) {
             $table->increments('id')->unsigned();
             $table->integer('Hall_id')->unsigned();
             $table->foreign('Hall_id')
                ->references('id')
                ->on('halles')
                ->onUpdate('cascade')
                ->onDelete('cascade');
              $table->integer('Type_id')->unsigned();   
             $table->foreign('Type_id')
                ->references('id')
                ->on('types')
                ->onUpdate('cascade')
                ->onDelete('cascade');
               
             $table->softDeletes();
             $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('halles_types');
    }
}
