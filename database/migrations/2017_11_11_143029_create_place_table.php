<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlaceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('places', function (Blueprint $table) {
             $table->increments('id')->unsigned();
             $table->string('Name', 150);
             $table->string('Address', 32);
             $table->string('Tele',11);
             $table->string('Tele2',11);
             $table->string('Facebook', 500);
             $table->string('Youtube', 500);
             $table->string('Twitter', 500);
             $table->string('website', 500);
             $table->integer('IsRegester');
             $table->longText('Logo');
             $table->string('Email', 150)->unique();
             $table->longText('Description', 100);
          
             $table->integer('IsActive');
             $table->integer('District_Id')->unsigned();
             $table->foreign('District_Id')
                ->references('id')
                ->on('districts')
                ->onUpdate('cascade')
                ->onDelete('cascade');
             $table->integer('Type')->unsigned();
             $table->foreign('Type')
                ->references('id')
                ->on('types')
                ->onUpdate('cascade')
                ->onDelete('cascade');
              
             $table->softDeletes();
             $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('places');
    }
}
