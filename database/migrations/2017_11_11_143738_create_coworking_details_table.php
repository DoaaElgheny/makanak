<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCoworkingDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
        Schema::create('coworking', function (Blueprint $table) {
             $table->increments('id')->unsigned();
             $table->string('Name', 150);
             $table->dateTime('TimeIN');
             $table->dateTime('TimeOut');
             $table->decimal('Price', 8, 2);
             $table->decimal('addtional', 8, 2);
             $table->time('Active');
             $table->integer('Hall_id')->unsigned();
             $table->foreign('Hall_id')
                ->references('id')
                ->on('halles')
                ->onUpdate('cascade')
                ->onDelete('cascade');
             $table->softDeletes();
             $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('coworking');//
    }
}
