<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHallRangeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('hall_range', function (Blueprint $table) {
             $table->increments('id')->unsigned();
             $table->integer('From');
             $table->integer('to');
             $table->decimal('price');
             $table->integer('Hall_Id')->unsigned();
             $table->foreign('Hall_Id')
                ->references('id')
                ->on('halles')
                ->onUpdate('cascade')
                ->onDelete('cascade');
             $table->softDeletes();
             $table->timestamps();
         });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('hall_range');
    }
}
