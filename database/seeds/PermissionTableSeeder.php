<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Reset cached roles and permissions
        app()['cache']->forget('spatie.permission.cache');

        // create permissions
        Permission::create(['name' => 'edit' ,'label' => 'User Managment']);
        Permission::create(['name' => 'delete' ,'label' => 'User Managment']);
        Permission::create(['name' => 'publish','label' => 'User Managment']);
        Permission::create(['name' => 'unpublish' ,'label' => 'User Managment']);
    }
}
