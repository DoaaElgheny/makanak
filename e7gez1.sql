-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 11, 2017 at 05:50 PM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 7.0.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `e7gez`
--

-- --------------------------------------------------------

--
-- Table structure for table `advertises`
--

CREATE TABLE `advertises` (
  `id` int(10) UNSIGNED NOT NULL,
  `Image` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `Description` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `Type` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `IsAdves` int(11) NOT NULL,
  `Place_id` int(10) UNSIGNED NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `attributes`
--

CREATE TABLE `attributes` (
  `id` int(10) UNSIGNED NOT NULL,
  `Name` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `Type` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `attributes`
--

INSERT INTO `attributes` (`id`, `Name`, `Type`, `created_at`, `updated_at`) VALUES
(1, 'FaceBook', 'text', NULL, NULL),
(2, 'youtube', 'text', NULL, NULL),
(3, ' اضافة مستخدمين', 'text', NULL, NULL),
(4, 'عرض مكان', 'text', NULL, NULL),
(6, 'ميزه جديده', '', '2017-06-28 13:34:01', '2017-06-28 13:34:01');

-- --------------------------------------------------------

--
-- Table structure for table `districts`
--

CREATE TABLE `districts` (
  `id` int(10) UNSIGNED NOT NULL,
  `Name` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `Government_Id` int(10) UNSIGNED NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `districts`
--

INSERT INTO `districts` (`id`, `Name`, `Government_Id`, `deleted_at`, `created_at`, `updated_at`) VALUES
(2, 'اسيوط', 2, NULL, '2017-06-23 11:37:49', '2017-06-23 11:37:49'),
(3, 'صدفا', 5, NULL, '2017-06-28 14:34:32', '2017-06-28 14:34:32');

-- --------------------------------------------------------

--
-- Table structure for table `facilites`
--

CREATE TABLE `facilites` (
  `id` int(10) UNSIGNED NOT NULL,
  `Name` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `class` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `Image` longtext COLLATE utf8_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `facilites`
--

INSERT INTO `facilites` (`id`, `Name`, `class`, `Image`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'داتا شو', 'glyphicon glyphicon-film', '/assets/img/Wi-Fi%20Filled.png', NULL, NULL, NULL),
(2, 'مكيف', 'glyphicon glyphicon-certificate', '/assets/img/Wi-Fi%20Filled.png', NULL, NULL, NULL),
(3, 'مشروبات', 'fa fa-glass', '/assets/img/Video%20Projector%20Filled.png\r\n', NULL, NULL, NULL),
(5, 'واى فاى', 'fa fa-wifi', '/assets/img/Wi-Fi%20Filled.png', NULL, NULL, NULL),
(12, 'ميزه جديده', '', '/assets/img/Video%20Projector%20Filled.png\n', NULL, '2017-06-28 13:25:23', '2017-06-28 13:25:23'),
(13, 'ميزه جديده', '', '/assets/dist/img/rf_detail_152_0.jpg', NULL, '2017-07-02 13:55:37', '2017-07-02 13:55:37');

-- --------------------------------------------------------

--
-- Table structure for table `governments`
--

CREATE TABLE `governments` (
  `id` int(10) UNSIGNED NOT NULL,
  `Name` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `governments`
--

INSERT INTO `governments` (`id`, `Name`, `deleted_at`, `created_at`, `updated_at`) VALUES
(2, 'القاهره', NULL, '2017-06-03 10:25:06', '2017-06-03 10:27:04'),
(3, 'جديد', NULL, '2017-06-23 11:57:12', '2017-06-23 11:57:12'),
(4, 'jjj', NULL, '2017-06-23 11:58:46', '2017-06-23 11:58:46'),
(5, 'اسيوط', NULL, '2017-06-28 14:33:49', '2017-06-28 14:33:49');

-- --------------------------------------------------------

--
-- Table structure for table `halles`
--

CREATE TABLE `halles` (
  `id` int(10) UNSIGNED NOT NULL,
  `Name` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `Price` int(11) NOT NULL,
  `Resv_Type` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `Capacity` int(11) NOT NULL,
  `Place_Id` int(10) UNSIGNED NOT NULL,
  `WorkStart` date NOT NULL,
  `WorkEnd` date NOT NULL,
  `Image` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `halles`
--

INSERT INTO `halles` (`id`, `Name`, `Price`, `Resv_Type`, `Capacity`, `Place_Id`, `WorkStart`, `WorkEnd`, `Image`, `deleted_at`, `created_at`, `updated_at`) VALUES
(15, 'jhxj', 20, '', 30, 29, '0000-00-00', '0000-00-00', '', NULL, NULL, NULL),
(17, 'ghg', 20, 'vhvhj', 30, 30, '2017-07-12', '2017-07-12', '', NULL, NULL, NULL),
(14, 'قاعة جديدة 20', 30, 'قاعة افراح', 20, 30, '0000-00-00', '0000-00-00', '', NULL, '2017-07-07 12:17:01', '2017-07-07 12:17:01'),
(18, 'قاعه مهندس ضاحي', 20, '', 30, 33, '0000-00-00', '0000-00-00', '', NULL, '2017-07-11 11:16:16', '2017-07-11 11:16:16');

-- --------------------------------------------------------

--
-- Table structure for table `halles_facilites`
--

CREATE TABLE `halles_facilites` (
  `id` int(10) UNSIGNED NOT NULL,
  `Hall_id` int(10) UNSIGNED NOT NULL,
  `Facility_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `halles_facilites`
--

INSERT INTO `halles_facilites` (`id`, `Hall_id`, `Facility_id`, `created_at`, `updated_at`) VALUES
(1, 1, 2, NULL, NULL),
(2, 1, 1, NULL, NULL),
(3, 2, 2, NULL, NULL),
(4, 2, 1, NULL, NULL),
(5, 2, 3, NULL, NULL),
(6, 2, 5, NULL, NULL),
(7, 5, 5, NULL, NULL),
(8, 5, 5, NULL, NULL),
(9, 6, 6, NULL, NULL),
(10, 6, 6, NULL, NULL),
(11, 6, 6, NULL, NULL),
(12, 6, 6, NULL, NULL),
(13, 7, 7, NULL, NULL),
(14, 7, 7, NULL, NULL),
(15, 7, 7, NULL, NULL),
(16, 7, 7, NULL, NULL),
(17, 8, 8, NULL, NULL),
(18, 9, 9, NULL, NULL),
(19, 9, 9, NULL, NULL),
(20, 10, 10, NULL, NULL),
(21, 10, 10, NULL, NULL),
(22, 11, 11, NULL, NULL),
(23, 13, 13, NULL, NULL),
(24, 18, 18, NULL, NULL),
(25, 20, 20, NULL, NULL),
(26, 20, 20, NULL, NULL),
(27, 20, 20, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `halles_types`
--

CREATE TABLE `halles_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `Hall_id` int(10) UNSIGNED NOT NULL,
  `Type_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `halles_types`
--

INSERT INTO `halles_types` (`id`, `Hall_id`, `Type_id`, `created_at`, `updated_at`) VALUES
(1, 5, 5, NULL, NULL),
(2, 6, 6, NULL, NULL),
(3, 6, 6, NULL, NULL),
(4, 6, 6, NULL, NULL),
(5, 7, 7, NULL, NULL),
(6, 7, 7, NULL, NULL),
(7, 8, 8, NULL, NULL),
(8, 8, 8, NULL, NULL),
(9, 8, 8, NULL, NULL),
(10, 8, 8, NULL, NULL),
(11, 9, 9, NULL, NULL),
(12, 10, 10, NULL, NULL),
(13, 10, 10, NULL, NULL),
(14, 11, 11, NULL, NULL),
(15, 12, 12, NULL, NULL),
(16, 12, 12, NULL, NULL),
(17, 13, 13, NULL, NULL),
(18, 13, 13, NULL, NULL),
(19, 18, 18, NULL, NULL),
(20, 18, 18, NULL, NULL),
(21, 20, 20, NULL, NULL),
(22, 20, 20, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `images`
--

CREATE TABLE `images` (
  `id` int(10) UNSIGNED NOT NULL,
  `Url` varchar(400) COLLATE utf8_unicode_ci NOT NULL,
  `places_id` int(11) NOT NULL,
  `places_type` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `IsImg` int(11) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2017_05_22_194410_create_governments_table', 1),
('2017_05_22_195341_create_districts_table', 1),
('2017_05_22_195908_create_places_table', 1),
('2017_05_23_193410_create_users_table', 1),
('2017_05_23_195321_create_attributes_table', 1),
('2017_05_23_195358_create_places_attributes_table', 1),
('2017_05_23_195711_create_packges_table', 1),
('2017_05_23_200113_create_types_table', 1),
('2017_05_23_200254_create_halles_table', 1),
('2017_05_23_200318_create_halles_types_table', 1),
('2017_05_23_201217_create_facilites_table', 1),
('2017_05_23_201338_create_halles_facilites_table', 1),
('2017_05_23_201747_create_images_table', 1),
('2017_05_23_202305_create_schadules_table', 1),
('2017_05_23_203529_create_advertises_table', 1),
('2017_05_22_194410_create_governments_table', 1),
('2017_05_22_195341_create_districts_table', 1),
('2017_05_22_195908_create_places_table', 1),
('2017_05_23_193410_create_users_table', 1),
('2017_05_23_195321_create_attributes_table', 1),
('2017_05_23_195358_create_places_attributes_table', 1),
('2017_05_23_195711_create_packges_table', 1),
('2017_05_23_200113_create_types_table', 1),
('2017_05_23_200254_create_halles_table', 1),
('2017_05_23_200318_create_halles_types_table', 1),
('2017_05_23_201217_create_facilites_table', 1),
('2017_05_23_201338_create_halles_facilites_table', 1),
('2017_05_23_201747_create_images_table', 1),
('2017_05_23_202305_create_schadules_table', 1),
('2017_05_23_203529_create_advertises_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `ouruseroffer`
--

CREATE TABLE `ouruseroffer` (
  `ID` int(255) NOT NULL,
  `Name` varchar(300) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `Email` varchar(500) NOT NULL,
  `Phone` int(14) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ouruseroffer`
--

INSERT INTO `ouruseroffer` (`ID`, `Name`, `Email`, `Phone`) VALUES
(0, '?????', 'doaa.elgheny88@hotmail.com', NULL),
(0, 'ميزه جديده', 'banzer@ymail.com', NULL),
(0, 'ميمي', 'klamiaa035@gmail.com', NULL),
(0, 'ميزه جديده', 'klamiaa035@gmail.com', NULL),
(0, 'ميزه جديده', 'klamiaa035@gmail.com', NULL),
(0, 'ميزه جديده', 'klamiaa035@gmail.com', NULL),
(0, 'ميزه جديده', 'klamiaa035@gmail.com', NULL),
(0, 'اسيوط', 'doaa.elgheny88@hotmail.com', NULL),
(0, 'ميزه جديده', 'doaa.elgheny88@hotmail.com', NULL),
(0, 'ميزه جديده', 'doaa.elgheny88@hotmail.com', NULL),
(0, 'اسيوط', 'klamiaa035@gmail.com', NULL),
(0, 'اا', 'doaa.elgheny88@hotmail.com', NULL),
(0, 'اسيوط', 'doaa.elgheny88@hotmail.com', NULL),
(0, 'ميزه جديده', 'klamiaa035@gmail.com', NULL),
(0, 'ميزه جديده', 'doaa.elgheny88@hotmail.com', NULL),
(0, 'اسيوط', 'doaa.elgheny88@hotmail.com', NULL),
(0, 'ميزه جديده', 'banzer@ymail.com', NULL),
(0, 'اسيوط', 'klamiaa035@gmail.com', NULL),
(0, 'ميزه جديده', 'doaa.elgheny88@hotmail.com', NULL),
(0, 'ميزه جديده', 'eman_hassan_87@hotmail.com', NULL),
(0, 'ميزه جديده', 'klamiaa035@gmail.com', NULL),
(0, 'ميزه جديده', 'doaa.elgheny88@hotmail.com', NULL),
(0, 'ميزه جديده', 'klamiaa035@gmail.com', NULL),
(0, 'اسيوط', 'doaa.elgheny88@hotmail.com', NULL),
(0, 'اسيوط', 'doaa.elgheny2016@gmail.com', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `packges`
--

CREATE TABLE `packges` (
  `id` int(10) UNSIGNED NOT NULL,
  `Name` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `Price` float DEFAULT NULL,
  `Description` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `packges`
--

INSERT INTO `packges` (`id`, `Name`, `Price`, `Description`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'المجانية', 0, 'نعرض لك كل الأماكن والقاعات الموجودة بخدماتها وإمكانيتها ونسهل لك اختيار المكان الأمثل لحجز ونسهل لك اختيار المكان الأمثل لحجز', NULL, NULL, '2017-06-26 17:34:44'),
(2, 'المميزة', 10, 'نعرض لك كل الأماكن والقاعات الموجودة بخدماتها وإمكانيتها ونسهل لك اختيار المكان الأمثل لحجز ونسهل لك اختيار المكان الأمثل لحجز', NULL, NULL, NULL),
(3, 'الذهبية', 100.55, 'نعرض لك كل الأماكن والقاعات الموجودة بخدماتها وإمكانيتها ونسهل لك اختيار المكان الأمثل لحجز ونسهل لك اختيار المكان الأمثل لحجز', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `packges_attributes`
--

CREATE TABLE `packges_attributes` (
  `id` int(10) UNSIGNED NOT NULL,
  `Value` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Packge_id` int(10) UNSIGNED NOT NULL,
  `attrbuti_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `packges_attributes`
--

INSERT INTO `packges_attributes` (`id`, `Value`, `Packge_id`, `attrbuti_id`, `created_at`, `updated_at`) VALUES
(18, '', 1, 2, NULL, NULL),
(17, '', 1, 2, NULL, NULL),
(16, '', 1, 1, NULL, NULL),
(19, '', 2, 3, NULL, NULL),
(20, '', 2, 6, NULL, NULL),
(21, '', 3, 6, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permission_role`
--

CREATE TABLE `permission_role` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `places`
--

CREATE TABLE `places` (
  `id` int(10) UNSIGNED NOT NULL,
  `Name` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `Address` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `Tele` varchar(11) COLLATE utf8_unicode_ci NOT NULL,
  `Email` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Description` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `Type` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `IsActive` int(11) NOT NULL,
  `Packge_id` int(10) UNSIGNED NOT NULL,
  `District_Id` int(10) UNSIGNED NOT NULL,
  `OwnerName` varchar(300) COLLATE utf8_unicode_ci DEFAULT NULL,
  `EmailOwner` varchar(300) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TelOwner` int(14) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `Tele2` int(14) DEFAULT NULL,
  `Facebook` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Youtube` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Twitter` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `website` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Logo` longtext COLLATE utf8_unicode_ci NOT NULL,
  `IsRegester` int(3) NOT NULL,
  `Upgrade_Package` int(2) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `places`
--

INSERT INTO `places` (`id`, `Name`, `Address`, `Tele`, `Email`, `Description`, `Type`, `IsActive`, `Packge_id`, `District_Id`, `OwnerName`, `EmailOwner`, `TelOwner`, `deleted_at`, `created_at`, `updated_at`, `Tele2`, `Facebook`, `Youtube`, `Twitter`, `website`, `Logo`, `IsRegester`, `Upgrade_Package`) VALUES
(29, 'يوفوريا', 'ابراج النصر برج ج2 شقه 1 الدور ا', '', NULL, 'ممكان يعطيك السعاده', '3', 1, 1, 2, '', '', 0, NULL, '2017-07-07 09:16:44', '2017-07-07 09:16:44', 0, '', '', '', '', '/assets/dist/img/604c36bae1d24a6869aaa789869c1160.jpg', 0, 1),
(30, 'ومضة 2', '', '', NULL, '', '2', 1, 0, 2, NULL, NULL, 0, NULL, '2017-07-07 12:17:01', '2017-07-07 12:39:33', NULL, NULL, NULL, NULL, NULL, '/assets/dist/img/rf_detail_152_0.jpg', 0, 1),
(32, 'مكان دعاء', 'صدفا', '010', NULL, 'عاشسغعسلؤغعل', '3', 1, 1, 0, 'دعاء سيد', 'ss@yahoo.com', 1062425848, NULL, '2017-07-11 08:56:10', '2017-07-11 09:27:18', 0, 'https://www.facebook.com/eng.elgheny', '', '', '', '/assets/dist/img/IMG_20161119_161255.jpg', 0, 1),
(33, 'تيك', 'جامعة', '', NULL, 'تسغعشسل', '3', 1, 2, 0, '', '', 0, NULL, '2017-07-11 11:11:29', '2017-07-11 11:12:25', 0, 'jhgh', '', '', '', '/assets/dist/img/d1c15ad710b22a78a4a981232f7d770e.jpg', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `role_user`
--

CREATE TABLE `role_user` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `schadules`
--

CREATE TABLE `schadules` (
  `id` int(10) UNSIGNED NOT NULL,
  `Name` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Price` float NOT NULL,
  `IsEvent` int(11) NOT NULL,
  `Date_From` datetime NOT NULL,
  `Date_To` datetime NOT NULL,
  `Notes` varchar(300) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Event_Name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `Tele` varchar(11) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Hall_Id` int(10) UNSIGNED NOT NULL,
  `User_Id` int(10) UNSIGNED NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `schadules`
--

INSERT INTO `schadules` (`id`, `Name`, `Price`, `IsEvent`, `Date_From`, `Date_To`, `Notes`, `Event_Name`, `Tele`, `Hall_Id`, `User_Id`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'jhjjj', 20, 0, '2017-06-20 00:00:00', '2017-06-26 00:00:00', '', '', '', 1, 1, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `types`
--

CREATE TABLE `types` (
  `id` int(10) UNSIGNED NOT NULL,
  `Name` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `EngName` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `types`
--

INSERT INTO `types` (`id`, `Name`, `EngName`, `deleted_at`, `created_at`, `updated_at`) VALUES
(2, 'قاعة افراح', 'type', NULL, '2017-05-30 01:17:27', '2017-06-14 13:13:20'),
(3, 'قاعة اجتماعات', 'asd', NULL, '2017-05-30 01:17:46', '2017-05-30 01:17:46'),
(4, 'نوادي', 'shaza', NULL, '2017-05-30 01:17:53', '2017-05-30 01:17:53');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `Name` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `Tele` varchar(11) COLLATE utf8_unicode_ci NOT NULL,
  `Password` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `Type` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `Supervisor` int(10) UNSIGNED DEFAULT NULL,
  `Place_Id` int(10) UNSIGNED NOT NULL,
  `District_Id` int(10) UNSIGNED NOT NULL,
  `Email` varchar(400) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Role` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `Name`, `Tele`, `Password`, `Type`, `Supervisor`, `Place_Id`, `District_Id`, `Email`, `Role`, `deleted_at`, `remember_token`, `created_at`, `updated_at`) VALUES
(12, 'محمد عرفه', '', '123', '', NULL, 29, 0, 'ss@yahoo.com', 'User', NULL, NULL, '2017-07-07 09:16:44', '2017-07-07 09:16:44'),
(13, 'بانزر', '', '12345', '', NULL, 31, 0, 'banzeror@yahoo.com', 'User', NULL, NULL, '2017-07-11 08:41:54', '2017-07-11 08:41:54'),
(14, 'شكولاته', '', '1234567', '', NULL, 32, 0, 'bror@yahoo.com', 'User', NULL, NULL, '2017-07-11 08:56:10', '2017-07-11 08:56:10'),
(15, 'lمهندس ضاحي ', '', '123', '', NULL, 33, 0, 'Ahmed.dahe@gmail.com', 'User', NULL, NULL, '2017-07-11 11:11:29', '2017-07-11 11:11:29');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `advertises`
--
ALTER TABLE `advertises`
  ADD PRIMARY KEY (`id`),
  ADD KEY `advertises_place_id_foreign` (`Place_id`);

--
-- Indexes for table `attributes`
--
ALTER TABLE `attributes`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `attributes_name_unique` (`Name`);

--
-- Indexes for table `districts`
--
ALTER TABLE `districts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `districts_government_id_foreign` (`Government_Id`);

--
-- Indexes for table `facilites`
--
ALTER TABLE `facilites`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `governments`
--
ALTER TABLE `governments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `halles`
--
ALTER TABLE `halles`
  ADD PRIMARY KEY (`id`),
  ADD KEY `halles_place_id_foreign` (`Place_Id`);

--
-- Indexes for table `halles_facilites`
--
ALTER TABLE `halles_facilites`
  ADD PRIMARY KEY (`id`),
  ADD KEY `halles_facilites_hall_id_foreign` (`Hall_id`),
  ADD KEY `halles_facilites_facility_id_foreign` (`Facility_id`);

--
-- Indexes for table `halles_types`
--
ALTER TABLE `halles_types`
  ADD PRIMARY KEY (`id`),
  ADD KEY `halles_types_hall_id_foreign` (`Hall_id`),
  ADD KEY `halles_types_type_id_foreign` (`Type_id`);

--
-- Indexes for table `images`
--
ALTER TABLE `images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `packges`
--
ALTER TABLE `packges`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `packges_attributes`
--
ALTER TABLE `packges_attributes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `packges_attributes_packge_id_foreign` (`Packge_id`),
  ADD KEY `packges_attributes_attrbuti_id_foreign` (`attrbuti_id`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `permissions_name_unique` (`name`);

--
-- Indexes for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `permission_role_role_id_foreign` (`role_id`);

--
-- Indexes for table `places`
--
ALTER TABLE `places`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `places_email_unique` (`Email`),
  ADD KEY `places_packge_id_foreign` (`Packge_id`),
  ADD KEY `places_district_id_foreign` (`District_Id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_unique` (`name`);

--
-- Indexes for table `role_user`
--
ALTER TABLE `role_user`
  ADD PRIMARY KEY (`user_id`,`role_id`),
  ADD KEY `role_user_role_id_foreign` (`role_id`);

--
-- Indexes for table `schadules`
--
ALTER TABLE `schadules`
  ADD PRIMARY KEY (`id`),
  ADD KEY `schadules_hall_id_foreign` (`Hall_Id`),
  ADD KEY `schadules_user_id_foreign` (`User_Id`);

--
-- Indexes for table `types`
--
ALTER TABLE `types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `users_place_id_foreign` (`Place_Id`),
  ADD KEY `users_district_id_foreign` (`District_Id`),
  ADD KEY `users_supervisor_foreign` (`Supervisor`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `advertises`
--
ALTER TABLE `advertises`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `attributes`
--
ALTER TABLE `attributes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `districts`
--
ALTER TABLE `districts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `facilites`
--
ALTER TABLE `facilites`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `governments`
--
ALTER TABLE `governments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `halles`
--
ALTER TABLE `halles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `halles_facilites`
--
ALTER TABLE `halles_facilites`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT for table `halles_types`
--
ALTER TABLE `halles_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `images`
--
ALTER TABLE `images`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `packges`
--
ALTER TABLE `packges`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `packges_attributes`
--
ALTER TABLE `packges_attributes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `places`
--
ALTER TABLE `places`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;
--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `schadules`
--
ALTER TABLE `schadules`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `types`
--
ALTER TABLE `types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
