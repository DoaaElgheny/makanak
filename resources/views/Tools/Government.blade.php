@extends('master')
@section('content')
 <section class="panel-body">


 <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                     <h2 style="margin-left:85%">المحافظات <i class="fa fa-tasks" ></i></h2>
                        <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                     
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

  
<a type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal1" style="margin-bottom: 20px; margin-left:85%;background-color: #841851"> اضافة محافظة جديدة </a>


<!-- model -->
@if(Session::has('flash_message'))
    <div class="alert alert-danger">
        {{ Session::get('flash_message') }}<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    </div>
@endif
 
<div class="modal fade" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
     <div class="modal-content">
    
          <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                 <h4 class="modal-title span7 text-center" id="myModalLabel"><span class="title">اضافة محافظة جديدة</span></h4>
          </div>
    <div class="modal-body">
        <div class="tabbable"> <!-- Only required for left/right tabs -->
        <ul class="nav nav-tabs">
  

        
        </ul>
        <div class="tab-content">
        <div class="tab-pane active" id="tab1">
              <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                 
                  <div class="x_content">
                    <br />
                   {!! Form::open(['action' => "GovernmentController@store",'method'=>'post','id'=>'profileForm']) !!} 
             <fieldset style="    padding: .35em .625em .75em; margin: 0 2px; border: 1px solid silver;" dir="rtl">
  <legend style="width: fit-content;border: 0px;" dir="rtl">بيانات اساسية</legend>
 

 
                    <div class="form-group">
                  

                      <div class="col-md-12 col-sm-12 col-xs-12 has-feedback">
                        <input type="text" class="form-control has-feedback-left"  id="txtName" name="txtName" placeholder="اسم المحافظة" required>


                        <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
                      </div>
                     

                       
                      </div>

                   
                    
                      
</fieldset>
        
           


                       


                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
                          <button type="button" class="btn btn-primary " data-dismiss="modal">الغاء</button>
                         <button class="btn btn-primary" type="reset">اعادة ادخال</button>
     <button type="submit" disabled="disabled" name="login_form_submit" class="btn btn-success" value="save">حفظ</button>
                        </div>
                      </div>

      {!!Form::close() !!}                
        </div>
                </div>
                   </div>
                </div>
        </div>
        </div>
        </div>

    </div>
 

         </div>
     </div>
  </div>
<!-- ENDModal -->



                   
                    <table class="table table-hover table-bordered  dt-responsive nowrap Brands" cellspacing="0" width="100%" dir="rtl">
  <thead>
   <th></th>
        <th>Name</th>
        <th>Show All districts</th> 
        <th>Process</th> 
</tfoot>


<tbody style="text-align:center;">
<?php $i=1;?>
   @foreach($Government as $government)
          <tr>
            <td>{{$i++}}</td>   
            <td><a  class="testEdit" data-type="text" data-name="Name" data-value="{{$government->Name}}" data-pk="{{ $government->id }}">{{$government->Name}}</a></td>

            <td>
          

<ol style="text-align: right;">
@foreach($government->GetDistricts as $Value)
<li>{{$Value->Name}}</li>
@endforeach
</ol>

            </td>

            <td><a href="/Government/destroy/{{$government->id}}" class="btn btn-default btn-sm" ><span class="glyphicon glyphicon-trash"></span>  </a></td>
          </tr>
        @endforeach
</tbody>
</table>


                  </div>
                </div>
              </div>
 
  @section('other_scripts')


<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>



 <script type="text/javascript">

var editor;

  $(document).ready(function(){
     $('#profileForm')
        .formValidation({
            framework: 'bootstrap',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
           fields: {
                'Name': {
                 
                    validators: {
                  

             notEmpty:{
                    message:"يجب ادخال الاسم"
                  }
                  }
                }
            }  
});
       
     var table= $('.Brands').DataTable({
  
    select:true,
    responsive: true,
    "order":[[0,"asc"]],
    'searchable':true,
    "scrollCollapse":true,
    "paging":true,
    "pagingType": "simple",
      dom: 'lBfrtip',
        buttons: [
           
            
            { extend: 'excel', className: 'btn btn-primary dtb' }
            
            
        ],

fnRowCallback: function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
$.fn.editable.defaults.send = "always";

$.fn.editable.defaults.mode = 'inline';

$('.testEdit').editable({

      validate: function(value) {
        name=$(this).editable().data('name');
        if(name=="Name" )
        {
                if($.trim(value) == '') {
                    return 'يجب ادخال القيمة.';
                  }
                    }
                              if(name=="Name")
        {

           var regexp = /^[\u0600-\u06FF\s0-9\_]+$/;          

           if (!regexp.test(value)) 
           {
                return 'هذا الادخال غير صحيح';
           }
            
         } 
            if(name=="EngName")
        {

           var regexp = /^[a-zA-Z\s0-9\_]+$/;          

           if (!regexp.test(value)) 
           {
                return 'هذا الادخال غير صحيح .';
           }
            
         }
      
        },

    placement: 'right',
    url:'{{URL::to("/")}}/updateGovernments',
  
     ajaxOptions: {
     type: 'get',
    sourceCache: 'false',
     dataType: 'json'

   },

       params: function(params) {
            // add additional params from data-attributes of trigger element
            params.name = $(this).editable().data('name');

            // console.log(params);
            return params;
        },
        error: function(response, newValue) {
            if(response.status === 500) {
                return 'This Data Already Exist,Enter Correct Data.';
            } else {
                return response.responseText;
            }
        }
        ,    success:function(response)
        {
            if(response.status==="You do not have permission.")
             {
              return 'You do not have permission.';
              }
        }


});

}
});
   $('.Brands tfoot th').each(function () {
      var title = $('.Brands thead th').eq($(this).index()).text();       
     if($(this).index()>=1 && $(this).index()<=2)
            {

           $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
}
        

    });
  table.columns().every( function () {
  var that = this;
 $(this.footer()).find('input').on('keyup change', function () {
          that.search(this.value).draw();
            if (that.search(this.value) ) {
               that.search(this.value).draw();
           }
        });
      
    });

 });



  </script>
@stop

@stop
