@extends('master')
@section('content')
    @if(session()->has('message'))
        <div class="alert alert-success">
            {{ session()->get('message') }}
        </div>
    @endif
    @if($errors->any())
        <div class="alert alert-danger">
            @foreach($errors->all() as $error)
            {{$error}}</br>
            @endforeach
        </div>
    @endif
    {!! Form::open(['route' => 'saveUser', 'class' => 'form-horizontal', 'role' => 'form' ]) !!}
    <div class="form-group">
        {!! Form::label('name', 'name', ['class' => 'col-lg-2 control-label']) !!}

        <div class="col-lg-10">

                {{ Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Name' ,'required']) }}
        </div>
    </div>
    <div class="form-group">
        {!! Form::label('name', 'Username', ['class' => 'col-lg-2 control-label']) !!}

        <div class="col-lg-10">

            {{ Form::text('username', null, ['class' => 'form-control', 'placeholder' => 'username' ,'required']) }}
        </div>
    </div>
    <div class="form-group">
        {!! Form::label('name', 'Telephone', ['class' => 'col-lg-2 control-label']) !!}

        <div class="col-lg-10">

            {{ Form::text('telephone', null, ['class' => 'form-control', 'placeholder' => 'telephone' ,'required']) }}
        </div>
    </div>
    <div class="form-group">
        {!! Form::label('name', 'Email', ['class' => 'col-lg-2 control-label']) !!}

        <div class="col-lg-10">

            {{ Form::text('email', null, ['class' => 'form-control', 'placeholder' => 'email' ,'required']) }}
        </div>
    </div>
    <div class="form-group">
        {!! Form::label('name', 'Password', ['class' => 'col-lg-2 control-label']) !!}

        <div class="col-lg-10">

            {{ Form::password('password', null, ['class' => 'form-control' ,'required']) }}
        </div>
    </div>
    <div class="form-group">
        {!! Form::label('name', 'Permissions', ['class' => ' control-label']) !!}
</br>
        @foreach($groupedPermissions as $label=>$permissions)
        {!! Form::label('name', $label, ['class' => ' control-label']) !!}
                </br>
        @foreach($permissions as $permission)
        {!! Form::label('name', $permission->name, ['class' => ' control-label']) !!}
        {!! Form::checkbox('permissions[]', $permission->name) !!}
        </br>
        @endforeach
        @endforeach

    </div>
    <div style="float: right">
        {{ Form::submit('Add User ', ['class' => 'btn btn-success btn-xs']) }}
    </div>
    <div class="pull-left">
        {{ link_to_route('users', 'Cancel', [], ['class' => 'btn btn-danger btn-xs']) }}
    </div><!--pull-left-->

    {!! Form::close() !!}
@endsection

