@extends('master')
@section('content')
    @if(session()->has('message'))
        <div class="alert alert-success">
            {{ session()->get('message') }}
        </div>
    @endif
    @if($errors->any())
        <div class="alert alert-danger">
            @foreach($errors->all() as $error)
            {{$error}}</br>
            @endforeach
        </div>
    @endif
    <form method="post" action="{{route('updateUser', $user->id)}}" class="form-horizontal">
        {{ csrf_field() }}
        <div class="form-group">
            {!! Form::label('name', 'name', ['class' => 'col-lg-2 control-label']) !!}

            <div class="col-lg-10">

                {{ Form::text('name', $user->Name, ['class' => 'form-control', 'placeholder' => 'Name' ,'required']) }}
            </div>
        </div>
        <div class="form-group">
            {!! Form::label('name', 'Username', ['class' => 'col-lg-2 control-label']) !!}

            <div class="col-lg-10">

                {{ Form::text('username', $user->Username, ['class' => 'form-control', 'placeholder' => 'username' ,'required']) }}
            </div>
        </div>
        <div class="form-group">
            {!! Form::label('name', 'Telephone', ['class' => 'col-lg-2 control-label']) !!}

            <div class="col-lg-10">

                {{ Form::text('telephone', $user->Tele, ['class' => 'form-control', 'placeholder' => 'telephone' ,'required']) }}
            </div>
        </div>
        <div class="form-group">
            {!! Form::label('name', 'Email', ['class' => 'col-lg-2 control-label']) !!}

            <div class="col-lg-10">

                {{ Form::text('email', $user->Email, ['class' => 'form-control', 'placeholder' => 'email' ,'required']) }}
            </div>
        </div>
        <div class="form-group">
            {!! Form::label('name', 'Password', ['class' => 'col-lg-2 control-label']) !!}

            <div class="col-lg-10">

                {{ Form::text('password', $user->Password, ['class' => 'form-control' ,'required']) }}
            </div>
        </div>
        <div class="form-group">
            {!! Form::label('name', 'Permissions', ['class' => ' control-label']) !!}
            </br>
            @foreach($groupedPermissions as $label=>$permissions)
            {!! Form::label('name', $label, ['class' => ' control-label']) !!}
            </br>
            @foreach($permissions as $permission)
            {!! Form::label('name', $permission->name, ['class' => ' control-label']) !!}
            @if(in_array($permission->id,$attachedPermissionsArr))
            {!! Form::checkbox('permissions[]', $permission->name , 'checked') !!}
            @else
            {!! Form::checkbox('permissions[]', $permission->name ) !!}
            @endif
            </br>
            @endforeach
            @endforeach

        </div>
        <div style="float: right">
            {{ Form::submit('Update User ', ['class' => 'btn btn-success btn-xs']) }}
        </div>
        <div class="pull-left">
            {{ link_to_route('users', 'Cancel', [], ['class' => 'btn btn-danger btn-xs']) }}
        </div><!--pull-left-->

    </form>
@endsection

