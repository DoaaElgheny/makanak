@extends('master')
@section('content')

    <div class="form-group">
        {!! Form::label('name', 'name', ['class' => 'col-lg-2 control-label']) !!}

        <div class="col-lg-10">

            {{ Form::label('name', $user->Name, ['class' => 'form-control', 'placeholder' => 'Name' ,'required']) }}
        </div>
    </div>
    <div class="form-group">
        {!! Form::label('name', 'Username', ['class' => 'col-lg-2 control-label']) !!}

        <div class="col-lg-10">

            {{ Form::label('username', $user->Username, ['class' => 'form-control', 'placeholder' => 'username' ,'required']) }}
        </div>
    </div>
    <div class="form-group">
        {!! Form::label('name', 'Telephone', ['class' => 'col-lg-2 control-label']) !!}

        <div class="col-lg-10">

            {{ Form::label('telephone', $user->Tele, ['class' => 'form-control', 'placeholder' => 'telephone' ,'required']) }}
        </div>
    </div>
    <div class="form-group">
        {!! Form::label('name', 'Email', ['class' => 'col-lg-2 control-label']) !!}

        <div class="col-lg-10">

            {{ Form::label('email', $user->Email, ['class' => 'form-control', 'placeholder' => 'email' ,'required']) }}
        </div>
    </div>
    <div class="form-group">
        {!! Form::label('name', '-> Attached Permissions', ['class' => ' control-label']) !!}
        </br>
        @foreach($user->permissions as $permission)
            <h4>{{$permission->name}}</h4>
        @endforeach
    </div>
    <div class="pull-left">
        {{ link_to_route('users', 'Cancel', [], ['class' => 'btn btn-danger btn-xs']) }}
    </div><!--pull-left-->
@endsection