@extends('master')
@section('content')
    @if(session()->has('message'))
        <div class="alert alert-success">
            {{ session()->get('message') }}
        </div>
    @endif
    @if($errors->any())
        <div class="alert alert-danger">
            @foreach($errors->all() as $error)
            {{$error}}</br>
            @endforeach
        </div>
    @endif
    <div>
        <h2>Users</h2>
    </div>
    <h1 class="pull-right">
        <a class="btn btn-primary pull-right" style="margin-top: -10px;margin-bottom: 5px"
           href="{!! route('addUser') !!}">Add User</a>
    </h1>
    <table class="table table-hover table-bordered  dt-responsive nowrap  users" cellspacing="0" width="100%" dir="ltr">


        <thead>
        <th>Name</th>
        <th>Username</th>
        <th>Email</th>
        <th>Telephone</th>

        <th>Action</th>

        </thead>

        <tbody style="text-align:center;">

        @foreach($users as $user)
            <tr>
                <td>
                    {{$user->Name}}
                </td>
                <td>
                    {{$user->Username}}
                </td>
                <td>
                    {{$user->Email}}
                </td>
                <td>
                    {{$user->Tele}}
                </td>
                <td>
                    {!! Form::open(['route' => ['deleteUser', $user->id]]) !!}
                    <div class='btn-group'>

                        <a href="{{ route('showUser', $user->id) }}" class='btn btn-default btn-xs'>
                            <i class="glyphicon glyphicon-eye-open"></i>
                        </a>
                        <a href="{{ route('editUser', $user->id) }}" class='btn btn-default btn-xs'>
                            <i class="glyphicon glyphicon-edit"></i>
                        </a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', [
                            'type' => 'submit',
                            'class' => 'btn btn-danger btn-xs',
                            'onclick' => "return confirm('Are you sure to delete role?')"
                        ]) !!}

                    </div>
                    {!! Form::close() !!}
                </td>

            </tr>
        @endforeach
        </tbody>
    </table>

@endsection