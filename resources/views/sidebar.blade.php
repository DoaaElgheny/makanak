<div class="col-md-3 left_col">
    <div class="left_col scroll-view">
        <div class="navbar nav_title" style="border: 0;">
            <a href="#" class="site_title"> <img src="/assets/img/lo1.png" class="img-responsive" alt="#"
                                                 style="margin-left: 35px; margin-top: 8px;"></a>
        </div>

        <div class="clearfix"></div>

        <!-- menu profile quick info -->
        <div class="profile clearfix">
            <div class="profile_pic">
                <img src="{{Session::get('Logo')}}" alt="" class="img-circle profile_img"
                     style="    width: 50px; height: 50px;">
            </div>
            <div class="profile_info">

                <h2>{{Session::get('username')}}</h2>
            </div>
        </div>
        <!-- /menu profile quick info -->
        <br/>

        <!-- sidebar menu -->
        <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
            <div class="menu_section">
                <h3></h3>
                <ul class="nav side-menu">


                    <li><a href="/Schadule"><i class="fa fa-calendar" aria-hidden="true"
                                               style="    font-size: 16px; float: right;     margin-left: 30px;"></i>المواعيد</a>
                    </li>


                    <li><a href="/PlaceData">

                            <i class="fa fa-asterisk" aria-hidden="true"
                               style="    font-size: 16px; float: right;     margin-left: 30px;"></i>
                            بيانات المكان</a>
                    </li>


                    <li><a href="/Showhalladmin"><i class="fa fa-university" aria-hidden="true"
                                                    style="    font-size: 16px; float: right;     margin-left: 30px;"></i>
                            القاعات</a>
                    </li>

                    <li><a href="/CoWorking">
                            <i class="fa fa-ticket" aria-hidden="true"
                               style="    font-size: 16px; float: right;     margin-left: 30px;"></i>

                            حجوزات فردية</a>
                    </li>
                    <li><a href="/ReportCoworking">
                            <i class="fa fa-bars" aria-hidden="true"
                               style="    font-size: 16px; float: right;     margin-left: 30px;"></i>
                            تقرير حجوزرات فردية</a>
                    </li>
                    <!-- <li><a href="/Events"><i class="fa fa-calendar" aria-hidden="true"
                                             style="    font-size: 16px; float: right;     margin-left: 30px;"></i>إيفنتاريا</a>
                    <li><a href="/showEvents"><i class="fa fa-calendar" aria-hidden="true"
                                                 style="    font-size: 16px; float: right;     margin-left: 30px;"></i>إيفنتاريا</a>

                    </li>
                    <li><a href="/Events"><i class="fa fa-calendar" aria-hidden="true"
                                             style="    font-size: 16px; float: right;     margin-left: 30px;"></i>إعلانات</a>
                    </li> -->

                    <!--      <li><a href="/Report">
             <i class="fa fa-bars" aria-hidden="true" style="    font-size: 16px; float: right;     margin-left: 30px;"></i>
                             تقارير</a>
                        </li> -->


                    @if(Session::get('role')!="User")

                        <li><a href="#"><span> ادوات التحكم </span><span class="fa fa-chevron-down"></span><i
                                        class="fa fa-wrench"></i></a>
                            <ul class="nav child_menu">


                                <li><a href="/Types">الانواع</a></li>

                                <li><a href="/Facilites">الامكانيات</a></li>
                                <li><a href="/Government">المحافظات</a></li>

                                <li><a href="/Districts">المراكز</a></li>

                            </ul>
                        </li>


                        <li><a href="#"><span>الاماكن</span><span class="fa fa-chevron-down"></span><i
                                        class="fa fa-wrench"></i></a>
                            <ul class="nav child_menu">


                                <li>
                                    <a href="/ShowPlace">

                                        كل الاماكن المتاحه
                                    </a>
                                </li>
                                <li>
                                    <a href="/ShowPlaceAdmin">

                                        اماكن التشنال
                                    </a>
                                </li>


                                <li>
                                    <a href="/UnRegisterPlace">

                                        اضافة قاعة جديده
                                    </a>
                                </li>

                                <li><a href="{{route('roles')}}"><i class="fa fa-id-badge" aria-hidden="true"
                                                                    style="    font-size: 16px; float: right;     margin-left: 30px;"></i>الوظائف</a>
                                </li>


                            </ul>
                        </li>


                        <li><a href="{{route('roles')}}"><i class="fa fa-id-badge" aria-hidden="true"
                                                            style="    font-size: 16px; float: right;     margin-left: 30px;"></i>الوظائف</a>
                        </li>
                        <li><a href="{{route('users')}}"><i class="fa fa-id-badge" aria-hidden="true"
                                                            style="    font-size: 16px; float: right;     margin-left: 30px;"></i>المستخدمين
                            </a>
                        </li>

                    @endif


                </ul>
            </div>


        </div>
    </div>

    <!-- /sidebar menu -->