@extends('master')
@section('content')
<br/>
<br/>
@if(Session::has('flash_message'))
    <div class="alert alert-success">
        {{ Session::get('flash_message') }}<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    </div>
@endif
@if($errors->any())
    <div class="alert alert-danger">
        @foreach($errors->all() as $error)
            <p>{{ $error }}</p>
        @endforeach
    </div>
@endif

<style>
  
    .modal {
        display: none; /* Hidden by default */
        position: absolute; /* Stay in place */
        z-index: 2; /* Sit on top */
        padding-top: 50px; /* Location of the box */
        left: 0;
        top: 0;
        width: 100%; /* Full width */
        height: 100%; /* Full height */
        overflow: auto; /* Enable scroll if needed */
        background-color: rgb(0,0,0); /* Fallback color */
        background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
    }
.modal2 {
        display: none; /* Hidden by default */
        position: absolute; /* Stay in place */
        z-index: 2; /* Sit on top */
        padding-top: 50px; /* Location of the box */
        left: 0;
        top: 0;
        width: 100%; /* Full width */
        height: 100%; /* Full height */
        overflow: auto; /* Enable scroll if needed */
        background-color: rgb(0,0,0); /* Fallback color */
        background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
    }
    /* Modal Content */
    .modal-content {
        position: relative;
        background-color: #fefefe;
        margin: auto;
        padding: 0;
        border: 1px solid #888;
        width: 70%;
        box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2),0 6px 20px 0 rgba(0,0,0,0.19);
        -webkit-animation-name: animatetop;
        -webkit-animation-duration: 0.4s;
        animation-name: animatetop;
        animation-duration: 0.4s
    }

    /* Add Animation */
    @-webkit-keyframes animatetop {
        from {top:-300px; opacity:0} 
        to {top:0; opacity:1}
    }

    @keyframes animatetop {
        from {top:-300px; opacity:0}
        to {top:0; opacity:1}
    }

    /* The Close Button */
    .close {
        color: black;
        float: left;
        font-size: 32px;
        font-weight: bold;
        padding-left:1%;
    }

    .close:hover,
    .close:focus {
        color: #000;
        text-decoration: none;
        cursor: pointer;
    }
    .close2 {
        color: black;
        float: left;
        font-size: 32px;
        font-weight: bold;
        padding-left:1%;
    }

    .close2:hover,
    .close2:focus {
        color: #000;
        text-decoration: none;
        cursor: pointer;
    }
    .modal-header {
        padding: 2px 16px;
    }

    .modal-body {
        margin: 2px 16px;
        margin-top: 5px;    
    }

    .modal-footer {
        padding: 2px 16px;
        margin : 5px;
    }
    input
    {
        max-width:95%;
    }
.btn-bg {
    border: 1px solid #841851;
    border-radius: 0px;
    color: #fff !important;
    padding: 10px 20px;
    font-weight: bold !important;
    font-size: 14px;
    background-color: #841851;
}
.btn-bg:hover, .btn-bg:focus{
    border: 1px solid #841851;
    color: white !important;
   background-color:#69053a;
}
td.fc-event-container
{
    padding-top : 2px;
}
</style>
<script src="https://code.jquery.com/jquery-1.11.2.min.js">
  $(document).ready(function() {



      $('#fromPicker').datetimepicker({
          
      });





      $('#toPicker').datetimepicker({
          format:'Y-m-d H:i'
      });
      $('#fromPicker2').datetimepicker({
          format:'Y-m-d H:i'
      });
      $('#toPicker2').datetimepicker({
          format:'Y-m-d H:i'
      });
  });
    
  </script>
  @section('other_scripts')

<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.2.1/moment.min.js"></script> -->

<script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.7.0/fullcalendar.min.js"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.7.0/fullcalendar.min.css"/>
<script src='/assets/plugins/fullcalendar/locale/ar-ly.js'></script>
<link rel="stylesheet" href="/assets/css/jquery.datetimepicker.css">
<script type="text/javascript" src="/assets/js/jquery.datetimepicker.full.min.js"></script>

@stop
<section class="panel panel-primary" style="    border-color: white;">

  <div class="panel-body" >
    <div class="container">
        <div class = "row">
                <div class = "col-lg-2" style = "">
                    <button type="button" id="myBtn" class="btn btn-bg btn-lg" style="">إضافة موعد جديد</button>
                </div>
                <input class="col-lg - 2" id="searchDate" type="date" required name = "" dir = "rtl">

            
            <div id="AddModal" class="modal" dir="rtl">

                    <div class="modal-content">
                        <div class="modal-header">
                            <span class="close">&times;</span>
                            <h2 style="text-align: center;
    color: #841851;
    font-size: 20px;
    font-weight: bold;">إضافة ايفينت</h2>
                        </div>
                        <div class="modal-body">
                            {!! Form::open(array('action' => 'SchaduleController@store')) !!}
                            
                            
                            
                            
                            
                                    <div class = "row">
                                    <div class = "col-sm-1">
                                    </div>
                                
                                    <div class = "col-sm-3">
                                        <input style="      border:  1px solid #841851;
    border-radius: 4px;
    box-shadow: 0 4px 20px rgba(0,0,0,0.19);height: 31px;  width: 207px;    margin-right: -70px;" id="toPicker" type="text" required name = "Date_To" dir = "ltr">

                                        <script type="text/javascript">
                                        $(function () {
                                        $('#toPicker').datetimepicker();

                                        });
                                        </script>

                                    </div>
                                    <div class = "col-sm-2">
                                        <label>التاريخ الى</label>
                                    </div>

                                    <div class = "col-sm-3">
                                        <input  style="    border:  1px solid #841851;
    border-radius: 4px;
    box-shadow: 0 4px 20px rgba(0,0,0,0.19);height: 31px;  width: 207px;    margin-right: -70px;" type = "text"  id="fromPicker" required name = "Date_From" dir = "ltr">

                                        <script type="text/javascript">
                                        $(function () {
                                        $('#fromPicker').datetimepicker();

                                        });
                                        </script>


                                    </div>
                                    <div class = "col-sm-2">
                                        <label>التاريخ من</label>
                                    </div>
                                      <div class = "col-sm-1">
                                       
                                    </div>
                                </div>
                            <br>
                            
                               <div class = "row">
                                    <div class = "col-sm-1">
                                    </div>
                                
                                   
                                    
                                    
                                    
                                        <div class = "col-sm-3">
                                        <select name = "Hall_Id" class="Halls" style="width: 165px;    border:  1px solid #841851;
    border-radius: 4px;
    box-shadow: 0 4px 20px rgba(0,0,0,0.19);height: 31px;  width: 207px;    margin-right: -70px;">
                                            @foreach($hall as $all)
                                                <option value ='{{ $all->id }}'>{{ $all->Name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class = "col-sm-2">
                                        <label>القاعة</label>
                                    </div> 
                                    <div class = "col-sm-3">
                                        <input  style="    border:  1px solid #841851;
    border-radius: 4px;
    box-shadow: 0 4px 20px rgba(0,0,0,0.19);height: 31px;      width: 207px;   margin-right: -70px;"type = "text" required name = "Event_Name" value="موعد" >
                                    </div>
                                    <div class = "col-sm-2">
                                        <label>إسم الحجز</label>
                                    </div>
                                      <div class = "col-sm-1">
                                       
                                    </div>
                                </div>
                            <br>
                                <div class = "row">
                                      <div class = "col-sm-1">
                                       
                                    </div>
                                    <div class = "col-sm-3 ">
                                        <input type = "text" name = "Tele" style=" border:  1px solid #841851;
    border-radius: 4px;
    box-shadow: 0 4px 20px rgba(0,0,0,0.19);height: 31px;     width: 207px;    margin-right: -70px;">
                                    </div>
                                    <div class = "col-sm-2">
                                        <label>تليفون</label>
                                    </div>

                                    <div class = "col-sm-3">
                                        <input type = "text" name = "Name" style=" border:  1px solid #841851;
    border-radius: 4px;
    box-shadow: 0 4px 20px rgba(0,0,0,0.19);height: 31px;  width: 207px;    margin-right: -70px;">
                                    </div>
                                    <div class = "col-sm-2">
                                        <label>الإسم</label>
                                    </div>
                                  <div class = "col-sm-1">
                                       
                                    </div>
                                </div>
                              
                             
                                <br/>
                                
                                <div class = "row">
                                      <div class = "col-sm-1">
                                     
                                    </div> 
                                        <div class = "col-sm-3">
                                        <input style="     border:  1px solid #841851;
    border-radius: 4px;
    box-shadow: 0 4px 20px rgba(0,0,0,0.19);height: 31px;  width: 207px;    margin-right: -70px;" type = "text" name = "Price" required>
                                    </div>
                                    <div class = "col-sm-2">
                                        <label>السعر</label>
                                    </div>
                                     <div class = "col-sm-3">
                                        <input  style="    border:  1px solid #841851;
    border-radius: 4px;
    box-shadow: 0 4px 20px rgba(0,0,0,0.19);height: 31px;  width: 230px;    margin-right: -70px;" type = "text" name = "Notes">
                                    </div>
                                    <div class = "col-sm-2">
                                        <label>ملاحظات</label>
                                    </div>
                             
                                    <div class = "col-sm-1">
                                       
                                    </div>
                                </div>
                               


                        
                                <br/>
                                @if($hall !=Null)
                              <div class = "row" hidden="hidden">
                                    <div class = "col-sm-3">
                                        <select name = "HallId" style="    border:  1px solid #841851;
    border-radius: 4px;
    box-shadow: 0 4px 20px rgba(0,0,0,0.19);height: 31px;  width: 207px;    margin-right: -70px;">
                                            @foreach($hall as $all)
                                                <option value ='{{ $all->id }}'>{{ $all->Name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class = "col-sm-2">
                                        <label>القاعة</label>
                                    </div> 
                                </div> 
                                @endif
                                <br/>
                                <div class = "row" hidden="hidden">
                               
                                        <div class = "col-sm-3" hidden="hidden">
                                        <label>IsEvent</label>
                                    </div>
                                    <div class = "col-sm-2" dir="ltr" hidden="hidden">
                                        <input type="checkbox" name = "IsEvent">
                                    </div>
                                </div>
                                <br/>
                                <input type="hidden" name="AddOrUpdate" value="add">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            
                        </div>
                        <div class="modal-footer" style="    margin-top: -5px;
    margin-bottom: 0px;">
                            <br/>
                            <div class = "row">
                                    <div class = "col-sm-7">
                                        <button type="submit" name="submitButton" class="btn btn-bg btn-lg" style="margin-right: 35px;" >حفظ </button> 
                                    </div>
                                    <div class = "col-sm-5">
                                    </div> 
                                </div>
                                <br/>
                        </div>
                        {!!Form::close() !!}
                    
                     </div>

                </div>

                <script>
                    // Get the modal
                    var modal = document.getElementById('AddModal');

                    // Get the button that opens the modal
                    var btn = document.getElementById("myBtn");

                    // Get the <span> element that closes the modal
                    var span = document.getElementsByClassName("close")[0];

                    // When the user clicks the button, open the modal 
                    btn.onclick = function() {
                        modal.style.display = "block";
                        $(".fc-month-button").css("display", "none"); 
                        $(".fc-agendaWeek-button").css("display", "none");
                        $(".fc-agendaDay-button").css("display", "none");
                    }

                    // When the user clicks on <span> (x), close the modal
                    span.onclick = function() {
                        modal.style.display = "none";
                        $(".fc-month-button").css("display", "block"); 
                        $(".fc-agendaWeek-button").css("display", "block"); 
                        $(".fc-agendaDay-button").css("display", "block"); 
                    }

                    // When the user clicks anywhere outside of the modal, close it
                    window.onclick = function(event) {
                        if (event.target == modal) {
                            modal.style.display = "none";
                            $(".fc-month-button").css("display", "block"); 
                            $(".fc-agendaWeek-button").css("display", "block"); 
                            $(".fc-agendaDay-button").css("display", "block"); 
                        }
                    }
                </script>

                <!-- Edit model -->
                <div id="EditModal" class="modal2" dir="rtl">

                    <div class="modal-content">
                        <div class="modal-header">
                            <span class="close2">&times;</span>
                            <h2 style="text-align: center;
    color: #841851;
    font-size: 20px;
    font-weight: bold;
">تعديل إيفينت</h2>
                        </div>
                        <div class="modal-body">
                            {!! Form::open(array('action' => 'SchaduleController@store')) !!}
                            
                            
                            
                                <div class = "row">
                                       <div class = "col-sm-1">
                                    </div>
                                    <div class = "col-sm-3">
                                        <input id="toPicker2" type="text" required name = "Date_To" dir = "ltr" style="      border:  1px solid #841851;
    border-radius: 4px;
    box-shadow: 0 4px 20px rgba(0,0,0,0.19);height: 31px;  width: 207px;    margin-right: -70px;">
                                         <script type="text/javascript">
            $(function () {
                $('#toPicker2').datetimepicker();
            });
        </script>
                                    </div>
                                    <div class = "col-sm-2">
                                        <label>التاريخ الى</label>
                                    </div>

                                    <div class = "col-sm-3">
                                        <input type = "text"  id="fromPicker2" required name = "Date_From" dir = "ltr" style="    border:  1px solid #841851;
    border-radius: 4px;
    box-shadow: 0 4px 20px rgba(0,0,0,0.19);height: 31px;  width: 207px;    margin-right: -70px;">
                                                                      <script type="text/javascript">
            $(function () {
                $('#fromPicker2').datetimepicker();
            });
        </script>
                                    </div>
                                    <div class = "col-sm-2">
                                        <label>التاريخ من</label>
                                    </div>
                                       <div class = "col-sm-1">
                                    </div>
                                </div>
                            <br>
                             <div class = "row">
                                      <div class = "col-sm-1">
                                    </div>
                                 
                                    <div class = "col-sm-3">
                                        <select name = "Hall_IdU" id = "H_Id" style="width: 165px;    border:  1px solid #841851;
    border-radius: 4px;
    box-shadow: 0 4px 20px rgba(0,0,0,0.19);height: 31px;  width: 207px;    margin-right: -70px;">
                                            @foreach($hall as $all)
                                                <option value ='{{ $all->id }}'>{{ $all->Name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class = "col-sm-2">
                                        <label>القاعة</label>
                                    </div>
                                      <div class = "col-sm-3">
                                        <input type = "text" required name = "Event_Name" id = "Event_Name" style="    border:  1px solid #841851;
    border-radius: 4px;
    box-shadow: 0 4px 20px rgba(0,0,0,0.19);height: 31px;      width: 207px;   margin-right: -70px;">
                                    </div>
                                    <div class = "col-sm-2">
                                        <label>إسم الحجز</label>
                                    </div>
                                      <div class = "col-sm-1">
                                    </div>
                                </div>
                             <br/>
                                <div class = "row">
                                    <div class = "col-sm-1">
                                    </div>
                                    <div class = "col-sm-3">
                                        <input type = "text" name = "Tele" id = "Tele" style=" border:  1px solid #841851;
    border-radius: 4px;
    box-shadow: 0 4px 20px rgba(0,0,0,0.19);height: 31px;     width: 207px;    margin-right: -70px;">
                                    </div>
                                    <div class = "col-sm-2">
                                        <label>تليفون</label>
                                    </div>

                                    <div class = "col-sm-3">
                                        <input type = "text" name = "Name" id = "Name" style=" border:  1px solid #841851;
    border-radius: 4px;
    box-shadow: 0 4px 20px rgba(0,0,0,0.19);height: 31px;  width: 207px;    margin-right: -70px;">
                                    </div>
                                    <div class = "col-sm-2">
                                        <label>الإسم</label>
                                    </div>
                                <div class = "col-sm-1">
                                    </div>
                                </div>
                            
                     <br/>
                            
                          
                            
                               
                             
                            
                                
                                <div class = "row">
                                      <div class = "col-sm-1">
                                    </div>
                                <div class = "col-sm-3">
                                        <input type = "text" style="     border:  1px solid #841851;
    border-radius: 4px;
    box-shadow: 0 4px 20px rgba(0,0,0,0.19);height: 31px;  width: 207px;    margin-right: -70px;" name = "Price" id = "Price" required>
                                    </div>
                                    <div class = "col-sm-2">
                                        <label>السعر</label>
                                    </div>
                                      <div class = "col-sm-3">
                                        <input type = "text" name = "Notes" id= "Notes"  style="    border:  1px solid #841851;
    border-radius: 4px;
    box-shadow: 0 4px 20px rgba(0,0,0,0.19);height: 31px;  width: 230px;    margin-right: -70px;" >
                                    </div>
                                    <div class = "col-sm-2">
                                        <label>ملاحظات</label>
                                    </div>
                                    <div class = "col-sm-1">
                                    </div>
                                </div>
                                
                                <br/>

                                  <div class = "row" hidden="hidden">
                                    <div class = "col-sm-1">
                                    </div>
                                    <div class = "col-sm-3" hidden="hidden">
                                        <label>IsEvent</label>
                                    </div>
                                    <div class = "col-sm-2" dir="ltr" hidden="hidden">
                                        <input type="checkbox" name = "IsEvent" id = "IsEvent">
                                    </div>
                                    
                                  
                                     <div class = "col-sm-5">
                                    </div>
                                </div>




                                <div class = "row">
                                  
                                </div>`
                                <br/>
                                <input type="hidden" name="AddOrUpdate" id = "Event_ID">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            
                        </div>
                        <div class="modal-footer">
                            <br/>
                            <div class = "row">
                              
                                <div class = "col-sm-12" style="    text-align: center;">
                                     <a id ="url" class="btn btn-bg btn-lg" >حذف </a> 
                                      <button type="submit" name="submitButton2" class="btn btn-bg btn-lg"  >حفظ </button> 
                                </div> 
                              
                                  
                              
                            </div>
                                <br/>
                        </div>
                        {!!Form::close() !!}
                    
                     </div>

                </div>
                <script>
                    // Get the modal
                    var modal2 = document.getElementById('EditModal');
                    
                    // Get the <span> element that closes the modal
                    var span = document.getElementsByClassName("close2")[0];

                    // When the user clicks on <span> (x), close the modal
                    span.onclick = function() {
                        modal2.style.display = "none";
                        $(".fc-month-button").css("display", "block"); 
                        $(".fc-agendaWeek-button").css("display", "block"); 
                        $(".fc-agendaDay-button").css("display", "block"); 
                    }

                    // When the user clicks anywhere outside of the modal, close it
                    window.onclick = function(event) {
                        if (event.target == modal2) {
                            modal2.style.display = "none";
                            $(".fc-month-button").css("display", "block"); 
                            $(".fc-agendaWeek-button").css("display", "block"); 
                            $(".fc-agendaDay-button").css("display", "block"); 
                        }
                    }
                </script>
                <!-- End of edit modal -->
         
                <div class = "col-sm-8">
               
                        <select id = "Hall_Id" style="    
    border-color: #841851;
     padding: 2px 30px;
    direction: rtl;
    font-size: 16px;
    color: black;">
                        <option value = "all">الكل </option>
                        @foreach($hall as $all)
                            <option value ='{{ $all->id }}'>{{ $all->Name }}</option>
                        @endforeach
                    </select>     
                </div> 
               
            </div>
          
               
             
        
            <script>
                $(document).ready(function() {
                    $('#calendar').fullCalendar({
                        
                            events:
                                {
                                    url: "{{ URL::to('GetAllEvents') }}",
                                   type: "POST",
                                    dataType: 'json',
                                    data: {"_token":$('#_token').val()},
                                    success: function(response)
                                        {
                                             console.log("ddl,",response);
                                        },
                                        error: function(response)
                                        {
                                            console.log('Error'+response);
                                        }
                                },
                                eventClick: function(calEvent, jsEvent, view) {
                                    console.log('Event class: ' + calEvent.className);
                                    console.log('Event id: ' + calEvent.id);
                                    $.ajax({
                                        url: "{{ URL::to('edit') }}",
                                        type: "POST",
                                        dataType: 'json',
                                        data: {"_token":$('#_token').val(),"id":calEvent.id},
                                        success: function(response)
                                            {
                                                // console.log("obj " + response.toSource());

                                                $("#Name").val(response[0].Name);
                                                $("#Price").val(response[0].Price);
                                                $("#fromPicker2").val(moment(response[0].Date_From).format('YYYY/MM/DD HH:mm'));
                                                $("#toPicker2").val(moment(response[0].Date_To).format('YYYY/MM/DD HH:mm'));
                                                $("#Notes").val(response[0].Notes);
                                                $("#Event_Name").val(response[0].Event_Name);
                                                $("#Tele").val(response[0].Tele);
                                                $("#H_Id").val(response[0].Hall_Id);
                                                $("#Event_ID").val(calEvent.id);
                                                
                                                $('#url').attr('href',"/destroy/" + calEvent.id);
                                                if(response[0].IsEvent == 1)
                                                {
                                                    $("#IsEvent").prop('checked', true);
                                                }
                                                else
                                                {
                                                    $("#IsEvent").prop('checked', false);
                                                }
                                            },
                                            error: function (xhr, err) {
                                                alert("readyState: " + xhr.readyState + "\nstatus: " + xhr.status);
                                                alert("responseText: " + xhr.responseText);
                                            }
                                    });
                                    $('#EditModal').css("display", "block");
                                    $(".fc-month-button").css("display", "none"); 
                                    $(".fc-agendaWeek-button").css("display", "none");
                                    $(".fc-agendaDay-button").css("display", "none"); 
                                },
                                header: {
                                 locale: 'ar-ly',
                                left: ' prev next',
                                center: 'title',
                                right: 'month agendaWeek agendaDay '
                                },
                                defaultView: 'month',
                                
                                dayClick: function(date, allDay, jsEvent, view) {
                                    if (view.name === "month") {
                                        $('#calendar').fullCalendar('gotoDate', date);
                                        $('#calendar').fullCalendar('changeView', 'agendaDay');
                                    }
                                },
                                eventRender: function(event, element) {
                                    $.ajax({
                                        url: "{{ URL::to('edit') }}",
                                        type: "POST",
                                        dataType: 'json',
                                        data: {"_token":$('#_token').val(),"id":event.id},
                                        success: function(response)
                                            {
                                                console.log("id " + event.id);
                                                $(element).popover({container: "body",html:true, content:'<div class="tooltiptopicevent" dir="rtl" >' + 'اسم الحجز ' + ': ' + event.title + '<br/>من:' + moment(event.start).format('DD-MM-YYYY HH:mm') + '<br/>الى:' +moment(event.end).format('DD-MM-YYYY HH:mm') + '<br/>اسم العميل : ' + response[0].Name + '<br/>رقم التليفون : ' + response[0].Tele + '<br/>السعر : ' + response[0].Price +'</div>', trigger: 'hover', placement: 'auto right'});
                                            },
                                            error: function (xhr, err) {
                                                //alert("readyState: " + xhr.readyState + "\nstatus: " + xhr.status);
                                                //alert("responseText: " + xhr.responseText);
                                            }
                                            
                                    });
                                    
                                },
                                eventAfterRender: function(event, element, view) {  
                                     $('.fc-content-skeleton').find()
                                  },
                                eventLimit: true,
                                views: 
                                {
                                    agenda: {
                                        eventLimit: 0
                                    }
                                }
                            });
                });
                 $("#Hall_Id").change(function () {
                        id = this.value;
                        if (id == "all")
                        {
                            $('#calendar').fullCalendar( 'destroy' );
                            $('#calendar').fullCalendar({ 
                                events:
                                {
                                    url: "{{ URL::to('GetAllEvents') }}",
                                    type: "POST",
                                    dataType: 'json',
                                    data: {"_token":$('#_token').val()},
                                    success: function(response)
                                        {
                                            console.log("obj " + response );
                                        },
                                        error: function(response)
                                        {
                                            console.log('Error'+response);
                                        }
                                    
                                },
                                 eventClick: function(calEvent, jsEvent, view) {
                                     console.log('Event class: ' + calEvent.className);
                                    console.log('Event id: ' + calEvent.id);
                                    $.ajax({
                                        url: "{{ URL::to('edit') }}",
                                        type: "POST",
                                        dataType: 'json',
                                        data: {"_token":$('#_token').val(),"id":calEvent.id},
                                        success: function(response)
                                            {
                                                console.log("obj " + response);

                                                $("#Name").val(response[0].Name);
                                                $("#Price").val(response[0].Price);
                                                $("#fromPicker2").val(moment(response[0].Date_From).format('YYYY/MM/DD HH:mm'));
                                                $("#toPicker2").val(moment(response[0].Date_To).format('YYYY/MM/DD HH:mm'));
                                                $("#Notes").val(response[0].Notes);
                                                $("#Event_Name").val(response[0].Event_Name);
                                                $("#Tele").val(response[0].Tele);
                                                $("#H_Id").val(response[0].Hall_Id);
                                                $("#Event_ID").val(calEvent.id);
                                                
                                                $('#url').attr('href',"/destroy/" + calEvent.id);
                                                if(response[0].IsEvent == 1)
                                                {
                                                    $("#IsEvent").prop('checked', true);
                                                }
                                                else
                                                {
                                                    $("#IsEvent").prop('checked', false);
                                                }
                                            },
                                            error: function (xhr, err) {
                                                //alert("readyState: " + xhr.readyState + "\nstatus: " + xhr.status);
                                                //alert("responseText: " + xhr.responseText);
                                            }
                                    });
                                    $('#EditModal').css("display", "block");
                                    $(".fc-month-button").css("display", "none"); 
                                    $(".fc-agendaWeek-button").css("display", "none");
                                    $(".fc-agendaDay-button").css("display", "none"); 
                                },
                                    header: {
                                        locale: 'ar-ly',
                                    left: ' prev next',
                                    center: 'title',
                                    right: 'month agendaWeek agendaDay '
                                    },
                                    defaultView: 'month',
                                    dayClick: function(date, allDay, jsEvent, view) {
                                        if (view.name === "month") {
                                            $('#calendar').fullCalendar('gotoDate', date);
                                            $('#calendar').fullCalendar('changeView', 'agendaDay');
                                        }
                                    },
                                eventRender: function(event, element) {
                                     $.ajax({
                                        url: "{{ URL::to('edit') }}",
                                        type: "POST",
                                        dataType: 'json',
                                        data: {"_token":$('#_token').val(),"id":event.id},
                                        success: function(response)
                                            {
                                                console.log("id " + event.id);
                                                $(element).popover({container: "body",html:true, content:'<div class="tooltiptopicevent" dir="rtl" >' + 'اسم الحجز ' + ': ' + event.title + '<br/>من:' + moment(event.start).format('DD-MM-YYYY HH:mm') + '<br/>الى:' +moment(event.end).format('DD-MM-YYYY HH:mm') + '<br/>اسم العميل : ' + response[0].Name + '<br/>رقم التليفون : ' + response[0].Tele + '<br/>السعر : ' + response[0].Price +'</div>', trigger: 'hover', placement: 'auto right'});
                                            },
                                            error: function (xhr, err) {
                                                //alert("readyState: " + xhr.readyState + "\nstatus: " + xhr.status);
                                                //alert("responseText: " + xhr.responseText);
                                            }
                                            
                                    });
                                    
                                },
                                eventLimit: true,
                                views: 
                                {
                                    agenda: {
                                        eventLimit: 0
                                    }
                                }
                            });
                        }
                        else
                        {
                            $('#calendar').fullCalendar( 'destroy' );
                            var ajaxResult=[];
                            $.ajax({
                                url: "{{ URL::to('/GetStartEndOfHall') }}",
                                type: "POST",
                                dataType: 'json',
                                async : false,
                                data: {"id":id,"_token":$('#_token').val()},
                                success: function(response)
                                {
                                    console.log("sssssssss " + response.toSource());
                                    ajaxResult.push(response[0].WorkStart);
                                    ajaxResult.push(response[0].WorkEnd);
                                },
                                error: function(response)
                                {
                                    console.log('Error'+response);
                                }      
                            });
                            $('#calendar').fullCalendar({ 
                                events:
                                {
                                    url: "{{ URL::to('GetEvents') }}",
                                    type: "POST",
                                    dataType: 'json',
                                    data: {'id' : id ,"_token":$('#_token').val()},
                                    success: function(response)
                                        {
                                            console.log("result " + ajaxResult[1]); 
                                        },
                                        error: function(response)
                                        {
                                            console.log('Error'+response);
                                        }
                                    
                                },
                                minTime : ajaxResult[0],
                                maxTime : ajaxResult[1],
                                 eventClick: function(calEvent, jsEvent, view) {
                                     console.log('Event class: ' + calEvent.className);
                                    console.log('Event id: ' + calEvent.id);
                                    $.ajax({
                                        url: "{{ URL::to('edit') }}",
                                        type: "POST",
                                        dataType: 'json',
                                        data: {"_token":$('#_token').val(),"id":calEvent.id},
                                        success: function(response)
                                            {
                                                // console.log("obj " + response.toSource());

                                                $("#Name").val(response[0].Name);
                                                $("#Price").val(response[0].Price);
                                                $("#fromPicker2").val(moment(response[0].Date_From).format('YYYY-MM-DD HH:mm'));
                                                $("#toPicker2").val(moment(response[0].Date_To).format('YYYY-MM-DD HH:mm'));
                                                $("#Notes").val(response[0].Notes);
                                                $("#Event_Name").val(response[0].Event_Name);
                                                $("#Tele").val(response[0].Tele);
                                                $("#H_Id").val(response[0].Hall_Id);
                                                $("#Event_ID").val(calEvent.id);
                                                
                                                $('#url').attr('href',"/destroy/" + calEvent.id);
                                                if(response[0].IsEvent == 1)
                                                {
                                                    $("#IsEvent").prop('checked', true);
                                                }
                                                else
                                                {
                                                    $("#IsEvent").prop('checked', false);
                                                }
                                            },
                                            error: function (xhr, err) {
                                                //alert("readyState: " + xhr.readyState + "\nstatus: " + xhr.status);
                                                //alert("responseText: " + xhr.responseText);
                                            }
                                    });
                                    $('#EditModal').css("display", "block");
                                    $(".fc-month-button").css("display", "none"); 
                                    $(".fc-agendaWeek-button").css("display", "none");
                                    $(".fc-agendaDay-button").css("display", "none"); 
                                },
                                    header: {
                                        locale: 'ar',
                                    left: ' prev next',
                                    center: 'title',
                                    right: 'month agendaWeek agendaDay '
                                    },
                                    defaultView: 'month',
                                    dayClick: function(date, allDay, jsEvent, view) {
                                        if (view.name === "month") {
                                            $('#calendar').fullCalendar('gotoDate', date);
                                            $('#calendar').fullCalendar('changeView', 'agendaDay');
                                        }
                                    },
                                eventRender: function(event, element) {
                                     $.ajax({
                                        url: "{{ URL::to('edit') }}",
                                        type: "POST",
                                        dataType: 'json',
                                        data: {"_token":$('#_token').val(),"id":event.id},
                                        success: function(response)
                                            {
                                                console.log("id " + event.id);
                                                $(element).popover({container: "body",html:true, content:'<div class="tooltiptopicevent" dir="rtl" >' + 'اسم الحجز ' + ': ' + event.title + '<br/>من:' + moment(event.start).format('DD-MM-YYYY HH:mm') + '<br/>الى:' +moment(event.end).format('DD-MM-YYYY HH:mm') + '<br/>اسم العميل : ' + response[0].Name + '<br/>رقم التليفون : ' + response[0].Tele + '<br/>السعر : ' + response[0].Price +'</div>', trigger: 'hover', placement: 'auto right'});
                                            },
                                            error: function (xhr, err) {
                                                //alert("readyState: " + xhr.readyState + "\nstatus: " + xhr.status);
                                                //alert("responseText: " + xhr.responseText);
                                            }
                                            
                                    });
                                    
                                },
                                eventLimit: true,
                                views: 
                                {
                                    agenda: {
                                        eventLimit: 0
                                    }
                                }
                            });
                        }
                    });

                $('#searchDate').on('change',function(){
                    var date = $(this).val();
                    console.log(date);
                    $('.fc-agendaDay-button').trigger("click");
                    $('#calendar').fullCalendar( 'gotoDate', date )
                });
                 $('.Halls').on('change',function(){
                     var hall_id = $(this).val();
                     $.ajax({
                         url : '/getHall/'+hall_id,
                        type : 'GET',
                        success : function(data) {   
                            $('input[name=Price]').val(data.Price);
                        }
                     });
                });
                 $('#H_Id').on('change',function(){
                     var hall_id = $(this).val();
                     $.ajax({
                         url : '/getHall/'+hall_id,
                        type : 'GET',
                        success : function(data) {   
                            $('input[name=Price]').val(data.Price);
                        }
                     });
                });
                 
               // $('#calendar').fullCalendar('option', 'contentHeight', 'auto');
            </script>

    <br/><br/>
    <div id = "calendar" style = "width: 100%;height: 100%;margin-top: -10px;" class = "cal"></div>
        <input type="hidden" value="{{ csrf_token() }}" id="_token" name="_token" />
    </div>   
  </div>
</section>

  @stop




