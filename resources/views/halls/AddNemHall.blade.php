@extends('master')
@section('content')
 <section class="panel-body">
 <style type="text/css">
    #blah{
  background: url('{{$HallData->Image}}') no-repeat;
  background-size: cover;

}
       .btn-bg {
    border: 1px solid #841851;
    border-radius: 0px;
    color: #fff !important;
    padding: 10px 20px;
    font-weight: bold !important;
    font-size: 14px;
    background-color: #841851;
}
     .btn-bg:hover, .btn-bg:focus{
	border: 1px solid #841851;
	color: white !important;
   background-color:#69053a;
}  
     .wizard-card .picture:hover {
  border-color:  #841851;
}
  .wizard-card .picture{
      border-radius: 50%;
      height: 195px;
    width: 280px;
}
     .the-legend {
         font-size: 16px;
     }
</style>
 <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
            
                          <h2 style="    float: right;
    font-size: 22px; margin-bottom: 10px;">      <i  class="fa fa-commenting-o"  aria-hidden="true" style="    font-size: 22px; float: right;     margin-left: 5px;
    color: #841851;"></i>   
             
                         تعديل بيانات القاعة </h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"></a>
                      </li>
                     
                      <li><a class="close-link"></a>
                      </li>

                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
<div class="col-md-12 " style="    margin-bottom: 5px;" dir="rtl">

 
  <fieldset class="the-fieldset"  style="margin-bottom: 15px; ">

                            <legend class="the-legend">بيانات اساسية</legend>

           <input type="hidden" name="_token" value="{{ csrf_token() }}">
  <input type="hidden" id="id" name="id" value="{{$HallData->id}}" required>
<div class="col-sm-5 col-sm-offset-1">
                            
                             <div class="col-sm-12 col-sm-offset-1 wizard-card" style="    top: -40px; left: -90px;" >
 



                                       <div class="picture-container" >
                                          <div class="picture" id="blah"  >
{!!Form::open(['action'=>'HallController@updateimage','method'=>'post','class'=>'form-horizontal form-label-left','id'=>'profileFormimage','files'=>'true']) !!} 
<input type="hidden" id="idd" name="idd" value="{{$HallData->id}}" required>             
                  <input id="file-upload1"  name="pic" type="file"/>
                  {!! Form::close() !!}

                                             
                                          </div>  
                                      </div>
                                  </div>     
                            
                            
                            </div> 
                            <div class="col-sm-5 col-sm-offset-1">
                            
                                       
       
        
                         <div class="col-sm-12 col-sm-offset-1">
      <p style="   margin: 0px;
    font-weight: 500;
    line-height: 2;
    color: black;
    font-weight: bold;">إسم القاعة</p>

        <input type="text" name="Name"  id="Name" value="{{$HallData->Name}}" style=" 
    width: 75%;
    height: 30px;
  
    padding: 0px 15px;
                             
             border: 1px solid #841851;
    border-radius: 4px;
    box-shadow: 0 4px 10px rgba(0,0,0,0.19);
    height: 31px;       " />
      <label style="color:red ;" id="NameEmpty"></label> 
      </div>
               
      <div class="col-sm-12 col-sm-offset-1">
      <p style="   margin: 0px;
    font-weight: 500;
    line-height: 2;
    color: black;
    font-weight: bold;">سعر القاعة</p>

   
    
        <input type="text" name="Price" min=1 step="0.1" id="Price" value="{{$HallData->Price}}"   style=" 
    width: 75%;
    height: 30px;
  
    padding: 0px 15px;
                             
             border: 1px solid #841851;
    border-radius: 4px;
    box-shadow: 0 4px 10px rgba(0,0,0,0.19);
    height: 31px;       " required="required">
                
      </div>       
                            
            <label style="color:red" id="EmptyPrice"></label>                
             <label style="color:red" id="ValuePrice"></label>                        
                            
           

      <div class="col-sm-12 col-sm-offset-1">
      <p style="   margin: 0px;
    font-weight: 500;
    line-height: 2;
    color: black;
    font-weight: bold;">سعة القاعة</p>
      
<input name="Capacity" id="Capacity" min=1 rows="14" value="{{$HallData->Capacity}}" style=" 
    width: 75%;
    height: 30px;
  
    padding: 0px 15px;
                             
             border: 1px solid #841851;
    border-radius: 4px;
    box-shadow: 0 4px 10px rgba(0,0,0,0.19);
    height: 31px;       " required="required">
            

  <label style="color:red" id="EmptyCapacity"></label>                
             <label style="color:red" id="ValueCapacity"></label>  

      </div>
    
                            
                            
                            </div>   
                            
            <label style="color:red" id="warn"></label>    
                            
 
                        </fieldset>

<fieldset class="the-fieldset"  style="margin-bottom: 15px; " dir="rtl">

                            <legend class="the-legend">نوع القاعة </legend> 
      @foreach($tyeWithchecked as $typ)
<div  class="col-md-2 " dir="rtl">
<p style="    font-size: 16px;
    font-weight: bold;"> {{$typ['Name']}}</p> 
<center><input type="checkbox" name="type"  {{$typ['Checked']}} value="{{$typ['id']}}" style="width:23px ;border: 1px solid #841851;     box-shadow: 0 2px 8px rgba(0,0,0,0.19);
    height: 22px;"  ></center>

</div>
@endforeach
                   
</fieldset>

 <fieldset class="the-fieldset"  style="margin-bottom: 15px; ">

             <legend class="the-legend">إمكانيات القاعة </legend> 
      @foreach($Facilitychecked as $Faci)
<div  class="col-md-2 " dir="rtl">
<p style="    font-size: 16px;
    font-weight: bold;"> {{$Faci['Name']}}</p> 
<center><input type="checkbox" name="Faci"  {{$Faci['Checked']}} value="{{$Faci['id']}}" style="width:23px ;border: 1px solid #841851;     box-shadow: 0 2px 8px rgba(0,0,0,0.19);
    height: 22px;"  ></center>
</div>
                        @endforeach
                   
</fieldset>                 
  



</div>


<div class="col-md-12 col-xs-6 "  style="      text-align: center;     margin-bottom: 10px;">
      <a href="/Showhalladmin" class="btn btn-danger" style="    padding: 10px 20px;
    font-size: 15px;">إلغاء </a> 
  <button type="submit"  name="submitButton" onclick="Save(this)"  class="btn  btn-success " style="    padding: 10px 20px;
    font-size: 15px;">تعديل </button> 
    

      
</div>



             
               
              
          
 


</div>
    
 
                   
                   

                  </div>
                </div>
              </div>
 
  @section('other_scripts')


<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>



    <script type="text/javascript">



  $(document).ready(function(){
     //change imgage    
$("#file-upload1").on("change", function()
    {
    
        var files = !!this.files ? this.files : [];

        if (!files.length || !window.FileReader) 
        {
           // $("#blah").css("background-image",'url(assets/UserFiles/img/noimage.jpg)');
        } // no file selected, or no FileReader support
 
        else if (/^image/.test( files[0].type)){ // only image file
            var reader = new FileReader(); // instance of the FileReader
            reader.readAsDataURL(files[0]); // read the local file
 
            reader.onloadend = function(){ // set image data as background of div
                $("#blah").css("background-image", "url("+this.result+")");
                 $("#profileFormimage").trigger('submit');
            }
        }
    });
/////////////////////
});
//Form Validation

function Save(e)
 {


  
     var typeValues = $('input[name=type]:checked').map(function()
            {

                return $(this).val();

            }).get();
      var FacilityValues = $('input[name=Faci]:checked').map(function()
            {

                return $(this).val();

            }).get();
       var Name = $('#Name').val();
       var Price = $('#Price').val();
       var Capacity = $('#Capacity').val();


       if(Name=="" )
       {
        document.getElementById("NameEmpty").innerHTML="يجب ادخال الاسم";
       }
       else if(Price =="")
       {
        document.getElementById("NameEmpty").innerHTML="";
       document.getElementById("EmptyPrice").innerHTML="يجب ادخال السعر";
       }
      else if(Capacity =="")
      {
         document.getElementById("NameEmpty").innerHTML="";
       document.getElementById("EmptyPrice").innerHTML="";
        document.getElementById("EmptyCapacity").innerHTML="يجب ادخال السعة";
      }
       else if(Price < 0)
       {
           document.getElementById("NameEmpty").innerHTML="";
       document.getElementById("EmptyPrice").innerHTML="";
        document.getElementById("EmptyCapacity").innerHTML="";
        document.getElementById("ValuePrice").innerHTML="يجب ادخال قيمة اكبر من الصفر";
       }
       else if(Capacity < 0)
       {
         document.getElementById("NameEmpty").innerHTML="";
       document.getElementById("EmptyPrice").innerHTML="";
        document.getElementById("EmptyCapacity").innerHTML="";
        document.getElementById("ValuePrice").innerHTML="";
        document.getElementById("ValueCapacity").innerHTML="يجب ادخال قيمة اكبر من الصفر";

       }
       else
       {

    $.ajax({
            type: "POST",
            url: '/SaveTypes',

            dataType: 'json',
            data: {'_token':$('#token').val(),
            'id':typeValues,
            'Faci':FacilityValues,
            'HallID':$('#id').val(),
            'Name':Name,
            'Price':Price,
            'Capacity':Capacity
          },
           success:function(response)
            {
               window.location = response;    
        }
        });
       }

   
 }


</script> 
@stop
@stop