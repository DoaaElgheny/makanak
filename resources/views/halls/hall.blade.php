@extends('masterUser')
@section('content')
<style>


@media (max-width: 360px){

    .ma1 {
            margin-top: 70px;
    margin-left: 120px;
    }
    .calendar2 { margin-top: 60px;}
}
    
    @media (max-width:412px){
        .ma2{    margin-top: -113px;
            margin-right: 120px;}
        
        
         .calendar2 { margin-top: 60px;}

}
        @media (max-width:375px){
        .ma2{    margin-top: -113px;
            margin-right: 120px;}
        
        
         .calendar2 { margin-top: 60px;}

}
      @media (max-width:414px){
        .ma2{    margin-top: -113px;
            margin-right: 120px;}
        
        
         .calendar2 { margin-top: 60px;}

}
    .lre {
          border: 1px solid #841851;
    color: white !important;
    background-color:  #841851;
    padding: 2px 15px;     margin-left: 90px;
    }
      .lre:hover {
          border: 1px solid #841851;
    color: white !important;
    background-color: #69053a;
              text-decoration: none;
   
    }
    
    
</style>
 

<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.2.7/fullcalendar.min.js"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.2.7/fullcalendar.min.css"/>
<script>

$(document).ready(function() {
    var numOfHalls = 0;
    var hallsID = [];
    @foreach ($hallall as $h)
        hallsID[numOfHalls++] = {{ $h->id }};
    @endforeach
    var counter = 0;
   
    @foreach ($hallall as $h)   
        $('#calendar'+ hallsID[counter]).fullCalendar({ 
            
            events:
            {
                url: "{{ URL::to('GetEventsForUser') }}",
                type: "POST",
                dataType: 'json',
                data: {'id' : hallsID[counter++] ,"_token":$('#_token').val()},
                success: function(response)
                    {
                    },
                    error: function(response)
                    {
                    }
                
            },
        
            header: {
            left: 'title',
            center: '',
            right: 'agendaDay month prev next'
            },
              defaultDate:new Date(),// '2017-08-15',
            defaultView: 'agendaDay',
            //editable: false,
            //droppable: true,
            minTime : '{{ $h->WorkStart }}',
            maxTime : '{{ $h->WorkEnd }}',
            eventLimit: true,
            views: 
            {
                agenda: {
                    eventLimit: 0 
                }
            }
        });
     @endforeach
});
</script>

 <br><br>

      <section  id ="pricing" class="col-md-3 righ">
       <div class="container" style="    margin-top: 40px; ">
        <div class="row">
      <div class="col-md-3 righ" style="border: 1px solid #CCC;">
           <div class="pricing-head" style="margin-bottom: 9px; border-bottom: 2px #ccc solid;">
          <img src="{{$PlaceData->Logo}}" class="img-responsive lama" alt="" style="    border-radius: 50%;
    width: 100px;
    margin-top: 20px;
    border: 1px solid #ddd;
                                                                            height: 90px;">
               <p class="text-center" style="margin-top: 10px;
    font-size: 20px;
    font-weight: bold; "> {{$PlaceData->Name}} </p>
               <br>
              </div>


          <div class="pricing-body">

                 <p style="text-align: right;     margin-bottom: -9px;">
                               <i class="fa fa-phone-square" aria-hidden="true" style="  color: #841851;"></i>

                   <!--  //{{$PlaceData->Tele}} -->01061142749 </p>

              <br>
               <p style="text-align: right;     margin-bottom: -9px;">
                        <i class="fa fa-envelope" aria-hidden="true" style="  color: #841851;"></i>
                 <!-- {{$PlaceData->Email}} --> makanka2019@gmail.com</p>
                <br>

                 <p style="text-align: right;">
                 <i class="fa fa-map-marker" aria-hidden="true" style="  color: #841851;"></i>
          {{$PlaceData->Address}} </p>
              <br>
                    <p style="text-align: right;     margin-bottom: -9px;">
                 <i class="fa fa-info"  aria-hidden="true" style="  color: #841851;"></i>
                        

            {{$PlaceData->Description}} </p>

                <br>


          <ul class="social-icons icon-circle icon-rotate list-unstyled list-inline">


            <div class=" head4">
                                       <div class="lamiaa">



                                  <li> <a href="" target="_blank"><i class="fa fa-facebook"></i></a></li>
             <li> <a href="#"><i class="fa fa-twitter" target="_blank"></i></a></li>


                                                 <li> <a href="#"><i class="fa fa-youtube-square"></i></a></li>

                                     </div>
                                </div>


              </ul>



          </div>





  


          </div>

          </Section >
          <section class="col-md-9 " style="padding-top: 45px">

          @foreach($hallall as $all)


      <div class="col-md-11 lif" style="border: 1px solid #CCC; " >

           <div class="head_title text-center " style="    margin-top: 0px;">
                                    <h2 >{{$all->Name}} </h2>


                                </div><!-- End off Head_title -->


             <div class="col-md-5 col-sm-5">
            <div class="">
              <!-- Plan  -->












              <div class="rightimg" style="
   ">
            
                  <img src="{{$all->Image}}" class="img-responsive" alt="img25" style="    width: 280px;
    height: 242px;">
                     @foreach ($all->GetFacilities as $value)
   <div class="rightimg" style="">
<!--   <li>{{$value->Name}}</li> -->
 <div  style="float:left;margin-bottom:10px;width:17%;height:50px;    margin-left: 2px;
">
                            <div style="width:100%;" onclick="changefilter({{$value->id}})">
                             <center><img src="{{$value->Image}}" style="    width: 40px;
    height: 40px;
    margin-top: 20px;"></center>
                            </div>

                          </div>
   </div>
  @endforeach


              </div>


            </div>
          </div>
          <div class="col-md-2 col-sm-2">
            <!-- Plean Detail -->
              <div class="  fli mart-15"  style="
         margin-right: -30px;


 ">

                  <p  class="ma1" style="    text-align: center;
    margin-top: 9px; font-size: 18px;
    font-weight: bold;     margin-right: 35px;">

                      <i class="fa fa-money" aria-hidden="true" style="    color: #841851;
    font-size: 30px;
    margin-right: 5px;
    margin-left: 5px;"></i>

                      <br>
                    {{$all->Price}} جنية



          </p>
                  <br>
                  <br>

                  <p  class="ma2" style="   text-align: center;
    margin-top: 9px; font-size: 18px;
    font-weight: bold;     margin-right: 35px;"><i class="fa fa-users" aria-hidden="true" style="    color: #841851;
    font-size: 30px;
    margin-right: 5px;
    margin-left: 5px; "></i>
                      <br>
                  {{$all->Capacity}} فرداً


                  </p>

              </div>
          </div>






                                 <div class="col-md-5" style="margin-top: -51px;">
                                    <input type="hidden" value="{{ csrf_token() }}" id="_token" name="_token" />
                                    <div id = ""></div> 
                                    <!-- //calendar{{ $all->id }} -->
                                    <br/><br/>
                                      <div class="col-md-12" style="    text-align:left; margin-bottom: 15px;     margin-top: -20px;">
<a href="#" data-target="#e7gezoline" data-toggle="modal"  class="lre"> إحجز الآن</a>

              </div>
                                  </div>



 <!--Modal box-->
    <div class="modal fade" id="e7gezoline" role="dialog">
      <div class="modal-dialog modal-sm">

        <!-- Modal content no 1-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title text-center form-title"> إحجز الآن</h4>
          </div>
          <div class="modal-body padtrbl">

            <div class="login-box-body">
         <!--  {!!Form::open(['action'=>'LoginController@Login','method' => 'post','files'=>true])!!} -->
              <div class="form-group">

                 <div class="form-group has-feedback"> <!----- username -------------->
                      <input class="form-control" name="Name" placeholder="الاسم "  id="Name" type="text" autocomplete="off" />
            <span style="display:none;font-weight:bold; position:absolute;color: red;position: absolute;padding:4px;font-size: 11px;background-color:rgba(128, 128, 128, 0.26);z-index: 17;  right: 27px; top: 5px;" id="span_loginid"></span><!---Alredy exists  ! -->
                      <span class="glyphicon glyphicon-user form-control-feedback"></span>
                  </div>
                   <div class="form-group has-feedback"> <!----- username -------------->
                      <input class="form-control" name="Tele" placeholder="التليفون"  id="Tele" type="number" autocomplete="off" />
            <span style="display:none;font-weight:bold; position:absolute;color: red;position: absolute;padding:4px;font-size: 11px;background-color:rgba(128, 128, 128, 0.26);z-index: 17;  right: 27px; top: 5px;" id="span_loginid"></span><!---Alredy exists  ! -->
                      <span class="fa fa-phone form-control-feedback"></span>
                  </div>
                   <div class="form-group has-feedback"> <!----- username -------------->
                      <input class="form-control" name="EmailRes" placeholder="البريد الإلكترونى "  id="EmailRes" type="Email" autocomplete="off" />
            <span style="display:none;font-weight:bold; position:absolute;color: red;position: absolute;padding:4px;font-size: 11px;background-color:rgba(128, 128, 128, 0.26);z-index: 17;  right: 27px; top: 5px;" id="span_loginid"></span><!---Alredy exists  ! -->
                      <span class="fa fa-envelope-o form-control-feedback"></span>
                  </div>
                  تفاصيل الحجز
                   <div class="form-group has-feedback"> <!----- username -------------->
                      <textarea class="form-control" name="Details"  placeholder="تفاصيل الحجز"  id="Details" type="text" autocomplete="off" />
       
                      </textarea>
                  </div>

<div >
        <label style="color:red" id="warn"></label> 
     </div>
               
                  <div class="row">
                     
                      <div class="col-xs-12">
                      <button type="submit" class="btn btn-bg red btn-block"  onclick="ReserveNow();">إحجز</button>

                      </div>

         
         

                  </div>
              <!--  {!!Form::close()!!} -->
              </div>
            </div>
          </div>
        </div>

      </div>
    </div>
    <!--/ Modal box-->








          </div>

   @endforeach


           </div>
          </div>


<div id="eventContent" title="Event Details" style="display:none;">
    Start: <span id="startTime"></span><br>
    End: <span id="endTime"></span><br><br>
    <p id="eventInfo"></p>
    <p><strong><a id="eventLink" href="" target="_blank">Read More</a></strong></p>
</div>


      </section>





<br/>
  <script >
      function ReserveNow()
      {

     var Name=  $("#Name").val();
     var Tele=  $("#Tele").val();
     var EmailRes=  $("#EmailRes").val();
     var Details=  $("#Details").val();
     if (Name=="")
     {
      document.getElementById("warn").innerHTML="من فضلك ادخل الاسم";
     }
       else if (Tele=="")
     {
      document.getElementById("warn").innerHTML="من فضلك ادخل رقم التلفون";
     }
       else if(EmailRes=="")
     {
      document.getElementById("warn").innerHTML="من فضلك ادخل البريد الالكتروني";
     }
    else{
       $.ajax({
      url: "/ReserveNow",
        data:{Tele:Tele,EmailRes:EmailRes,Name:Name,Details:Details},
      type: "get",
      success: function(data){
        
       window.location = data;
      }
    });

    }
        
      }
    </script>

  @stop