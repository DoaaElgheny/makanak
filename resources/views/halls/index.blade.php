<!DOCTYPE html>
<html lang="en">
<head>
  <title>القاعات</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    

 <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Open+Sans|Candal|Alegreya+Sans">
    <link rel="stylesheet" type="text/css" href="/assets/UserFiles/css/font-awesome.min.css">
       
    <link rel="stylesheet" type="text/css" href="/assets/UserFiles/css/style.css">

<link href="https://fonts.googleapis.com/css?family=Cairo" rel="stylesheet">

    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="https://code.jquery.com/ui/1.10.4/jquery-ui.min.js"></script>
    <script type="text/javascript" src="/assets/UserFiles/js/bootstrap-datetimepicker.js" charset="UTF-8"></script>
    <script type="text/javascript" src="/assets/UserFiles/js/locales/bootstrap-datetimepicker.fr.js" charset="UTF-8"></script>
     <link rel="stylesheet" href="/assets/UserFiles/css/bootstrap-datetimepicker.min.css">
  <style type="text/css">
.range {
    display: table;
    position: relative;
    height: 25px;
    margin-top: 20px;
    background-color: rgb(245, 245, 245);
    border-radius: 4px;
    -webkit-box-shadow: inset 0 1px 2px rgba(0, 0, 0, 0.1);
    box-shadow: inset 0 1px 2px rgba(0, 0, 0, 0.1);
    cursor: pointer;
}

.range input[type="range"] {
    -webkit-appearance: none !important;
    -moz-appearance: none !important;
    -ms-appearance: none !important;
    -o-appearance: none !important;
    appearance: none !important;

    display: table-cell;
    width: 100%;
    background-color: transparent;
    height: 25px;
    cursor: pointer;
}
.range input[type="range"]::-webkit-slider-thumb {
    -webkit-appearance: none !important;
    -moz-appearance: none !important;
    -ms-appearance: none !important;
    -o-appearance: none !important;
    appearance: none !important;

    width: 11px;
    height: 25px;
    color: rgb(255, 255, 255);
    text-align: center;
    white-space: nowrap;
    vertical-align: baseline;
    border-radius: 0px;
    background-color: rgb(153, 153, 153);
}

.range input[type="range"]::-moz-slider-thumb {
    -webkit-appearance: none !important;
    -moz-appearance: none !important;
    -ms-appearance: none !important;
    -o-appearance: none !important;
    appearance: none !important;
    
    width: 11px;
    height: 25px;
    color: rgb(255, 255, 255);
    text-align: center;
    white-space: nowrap;
    vertical-align: baseline;
    border-radius: 0px;
    background-color: rgb(153, 153, 153);
}

.range output {
    display: table-cell;
    padding: 3px 5px 2px;
    min-width: 40px;
    color: rgb(255, 255, 255);
    background-color: rgb(153, 153, 153);
    text-align: center;
    text-decoration: none;
    border-radius: 4px;
    border-bottom-left-radius: 0;
    border-top-left-radius: 0;
    width: 1%;
    white-space: nowrap;
    vertical-align: middle;

    -webkit-transition: all 0.5s ease;
    -moz-transition: all 0.5s ease;
    -o-transition: all 0.5s ease;
    -ms-transition: all 0.5s ease;
    transition: all 0.5s ease;

    -webkit-user-select: none;
    -khtml-user-select: none;
    -moz-user-select: -moz-none;
    -o-user-select: none;
    user-select: none;
}
.range.range-success input[type="range"]::-webkit-slider-thumb {
    background-color: #841851;
}
.range.range-success input[type="range"]::-moz-slider-thumb {
    background-color: #841851;
}
.range.range-success output {
    background-color: #841851;
}
.range.range-success input[type="range"] {
    outline-color: #841851;
}

.range.range-warning input[type="range"]::-webkit-slider-thumb {
    background-color: #841851;
}
.range.range-warning input[type="range"]::-moz-slider-thumb {
    background-color: #841851;
}
.range.range-warning output {
    background-color: #841851;
}
.range.range-warning input[type="range"] {
    outline-color: #841851;
}
    

    </style>
</head>
<body style="font-size: 15px;
    line-height: 1.52;
    color: black;
    font-size: 16px;
font-family:'Cairo', sans-serif;
    font-weight: 400;background-color: #ebeced">
  


 <nav class="navbar navbar-default navbar-fixed-top" >
      <div class="container">
        <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
       
        </div>
        <div class="collapse navbar-collapse" id="myNavbar">
           <a class="navbar-brand" style="float:right" href="/Home"><img src="assets/img/lo3.png" class="img-responsive" alt="#" style="margin-top: -6px;">
            
            </a>
        <ul class="nav navbar-nav navbar-left"   style="margin-top: 10px;">
 
          <li class="btn-trial"><a href="/Regest">مستخدم جديد</a></li>
          <li class="btn-trial"><a href="#" data-target="#login" data-toggle="modal" >تسجيل دخول</a></li>
          <li><a href="/Halls">المناسبات</a></li>
            <li><a href="/Halls">القاعات</a></li>
           <li class="nav-item active"><a href="/Home">الرئيسية <span class="sr-only">(current)</span></a></li>
          
        </ul>

        </div>
      </div>
    </nav>
    <br/><br/><br/>
<br/><br/>

    <div class="modal fade" id="login" role="dialog">
      <div class="modal-dialog modal-sm">
      
        <!-- Modal content no 1-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title text-center form-title" style="margin-left: 17px;">تسجيل دخول</h4>
          </div>
          <div class="modal-body padtrbl">

            <div class="login-box-body">
         <!--  {!!Form::open(['action'=>'LoginController@Login','method' => 'post','files'=>true])!!} -->
              <div class="form-group">
               
                 <div class="form-group has-feedback"> <!----- username -------------->
                      <input class="form-control" name="Username" placeholder="البريد الإلكترونى "  id="loginid" type="text" autocomplete="off"  style="    direction: rtl;
    padding-right: 12px;" /> 
            <span style="display:none;font-weight:bold; position:absolute;color: red;position: absolute;padding:4px;font-size: 11px;background-color:rgba(128, 128, 128, 0.26);z-index: 17;  right: 27px; top: 5px;" id="span_loginid"></span><!---Alredy exists  ! -->
                      <span class="glyphicon glyphicon-user form-control-feedback"></span>
                  </div>
                  <div class="form-group has-feedback"><!----- password -------------->
                      <input class="form-control" name="Password" placeholder="كلمة السر " id="loginpsw" type="password" autocomplete="off"   style="    direction: rtl;
    padding-right: 12px;" />
            <span style="display:none;font-weight:bold; position:absolute;color: grey;position: absolute;padding:4px;font-size: 11px;background-color:rgba(128, 128, 128, 0.26);z-index: 17;  right: 27px; top: 5px;" id="span_loginpsw"></span><!---Alredy exists  ! -->
                      <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                  </div>
                  <div class="row">
                      <div class="col-xs-12">
                          <div class="checkbox icheck" style="    direction: rtl;">
                              <label>
                                <input type="checkbox" id="loginrem" > <span style="margin-right: 20px;">تذكر كلمة السر</span>
                              </label>
                          </div>
                      </div>
                      <div class="col-xs-12">
                      <button type="submit" class="btn btn-bg red btn-block" onclick="login()">تسجيل</button>
                      
                      </div>
             <div class="alert alert-danger" hidden="hidden" id="Error" style="text-align:center;" >
                <strong></strong> كلمة المرور او البريد الالكتروني غير صحيح <br><br>
          </div>
          <div class="alert alert-danger" hidden="hidden" id="Error2" style="text-align:center;" >
                <strong></strong> من فضلك ادخل البيانات اولا<br><br>
          </div>
          <div class="alert alert-danger" hidden="hidden" id="Error3" style="text-align:center;" >
                <strong></strong>هذا الحساب غير مسجل لدينا<br><br>
          </div>
           <div class="alert alert-danger" hidden="hidden" id="Error4" style="text-align:center;" >
                <strong></strong>انتظر سيم تفعيل حسابك خلال ساعات<br><br>
          </div>
                  </div>
              <!--  {!!Form::close()!!} -->
              </div>
            </div>
          </div>
        </div>

      </div>
    </div>

    <div class="row" style="background-color:#841851;padding-top: 10px;
    padding-bottom: 10px; margin-top: -43px;">
      <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
      <div class="col-md-2">
      </div>
      <div class="col-md-8">
      
        <div  class="row" style="width:100%;margin-top:.2cm;margin-bottom:.2cm;" >
          <div class="col-md-4">
            <div style="background-color:white;width:100%;height:45px;">
              <div style="background-color:#cdd6e0;width:5%;height:100%;float:left;">&nbsp;&nbsp;</div>
              <div style="width:20%;height:100%;float:left;">
               <center> <i  class="glyphicon glyphicon-calendar" style="margin-top: 15px;
    color: #841851;"></i></center> 
              </div>
             
  <div style="width:75%;height:100%;float:left;    padding-right: 20px;" dir="rtl">
                
                <br/>
                <input class="form-control Todate"  type="text" value="{{$dateNow}}" style="height:30px;    background-color:white;     margin-top: -15px;font-size: 16px;     border: 0px;"  readonly>        
              </div>

            </div>
          </div>
          <div class="col-md-4">
            <div style="background-color:white;width:100%;height:45px;" class="trigger1">
              <div style="background-color:#cdd6e0;width:5%;height:100%;float:left;">&nbsp;&nbsp;</div>
              <div style="width:20%;height:100%;float:left;">
               <center> <i  class="glyphicon glyphicon-calendar" style="margin-top: 15px;
    color: #841851;"></i></center> 
              </div>

               <div style="width:75%;height:100%;float:left;    padding-right: 20px;" dir="rtl">
               
                <br/> 
               <input class="form-control Fromdate"  type="text" value="{{$dateNow}}" style="height:30px;     background-color:white;     margin-top: -15px;font-size: 16px;     border: 0px;" readonly>       
              </div>
            
            </div>
          </div>
          <div class="col-md-4">
            <div class="dropdown" style="width:100%;height:45px;" dir="rtl">
              {!!Form::select('Type', ([null => 'اختر نوع القاعة']+$types->toArray()) ,null,['class'=>'form-control btn btn-default dropdown-toggle','id' => 'Type','style'=>'height:100%;'])!!}
    
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-2">
      </div>
    </div>
    <div class="row" style="background-color:#ebeced;">
      <div class="container">
        <div class="row" style="margin-top:.5cm;">
          <div class="col-md-9" >
            <div class="col-md-12" id="partial">
               @include('halls.particalview',$stats)
            </div>
          </div>
          <div class="col-md-3">
        <!--     <div style="width:100%;height:80px;background-color:white;background-image: url(//ie2.trivago.com/images/layoutimages/content/map-access.jpg);background-size: 100%;border:8px solid white;"> -->
             <!--  <center>
                <div style="border:1px solid black;background-color:white;width:40%;height:45px;margin-top:.3cm;">
                  <h5 style="color:black;"><b></b></h5>
                </div>
              </center> -->
          <!--   </div> -->
            <div style="width:100%;background-color:white;margin-top:.3cm;margin-bottom: 20px;">
            <div>
                   <div class="head_title text-center " style="    padding-top: 20px;">
                                 <h2> تصفية النتائج   </h2>
            
                                   
                                    <div class=" separator"></div>
                                </div><!-- End off Head_title -->
            </div>
            <!--   <ul class="nav nav-tabs">
                <li class="nav active"><a href="#A" data-toggle="tab"><h5 style="color:black;"><b>To Filters</b></h5></a></li>
              </ul> -->
              <!-- Tab panes -->
              <div class="tab-content">
                  <div class="tab-pane fade in active" style="margin-left:10px;margin-top:.2cm;"  id="A">          
                
                    
                    <div class="row" style="margin-left:5px;margin-top:.1cm;">
                      <h5 style="    color: black;
    text-align: right;
    padding-right: 40px;"><b>المحافظة </b></h5> 
                      <div class="col-md-11" dir="rtl" style="    height: 38px;">
                        {!!Form::select('Government', ([null => 'اختار المحافظه'] + $Government->toArray()) ,null,['class'=>'form-control','id' => 'Government'])!!}
                      </div>
                    </div>
                   
                    <div class="row" style="margin-left:5px;margin-top:.1cm;">
                      <h5 style="    color: black;
    text-align: right;
    padding-right: 40px;"><b>المركز</b></h5> 
                      <div class="col-md-11" dir="rtl" style="    height: 38px;">
                        {!!Form::select('District', ([null => 'اختار المركز'] + $District->toArray()) ,null,['class'=>'form-control','id' => 'District'])!!}
                      </div>
                    </div>
                    
                    <div class="row" style="margin-left:5px;margin-top:.1cm;">
                      <h5 style="    color: black;
    text-align: right;
    padding-right: 40px;"><b>السعر /ساعة</b></h5>  
                    </div>
                    <div class="row" style="margin-top:.1cm;">
                      <!-- <center>
                        <h5 style="    color: black;
    text-align: right;
    padding-right: 40px;">max:L.E<span id="avragprice">{{$MaxPrice}}</span></h5>
                      </center>  -->
                    </div>
                    <div class="row">
                      <h6 style="color:black;float:left;margin-left:15px;">L.E{{$MinPrice}}</h6>  
                      <h6 style="color:black;float:right;margin-right:44px;" >L.E{{$MaxPrice}}</h6> 
                    </div>
                    <div class="row">
                      <div class="col-md-11" >
                        <div class="range range-success">
                          <input type="range" id="rangeprice" name="range" min="{{$MinPrice}}" max="{{$MaxPrice}}" value="{{$MaxPrice}}" onchange="changeprice(this)">
                          <output id="rangesuccess">{{$MaxPrice}}</output>
                        </div>
                      </div>
                      <div class="col-md-1" >
                      </div>
                    </div>
                   
                    <div class="row" style="margin-left:5px;margin-top:.1cm;">
                      <h5 style="    color: black;
    text-align: right;
    padding-right: 40px;"><b>السعة/فرد</b></h5>  
                    </div>
                    <div class="row" style="margin-top:.1cm;">
                     <!--  <center>
                        <h5 style="color:#cdd6e0;">max:$<span id="avargeCap">{{$MaxCapcity}}</span></h5>
                      </center> --> 
                    </div>
                    <div class="row">
                      <h6 style="color:black;float:left;margin-left:15px;">  {{$MinCapcity}} </h6>  
                      <h6 style="color:black;float:right;margin-right:44px;" >{{$MaxCapcity}} </h6> 
                    </div>
                    <div class="row">
                      <div class="col-md-11" >
                        <div class="range range-warning">
                          <input type="range" id="rangecap" name="range" min="{{$MinCapcity}}" max="{{$MaxCapcity}}" value="{{$MaxCapcity}}" onchange="changecap(this)">
                          <output id="rangeWarning">{{$MaxCapcity}}</output>
                        </div>
                      </div>
                      <div class="col-md-1" >
                      </div>
                    </div>
                   
                    <div class="row" style="margin-left:5px;margin-top:.1cm;">
                      <h5 style="    color: black;
    text-align: right;
    padding-right: 40px;"><b>الإمكانيات</b></h5>  
                    </div>
                    <div class="row">
                      <div class="col-md-12 checkbox">
                        @foreach($Facilities as $Facility)
                          <div  style="float:left;margin-left:8px;margin-bottom:10px;width:20%;height:50px;">
                            <div style="border: 1px solid #CCC;width:100%;">
                            <center><input name="Facility" type="checkbox"onclick="changefilter(this)" value="{{$Facility->id}}"></center>
                            <img src="{{$Facility->Image}}" class="img-responsive"   style="  margin-top: 15px;     width: 40px; ">
                            </div>
                         
                             <br/>
                          <!--   <center><p>{{$Facility->Name}}</p></center> -->
                          </div>
                        @endforeach
                      </div>
                    </div>
                    <div class="row" style="height:50px;">
                    </div> 
                  </div> 
                </div>  
              </div>
            </div>
        </div>
      </div>
    </div>

    <!--Footer-->
    <footer id="footer" class="footer" >
      <div class="container text-center" >
    
      <h3 class="wow animated fadeInDown" data-wow-offset="70">اشترك معنا ليصلك أخر العروض</h3>
      <div class="col-md-2 "></div>
     {!! Form::open(['action' => "LoginController@Offers",'method'=>'post','id'=>'profileForm']) !!} 
  <div class="col-md-2 col-sm-4">
          <p>
            <button name="submit" type="submit" class="btn btn-block btn-submit"><i class="fa fa-arrow-left" aria-hidden="true"></i>
          اشترك </button>
          </p>
            
            
            
        </div>
       <!-- End email input -->
        <div class="form-group col-md-3 col-sm-4" >
          <div class=" controls">
            <input name="EMAIL" placeholder="البريد الإلكترونى" class="form-control" type="email" style="direction: rtl;">
          </div>
        </div><!-- End email input -->
       <div class="form-group col-md-3 col-md-offset-2 col-sm-4" style="    margin-left: 0px;">
          <div class=" controls">
            <input name="txtName" placeholder="الإسم" class="form-control" type="text" style="direction: rtl;">
          </div>
        </div>
       {!!Form::close()!!}<!-- End newsletter-form -->
          <div class="col-md-2 "></div>
      <ul class="social-links social-icons icon-circle icon-zoom list-unstyled list-inline">
      
           <li class="wow animated zoomIn" data-wow-delay="0.6s" data-wow-offset="40"> <a href="#"><i class="fa fa-youtube"></i></a></li>
         
             <li  class="wow animated zoomIn" data-wow-delay="0.3s" data-wow-offset="40"> <a href="https://www.facebook.com/Eventarya/" target="_blank"><i class="fa fa-facebook"></i></a></li> 
     <li class="wow animated zoomIn" data-wow-offset="40"> <a href="#"><i class="fa fa-google-plus"></i></a></li>
      </ul>
       <div class="col-sm-3 col-xs-12 main_footer third3" style="">
                                <p style="text-align: left;     margin-left: 6px;">
                                   <i class="fa fa-phone" aria-hidden="true"></i>
                                   01061142749
                                    <br>
                                    <i class="fa fa-envelope-o" aria-hidden="true"></i>
                                    makanka2019@gmail.com
                                     
                                </p>
                    </div>
            
                
                    
 <div class="col-md-4 two2" style="">
<div id="templatemo_footer" style="        margin-left: 80px;">
 <a href="/Home">الرئيسية</a> | <a href="/Halls">القاعات </a> | <a href="/Halls">المناسبات </a>  <a href="contact.html"> </a> <br />
                               جميع الحقوق محفوظة  مكانك© 2019
                                <br>
                            </div>
                    </div>
     

                   
 
           <div class="col-sm-3 col-xs-12 main_footer one1" style="     margin-left: 70px; ">
                        
                       


                                <p style="     margin-right: -30px;
    margin-left: 22px;">
                                
   
                                   
                                    
                                   
                        <!-- جامعة أسيوط -مبنى شبكة المعلومات -->
                                    <BR>
                                 <!-- E-Rserve   الدور السادس- شركة  -->

               </p>
                    </div>
<div class="col-sm-1 col-xs-12 main_footer" style="    margin-left: 10px;">
                                <p >
                                 
                                
    </p>

                      
                    </div>
                
      </div>
    </footer>
    <!--/ Footer-->
   
    <script src="assets/UserFiles/js/custom.js"></script>
   <!--  <script src="contactform/contactform.js"></script> -->
 <script>
 function changefilter(e)
 {

     var checkValues = $('input[name=Facility]:checked').map(function()
            {
                return $(this).val();
            }).get();
    $.ajax({
            type: "POST",
            url: '/changeFacilty',
            dataType: 'json',
            data: {'_token':$('#token').val(),
            'Price':$('#rangeprice').val(),
            'Capcity':$('#rangecap').val(),
            'id':checkValues

          },
            success: function(data) {   
                $('#partial').empty();
                $('#partial').append(data['html']);
             }
        });
 }
 function changeprice(rang)
 {
  $('#avragprice').html(rang.value);
  $('#rangesuccess').val(rang.value);
  $.ajax({
            type: "POST",
            url: '/changeAll',
            dataType: 'json',
            data: {'_token':$('#token').val(),
            'Price':$('#rangeprice').val(),
            'Capcity':$('#rangecap').val(),
            'Type':$('#Type').val(),
            'fromdate':$('.Fromdate').val(),
            'todate':$('.Todate').val(),
            'Government':$('#Government').val(),
            'District':$('#District').val()

          },
            success: function(data) {   
                $('#partial').empty();
                $('#partial').append(data['html']);
             }
        });
 }
 function changecont(count)
 {
   $.ajax({
            type: "POST",
            url: '/changecont',
            dataType: 'json',
            data: {'_token':$('#token').val(),
            'Price':$('#rangeprice').val(),
            'Capcity':$('#rangecap').val(),
            'Type':$('#Type').val(),
            'fromdate':$('.Fromdate').val(),
            'todate':$('.Todate').val(),
            'Government':$('#Government').val(),
            'District':$('#District').val(),
            'count':count
          },
            success: function(data) {   
                $('#partial').empty();
                $('#partial').append(data['html']);
             }
        });
 }

 function changecap(rang)
 {
  $('#avargeCap').html(rang.value);
  $('#rangeWarning').val(rang.value);
  $.ajax({
            type: "POST",
            url: '/changeAll',
            dataType: 'json',
            data: {'_token':$('#token').val(),
            'Price':$('#rangeprice').val(),
            'Capcity':$('#rangecap').val(),
            'Type':$('#Type').val(),
            'fromdate':$('.Fromdate').val(),
            'todate':$('.Todate').val(),
            'Government':$('#Government').val(),
            'District':$('#District').val()

          },
            success: function(data) {   
                $('#partial').empty();
                $('#partial').append(data['html']);
             }
        });
 }

  $( function() {
    $("#Type").on('change',function(){
   
     $.ajax({
            type: "POST",
            url: '/changeAll',
            dataType: 'json',
            data: {'_token':$('#token').val(),
            'Price':$('#rangeprice').val(),
            'Capcity':$('#rangecap').val(),
            'Type':$('#Type').val(),
            'fromdate':$('.Fromdate').val(),
            'todate':$('.Todate').val(),
            'Government':$('#Government').val(),
            'District':$('#District').val()

          },
            success: function(data) {   
                $('#partial').empty();
                $('#partial').append(data['html']);
             }
        });
    });

   $('.Fromdate').datetimepicker({
        //language:  'fr',
        format: 'yyyy-mm-dd hh:ii:ss',
        weekStart: 1,
        todayBtn:  1,
    autoclose: 1,
    todayHighlight: 1,
    startView: 2,
    forceParse: 0,
        showMeridian: 1
    }) .on("changeDate", function(e) {
       $.ajax({
            type: "POST",
            url: '/changeAll',
            dataType: 'json',
            data: {'_token':$('#token').val(),
            'Price':$('#rangeprice').val(),
            'Capcity':$('#rangecap').val(),
            'Type':$('#Type').val(),
            'fromdate':$('.Fromdate').val(),
            'todate':$('.Todate').val(),
            'Government':$('#Government').val(),
            'District':$('#District').val()

          },
            success: function(data) {   
                $('#partial').empty();
                $('#partial').append(data['html']);
             }
        });
    });
    $('.Todate').datetimepicker({
        //language:  'fr',
        format: 'yyyy-mm-dd hh:ii:ss',
        weekStart: 1,
        todayBtn:  1,
    autoclose: 1,
    todayHighlight: 1,
    startView: 2,
    forceParse: 0,
        showMeridian: 1
    }) .on("changeDate", function(e) {
       $.ajax({
            type: "POST",
            url: '/changeAll',
            dataType: 'json',
            data: {'_token':$('#token').val(),
            'Price':$('#rangeprice').val(),
            'Capcity':$('#rangecap').val(),
            'Type':$('#Type').val(),
            'fromdate':$('.Fromdate').val(),
            'todate':$('.Todate').val(),
            'Government':$('#Government').val(),
            'District':$('#District').val()

          },
            success: function(data) {   
                $('#partial').empty();
                $('#partial').append(data['html']);
             }
        });
    });


    $('#Government').on('change', function() {
   $.ajax({
            type: "POST",
            url: '/changeAll',
            dataType: 'json',
            data: {'_token':$('#token').val(),
            'Price':$('#rangeprice').val(),
            'Capcity':$('#rangecap').val(),
            'Type':$('#Type').val(),
            'fromdate':$('.Fromdate').val(),
            'todate':$('.Todate').val(),
            'Government':$('#Government').val(),
            'District':$('#District').val()

          },
            success: function(data) {   
                $('#partial').empty();
                $('#partial').append(data['html']);
             }
        });  });
 $('#District').on('change', function() {
$.ajax({
            type: "POST",
            url: '/changeAll',
            dataType: 'json',
            data: {'_token':$('#token').val(),
            'Price':$('#rangeprice').val(),
            'Capcity':$('#rangecap').val(),
            'Type':$('#Type').val(),
            'fromdate':$('.Fromdate').val(),
            'todate':$('.Todate').val(),
            'Government':$('#Government').val(),
            'District':$('#District').val()

          },
            success: function(data) {   
                $('#partial').empty();
                $('#partial').append(data['html']);
             }
        });
  });
     

         
      });

     
  </script>
     <script >
      function login()
      {

     var Username=  $("#loginid").val();
     var Password=  $("#loginpsw").val();
    
         $.ajax({
      url: "/Login",
        data:{Username:Username,Password:Password},
      type: "get",
      success: function(data){
        if(data==2)
        {
         $('#Error2').show();
         $('#Error').hide();
         $('#Error4').hide();
         $('#Error3').hide();
        }
      else  if(data==1)
        {
         $('#Error').show();
         $('#Error2').hide();
$('#Error4').hide();
         $('#Error3').hide();
        }
        else  if(data==4)
          {
            $('#Error4').show();
           $('#Error3').hide();
           $('#Error').hide();
           $('#Error2').hide();
          }
        else  if(data==3)
          {
           $('#Error3').show();
           $('#Error4').hide();
           $('#Error').hide();
           $('#Error2').hide();
          }
        else
        {
          // alert(data);
       window.location = data;
        }


      }
    });
      }
    </script>
</body>
</html>
