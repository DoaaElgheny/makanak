@extends('master')
@section('content')
 <section class="panel-body">
 <style type="text/css">
    #blah{
        background: url(assets/UserFiles/dist/img/33.jpg) no-repeat;
        background-size: cover;
    }
    table.dataTable.row-border tbody tr th, table.dataTable.row-border tbody tr td, table.dataTable.display tbody tr th, table.dataTable.display tbody tr td  {
           text-align: right;
    }
    div.dt-buttons .dt-button {
      margin-right: 0;
      background-color: #841851;
      font-size: 16px;
      padding-left: 30px;
      padding-right: 30px;
      border: 1px solid #841851;
      margin-bottom: 20px;
      height: 35px;
    }
    div.dt-buttons .dt-button:hover , .dt-button:focus {
      border: 1px solid #841851;
      color: white !important;
      background-color: #69053a; 
    }
    div.dataTables_wrapper div.dataTables_filter label {
      color: #841851;
      font-size: 17px;
      direction: rtl;
    }
    div.dataTables_wrapper div.dataTables_filter input {
      width: 170px;
      margin-right: 7px;
      height: 33px;
    }
    div.dataTables_wrapper div.dataTables_length label {
      color: #841851;
      font-size: 17px;
    }
    div.dataTables_wrapper div.dataTables_length select {
      width: 170px;
      display: inline-block;
      text-align: right;
      direction: rtl;
    }
    select.input-sm {
      height: 33px;
      line-height: 30px; 
    } 
    .btn-primary:hover , .btn-primary:focus {
      border: 1px solid #841851;
      color: white !important;
      background-color: #69053a; 
    }
    .chosen-container-multi .chosen-choices{
      border: 1px solid #841851;
      border-radius: 4px;
      margin-bottom: 9px;
    }
    #HallID {
      border: 1px solid #841851;
      border-radius: 4px;
      height: 36px;
    }
     
</style>
 <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2 style="    float: right;
    font-size: 22px; margin-bottom: 10px;">      <i class="fa fa-ticket" aria-hidden="true" style="    font-size: 22px; float: right;     margin-left: 5px;
    color: #841851;"></i>   
                        
                        حجوزات فرديه </h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"></a>
                      </li>
                     
                      <li><a class="close-link"></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                  <div class="Hall_data">
                    <form id="updateForm" action="/updateHallData" method="post">
                      {{ csrf_field() }}
                        <!-- <div class="pull-right" style="width: 20%;">
                          {!!Form::select('HallID',( $halls->toArray()) ,null,['class'=>'form-control ','id' => 'HallID','required'=>'required','placeholder'=>'اختر'])!!}
                        </div> -->
                        <!-- <div class="hall_info pull-right form" style="padding-right:10px;font-size: 18px;">
                          <input type="text" name="h_capacity" id="h_capacity">
                          <label style="font-size: 18px;">السعة</label>
                        </div> -->
                        <!-- <div class="hall_info pull-right form" style="padding-right:10px;font-size: 18px;">
                          <input type="text" name="h_price" id="h_price">
                          <label style="font-size: 18px;">السعر</label>
                        </div> -->
                        <!-- <button class="pull-right btn btn-primary" style="background-color: #841851;border: 1px solid #841851 ">تعديل</button> -->
                      </form>
                    </div>



 

   <a type="button"  class="btn btn-primary" data-toggle="modal" data-target="#myModal1"  style="margin-bottom: 20px; background-color: #841851;
    padding: 9px 22px;
    border: 1px solid #841851 " >  إضافة فرد جديد </a>

     <!-- model -->
<br>
 @if(Session::has('flash_message'))
    <div class="alert alert-danger">
        {{ Session::get('flash_message') }}<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    </div>
@endif
<div class="modal fade" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
     <div class="modal-content">
    
          <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                 <h4 class="modal-title span7 text-center" id="myModalLabel"><span class="title" style="    text-align: center;
    color: #841851;
    font-size: 20px;
    font-weight: bold;"> إضافة فرد جديد</span></h4>
          </div>
    <div class="modal-body">
        <div class="tabbable"> <!-- Only required for left/right tabs -->
     
        <div class="tab-content">
        <div class="tab-pane active" id="tab1">
              <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel"  style="    border: 0px solid #E6E9ED;  ">
                 
                  <div class="x_content">
                    <br />
                    {!! Form::open(['action' => "CoWorkingController@store",'method'=>'post','id'=>'profileForm','files' => 'true']) !!}
          
        <fieldset style="    padding: .35em .625em .75em; margin: 0 2px; border: 0px solid silver;" dir="rtl">
  

                      
                          <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
                        <!--   <input type="Number" class="form-control has-feedback-left" id ="Price" name="Price" placeholder="السعر">
                         -->
                           {!!Form::select('HallID',( $halls->toArray()) ,null,['class'=>'form-control ','id' => 'HallID','required'=>'required'])!!}
                        </div>
                     
                        <div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
                          <input type="text" class="form-control has-feedback-left" id ="Name" name="Name" placeholder="الاسم" style="border: 1px solid #841851;
    border-radius: 4px;
    height: 36px;">
                       
                        </div>





                      </div>
                      </fieldset>

          


                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-12  " style="    text-align: center;">
                          <button type="button" class="btn btn-danger " data-dismiss="modal">إغلاق</button>
                         <button class="btn btn-info"  type="reset">إعادة  إدخال</button>
                          <button type="submit"  name="login_form_submit" class="btn btn-success" value="Save" disabled="disabled">إضافة</button>
                        </div>
                      </div>

      {!!Form::close() !!}                
        </div>
                </div>
                   </div>
                </div>
        </div>
        </div>
        </div>

    </div>
 

         </div>
     </div>
  </div>
    <div style="padding-bottom: 10px;">
      <form action="/addCowrking" method="post" id="addToCowork">
        
       
        </form>
      </div>
<!-- ENDModal -->
  <div class="table-data">
    @include('CoWorking.cowrkPartial')
  </div>
 
    </div>  
  </div>
</div>            
  @section('other_scripts')
  <script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>



  <script type="text/javascript">

          
 

  $(document).ready(function(){
    $('#profileForm').formValidation({
            framework: 'bootstrap',
            excluded: ':disabled',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
               'Name': {
                    validators: {
                  

             notEmpty:{
                    message:"يجب ادخال الاسم"
                  }
                    
                  }
                },
                     'Price': {
                    validators: {
                     

             notEmpty:{
                    message:"يجب ادخال السعر"
                  }
                    
                       
                    }
                }
         
            }
        });
      var table= $('.users').DataTable({
        select:true,
        responsive: true,
        "order":[[0,"asc"]],
        'searchable':true,
        "scrollCollapse":true,
        "paging":true,
        "pagingType": "simple",
         dom: 'lBfrtip',
         buttons: [
            { extend: 'excel', className: 'btn btn-primary dtb' }
          ],

        fnRowCallback: function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
          $.fn.editable.defaults.send = "always";

          $.fn.editable.defaults.mode = 'inline';

          $('.testEdit').editable({

                validate: function(value) {
                  name=$(this).editable().data('name');
                  if(name=="name"||name=="username"||name=="email"||name=="role")
                  {
                    if($.trim(value) == '') {
                        return 'Value is required.';
                    }
                  }
                  if(name=="name"||name=="username")
                  {

                      var regexp = /^[a-zA-Z\u0600-\u06FF,-][\sa-zA-Z\u0600-\u06FF,-]+ {1}[a-zA-Z\u0600-\u06FF,-][\sa-zA-Z\u0600-\u06FF,-]/;            if (!regexp.test(value)) {
                            return 'This field is not valid';
                        }
                  }
                },
                placement: 'right',
                url:'{{URL::to("/")}}/fupdate',
                ajaxOptions: {
                  type: 'get',
                  sourceCache: 'false',
                  dataType: 'json'
                },
                params: function(params) {
                  // add additional params from data-attributes of trigger element
                  params.name = $(this).editable().data('name');

                  // console.log(params);
                  return params;
                },
                error: function(response, newValue) {
                  if(response.status === 500) 
                  {
                      return 'This Data Already Exist,Enter Correct Data.';
                  } 
                  else 
                  {
                      return response.responseText;
                  }
                }
            });
          }
        });
        $('.users tfoot th').each(function () {
        var title = $('.users thead th').eq($(this).index()).text();         
        if($(this).index()>=1 && $(this).index()<=14)
        {
            $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
        }
      });
      table.columns().every( function () {
        var that = this;
        $(this.footer()).find('input').on('keyup change', function ()
        {
            that.search(this.value).draw();
            if (that.search(this.value) ) {
              that.search(this.value).draw();
            }
        });
      });
      $('[data-toggle="popover"]').popover({ trigger: "hover" }); 
      $('[data-toggle="popover"]').popover({ trigger: "hover" }); 

 });
  </script>
  <script>
    var h_id;
   $(document).ready(function(){
    $('#HallID').on('change',function(){
      h_id = $(this).val();
      if(h_id)
      {
        $.ajax({
         url : '/getHall/'+h_id,
            type : 'GET',
            success : function(data) {   
              console.log(data);
                $('#h_price').val(data.Price);
                $('#h_capacity').val(data.Capacity);
            }
        });
         $.ajax({
         url : '/getTable/'+h_id,
            type : 'GET',
            success : function(data) {   
              console.log(data);
                $('.table-data').empty();
                $('.table-data').html(data);
            }
        });
      }
      else
      {
          $('#h_price').val("");
          $('#h_capacity').val(""); 
      }
    });
  });
  </script>
  <script>
    $('#updateForm').on('submit',function(event){
       event.preventDefault();
       event.stopImmediatePropagation();
       $.ajax({
            type: $('#updateForm').attr('method'),
            url: $('#updateForm').attr('action'),
            data: $('#updateForm').serialize(),
            success: function(data)
            {
                console.log(data);
                $('#h_price').val(data.Price);
                $('#h_capacity').val(data.Capacity);
            }
        });
    });
    $('#addToCowork').on('submit',function(event){
       event.preventDefault();
       event.stopImmediatePropagation();
       $.ajax({
            type: $('#addToCowork').attr('method'),
            url: $('#addToCowork').attr('action'),
            data: $('#addToCowork').serialize() + "&h_id=" + $('#HallID').val(),
            success: function(data)
            {
                console.log(data);
                 $('.table-data').empty();
                $('.table-data').html(data);
            }
        });
    })
  </script>
@endsection
@endsection
