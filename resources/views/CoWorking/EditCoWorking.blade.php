@extends('master')
@section('content')
 <section class="panel-body">
 <style type="text/css">
    #blah{
  background: url(assets/UserFiles/dist/img/33.jpg) no-repeat;
  background-size: cover;

}
</style>
 <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h1 dir="rtl"><i class='fa fa-tasks'></i>خروج الحجز</h1>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"></a>
                      </li>
                     
                      <li><a class="close-link"></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">


 

 <a href="/CoWorking"  ><span class="glyphicon glyphicon-circle-arrow-left" title="رجوع" style="font-size: 2em;
"></span></a>

  

  {!! Form::open(['action' => "CoWorkingController@UpdateCoWorking",'method'=>'post','id'=>'profileForm','files'=>'true']) !!}  
       <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <input type="hidden" id="id" name="id" value="{{ $CoWorkingdata['id'] }}">
               <div class="form-group">
                       
           
                    </div>
                    <div class="row" dir="rtl" style="padding-right: 60px">
                    <label>عدد الساعات</label>
                      <input type="number" name="NoClock" min=0 onchange="asd();" value="{{$diff_in_hours}}">
                      <label>*</label>
                      <input type="number" name="Price" onchange="asd();" min=0  value="{{$CoWorkingdata['Price']}}">
                     


                    </div>
                    <div class="row" dir="rtl" style="padding-right: 60px;padding-top:30px ">
                      <label>الاضافات   </label>
                      <input type="number" name="addtional" onchange="asd();" id="addtional" value="{{$CoWorkingdata['addtional']}}" onblur="" style="padding-right: 10px" min=0 >

                    </div>
                      <div class="row" dir="rtl" style="padding-right: 300px;padding-top:30px ">
                      <label>الاجمالي</label>
                      <label id="SumTotal">{{$totalClock}}</label>
                      

                    </div>
                    </div>
                  <div style="padding-top: 25px ;padding-bottom:50px">
              <button type="submit" class="btn btn-primary "style="float:right" name="login_form_submit" value="Save">خروج</button>
        </div>
  

      {!!Form::close() !!}

                  </div>
                </div>
              </div>
 
  @section('other_scripts')


<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>



  <script type="text/javascript">

          
 

  $(document).ready(function(){
    $('#profileForm').formValidation({
            framework: 'bootstrap',
            excluded: ':disabled',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
               'Name': {
                    validators: {
                  

             notEmpty:{
                    message:"يجب ادخال الاسم"
                  }
                    
                  }
                },
                     'Price': {
                    validators: {
                     

             notEmpty:{
                    message:"يجب ادخال السعر"
                  }
                    
                       
                    }
                }
         
            }
        });




     var table= $('.users').DataTable({
  
    select:true,
    responsive: true,
    "order":[[0,"asc"]],
    'searchable':true,
    "scrollCollapse":true,
    "paging":true,
    "pagingType": "simple",
      dom: 'lBfrtip',
        buttons: [
           
            
            { extend: 'excel', className: 'btn btn-primary dtb' }
            
            
        ],

fnRowCallback: function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
$.fn.editable.defaults.send = "always";

$.fn.editable.defaults.mode = 'inline';

$('.testEdit').editable({

      validate: function(value) {
        name=$(this).editable().data('name');
        if(name=="name"||name=="username"||name=="email"||name=="role")
        {
                if($.trim(value) == '') {
                    return 'Value is required.';
                  }
                    }
        if(name=="name"||name=="username")
        {

      var regexp = /^[a-zA-Z\u0600-\u06FF,-][\sa-zA-Z\u0600-\u06FF,-]+ {1}[a-zA-Z\u0600-\u06FF,-][\sa-zA-Z\u0600-\u06FF,-]/;            if (!regexp.test(value)) {
            return 'This field is not valid';
        }
        
        }
        },

    placement: 'right',
    url:'{{URL::to("/")}}/fupdate',
  
     ajaxOptions: {
     type: 'get',
    sourceCache: 'false',
     dataType: 'json'

   },

       params: function(params) {
            // add additional params from data-attributes of trigger element
            params.name = $(this).editable().data('name');

            // console.log(params);
            return params;
        },
        error: function(response, newValue) {
            if(response.status === 500) {
                return 'This Data Already Exist,Enter Correct Data.';
            } else {
                return response.responseText;
            }
        }


});

}
});

 



   $('.users tfoot th').each(function () {



      var title = $('.users thead th').eq($(this).index()).text();
               
 if($(this).index()>=1 && $(this).index()<=14)
        {

           $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
}
        

    });
  table.columns().every( function () {
  var that = this;
 $(this.footer()).find('input').on('keyup change', function () {
          that.search(this.value).draw();
            if (that.search(this.value) ) {
               that.search(this.value).draw();
           }
        });
      
 
   });


    $('[data-toggle="popover"]').popover({ trigger: "hover" }); 

 

    $('[data-toggle="popover"]').popover({ trigger: "hover" }); 
 });
  </script>
  <script type="text/javascript">
function asd()
{
 
var c = $('input[name=Price]').val();
 var M = $('input[name=NoClock]').val();
var b = parseInt(M) * parseInt(c);



  var a = $('input[name=addtional]').val();

      var total = parseInt(a) + parseInt(b);
     
       
   document.getElementById('SumTotal').innerHTML = total;
}
  
    
 

</script>
@endsection
@endsection