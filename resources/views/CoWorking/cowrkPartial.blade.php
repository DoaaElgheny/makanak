<table class="table table-hover table-bordered  dt-responsive nowrap  users" cellspacing="0" width="100%" dir="rtl">
  <thead>
    <th style="   width: 30px;text-align: center;color: #841851;font-size: 16px;">العدد
    </th>
    <th style="   width: 116px;text-align: center;color: #841851;font-size: 16px;"> الإسم 
    </th>
    <th style="   width: 50px;text-align: center;color: #841851;font-size: 16px;"> الدخول 
    </th>
    <th style="   width: 50px;text-align: center;color: #841851;font-size: 16px;"> خروج 
    </th>
    <th style="   width: 50px;text-align: center;color: #841851;font-size: 16px;"> الساعات 
    </th>
   
    <!-- <th style="   width: 50px;text-align: center;color: #841851;font-size: 16px;"> التكلفة 
    </th> -->
    <th style="   width: 50px;text-align: center;color: #841851;font-size: 16px;"> الإضافات 
    </th>
    <th style="   width: 50px;text-align: center;color: #841851;font-size: 16px;"> الإجمالى 
    </th>
    <th style="   width: 50px;text-align: center;color: #841851;font-size: 16px;"> المدفوع 
    </th>
    <th style="   width: 50px;text-align: center;color: #841851;font-size: 16px;">  
    </th>

  </tfoot>
  <tbody style="text-align:center;">
    <?php $i=1;?>
      @foreach($CoWorking as $Value)
      @if($Value->TimeOut==null)
        <tr>
        @else
        <tr style="background-color:#ccc">
        @endif
          <td>{{$i++}} </td> 
          <td class="testEdit" data-type="select"  data-value="{{$Value->Name}}" data-source ="{{$Value}}" data-name="government_id"  data-pk="{{$Value->id}}">{{$Value->Name}}</td>
          <td >{{$Value->TimeIN}}</td>
          <td >{{$Value->TimeOut}}</td>
          <td>{{$Value->calculateAccurateTime()}}</td>
          <!-- <td>{{$Value->calculated_time}}</td> -->
          <!-- <td>{{$Value->Price}}</td> -->
          <td>{{$Value->addtional}}</td>
          <td>{{$Value->total_price}}</td>
          <td>{{$Value->paid_price}}</td>
          <td style="width:20px">
          @if($Value->TimeOut==null)
          <a href="/LogOut/{{$Value->id}}" class="btn btn-default btn-sm" >
          <span class="fa fa-sign-out pull-right"></span>
            </a>
          @endif
            
              <a href="/CoWorking/destroy/{{$Value->id}}" class="btn btn-default btn-sm" ><span class="glyphicon glyphicon-trash"></span>  </a>
          </td>
        </tr>
      @endforeach
  </tbody>
</table>                  


