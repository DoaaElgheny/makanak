@extends('master')
@section('content')
<script src="http://code.jquery.com/jquery-1.11.1.min.js"></script>
<meta name="viewport" content="initial-scale=1.0">
<meta charset="utf-8">

<style>
  #blah{
  background: url(assets/img/noimage.jpg) no-repeat;
  background-size: cover;
  height: 180px;
  width: 180px;
}
    .the-fieldset {
        direction: rtl;
    }
.wizard-card .picture {
    width: 195px;
    height: 195px;
    background-color: #999999;
    border: 4px solid #CCCCCC;
    color: #FFFFFF;
   border-radius: 0;
    margin-right: 0px;
    margin-top: 60px;
    overflow: hidden;
    transition: all 0.2s;
    -webkit-transition: all 0.2s;
}
  
    p {
         font-size: 16px;
    font-weight: bold;
    } 
    .the-fieldset {
            margin-bottom: 20px;
    }
    .form-check-label {
        color: #841851;
            margin-bottom: 14px;
    }
     /* Always set the map height explicitly to define the size of the div
       * element that contains the map. */
 #map {
        height: 100%;
      }
      /* Optional: Makes the sample page fill the window. */
      html, body {
        height: 100%;
        margin: 0;
        padding: 0;
      }
</style>
    <title> الاماكن</title>
   
      <!-- GMAPAPI==>AIzaSyBqLrOdeB2oF_wbetq9K-g9ssrfiWv5wbk -->




<div class="col-md-12 ">
{!! Form::open(['id'=>'myForm','class'=>'form-horizontal','method'=>'POST','action'=>['EventsController@store'],'files' => true]) !!}
<fieldset class="the-fieldset">

<legend class="the-legend">

بيانات الإيفنت 
</legend>

<div class="col-sm-5 ">

<div class="col-sm-12 col-sm-offset-1 wizard-card">
<div class="picture-container" >
<div class="picture" id="blah">
<input id="file-upload"  name="file_picture" type="file"/>

</div> 
</div>
</div>     
</div> 


<div class="col-sm-7 ">

<div class="col-sm-12 ">
<p>اسم الإيفنت</p>
<input type="text" name="event_name" id="event_name" data-rule="required"   style="border: 1px #841851 solid;
width: 290px;"/>
<div class="validation"></div>
</div>


<div class="col-sm-12 ">

<div class="form-group">
{!! Form::label('event_type_id', 'نوع الإيفنت :') !!}
{!! Form::select('event_type_id', $event_types->pluck('name','id'), null, ['class' => 'form-control select2','placeholder'=>'حدد النوع','style'=>"border: 1px #841851 solid;width: 290px;"]) !!}
</div>
</div>       




<div class="col-sm-12">
<p>المستضيف </p>

{!! Form::select('place_id', $places->pluck('Name','id'), null, ['id'=>'slectP','class' => 'form-control select2','placeholder'=>'حدد النوع','style'=>"border: 1px #841851 solid;width: 290px;"]) !!}
<div class="validation"></div>
</div>                    


<div class="col-sm-12 ">
<p>عن الإيفنت</p>

<textarea name="description" rows="14" data-rule="required"  style="border: 1px #841851 solid;
width: 290px; 
height: 100px;"></textarea>
<div class="validation"></div>
</div>



</div>   


</fieldset>


<fieldset class="the-fieldset">
<legend class="the-legend"> 

تحديد العنوان</legend>


<div class="col-sm-6 ">

<div class="col-sm-12 ">
<p>حدد مكانك </p> 
<div id="map" style="width: 500px; height: 400px;"></div>
</div>                       
</div> 
<div class="col-sm-6 ">

<div class="col-sm-12 ">

<div class="form-group">
{!! Form::label('government_id', 'المحافظة :') !!}
{!! Form::select('government_id', $governments->pluck('Name','id'), null, ['class' => 'form-control select2','placeholder'=>'حدد المحافظة','style'=>"border: 1px #841851 solid;width: 290px;",'id'=>'gov_id']) !!}
</div>
</div>       



<div class="col-sm-12 ">
<p>المركز </p>

<select class="form-control" name="region" id="region_id" style="border: 1px #841851 solid;
width: 290px;">
<option></option>
</select>
<div class="validation"></div>
</div>

<div class="col-sm-12 ">
<p>العنوان </p>

<input type="text" name="address" id="address" data-rule="required"    style="border: 1px #841851 solid;
width: 290px;"/>
<div class="validation"></div>
</div>

</div>   
<div class="col-sm-12 ">

<div class="col-sm-6 ">
<input hidden="hidden"  type="number" step="0.0001" name="lat" id="lat_text" value=0 data-rule="required"   style="border: 1px #841851 solid;
width: 290px;"/>
</div>
<div class="col-sm-6 ">
<input hidden="hidden" type="number" step="0.0001" name="long" id="long_text" value=0  data-rule="required"    style="border: 1px #841851 solid;
width: 290px;"/>
</div>
<div class="validation"></div>
</div>
</div>   
</fieldset>
<fieldset class="the-fieldset">
<legend class="the-legend">  تفاصيل الإيفنت</legend>


<div class="col-sm-6">

<div class="col-sm-12 ">
<p> التاريخ من </p> 

<input style="      border:  1px solid #841851;
border-radius: 4px;
box-shadow: 0 4px 20px rgba(0,0,0,0.19);height: 31px;  width: 207px;    margin-right: -70px;" id="date_from" type="datetime-local" required name = "date_from" dir = "rtl">

<div class="validation"></div>
</div>
<div class="col-sm-12 ">
<p> التاريخ إلى </p> 

<input class="form-control Todate"  type="text"   style="   border:  1px solid #841851;
border-radius: 4px;
box-shadow: 0 4px 20px rgba(0,0,0,0.19);height: 31px;  width: 207px;    margin-right: -70px;" name="date_to" id="date_to"  />

              

<div class="validation"></div>

<div class="col-md-4">
            <div style="background-color:white;width:100%;height:45px;" class="trigger1">
              <div style="background-color:#cdd6e0;width:5%;height:100%;float:left;">&nbsp;&nbsp;</div>
              <div style="width:20%;height:100%;float:left;">
               <center> <i  class="glyphicon glyphicon-calendar" style="margin-top: 15px;
    color: #841851;"></i></center> 
              </div>

               <div style="width:75%;height:100%;float:left;    padding-right: 20px;" dir="rtl">
               
                <br/> 
               <input class="form-control Fromdate"  type="text" value="{{$dateNow}}" style="height:30px;     background-color:white;     margin-top: -15px;font-size: 16px;     border: 0px;" >       
              </div>
            
            </div>
          </div>

</div>  



</div>

<div class="col-sm-6 ">

<div class="col-sm-12 pull-right ">
<p>السعة </p>

<input type="number" name="capacity" id="capacity" data-rule="required"   style="border: 1px #841851 solid;
width: 290px;"/>
<div class="validation"></div>
</div>
<div class="col-sm-12 pull-right ">
<p>رابط الايفنت </p>

<input type="text" name="fb_link" id="fb_link" data-rule="required"    style="border: 1px #841851 solid;
width: 290px;"/>
<div class="validation"></div>
</div>




<div class="col-sm-12 "> 
<p> الحضور </p>
<div class=" col-sm-6 form-check form-check-inline ">
<label class="form-check-label">
<input class="form-check-input" type="radio" name="attendance" id="inlineRadio1" value="0"> مجانى
</label>
</div>
<div class=" col-sm-6 form-check form-check-inline">
<label class="form-check-label">
<input class="form-check-input" type="radio" name="attendance" id="inlineRadio2" value="1"> تكلفة
</label>
</div>
<div class="col-sm-12 " id="price_div" style="visibility: hidden;"> 
<p> السعر </p>
<input type="text" name="price" id="price" value="0"  data-rule="required"   style="border: 1px #841851 solid;
width: 290px;"/>
</div>

</div>
<div class="col-sm-12 "> 
<p> الإشتراك </p>
<div class=" col-sm-4 form-check form-check-inline">
<label class="form-check-label">
<input class="form-check-input" type="radio" name="subscribe" id="inlineRadio1" value="0"> مفتوح
</label>
</div>
<div class=" col-sm-4 form-check form-check-inline">
<label class="form-check-label">
<input class="form-check-input" type="radio" name="subscribe" id="inlineRadio2" value="1"> بالتسجيل
</label>
</div>
<div class=" col-sm-4 form-check form-check-inline">
<label class="form-check-label">
<input class="form-check-input" type="radio" name="subscribe" id="inlineRadio1" value="2"> بالدفع
</label>
</div>
</div>
<div class="col-sm-12 ">

<div class="form-group select2-bootstrap-append">
{!! Form::label('target_id', 'المستهدفين :') !!}

{!! Form::select('target_id[]', $targets->pluck('name','id'), null, ['id'=>'multi-append','class'=>'form-control select2 input-bg','multiple','style'=>'width:300px']);
!!}
<span class="input-group-btn">
<button class="btn btn-default" type="button" data-select2-open="multi-append">
<span class="glyphicon glyphicon-search"></span>
</button>
</span>
{{--{!! Form::select('target_id', $targets->pluck('name','id'), null, ['class' => 'form-control select2','placeholder'=>'حدد المحافظة','style'=>"border: 1px #841851 solid;width: 290px;",'id'=>'gov_id','multiple']) !!}--}}
</div>
</div>               






</div>

{{--<div class="form-group{{ $errors->has('logo') ? ' has-error' : '' }}">
<label for="logo" class="col-md-4 control-label">logo</label>
<div  class="col-md-6">
<input id="logo" type="file" class="form-control" name="logo">

@if ($errors->has('logo'))
<span class="help-block">
<strong>{{ $errors->first('logo') }}</strong>
</span>
@endif
</div>
</div>--}}
<button>Submit</button>        
</fieldset>
{{form::close() }}
</div>



<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>

<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="https://code.jquery.com/ui/1.10.4/jquery-ui.min.js"></script>
    <script type="text/javascript" src="/assets/UserFiles/js/bootstrap-datetimepicker.js" charset="UTF-8"></script>
    <script type="text/javascript" src="/assets/UserFiles/js/locales/bootstrap-datetimepicker.fr.js" charset="UTF-8"></script>
     <link rel="stylesheet" href="/assets/UserFiles/css/bootstrap-datetimepicker.min.css">

  <script >

var editor;

  $(document).ready(function(){
         //change imgage    
$("#file-upload").on("change", function()
    {
    
        var files = !!this.files ? this.files : [];

        if (!files.length || !window.FileReader) 
        {
           $("#blah").css("background-image",'url(assets/img/noimage.jpg)');
        } // no file selected, or no FileReader support
 
        else if (/^image/.test( files[0].type)){ // only image file
            var reader = new FileReader(); // instance of the FileReader
            reader.readAsDataURL(files[0]); // read the local file
 
            reader.onloadend = function(){ // set image data as background of div
                $("#blah").css("background-image", "url("+this.result+")");
            }
        }
    });
/////////////////////


  });
    
  </script>
  <script>
    
  </script>

  <script>
      var map;
      var marker;
      var myLatLng= {lat: 27.180134, lng: 31.189283};
      function initMap() {
        map = new google.maps.Map(document.getElementById('map'), {
        zoom: 8,
        center:myLatLng
        });
              map.addListener('click', function(e) {
            placeMarkerAndPanTo(e.latLng, map);
          });
        

        function placeMarkerAndPanTo(latLng, map) {
          if(marker != null)
               marker.setMap(null);
           marker = new google.maps.Marker({
            position: latLng,
            map: map
          });
          marker.setMap(map);
          map.panTo(latLng);
          console.log("lat: " + marker.getPosition().lat() + " long "+marker.getPosition().lng());
          $('#lat_text').val(marker.getPosition().lat().toFixed(5));
          $('#long_text').val(marker.getPosition().lng().toFixed(5));
        }
      }
      // var marker = new google.maps.Marker({
      //     position: myLatLng,
      //     map: map,
      //     draggable: true,
      //     animation: google.maps.Animation.DROP,
      //     position: myLatLng,
      //     title: 'Hello World!'
      //   });
      //    marker.addListener('click', toggleBounce);
      //    function toggleBounce() {

      //           console.log(myLatLng['lat']);
      //           if (marker.getAnimation() !== null) {
      //             marker.setAnimation(null);
      //           } else {
      //             marker.setAnimation(google.maps.Animation.BOUNCE);
      //           }
      // //       }
      //   }
      $('#gov_id').on('change',function(){
      var id = $(this).val();
      $.ajax({

        url : '/getDists/'+id,
        type : 'GET',
        success : function(data) {   
        var $select = $('#region_id'); 

        $select.find('option').remove();
        data.forEach(function(element)
        {
            console.log(element.id);
            $select.append('<option value='+element.id +'>'+element.Name+'</option>')
          });
        },
        error : function(request,error)
        {
            alert("Request: "+JSON.stringify(request));
        }
      });
      $.ajax({
        url : '/getLocation/'+id,
        type : 'GET',
        success : function(data) {
        myLatLng= {lat: data.lat, lng: data.long};
        map.setCenter({lat: parseFloat(data.lat), lng: parseFloat(data.long)}); 
        map.setZoom(10);
        console.log(myLatLng);
        $('#lat_text').val(data.lat.toFixed(5));
        $('#long_text').val(data.long.toFixed(5));
        },
        error : function(request,error)
        {
            alert("Request: "+JSON.stringify(request));
        }
      });
    });
    $(document).ready(function() {
      var el = document.getElementById("price_div");
    $('input[type=radio][name=attendance]').change(function() {
        if (this.value == '1') {            
            el.style.visibility="visible" ;
        }
        else if (this.value == '0') {
            el.style.visibility="hidden";
        }
      });
    });
     $('#region_id').on('change',function(){
      var id = $(this).val();
      $.ajax({
        url : '/getLocationRegion/'+id,
        type : 'GET',
        success : function(data) {
        myLatLng= {lat: data.lat, lng: data.long};
        map.setCenter({lat: parseFloat(data.lat), lng: parseFloat(data.long)}); 
        map.setZoom(10);
        $('#lat_text').val(data.lat.toFixed(5));
        $('#long_text').val(data.long.toFixed(5));
        console.log(myLatLng);
        },
        error : function(request,error)
        {
            alert("Request: "+JSON.stringify(request));
        }
      });
    });
    
  </script>
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBqLrOdeB2oF_wbetq9K-g9ssrfiWv5wbk&callback=initMap"
  async defer>
    
  </script>
@stop
