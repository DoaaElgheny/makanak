@extends('master')
@section('content')
 <section class="panel-body">

 <style type="text/css">

   table.dataTable.row-border tbody tr th, table.dataTable.row-border tbody tr td, table.dataTable.display tbody tr th, table.dataTable.display tbody tr td  {
    

           text-align: right;
   }
     div.dt-buttons .dt-button {
    margin-right: 0;
    background-color: #841851;
    font-size: 16px;
    padding-left: 30px;
    padding-right: 30px;
    border: 1px solid #841851;
    margin-bottom: 20px;
             height: 35px;
}
       div.dt-buttons .dt-button:hover , .dt-button:focus {
    
   border: 1px solid #841851;
    color: white !important;
    background-color: #69053a; }
 

           div.dataTables_wrapper div.dataTables_filter label {
   color: #841851;
    font-size: 17px;
    direction: rtl;
}
     div.dataTables_wrapper div.dataTables_filter input {
    width: 170px;
    margin-right: 7px;
           height: 33px;
}
     div.dataTables_wrapper div.dataTables_length label {
  
    color: #841851;
    font-size: 17px;
}
     div.dataTables_wrapper div.dataTables_length select {
    width: 170px;
    display: inline-block;
    text-align: right;
    direction: rtl;
}
     select.input-sm {
    height: 33px;
    line-height: 30px; }
     .btn-primary:hover , .btn-primary:focus {
         border: 1px solid #841851;
    color: white !important;
    background-color: #69053a; 
     }
     .chosen-container-multi .chosen-choices{
          border: 1px solid #841851;
    border-radius: 4px;
    margin-bottom: 9px;
     }
</style> 

         
                  <div class="x_title" >
                 <h2 style="    float: right;
    font-size: 22px; margin-bottom: 10px;">      <i  class="fa fa-university" aria-hidden="true" style="    font-size: 22px; float: right;     margin-left: 5px;
    color: #841851;"></i>   
                        
                 الايفنتات </h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"></a>
                      </li>
                     
                      <li><a class="close-link"></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
  <div class="col-md-12 col-ms-12 col-xs-12" style="padding-bottom: 30px;padding-top:5px ">                
<a href="{{ url('/InsertEvents') }}" style="margin-bottom: 20px; background-color: #841851;padding: 9px 15px;border: 1px solid #841851;color: white " > اضافة ايفنت جديد </a>
</div>
 <table class="table table-hover table-bordered  dt-responsive nowrap display purchase" cellspacing="0" width="100%" dir="rtl"  >
  <thead>
 <th style="width: 30px;text-align: center;color: #841851;font-size: 16px;">العدد</th>

<th style="   width: 116px;
   
    text-align: center;
      color: #841851;
    
    font-size: 16px;"> الإسم </th>
<th style="   width: 30px;
   
    text-align: center;
   color: #841851;
  
    font-size: 16px;"> السعر </th>

 <th style="   width: 116px;
   
    text-align: center;
     color: #841851;
    font-weight: bold;
    font-size: 17px;">نوع القاعة</th>
<th style="   width: 116px;
   
    text-align: center;
     color: #841851;
    
    font-size: 16px;">الإمكانيات</th>
<!-- <th>معرض الصور</th> -->
<th style="   width: 30px;
  
    text-align: center;
      color: #841851;
 
    font-size: 16px;">تعديل</th>
<th style="   width: 30px;
   
    text-align: center;
    color: #841851;
    
    font-size: 16px;">حذف</th>         
  </thead>
</tfoot>


<tbody style="text-align:center;">
<?php $i=1;?>
  @foreach($events as $event)
<tr>
 <td >{{$i++}} </td>
<td style="width:75px">{{$event->name}}</td>
<td style="width:20px ">{{$event->Price or ''}}</td>



     <td >
     @if($event->id !=Null)
<ul style="text-align: right; list-style-type: NONE;">
@foreach($event->getTargets() as $Value)
<li>{{$Value->target->name or ''}}</li>
@endforeach
</ul>
@endif
</td>

 <td >
 @if($event->id !=Null)
<ul style="text-align: right;">
<li>{{$event->type->name}}</li>
</ul>
@endif
</td>

 

{{--<td style="width:20px "><a href="/showgallary/{{$hal->id}}" class="btn btn-default btn-sm" ><span class="glyphicon glyphicon-plus"></span>  </a></td>--}}


<td><a href="/editEvent/{{$event->id}}" class="btn btn-default btn-sm" ><span class="glyphicon glyphicon-edit"></span>  </a></td>

<td><a href="/destroyEvent/{{$event->id}}" class="btn btn-default btn-sm" ><span class="glyphicon glyphicon-trash"></span>  </a></td>
</tr>
        @endforeach
</tbody>
</table>


                
 
  @section('other_scripts')


<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>



  <script type="text/javascript">

var editor;

  $(document).ready(function(){


     var table= $('.purchase').DataTable({
  
    select:true,
    responsive: true,
    "order":[[0,"asc"]],
    'searchable':true,
    "scrollCollapse":true,
    "paging":true,
    "pagingType": "simple",
      dom: 'lBfrtip',
        buttons: [
           
            
            { extend: 'excel', className: 'btn btn-primary dtb' }
            
            
        ],

fnRowCallback: function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
$.fn.editable.defaults.send = "always";

$.fn.editable.defaults.mode = 'inline';

$('.testEdit').editable({

      validate: function(value) {
        name=$(this).editable().data('name');
        if(name=="PurchesNo"||name=="Date"||name=="location")
        {
                if($.trim(value) == '') {
                    return 'Value is required.';
                  }
                    }
      
       
         if(name=="Date")
        {

       var regexp = /^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/;             
      if (!regexp.test(value)) {
            return 'This field is not valid';
        }
        
        }
    },
       
    placement: 'right',
    url:'{{URL::to("/")}}/updatePurches',
  
     ajaxOptions: {
     type: 'get',
    sourceCache: 'false',
     dataType: 'json'

   },

       params: function(params) {
            // add additional params from data-attributes of trigger element
            params.name = $(this).editable().data('name');

            // console.log(params);
            return params;
        },
        error: function(response, newValue) {
            if(response.status === 500) {
                return 'This Data Already Exist,Enter Correct Data.';
            } else {
                return response.responseText;
            }
        }
        ,
        success:function(response)
        {
            if(response.status==="You do not have permission.")
             {
              return 'You do not have permission.';
              }
        }


});

}
});

 



   $('.purchase tfoot th').each(function () {



      var title = $('.purchase thead th').eq($(this).index()).text();
               
 if($(this).index()>=1 && $(this).index()<=6)
        {

           $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
}
        

    });
  table.columns().every( function () {
  var that = this;
 $(this.footer()).find('input').on('keyup change', function () {
          that.search(this.value).draw();
            if (that.search(this.value) ) {
               that.search(this.value).draw();
           }
        });
      
    });
$('#warehouseaction').on('change',function(){
 $('#quantity').text('');
  // document.getElementById('quantity').innerHTML="";  
});
   
 });
// $('#activityitems').change(function(){
//   if ($(this).val()==0)
//   {
//   $("#Descr").removeAttr('disabled');
//   $("#Descr").attr("required","required");
//   }
//   else
//   {
//      $("#Descr").attr("disabled","disabled");
//   }
 
 
//     });
  </script>

@stop

@stop
