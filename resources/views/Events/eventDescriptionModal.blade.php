<div class="modal-dialog " id="modalDescription">
        <!-- Modal content no 1-->
        <div class="modal-content" style="width: 700px; margin-right: -40px;">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title text-center form-title">{{$event->name}} </h4>
          </div>
          <div class="modal-body padtrbl " style="padding-bottom: 430px; padding-top: 60px;">
            <div class="row col-md-12">
              <div class="col-md-6 col-sm-6" style="margin-top: -35px;">
                <div class="description" style="border: 1px solid #ddd;">
                  <h3 style=" text-align: center;color: #841851;font-size: 19px;"> تفاصيل </h3>
                  <p style="margin-right: 3px;"> 
                    {{$event->description}}
                  </p>
                </div>
              </div>
              <div class="col-md-6 col-sm-6 " style="margin-top: -25px;">
               <div class="description" style="border: 1px solid #ddd;">
                  <h3 style="text-align: center;color: #841851;font-size: 19px;"> تفاصيل إضافية</h3>
                  <p style="margin-right: 3px;"> 
                     السعر: {{$event->price}} جنيهاً
                  </p>    
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>