@php
  $key=1;
@endphp
@foreach($events as $key=>$event)
<div class="col-md-5 portfolio1 ">
<div class="portfolio-item">
    <div class="item-description col-md-6">
        <div class="row">
            <div class="">
                <span class="item-name title">       
                  {{$event->name}} 
                 </span>
                 <span class="item-name">
                    <i class="fa fa-dot-circle-o" aria-hidden="true"></i>
                       {{$event->type->name}}
                 </span>
                 <span class="item-name">
                    <i class="fa fa-money" aria-hidden="true"></i>
                     {{$event->attendance_type? "تكلفة" : "مجاني"}} 
                 </span>
                 <span class="item-name">
                      <i class="fa fa-map-marker" aria-hidden="true"></i>
                     أسيوط
                  </span>
              
              </div>
              <div class="col-md-12 tica2" >
                 <span class="col-md-12">
                 <i class="fa fa-calendar" aria-hidden="true"></i>
                     {{Carbon\Carbon::parse($event->date_from)->format('d-m')}}
                 </span> 
                 <span class=" col-md-12">
                 <i class="fa fa-clock-o" aria-hidden="true"></i>
                     {{Carbon\Carbon::parse($event->date_from)->format('h:i')}}
                 </span>   
                
              </div>
          </div>
    </div> 
    <div class="item-image col-md-6">
        <a href="#">
            <img src="{{'/'.$event->cover_photo}}" class="img-responsive center-block" alt="portfolio1"> 
        </a>
    </div>
    <div class="col-md-12 text-center" data-target="#lami" data-toggle="modal">
    
        <button class="andmore1" eventId = {{$event->id}}> اعرف المزيد </button>
    </div>
    </div> 
</div>
@if($key % 2 ==0)
<div class="col-md-2"></div>
@endif
@endforeach

@if ($events->lastPage() > 1)
<div class="clear text-center">
<ul class="pagination">
    <li class="{{ ($events->currentPage() == 1) ? ' disabled' : '' }}">
        <a href="{{ $events->url(1) }}">السابق</a>
    </li>
    @for ($i = 1; $i <= $events->lastPage(); $i++)
        <li class="{{ ($events->currentPage() == $i) ? ' active' : '' }}">
            <a href="{{ $events->url($i) }}">{{ $i }}</a>
        </li>
    @endfor
    <li class="{{ ($events->currentPage() == $events->lastPage()) ? ' disabled' : '' }}">
        <a href="{{ $events->url($events->currentPage()+1) }}" >التالي</a>
    </li>
</ul>
</div>
@endif