@extends('master')
@section('content')
<meta name="viewport" content="initial-scale=1.0">
<meta charset="utf-8">
<link rel="stylesheet" href="/assets/css/jquery.datetimepicker.css">
<script type="text/javascript" src="/assets/js/jquery.datetimepicker.full.min.js"></script>
<style>
.wizard-card .picture {
    width: 200px;
    height: 85px;
    background-color: #999999;
    border: 4px solid #CCCCCC;
    color: #FFFFFF;
   border-radius: 0;
   
}
/* Always set the map height explicitly to define the size of the div
       * element that contains the map. */
      #map {
        height: 100%;
      }
      /* Optional: Makes the sample page fill the window. */
      html, body {
        height: 100%;
        margin: 0;
        padding: 0;
      }
</style>
<form action="/update/event/{{$event->id}}" method="post" enctype="multipart/form-data">
{{ csrf_field() }}
<div class="pull-right clear">
{{ Form::text('name', $event->name) }}
{{ Form::label('name','اسم اليفنت') }}
</div>
<div class="pull-right clear">
{!! Form::select('event_type_id', $event_types->pluck('name','id'), $event->type->id, ['placeholder'=>'حدد النوع']) !!}
{{ Form::label('event_type','نوع اليفنت') }}

</div>
<div class="pull-right clear">
{{ Form::text('organizer_id', $event->place->Name,['disabled']) }}
{{ Form::label('organizer_id','المستضيف') }}
</div>
{{--{{ Form::image($event->cover_photo, 'cover_photo',['class' => 'btn','width' => 200 , 'height' => 100]) }}
--}}
<div class="wizard-card pull-right clear">
<span class="pull-right">
 {{ Form::label('file_picture','الغلاف') }}
 </span>
	 <div class="picture-container">
	      <div class="picture">
	          <img src="{{'/'.$event->cover_photo}}" class="picture-src" id="wizardPicturePreview" title=""/>
	          <input name="file_picture" type="file" id="wizard-picture">
	      </div>         
	  </div>
 </div> 

 <div class="pull-right clear">
 	{!! Form::select('government_id', $governments->pluck('Name','id'), $event->region->GetGovernment->id, ['id'=>'gov_id']) !!}
	{{ Form::label('region_id','المحافظة') }}
 </div>
 <div class="pull-right clear">
 	<select class="form-control" name="region" id="region_id" style="border: 1px #841851 solid;
          width: 290px;">
          <option value="{{$event->region->GetGovernment->id}}">{{$event->region->GetGovernment->Name}}</option>
          </select>
	{{ Form::label('region_id','المركز') }}
 </div>
 <div class="pull-right clear">
 	{{ Form::textarea('description', $event->description) }}
	{{ Form::label('description','عن الايفينت') }}
 </div>
 
 <p class="pull-right clear">الحضور</p>
 <div class="pull-right">           	
 	<div class=" col-sm-6 form-check form-check-inline ">
  		<label class="form-check-label">
    		<input class="form-check-input" type="radio" {{$event->attendance_type == 0 ? 'checked' :" "}} name="attendance" id="inlineRadio1" value="0"> مجانى
  		</label>
	</div>
	<div class=" col-sm-6 form-check form-check-inline">
  		<label class="form-check-label">
    		<input class="form-check-input" type="radio" {{$event->attendance_type == 1 ? 'checked' :" "}} name="attendance" id="inlineRadio2" value="1"> تكلفة
  		</label>
	</div>
</div>
<div class="col-sm-12 " id="price_div" style="visibility: hidden;"> 
                            <p> السعر </p>
        <input type="text" name="price" id="price" value="0"  data-rule="required" data-msg="Vérifiez votre saisie sur les champs : Le champ 'Société' doit être renseigné."   style="border: 1px #841851 solid;
    width: 290px;"/>
</div>  

 <p class="pull-right clear">الاشتراك</p>
 <div class="pull-right">           	
        	<div class=" col-sm-4 form-check form-check-inline">
  				<label class="form-check-label">
    				<input class="form-check-input" type="radio" name="subscribe" id="inlineRadio1" {{$event->subscribe_type == 0 ? 'checked':" "}} value="0"> مفتوح
  				</label>
			</div>
			<div class=" col-sm-6 form-check form-check-inline">
	  			<label class="form-check-label">
	  		  		<input class="form-check-input" type="radio" name="subscribe" id="inlineRadio2" {{$event->subscribe_type == 1 ? 'checked':" "}} value="1"> بالتسجيل
	  			</label>
			</div>
            <div class=" col-sm-4 form-check form-check-inline">
  				<label class="form-check-label">
    				<input class="form-check-input" type="radio" name="subscribe" id="inlineRadio1" {{$event->subscribe_type == 2 ? 'checked' :" "}} value="2"> بالدفع
  				</label>
			</div>
			
 </div>
 @if($event->subscribe_type == 2)
 <div class="pull-right clear">
 	{{ Form::text('attendance', $event->price) }}
	{{ Form::label('attendance','السعر') }}
 </div>
  @endif
 <div class="pull-right clear">
 	{{ Form::text('address', $event->address) }}
	{{ Form::label('address','العنوان') }}
 </div>
  
 <div class="pull-right clear">
 	{{ Form::number('capacity', $event->capacity) }}
	{{ Form::label('capacity','السعة') }}
 </div>
 <div class="pull-right clear">
 	{{ Form::text('fb_link', $event->fb_link) }}
	{{ Form::label('fb_link','رابط FB') }}
 </div>
 <div class="pull-right clear">
 	{{ Form::date('date_from', \Carbon\Carbon::parse($event->date_from)) }}
	{{ Form::label('date_from','التاريخ من') }}
 </div>
 <div class="pull-right clear">
 	{{ Form::date('date_to', \Carbon\Carbon::parse($event->date_to)) }}
	{{ Form::label('date_to','التاريخ إلي') }}
 </div>
 <div class="pull-right clear">
 	{!! Form::hidden('isActive', 0) !!}
    {!! Form::checkbox('isActive', 1, $event->isActive == 1) !!}
	{{ Form::label('isActive','مفعل') }}
 </div>
 



  <div class="pull-right clear">
	<select multiple name="target_id[]">
		@foreach($targets as  $target)
		<option value="{{$target->id}}" {{in_array($target->id, $target_items->pluck('id')->toArray(), true) ? 'selected' : ' ' }}>{{$target->name}}</option>
		@endforeach
	</select>
	 {{ Form::label('target_id','المستهدفين') }}
  </div>
  <div class="clear padding">
  	<div id="map" style="width: 600px; height: 400px;"></div>
  </div>

<div class="pull-right clear ">
	        خط الطول<input  type="number" step="0.0001" name="lat" id="lat_text" value={{$event->lat}} data-rule="required" data-msg="Vérifiez votre saisie sur les champs : Le champ 'Société' doit être renseigné."   style="border: 1px #841851 solid;
	    	width: 290px;"/>
        	خط العرض<input  type="number" step="0.0001" name="long" id="long_text" value={{$event->lat}}  data-rule="required" data-msg="Vérifiez votre saisie sur les champs : Le champ 'Société' doit être renseigné."   style="border: 1px #841851 solid;
    		width: 290px;"/>
 </div>  

 <div class="row">
        <div class="col-md-4 col-md-offset-4 error">
            <ul>
                @foreach($errors->all() as $error)
                    <li>{{$error}}</li>
                @endforeach
            </ul>
        </div>
    </div>

  <button>submit</button>
</form>

<script
  src="https://code.jquery.com/jquery-2.2.4.min.js"
  integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44="
  crossorigin="anonymous"></script>
<script>
    $('#gov_id').on('change',function(){
      var id = $(this).val();
      $.ajax({

        url : '/getDists/'+id,
        type : 'GET',
        success : function(data) {   
        var $select = $('#region_id'); 

        $select.find('option').remove();
        data.forEach(function(element)
        {
            console.log(element.id);
            $select.append('<option value='+element.id +'>'+element.Name+'</option>')
          });
        },
        error : function(request,error)
        {
            alert("Request: "+JSON.stringify(request));
        }
  	 });
    });
    $(document).ready(function() {
      var el = document.getElementById("price_div");
    $('input[type=radio][name=attendance]').change(function() {
        if (this.value == '1') {            
            el.style.visibility="visible" ;
        }
        else if (this.value == '0') {
            el.style.visibility="hidden";
        }
      });
    });
  </script>
 <script>
      var map;
      var marker;
      var myLatLng= {lat: 27.180134, lng: 31.189283};
      function initMap() {
        map = new google.maps.Map(document.getElementById('map'), {
        zoom: 8,
        center:myLatLng
        });
              map.addListener('click', function(e) {
            placeMarkerAndPanTo(e.latLng, map);
          });
        

        function placeMarkerAndPanTo(latLng, map) {
          if(marker != null)
               marker.setMap(null);
           marker = new google.maps.Marker({
            position: latLng,
            map: map
          });
          marker.setMap(map);
          map.panTo(latLng);
          console.log(marker.getPosition().lat());
          $('#lat_text').val(marker.getPosition().lat().toFixed(3));
          $('#long_text').val(marker.getPosition().lng().toFixed(3));
        }
      }
      // var marker = new google.maps.Marker({
      //     position: myLatLng,
      //     map: map,
      //     draggable: true,
      //     animation: google.maps.Animation.DROP,
      //     position: myLatLng,
      //     title: 'Hello World!'
      //   });
      //    marker.addListener('click', toggleBounce);
      //    function toggleBounce() {

      //           console.log(myLatLng['lat']);
      //           if (marker.getAnimation() !== null) {
      //             marker.setAnimation(null);
      //           } else {
      //             marker.setAnimation(google.maps.Animation.BOUNCE);
      //           }
      // //       }
      //   }
  </script>
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBqLrOdeB2oF_wbetq9K-g9ssrfiWv5wbk&callback=initMap"
  async defer>
    
  </script>
@stop
