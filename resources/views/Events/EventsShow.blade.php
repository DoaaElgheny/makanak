@extends('masterUser')
@section('content')
    <!--Pricing-->
    <title> الأحداث</title>
   
      
      <style>/*------------------------------  header -----------------------------------*/

#header {
    background:  url('../img/slider1.jpg') no-repeat center center;
    background-attachment: fixed;
    background-size: cover;
    display: table;
    height: calc(100vh - 72px);
    width: 100%;
    position: relative;
    z-index: 1;
    overflow-x: hidden;
}

.header-wrapper .header-overlay {
    min-height: 100vh;
    opacity: 0.4;
    position: absolute;
    top: 0;
    left: 0;
}   

.header-wrapper .header-wrapper-inner {
    display: table-cell;
    text-align: center;
    vertical-align: middle;
}

.header-wrapper .header-wrapper-inner .welcome-speech {
  margin-top: 7em;
}

.header-wrapper .header-wrapper-inner .welcome-speech h1 {
  color: #232c35;
    font-size: 35px;
    font-weight: bold;
    letter-spacing: 6px;
    margin-bottom: 12px;
    margin-top: 0;
    text-transform: uppercase;
}

.header-wrapper .header-wrapper-inner .welcome-speech p {
  color: #232c35;
    font-size: 14px;
    letter-spacing: 3px;
    margin-bottom: 4.2em;
}
          /*---------------------------  portfolio  ----------------------------------*/

.portfolio-item {
  margin-bottom:30px;
    margin-top: 20px;
}

.portfolio-item .item-image {
  position: relative;
}

.portfolio-item .item-image a,
.portfolio-item .item-image a img {
  display: block;
  position: relative;
        border-radius: 70px 7px;
                  width: 170px;
    height: 275px;

}

.portfolio-item .item-image a {
  overflow: hidden;
}



.portfolio-item .item-image a div span {
  color: #ffffff;
  display: block;
  font-size: 50px;
  position: absolute;
  left: 45%;
  top: 35%;
}

.portfolio-item .item-description {
    padding: 15px 0px;
}

.portfolio-item .item-description span {
    display: block;
    color: black;
 
}

.portfolio-item .item-description span.item-name {
    margin-bottom: 2px;
    font-size: 16px;
    color: black;
    text-align: right;
    margin-right: 4px;
   
}
       .portfolio-item .item-description span.title {
              font-size: 17px;
              font-weight: bold;
                margin-bottom: 40px;
           margin-right: 12px;
           color: #841851;
          }

          .portfolio-item .item-description span .fa {
              margin-right: 15px;
    margin-left: 10px;
          margin-bottom: 7px;
             color: #841851;}
          
.portfolio-item .item-description span.like {
    float: right;
    margin-top: 12px;
}

@media (max-width: 767px) {
  .portfolio-item .item-description {
      padding: 12px 50px;
  }
}
          
          .portfolio1
          {margin-bottom: 20px;
           
       
                  border-bottom: 4px solid #ddd;
    border-right: 4px solid #ddd;
              border: 3px solid #ddd;
          }
          .portfolio-item .item-image a {
 
  margin: 0 15px 15px 0;
          width: 170px;
   
  overflow: hidden;      

}



.andmore1 {
    background: #841851;
               margin-bottom: 7px;
    color: white;
    padding-top: 4px;
    padding-bottom: 4px;
   
    border: 1px solid #841851;
    border-radius: 3%;
}

          .serch7 {
              background: #841851;
                margin-right: 30px;
    color: white;
    padding-top: 4px;
    padding-bottom: 4px;
    margin-top: -1px;
    border: 1px solid #841851;
    border-radius: 3%;
          }


          .ui-radio {
    display: none;
}
 
.ui-radio + label{
  position: relative;
  padding-left: 20px;
  display: inline-block;
        font-size: 13px;
}
.ui-radio + label:before {
    background-color: #fff;
    border: 1px solid #841851;
    border-radius: 50px;
    display: inline-block;
    position: absolute;
    content: ' ';
    width: 20px;
    height: 20px;
    top:0px;
    left:0px;
}
 
.ui-radio:checked + label:after {
    content: ' ';
    width: 12px;
    height: 12px;
    border-radius: 50px;
    position: absolute;
    background: #841851;
    top: 4px;
    left: 4px;
}
 
.ui-radio:checked + label:before {
    color: #99a1a7;
    border: 1px solid #841851;
}
          .ui-checkbox {
    display: none;
}
.ui-checkbox {
    display: none;
}

.ui-checkbox + label {
  position: relative;
  padding-left: 25px;
  display: inline-block;
  font-size: 13px;
}
 
.ui-checkbox + label:before {
    background-color: #fff;
    border: 1px solid #841851;   
    padding: 9px;
    border-radius: 3px;
    display: block;
    position: absolute;
    top: 0;
    left:0;
    content: "";
}
 

 
.ui-checkbox:checked + label:before {
    border: 1px solid #841851;    
    color: #99a1a7;
}

.ui-checkbox.no-border:checked + label:before {
    border-color: transparent;
}


 
.ui-checkbox:checked + label:after {
    content: '\2714';
    font-size: 14px;
    position: absolute;
    top: 1px;
    left: 4px;
    color: #841851;
}
 
          .te90 {
    font-weight: 700;
    font-size: 16px;
    margin-top: 20px;
    margin-bottom: 20px; } 
          
          .tica2 {
               MARGIN-TOP: 40px;
    margin-right: -20px;
              color: black;
          }     
          
          
           
           .tica2 .fa {
               color: #841851;
          }  
          .pagination .page-link {
          
              font-size: 17px;
          }
</style>
      <header id="header" class="header-wrapper home-parallax home-fade" style="height: 90px;">
                <div class="header-overlay"></div>
                <div class="header-wrapper-inner">
                    <div class="container">

                        <div class="welcome-speech">
                            <h1>مرحباً بك فى ايفنتاريا</h1>
                         

<div style="   
    border: 1px #ddd solid;
    padding-bottom: 50px;
    padding-top: 20px;margin-bottom: 40px;
 ">

<div class="form-group row col-md-5 ">
  <label for="date_from" class="col-2 col-form-label col-md-4">من التاريخ </label>
  <div class="col-md-8">
    <input class="form-control" type="datetime-local" value="2011-08-19T13:45:00" id="date_from">
  </div>
</div>


<div class="form-group row col-md-5 ">
  <label for="date_to" class="col-2 col-form-label col-md-4">إلى التاريخ</label>
  <div class="col-md-8">
    <input class="form-control" type="datetime-local" value="2011-08-19T13:45:00" id="date_to">
  </div>
</div>
     <button type="button" id="submitSearchDate" name="submit" class="  col-md-2  serch7" style=" padding: 5px;margin-right: 30px;">بحث  </button>
</div>
    
                        </div><!-- /.intro -->
                        
                    </div><!-- /.container -->

                </div><!-- /.header-wrapper-inner -->
            </header>
    <!-- end header-->   

      <section  id ="pricing" class=" section-padding " style="padding: 45px 0px;"  >
       <div class="container" >
        <div class="row">
      <div class="col-md-3 righ" style="border: 1px solid #CCC;">  
          <div class="col-md-12">
          
          
                                 <div class="head_title text-center ">
                                    <h4 style="    font-size: 20px;
    margin-top: 30px;
    margin-bottom: 40px;
    color: #841851;" >  تصفية النتائج  </h4>
                                 
                                </div><!-- End off Head_title -->
                            
          
          </div>
               <div class="col-sm-12 ">
   
          <div class="form-group">
    <label for="govSelect" class="te90">  المحافظة</label>
    <select class="form-control" id="govSelect" style="border: 1px #841851 solid;
    ">
    <option value="" disabled selected>اختر المحافظة</option>
    @foreach($governments as $gov)
      <option value="{{$gov->id}}"> {{$gov->Name}} </option>
    @endforeach
     
    </select>
  </div>
      </div>
                <div class="col-sm-12 ">
   
          <div class="form-group">
    <label for="region_id" class="te90">  المركز</label>
    <select class="form-control" id="region_id" style="border: 1px #841851 solid;
    ">
    </select>
  </div>
      </div>
       
<div class="col-sm-12 ">
    <p class="te90">  التصنيفات</p>
     @foreach($event_types as $event_type)
     <input type="checkbox"  name="checkboxType[]" id="checkbox{{$event_type->id}}" class=" checkboxTypes col-sm-6 ui-checkbox no-border" value="{{$event_type->id}}" /> <label class=" col-sm-6" for="checkbox{{$event_type->id}}">{{$event_type->name}}</label>
    <br>
  @endforeach
    <br>
</div>
 
          
          <div class="col-sm-12 ">
    <p class="te90">  التكلفة</p>
          
            <input type="radio" id="radio-1-1" name="attendance" class="ui-radio col-sm-6" value="0" />  <label class=" col-sm-6" for="radio-1-1">
مجانى
</label>

<br>
<input type="radio" id="radio-1-2" name="attendance" class="ui-radio col-sm-6 " /> <label class=" col-sm-6" value="1" for="radio-1-2"> تكلفة</label>
           <br>
              <br>
          </div>
            
            </div>
            
            
  
 <div class="col-md-8 lif" style="margin-right: 0px;" >
               
                  
      <div class="portfolio-item-list">
        @include('Events.eventsPartial')       
      </div> <!-- end of portfolio-item-list -->
                            
                    
                
      </div>
           </div>
          </div>
      </section>

      
      
      
         <!--Modal box-->
    <div class="modal fade" id="lami" role="dialog">
      
    </div>


<script>
var data_ids=[];
var id=""; /*goverment_di*/
var disId="";
var attendance="";
var date_from="";
var date_to="";
  $(document).on('click','.andmore1',function(ev){
    var eventId = $(this).attr('eventId');
    $.get("event/modal/"+eventId,function (data) {
      $(".modalDescription").remove();
      $("#lami").html(data);
    });
    ev.preventDefault();
  });

  $(document).on('change','.checkboxTypes',function(){
    //alert($(this).val());
     data_ids = [];
    if($(".checkboxTypes:checked").length > 0)
    {
      $(".checkboxTypes:checked").each(function() {
        data_ids.push($(this).val());
      });
      $.get("/events/types",{data_ids,id,disId,attendance,date_from,date_to},function(data){
        $('.portfolio-item-list').empty();
        $('.portfolio-item-list').html(data);
        console.log(data);
      });
    }
    else
    {
      $.get("/events/types",{data_ids,id,disId,attendance,date_from,date_to},function(data){
        $('.portfolio-item-list').empty();
        $('.portfolio-item-list').html(data);
        console.log(data);
      });
    }
  });
/**
*select Distirict
*/
$('#govSelect').on('change',function(){
   id = $(this).val();
   disId ="";
  $.get("/events/types",{data_ids,id,disId,attendance,date_from,date_to},function(data){
        $('.portfolio-item-list').empty();
        $('.portfolio-item-list').html(data);
        console.log(data);
      });
  $.ajax({

    url : '/getDists/'+id,
    type : 'GET',
    success : function(data) {   
    var $select = $('#region_id'); 

    $select.find('option').remove();
    $select.append('<option value="" disabled selected>اختر المركز</option>');
    data.forEach(function(element)
    {
        console.log(element.id);
        $select.append('<option value='+element.id +'>'+element.Name+'</option>')
      });
    },
    error : function(request,error)
    {
        alert("Request: "+JSON.stringify(request));
    }
  });
});
/***
  When Select a specified Discrete load the events in it
***/
 $(document).on('change','#region_id',function(){
   disId = $(this).val();
      $.get("/events/types",{data_ids,id,disId,attendance,date_from,date_to},function(data){
        $('.portfolio-item-list').empty();
        $('.portfolio-item-list').html(data);
        console.log(data);
      });
  });

/***
  When Select an attendanceType load events depen what you chose
***/
 $(document).ready(function() {
      //var el = document.getElementById("price_div");
    $('input[type=radio][name=attendance]').change(function() {
      // alert($(this).val());
         attendance = $(this).val();         
            $.get("/events/types",{data_ids,id,disId,attendance,date_from,date_to},function(data){
              $('.portfolio-item-list').empty();
              $('.portfolio-item-list').html(data);
              console.log(data);
            });
      });
    });

 /***
  Search By Date
  ***/
  $('#submitSearchDate').on('click',function(){
     date_from = $('#date_from').val();
     date_to = $('#date_to').val();
    $.get("/events/betweenDates",{date_from,date_to},function(data){
        $('.portfolio-item-list').empty();
        $('.portfolio-item-list').html(data);
        console.log(data);
      });
  });
  </script>
      
      
@stop