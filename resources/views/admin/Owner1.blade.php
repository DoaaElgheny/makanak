 @extends('masterUser')
@section('content')
<title>مستخدم جديد</title>
<br/>
<br/>

    <!--Pricing-->
    <section id ="pricing" class="section-padding">
      <div class="container">
        <div class="row">

              <div class="head_title text-center " style="    margin-top: 40px;">
                                    <h2 >بيانات الحساب
                  </h2>

                                    <div class=" separator"></div>
                                </div><!-- End off Head_title -->
            <div class="col-md-2"></div>
                @if(Session::has('message'))

      <p class="alert alert-danger">{{ Session::get('message') }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
      @endif
          <div class="col-md-8 ">

              {!! Form::open(['action' => "OwnerController@store",'method'=>'post','id'=>'profileForm']) !!}



              <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">

     <div class="form-group">
    <label class="control-label col-sm-4" for="name" style="padding-right: 120px;" >اسم المستخدم:</label>
    <div class="col-sm-8">
      <input type="text" class="form-control" name="Name" id="Name" placeholder="ادخل اسم المستخدم"  style="margin-bottom: 15px;">
    </div>
  </div>


  <div class="form-group">
    <label class="control-label col-sm-4" for="email" style="padding-right: 120px;" >البريد الإلكترونى:</label>
    <div class="col-sm-8">
      <input type="email" class="form-control"  name="Email" id="Email" placeholder="ادخل الايميل"  style="margin-bottom: 15px;">
    </div>
  </div>


  <div class="form-group">
    <label class="control-label col-sm-4" for="pwd" style="padding-right: 120px;" >كلمة السر:</label>
    <div class="col-sm-8">
      <input type="password" class="form-control" id="Password" name="Password" placeholder="ادخل كلمة المرور"  style="margin-bottom: 15px;">
    </div>
  </div>

                       <div class="form-group">
    <label class="control-label col-sm-4" for="pwd" style="padding-right: 120px;" > تأكيد كلمة السر :</label>
    <div class="col-sm-8">
      <input type="password" class="form-control"  id="confirmpassword"   name="confirmpassword"   placeholder="تاكيد كلمة المرور" style="margin-bottom: 15px;">

    </div>
  </div>

<div class="form-group">
  <label class="control-label col-sm-4" for="sel1" style="padding-right: 120px;" >نوع المكـان:</label>
     <div class="col-sm-8">
{!!Form::select('types', ([null => 'اختر نوع المكان'] + $types->toArray()) ,null,['class'=>'form-control','id' => 'Type','name'=>'Type'])!!}
    </div>
</div>




         </div>
                <div class="col-md-2"></div>
        </div>
      </div>
    </section>
    <!--/ Pricing-->

<!-- Price section -->
      <section id="price">
        <div class="container">
          <div class="row">

              <div class="head_title text-center ">
                             <!--        <h2 >       باقــتـنـا
                  </h2>

                                    <div class=" separator"></div> -->
                                </div><!-- End off Head_title -->

          <!--   <div class="col-md-4 wow animated fadeInUp">
              <div class="price-table1 text-center">
                @foreach($Packa1 as $Pack1)
                <span>{{$Pack1->Name}}</span>
                <div class="value">
                  <span>L.E</span>
                  <span>{{$Pack1->Price}}</span><br>
                  <span>/زُشهريا</span>
                </div>
                @endforeach
                <ul>
                  @foreach($Packages1 as $Pack1)
                  <li><i class="fa fa-check-circle-o" aria-hidden="true" style="    padding-left: 10px;"></i>{{$Pack1->Name}}</li>

                  @endforeach

                </ul>
              </div>
            </div> -->

           <!--  <div class="col-md-4 wow animated fadeInUp" data-wow-delay="0.4s">
              <div class="price-table1 featured text-center">
                 @foreach($Packa2 as $Pack2)
                <span>{{$Pack2->Name}}</span>
                <div class="value">
                  <span>L.E</span>
                  <span>{{$Pack2->Price}}</span><br>
                  <span>شهرياً</span>
                </div>
                @endforeach
                <ul>
                              @foreach($Packages2 as $Pack2)
                  <li><i class="fa fa-check-circle-o" aria-hidden="true" style="    padding-left: 10px;"></i>{{$Pack2->Name}}</li>

@endforeach
                </ul>
              </div>
            </div> -->
<!-- 
            <div class="col-md-4 wow animated fadeInUp" data-wow-delay="0.8s">
              <div class="price-table1 text-center">
                  @foreach($Packa3 as $Pack3)
                <span>{{$Pack3->Name}}</span>
                <div class="value">
                  <span>L.E</span>
                  <span>{{$Pack3->Price}}</span><br>
                  <span>شهرياً</span>
                </div>
                @endforeach
                <ul>
                    @foreach($Packages3 as $Pack3)
                  <li><i class="fa fa-check-circle-o" aria-hidden="true" style="    padding-left: 10px;"></i>{{$Pack3->Name}} </li>
                  @endforeach
                </ul>
              </div>

            </div> -->





    <div class="col-md-6">
          </div>
                        <div class="col-md-3">
         </div>






                        <div class="col-md-3">

            </div>
          </div>
        </div>
<br/>
<br/>
        <div class="col-md-12" hidden="hidden" style="    border: 1px solid #e3e3e3;     background-color: #841952;
    color: white;">

        <div class="col-md-4"> <p class="lead"  style="text-align: center;
    padding-top: 9px;
    margin-bottom: 0px;     padding-bottom: 9px;"><input name="Packge_id" checked="checked" type="radio" value="1" >
    المجانية
         </p>
        </div>
        <div class="col-md-4"><p class="lead" style="text-align: center;
    padding-top: 9px;
    margin-bottom: 0px;     padding-bottom: 9px;" > <input name="Packge_id" type="radio" value="2">
        المميزة  </p>
        </div>
        <div class="col-md-4"><p class="lead"  style="text-align: center;
    padding-top: 9px;
    margin-bottom: 0px;     padding-bottom: 9px;"> <input  name="Packge_id" type="radio" value="3">
        الذهبية </p>
        </div>



</div>


  <div class="col-md-6">
          </div>
                        <div class="col-md-3  btn-trial2">
        <button type="submit" id="#submit"  style="margin-bottom: 20px;margin-top: 20px;">متابعة</button>


         </div>
         {!!Form::close()!!}
                        <div class="col-md-3 ">


                        </div>
<br>
<br>
<br>


<script type="text/javascript">
  $(document).ready(function()
    {   
        $('#profileForm').formValidation({
        framework: 'bootstrap',
        icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
              },
              fields: { 
                        'Name': 
                        {
                          validators: {
                 
                notEmpty:{
                    message:"يجب ادخال اسم المستخدم "
                  }
                        
                    }
                    },     
                        'Email': 
                        {
                          validators: {
                             regexp: {
                        regexp: "[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$",
                       
                    },

                                   remote: {
                        url: '/checkEmailUnique',
                      
                        message: 'البريد الالكتروني مدخل من قبل ',
                        type: 'GET'
                    },
                notEmpty:{
                    message:"ادخل البريد الالكتروني "
                  }
                    
                 
                        
                    }
                        },
                         'confirmpassword': 
                        {
                          validators: 
                          {
                            notEmpty:
                            {
                              message: 'يجب ادخال تاكيد كلمة المرور'
                            },
                            identical: {
                        field: 'Password',
                        message: 'كلمة المرور غير مطابقة'
                    }
                          }
                        },
                        'Type': 
                        {
                          validators: 
                          {
                            notEmpty:
                            {
                              message: 'يجب ادخال نوع المكان'
                            },
                     
                          }
                        },
                     
                        'Password':
                        {
                          validators: 
                          {
                            notEmpty:
                            {
                              message:"يجب ادخال كلمة المرور"
                            }
                          }
                        } 
                       
              } ,submitHandler: function (form) {
              
   form.submit();
}
          })
          .on('success.form.fv', function(e)
          {
            var Packge_id = $('input[name=Packge_id]:checked').map(function()
            {

                return $(this).val();

            }).get();


    $.ajax({
     
            type: "POST",
            url: '/storeownerfirst',
            dataType: 'json',
            data: {'_token':$('#token').val(),
            'Name':$('#Name').val(),
            'Email':$('#Email').val(),
            'Password':$('#Password').val(),
            'Type':$('#Type').val(),
            'Packge_id':Packge_id


          },
           success:function(response)
            {
               window.location = response;
            // $('#asd').hidden=fal
         
             
        }
        });


            // var $form = $(e.target);
             
            // Enable the submit button 
             // $form.formValidation('disableSubmitButtons', false);
          });
        });
          </script>

       @stop
