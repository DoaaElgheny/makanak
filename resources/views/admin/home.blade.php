<!DOCTYPE html>
<html lang="ar">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Yalla E7gez</title>
    <meta name="description" content="أول موقع حجز قاعات">
    <meta name="keywords" content="أول موقع حجز قاعات">

    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Open+Sans|Candal|Alegreya+Sans">
    <link rel="stylesheet" type="text/css" href="/assets/UserFiles/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="/assets/UserFiles/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="/assets/UserFiles/css/bootstrap-rtl.css">
    <link rel="stylesheet" type="text/css" href="/assets/UserFiles/css/imagehover.min.css">
    <link rel="stylesheet" type="text/css" href="/assets/UserFiles/css/style.css">
     <link rel="stylesheet" type="text/css" href="/assets/UserFiles/css/animate.css">
<link href="https://fonts.googleapis.com/css?family=Cairo" rel="stylesheet">
      
  </head>
  <body>
    <!--Navigation bar-->
    <nav class="navbar navbar-default navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <div style="height:100px width:200px;"><a class="navbar-brand" href="/Home"><img src="assets/UserFiles/img/lo3.png"  alt="img25" style="margin-top: -6px;">

            </a></div>


        </div>
        <div class="collapse navbar-collapse" id="myNavbar">
        <ul class="nav navbar-nav navbar-right"  style="margin-top: 10px;">
          <li class="nav-item active"><a href="/Home">الرئيسية <span class="sr-only">(current)</span></a></li>
          <li><a href="/Halls">القاعات</a></li>
          <li><a href="/Halls">المناسبات</a></li>



          <li class="btn-trial"><a href="#" data-target="#login" data-toggle="modal">تسجيل دخول</a></li>

          <li class="btn-trial"><a href="/Regest">مستخدم جديد</a></li>
        </ul>
        </div>
      </div>
    </nav>
    <!--/ Navigation bar-->
    <!--Modal box-->
    <div class="modal fade" id="login" role="dialog">
      <div class="modal-dialog modal-sm">

        <!-- Modal content no 1-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title text-center form-title">تسجيل دخول</h4>
          </div>
          <div class="modal-body padtrbl">

            <div class="login-box-body">
         <!--  {!!Form::open(['action'=>'LoginController@Login','method' => 'post','files'=>true])!!} -->
              <div class="form-group">

                 <div class="form-group has-feedback"> <!----- username -------------->
                      <input class="form-control" name="Username" placeholder="البريد الإلكترونى "  id="loginid" type="text" autocomplete="off" />
            <span style="display:none;font-weight:bold; position:absolute;color: red;position: absolute;padding:4px;font-size: 11px;background-color:rgba(128, 128, 128, 0.26);z-index: 17;  right: 27px; top: 5px;" id="span_loginid"></span><!---Alredy exists  ! -->
                      <span class="glyphicon glyphicon-user form-control-feedback"></span>
                  </div>
                  <div class="form-group has-feedback"><!----- password -------------->
                      <input class="form-control" name="Password" placeholder="كلمة السر " id="loginpsw" type="password" autocomplete="off" />
            <span style="display:none;font-weight:bold; position:absolute;color: grey;position: absolute;padding:4px;font-size: 11px;background-color:rgba(128, 128, 128, 0.26);z-index: 17;  right: 27px; top: 5px;" id="span_loginpsw"></span><!---Alredy exists  ! -->
                      <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                  </div>
                  <div class="row">
                      <div class="col-xs-12">
                          <div class="checkbox icheck">
                              <label>
                                <input type="checkbox" id="loginrem" > تذكر كلمة السر
                              </label>
                          </div>
                      </div>
                      <div class="col-xs-12">
                      <button type="submit" class="btn btn-bg red btn-block"  onclick="login();">تسجيل</button>

                      </div>

          <div class="alert alert-danger" hidden="hidden" id="Error" style="text-align:center;" >
                <strong></strong> كلمة المرور او البريد الالكتروني غير صحيح <br><br>
          </div>
          <div class="alert alert-danger" hidden="hidden" id="Error2" style="text-align:center;" >
                <strong></strong> من فضلك ادخل البيانات اولا<br><br>
          </div>
          <div class="alert alert-danger" hidden="hidden" id="Error3" style="text-align:center;" >
                <strong></strong>هذا الحساب غير مسجل لدينا<br><br>
          </div>
           <div class="alert alert-danger" hidden="hidden" id="Error4" style="text-align:center;" >
                <strong></strong>انتظر سيم تفعيل حسابك خلال ساعات<br><br>
          </div>

                  </div>
              <!--  {!!Form::close()!!} -->
              </div>
            </div>
          </div>
        </div>

      </div>
    </div>
    <!--/ Modal box-->
    <!--Banner-->
    <div class="banner">
      <div class="bg-color">
        <div class="container">
          <div class="row">
            <div class="banner-text text-center">

              <div class="intro-para text-center quote">
                <p class="big-text ">أول موقع حجز قاعات </p>

                    <p class="para-w3l">
                        
                        
                        
                        
                        نعرض لك كل الأماكن والقاعات الموجودة بخدماتها وإمكانياتها
<br>
                        ونسهل لك اختيار المكان الأمثل لحجز موعدك 
                        
                        


        </p>
                <a href="/AllPlace" class="btn get-quote" style="    padding-right: 30px;
    padding-left: 30px;
    font-size: 15px;
    font-weight: bold;">منصة الأماكن</a>
              </div>
              <a href="#feature" class="mouse-hover"><div class="mouse"></div></a>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!--/ Banner-->
    <!--Feature-->
    <section id ="feature" class="section-padding">
      <div class="container">
        <div class="row">
            <div class="main_feature text-center">

                                 <div class="head_title text-center ">
                                    <h2 >  ما الذى يقدمه <span style="color:#841851;"> يلا احجز</span> </h2>

                                    <div class=" separator"></div>
                                </div><!-- End off Head_title -->

                            <div class="col-sm-3 wow animated zoomIn" data-wow-offset="160">
                                <div class="single_feature">
                                    <div class="single_feature_icon">
                       <i class="fa fa-clock-o" aria-hidden="true"></i>

                                    </div>

                                    <h4>الراحة</h4>
                                        <div class="separator3"></div>
                                              <p class="lead"> لم تعد بحاجة لزيارة كل مكان بنفسك ، وفر وقتك وقم بزيارتنا </p>


                                </div>
                            </div>

                            <div class="col-sm-3 wow animated zoomIn" data-wow-offset="160" data-wow-delay="0.3s">
                                <div class="single_feature">
                                    <div class="single_feature_icon">
                                   <i class="fa fa-search" aria-hidden="true"></i>
                                    </div>

                                    <h4>سهولة البحث</h4>
                                        <div class="separator3"></div>
                                      <p class="lead"> اختر ما يناسب احتياجاتك من القاعات وإمكانياتها المتاحة لدينا </p>
                                </div>
                            </div>

                            <div class="col-sm-3 wow animated zoomIn" data-wow-offset="160" data-wow-delay="0.6s">
                                <div class="single_feature">
                                    <div class="single_feature_icon">
                                     <i class="fa fa-calendar" aria-hidden="true"></i>
                                    </div>
                                    <h4>تنظيم المواعيد</h4>
                                    <div class="separator3"></div>
 <p class="lead">
                                                 لم تعد بحاجة للتسجيل الورقي ، أصبح ترتيب مواعيدك الآن سهلاً  </p>
                                </div>
                            </div>


                            <div class="col-sm-3 wow animated zoomIn" data-wow-offset="150" data-wow-delay="0.9s">
                                <div class="single_feature">
                                    <div class="single_feature_icon">
                                    <i class="fa fa-usd" aria-hidden="true"></i>
                                    </div>
                                    <h4>التوفير</h4>
                                   <div class="separator3"></div>
                                             <p class="lead">  قم بالإستفادة من العروض الخاصة بنا واحجز مواعيدك بافضل الاسعار</p>


                                </div>
                            </div>


                        </div>
        </div>
      </div>
    </section>
    <!--/ feature-->
    <section id ="feature" class="section-padding">
      <div class="container">
        <div class="row">
            <div class="col-md-12">

                      
  
 <!-- Wrapper for slides -->
  <div id="myCarousel" class="carousel slide" data-ride="carousel" style="border:5px solid #ddd;">
    <!-- Indicators -->
    <ol class="carousel-indicators">
      @foreach($ads as $key=>$ad)
      <li data-target="#myCarousel" data-slide-to="{{$key}}" class="{{$ads->first()->id==$ad->id?'active':''}}"> </li>
      @endforeach
      
    </ol>

    <!-- Wrapper for slides !-->
    <div class="carousel-inner">
      @foreach($ads as $ad)
        <div class="item {{$ads->first()->id==$ad->id?'active':''}}">
          <img src="{{'/'.$ad->photo}}" alt="add" style="width:100%;height: 270px;
  ">
        </div>
      @endforeach
    </div>

    <!-- Left and right controls -->
  
  </div>


                         

                        </div>
        </div>
      </div>
    </section>

  
    



    <!--Contact-->
    <section id ="contact" class="section-padding">
      <div class="container">
        <div class="row">


               <div class="head_title text-center ">
                                 <h2>اتـصـل بـنـا </h2>
            <p> يمكننك التواصل معنا من خلال القائمة التالية</p>

                                    <div class=" separator"></div>
                                </div><!-- End off Head_title -->






       {!! Form::open(['action' => "ContactUsController@SendContactUs",'method'=>'post','id'=>'profileForm']) !!}

              <div class="col-md-6 col-sm-6 col-xs-12 left  wow animated fadeInLeft" data-wow-offset="150">
                <div class="form-group">
                    <input type="text" name="name" class="form-control form" id="name" placeholder="الإسم" data-rule="minlen:4" data-msg="Please enter at least 4 chars" />
                    <div class="validation"></div>
                </div>
                <div class="form-group">
                    <input type="email" class="form-control" name="Email" id="email" placeholder="البريد الإلكترونى" data-rule="email" data-msg="Please enter a valid email" />
                    <div class="validation"></div>
                </div>
                <div class="form-group">
                    <input type="text" class="form-control" name="Subject" id="subject" placeholder="الموضوع" data-rule="minlen:4" data-msg="Please enter at least 8 chars of subject" />
                    <div class="validation"></div>
                </div>
              </div>

              <div class="col-md-6 col-sm-6 col-xs-12 right wow animated fadeInRight" data-wow-offset="150">
                <div class="form-group">
                    <textarea class="form-control" name="message" rows="5" data-rule="required" data-msg="Please write something for us" placeholder="الرسالة"></textarea>
                    <div class="validation"></div>
                </div>
              </div>

              <div class="col-xs-12">
                <!-- Button -->
                <button type="submit" id="submit" name="submit" class="form contact-form-button light-form-button oswald light">إرسال  </button>
              </div>
          {!!Form::close()!!}

        </div>
      </div>
    </section>
    <!--/ Contact-->
    <!--Footer-->
    <footer id="footer" class="footer">
      <div class="container text-center">
 {!! Form::open(['action' => "LoginController@Offers",'method'=>'get','id'=>'profileForm']) !!}

      <h3 class="wow animated fadeInDown" data-wow-offset="70">اشترك معنا ليصلك أخر العروض</h3>


        <div class="form-group col-md-3 col-md-offset-2 col-sm-4">
          <div class=" controls">
            <input name="txtName" placeholder="الإسم" class="form-control" type="text">
          </div>
        </div><!-- End email input -->
        <div class="form-group col-md-3 col-sm-4">
          <div class=" controls">
            <input name="EMAIL" placeholder="البريد الإلكترونى" class="form-control" type="email">
          </div>
        </div><!-- End email input -->
        <div class="col-md-2 col-sm-4">
          <p>
            <button name="submit" type="submit" class="btn btn-block btn-submit" ><i class="fa fa-arrow-left" aria-hidden="true"></i>
          اشترك </button>
          </p>



        </div>
         {!!Form::close()!!} 
     <!-- End newsletter-form -->
      <ul class="social-links social-icons icon-circle icon-zoom list-unstyled list-inline">


             <li class="wow animated zoomIn" data-wow-offset="40"> <a href="#"><i class="fa fa-google-plus"></i></a></li>
             <li  class="wow animated zoomIn" data-wow-delay="0.3s" data-wow-offset="40"> <a href="https://www.facebook.com/Eventarya/" target="_blank"><i class="fa fa-facebook"></i></a></li>
          <li class="wow animated zoomIn" data-wow-delay="0.6s" data-wow-offset="40"> <a href=""><i class="fa fa-youtube"></i></a></li>
      </ul>

             <div class="col-sm-1 col-xs-12 main_footer">




                                <p>

                                

                 </p>


                    </div>
           <div class="col-sm-3 col-xs-12 main_footer one1" style="    margin-right: 0px;
    margin-left: 80px;  ">




                                <p>


                             <!-- جامعة أسيوط -مبنى شبكة المعلومات الدور السادس- شركة E-Reserve -->
               </p>

                    </div>


 <div class="col-md-4 two2" style="
  ">




                            <div id="templatemo_footer" style="margin-right: -40px;
    margin-left: 50px;">
                                <a href="/Home">الرئيسية</a> | <a href="/Halls">القاعات </a> | <a href="/Halls">المناسبات </a>  <a href="contact.html"> </a> <br />
                               جميع الحقوق محفوظة  مكانك© 2019
                                <br>





                            </div>


                    </div>




            <div class="col-sm-3 col-xs-12 main_footer third3" style="
   ">



                                <p style="text-align: left;">


                                01061142749
                                        <i class="fa fa-phone" aria-hidden="true"></i>
                                    <br>


                                      
                                      <i class="fa fa-envelope-o" aria-hidden="true"></i>
                                      makanka2019@gmail.com
                                </p>





                    </div>




      </div>
    </footer>
    <!--/ Footer-->
       
  

    <script src="/assets/UserFiles/js/jquery.min.js"></script>
    <script src="/assets/UserFiles/js/jquery.easing.min.js"></script>
    <script src="/assets/UserFiles/js/bootstrap.min.js"></script>
    <script src="/assets/UserFiles/js/custom.js"></script>
  
     <script src="/assets/UserFiles/js/wow.min.js"></script>
       <script> new WOW().init();</script>
    <script >
      function login()
      {

     var Username=  $("#loginid").val();
     var Password=  $("#loginpsw").val();
    
         $.ajax({
      url: "/Login",
        data:{Username:Username,Password:Password},
      type: "get",
      success: function(data){
        if(data==2)
        {
         $('#Error2').show();
         $('#Error').hide();
         $('#Error4').hide();
         $('#Error3').hide();
        }
      else  if(data==1)
        {
         $('#Error').show();
         $('#Error2').hide();
$('#Error4').hide();
         $('#Error3').hide();
        }
        else  if(data==4)
          {
            $('#Error4').show();
           $('#Error3').hide();
           $('#Error').hide();
           $('#Error2').hide();
          }
        else  if(data==3)
          {
           $('#Error3').show();
           $('#Error4').hide();
           $('#Error').hide();
           $('#Error2').hide();
          }
        else
        {
          // alert(data);
       window.location = data;
        }


      }
    });
      }
    </script>
  </body>
</html>
