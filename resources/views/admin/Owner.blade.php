@extends('masterUser')
@section('content')
<title>مستخدم جديد</title>
<style type="text/css">
    #blah{
  background: url('assets/UserFiles/img/33.jpg') no-repeat;
  background-size: cover;

}
        .wizard-card .picture:hover {
  border-color:  #841851;
}
  .wizard-card .picture  {
         
        width: 200px;
    height: 180px;
         border-radius: 50%;
         
     }
</style>
<div class="col-md-12 " style="    margin-bottom: 27px;">
<br/>
<br/>
<br/>
<br/>

  <fieldset class="the-fieldset"  style="margin-bottom: 15px;">

                            <legend class="the-legend">بيانات المكان</legend>
  {!!Form::open(['route' =>[ 'Owner.update',1],'method' => 'put','id'=>'profileForm','files'=>'true']) !!}
  
   <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                            <div class="col-sm-5 col-sm-offset-1">
                            
                                       
       
        
                         <div class="col-sm-12 col-sm-offset-1">
      <p>اسم المكان</p>
      <span class="icon-case"><i class="fa fa-home"></i></span>
        <input type="text" name="PlaceName" id="PlaceName" required="required">
                
      </div>
             
                      
      <div class="col-sm-12 col-sm-offset-1">
      <p>عنوان المكان </p>
      <span class="icon-case"><i class="fa fa-location-arrow"></i></span>
        <input type="text" name="adress" id="adresse" >
                
      </div>       
                            
                           
                            
                            
           

      <div class="col-sm-12 col-sm-offset-1">
      <p>عن المكان</p>
      <span class="icon-case"><i class="fa fa-comments-o"></i></span>
                <textarea name="Desc" rows="14" ></textarea>
                
      </div>
    
                            
                            
                            </div>   
                            
                            
                            <div class="col-sm-5 col-sm-offset-1">
                            
                             <div class="col-sm-12 col-sm-offset-1 wizard-card">


          


                                       <div class="picture-container" >
                                          <div class="picture" id="blah" style="    

    margin-top: 27px;
    margin-bottom: 10px;" >
                                              <input type="file" id="file-upload"  name="pic" type="file"/>
                                          </div>  
                                      </div>
                                 <h6 class=" hh6" style="  font-size: 18px;
    margin-top: 3px;
    text-align: center;
    margin-left: 240px;    margin-right: 40px;">صورة اللوجو</h6>
                                  </div>     
                            
                            
                            </div> 
 
                        </fieldset>


                        <fieldset class="the-fieldset" style="margin-bottom: 15px;">
                            <legend class="the-legend"> مسؤول المكان</legend>
                           
            <div class="col-sm-3 col-sm-offset-1">
      <p>الإسم</p>
      <span class="icon-case"><i class="fa fa-user" aria-hidden="true"></i></span>
        <input type="text" required="required"  name="OwnerName" >
               
      </div>  
                            
               
                           
                            
            
              <div class="col-sm-3 col-sm-offset-1">
      <p>رقم التليفون</p> 
      <span class="icon-case"><i class="fa fa-phone"></i></span>
        <input type="number" name="TelOwner"  pattern=".{11,10}" required  >
                
      </div>
                
                <div class="col-sm-3 col-sm-offset-1">
      <p>البريد الإلكترونى </p> 
      <span class="icon-case"><i class="fa fa-envelope-o"></i></span>
                <input type="email" name="EmailOwner"  >
                
      </div>  
    
        
                            
                            
                            
                        </fieldset>
                  
  
                        
       
                        <fieldset class="the-fieldset">
                            <legend class="the-legend"> بيانات التواصل</legend>
                      
                            
                            <div class="col-sm-5 col-sm-offset-1">
                            
                            
                              <div class="col-sm-12 col-sm-offset-1">
      <p>تليفون 1</p> 
      <span class="icon-case"><i class="fa fa-phone"></i></span>
        <input type="number" pattern="[11-11]" name="phone1"   required="required"  >
                
      </div>
                            <div class="col-sm-12 col-sm-offset-1">
      <p>تليفون 2</p> 
      <span class="icon-case"><i class="fa fa-phone"></i></span>
        <input type="number" name="phone2" id="phone2" pattern=".{11,10}"/>
                
      </div>
                              <div class="col-sm-12 col-sm-offset-1">
      <p>البريد الإلكتروني </p> 
      <span class="icon-case"><i class="fa fa-envelope-o"></i></span>
                <input type="text"  name="email"  >
                
      </div>  
                            
                     <div class="col-sm-12 col-sm-offset-1">
      <p> رابط  الفيسبوك </p>  
                    <span class="icon-case"><i class="fa fa-facebook"></i></span>
                <input type="url" name="facebook"    required="required" />
                
      </div>
        
      <div class="col-sm-12 col-sm-offset-1">
      <p>الموقع الالكتروني</p> 
                    <span class="icon-case"><i class="fa fa-facebook"></i></span>
                <input type="text" name="WebSite"  >
                
      </div>
        
    <div class="col-sm-12 col-sm-offset-1">
      <p> رابط اليوتيوب  </p>  
                    <span class="icon-case"><i class="fa fa-youtube" aria-hidden="true"></i></span>
                <input type="text" name="youtube" >
                
      </div>
        <div class="col-sm-12 col-sm-offset-1">
      <p> رابط  تويتر </p> 
                    <span class="icon-case"><i class="fa fa-twitter" aria-hidden="true"></i></span>
                <input type="text" name="Twitter"  >
                
      </div>

                            
                            </div>
                            
                                <div class="col-sm-5 col-sm-offset-1">
                            
                            
                          <div class="col-sm-12 col-sm-offset-1">
      <p>حدد مكانك </p> 
          <iframe class="actAsDiv"  frameborder="0" scrolling="no" marginheight="0" marginwidth="0"  src="https://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=en&amp;q=Asyut%2CEl-Gomhoreya%2C%20Assiut%20Governorate&amp;aq=0&amp;ie=UTF8&amp;t=m&amp;z=16&amp;iwloc=A&amp;output=embed"></iframe>
      </div>  
                            
                            
                            </div>
                            
                     
                            
    </fieldset>


</div>




<div class="col-md-6 col-xs-6 btn-trial3" style="    margin-top: 10px;
    margin-bottom: 15px;">
 <p> <a href="/Owner" >رجوع</a> </p>
 </div>

<div class="col-md-6 col-xs-6 btn-trial4" style="">
  <button type="submit"  name="submitButton"   >تسجيل </button>   
</div>

{!!Form::close() !!}
  <script type="text/javascript">



  $(document).ready(function(){
     //change imgage    
$("#file-upload").on("change", function()
    {
    
        var files = !!this.files ? this.files : [];

        if (!files.length || !window.FileReader) 
        {
           $("#blah").css("background-image",'url(assets/UserFiles/img/noimage.jpg)');
        } // no file selected, or no FileReader support
 
        else if (/^image/.test( files[0].type)){ // only image file
            var reader = new FileReader(); // instance of the FileReader
            reader.readAsDataURL(files[0]); // read the local file
 
            reader.onloadend = function(){ // set image data as background of div
                $("#blah").css("background-image", "url("+this.result+")");
            }
        }
    });
/////////////////////



});








</script>  
               
              
          
 


</div>


       @stop