@extends('master')
@section('content')
<meta name="viewport" content="initial-scale=1.0">
<meta charset="utf-8">
<link rel="stylesheet" href="/assets/css/jquery.datetimepicker.css">
<script type="text/javascript" src="/assets/js/jquery.datetimepicker.full.min.js"></script>
<style>
.wizard-card .picture {
    width: 200px;
    height: 85px;
    background-color: #999999;
    border: 4px solid #CCCCCC;
    color: #FFFFFF;
   border-radius: 0;
   
}
/* Always set the map height explicitly to define the size of the div
       * element that contains the map. */
      #map {
        height: 100%;
      }
      /* Optional: Makes the sample page fill the window. */
      html, body {
        height: 100%;
        margin: 0;
        padding: 0;
      }
</style>
<form action="/update/ad/{{$ad->id}}" method="post" enctype="multipart/form-data">
{{ csrf_field() }}
<div class="pull-right clear">
{{ Form::text('name', $ad->name) }}
{{ Form::label('name','اسم اليفنت') }}
</div>
<div class="wizard-card pull-right clear">
<span class="pull-right">
 {{ Form::label('photo','الغلاف') }}
 </span>
	 <div class="picture-container">
	      <div class="picture">
	          <img src="{{'/'.$ad->photo}}" class="picture-src" id="wizardPicturePreview" title=""/>
	          <input name="photo" type="file" id="wizard-picture">
	      </div>         
	  </div>
 </div> 
<div class="clear  pull-right">
  {!! Form::hidden('approval', 0) !!}
  {!! Form::checkbox('approval', 1, $ad->approval == 1) !!}
  {{ Form::label('approval','مفعل') }}
 </div>
 <div class="pull-right clear">
 	{{ Form::textarea('description', $ad->description) }}
	{{ Form::label('description','عن الإعلان') }}
 </div>
 <div class="pull-right clear">
  {{ Form::number('index', $ad->k_index) }}
  {{ Form::label('index','الترتيب') }}
 </div>
 <div class="row">
        <div class="col-md-4 col-md-offset-4 error">
            <ul>
                @foreach($errors->all() as $error)
                    <li>{{$error}}</li>
                @endforeach
            </ul>
        </div>
    </div>

  <button>submit</button>
</form>

<script
  src="https://code.jquery.com/jquery-2.2.4.min.js"
  integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44="
  crossorigin="anonymous"></script>
@stop
