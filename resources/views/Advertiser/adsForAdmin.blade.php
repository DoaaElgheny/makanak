@extends('master')
@section('content')
 <section class="panel-body">

 <style type="text/css">
  #blah{
  background: url(assets/img/noimage.jpg) no-repeat;
  background-size: cover;
  height: 80px;
  width: 80px;
}
   table.dataTable.row-border tbody tr th, table.dataTable.row-border tbody tr td, table.dataTable.display tbody tr th, table.dataTable.display tbody tr td  {
    

           text-align: right;
   }
     div.dt-buttons .dt-button {
    margin-right: 0;
    background-color: #841851;
    font-size: 16px;
    padding-left: 30px;
    padding-right: 30px;
    border: 1px solid #841851;
    margin-bottom: 20px;
             height: 35px;
}
       div.dt-buttons .dt-button:hover , .dt-button:focus {
    
   border: 1px solid #841851;
    color: white !important;
    background-color: #69053a; }
 

           div.dataTables_wrapper div.dataTables_filter label {
   color: #841851;
    font-size: 17px;
    direction: rtl;
}
     div.dataTables_wrapper div.dataTables_filter input {
    width: 170px;
    margin-right: 7px;
           height: 33px;
}
     div.dataTables_wrapper div.dataTables_length label {
  
    color: #841851;
    font-size: 17px;
}
     div.dataTables_wrapper div.dataTables_length select {
    width: 170px;
    display: inline-block;
    text-align: right;
    direction: rtl;
}
     select.input-sm {
    height: 33px;
    line-height: 30px; }
     .btn-primary:hover , .btn-primary:focus {
         border: 1px solid #841851;
    color: white !important;
    background-color: #69053a; 
     }
     .chosen-container-multi .chosen-choices{
          border: 1px solid #841851;
    border-radius: 4px;
    margin-bottom: 9px;
     }
</style> 



<!-- model -->

@if(Session::has('flash_message'))
    <div class="alert alert-danger">
        {{ Session::get('flash_message') }}<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    </div>
@endif
 
<div class="modal fade" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
     <div class="modal-content">
    
          <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                 <h4 class="modal-title span7 text-center" id="myModalLabel"><span class="title"  style="    text-align: center;
    color: #15202a;
    font-size: 20px;
    font-weight: bold;">إضافة قاعة جديدة</span></h4>
          </div>
    <div class="modal-body">
        <div class="tabbable"> <!-- Only required for left/right tabs -->
       
        <div class="tab-content">
        <div class="tab-pane active" id="tab1">
              <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel" style="    border: 0px solid #E6E9ED;  ">
                 
                  <div class="x_content">
                    <br />
                 {!! Form::open(['action' => "HallController@store",'method'=>'post','id'=>'profileForm', 'files' => 'true']) !!}   
             
             <fieldset style="    padding: .35em .625em .75em; margin: 0 2px; border: 0px solid silver;" dir="rtl">
 
 

          <div class="form-group col-md-12">
                
                <div class="col-md-9" dir="rtl">
                    <input type="text" class="form-control" id="Name" name="Name" placeholder="اسم القاعه" required="required" style="    border: 1px solid #841851;
    border-radius: 4px;
    margin-bottom: 9px;">
                </div>
                <label for="input_product" class="control-label col-md-3">إسم الإعلان</label>
            </div>
          
 <div class="form-group col-md-12">
                
                <div class="col-md-9" dir="rtl">
                    <input type="text" class="form-control" step="0.01" MIN=1 id="Price" name="Price" placeholder="سعر القاعه" required="required" style="      border: 1px solid #841851;
    border-radius: 4px;
    margin-bottom: 9px;">
                </div>
                <label for="input_product" class="control-label col-md-3">الوصف/label>
            </div>
             <div class="form-group col-md-12">
                
                <div class="col-md-9" dir="rtl">
                    <input type="number"  MIN=1 class="form-control" id="Capacity" name="Capacity" placeholder="سعه القاعه" required="required" style="       border: 1px solid #841851;
    border-radius: 4px;
    margin-bottom: 9px;">
                </div>
                <label for="input_product" class="control-label col-md-3">السعة</label>
            </div>
             <div class="form-group col-md-12">
                
                <div class="col-md-9" dir="rtl">
                   <select></select>
                </div>
                <label for="input_product" class="control-label col-md-3">نوع القاعة</label>
            </div>
             <div class="form-group col-md-12">
                
                <div class="col-md-9" dir="rtl">
                     <select></select>
                </div>
                <label for="input_product" class="control-label col-md-3">الإمكانيات</label>
            </div>

          
          
               <div class="form-group col-md-12">
                 <div class="col-md-9" dir="rtl">
 <div id="blah"></div>
            </div>
                        <label for="file-upload" class="custom-file-upload" style=" background: #841851;
    color: white;
    MARGIN-TOP: 25PX;
    BORDER: 1px solid #841851;">
    <i class="fa fa-cloud-upload"></i> صورة القاعة
</label>
<input id="file-upload"  name="pic" type="file"/>
               
             </div>
            </fieldset>
     <!--         <fieldset style="    padding: .35em .625em .75em; margin: 0 2px; border: 1px solid silver;" dir="rtl">
  <legend class="the-legend" style="width: fit-content;border: 0px;">صور القاعة</legend>

                 
</fieldset> -->
        
                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-12  " style="    text-align: center;">
                          <button type="button" class="btn btn-danger " data-dismiss="modal">إلغاء</button>
                         <button class="btn btn-info" type="reset">إعادة إدخال</button>
     <button type="submit" disabled="disabled" name="login_form_submit" class="btn btn-success" value="save">حفظ</button>
                        </div>
                      </div>

      {!!Form::close() !!}                
        </div>
                </div>
                   </div>
                </div>
        </div>
        </div>
        </div>

    </div>
 

         </div>
     </div>
  </div>
<!-- ENDModal -->


<input type="checkbox"  name="checkboxType[]" id="checkbox1" class=" checkboxTypes col-sm-6 ui-checkbox no-border" value="1" /> <label class=" col-sm-6" for="checkbox2">مفعل</label>

<input type="checkbox"  name="checkboxType[]" id="checkbox2" class=" checkboxTypes col-sm-6 ui-checkbox no-border" value="0" /> <label class=" col-sm-6" for="checkbox2">غير مفعل</label>


 <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title" >
                 <h2 style="    float: right;
    font-size: 22px; margin-bottom: 10px;">      <i  class="fa fa-university" aria-hidden="true" style="    font-size: 22px; float: right;     margin-left: 5px;
    color: #841851;"></i>   
                      
                 الإعلانات </h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"></a>
                      </li>
                     
                      <li><a class="close-link"></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                  
<a href="{{ url('/Advertisadmin') }}"" style="margin-bottom: 20px; background-color: #841851;
    padding: 9px 15px;
    border: 1px solid #841851 " > إضافة </a>

      <div class="portfolio-item-list">
        @include('Advertiser.adsPartial')
      </div>


                  </div>
                </div>
              </div>
 
  @section('other_scripts')


<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>



  <script type="text/javascript">

var editor;

  $(document).ready(function(){
     $(".types").chosen({ 
                   width: '100%',
                   no_results_text: "No Results",
                   allow_single_deselect: true, 
                   search_contains:true, });
 $(".types").trigger("chosen:updated");

 $(".Facilities").chosen({ 
                   width: '100%',
                   no_results_text: "No Results",
                   allow_single_deselect: true, 
                   search_contains:true, });
 $(".Facilities").trigger("chosen:updated");
         //change imgage    
$("#file-upload").on("change", function()
    {
    
        var files = !!this.files ? this.files : [];

        if (!files.length || !window.FileReader) 
        {
           $("#blah").css("background-image",'url(assets/img/noimage.jpg)');
        } // no file selected, or no FileReader support
 
        else if (/^image/.test( files[0].type)){ // only image file
            var reader = new FileReader(); // instance of the FileReader
            reader.readAsDataURL(files[0]); // read the local file
 
            reader.onloadend = function(){ // set image data as background of div
                $("#blah").css("background-image", "url("+this.result+")");
            }
        }
    });
/////////////////////
     // if ( $('#Pdate')[0].type != 'date' ) 
     // {
     //  $('#Pdate').datepicker();
     // }
     var $i=0;

     $('#item_id0').on('change',function(e){
      $('#quantity_id').val(0);
     });





  $('#profileForm')
        .formValidation({
            framework: 'bootstrap',

            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
               'Name':{
                validators: {

                         notEmpty: {
                            message: 'يجب ادخال الاسم'
                          }
                
                }
            },
                   'Price':{
                validators: {
                         notEmpty: {
                            message: 'يجب ادخال السعر'
                    }
                }
            }
            ,
                
                   'Capacity':{
                validators: {
                         notEmpty: {
                            message: 'يجب ادخال السعر'
                    }
                }
            }
                
            } ,submitHandler: function(form) {
        form.submit();
    }
        })
        .on('success.form.fv', function(e) {
   // e.preventDefault();
    var $form = $(e.target);

    // Enable the submit button
   
    $form.formValidation('disableSubmitButtons', false);
})
        // Add button click handler
        .on('click', '.addButton', function() {
            var $template = $('#emailTemplate'),
                $clone    = $template
                                .clone()
                                .removeClass('hide')
                                .removeAttr('id')
                                .insertBefore($template),
                $email    = $clone.find('[name="img[]"]');

            // Add new field
            $('#profileForm').formValidation('addField', $email);
        })

        // Remove button click handler
        .on('click', '.removeButton', function() {
            var $row   = $(this).closest('.form-group'),
                $email = $row.find('[name="img[]"]');
              
            // Remove element containing the email
            $row.remove();

            // Remove field
            $('#profileForm').formValidation('removeField', $email);
        });

     var table= $('.purchase').DataTable({
  
    select:true,
    responsive: true,
    "order":[[0,"asc"]],
    'searchable':true,
    "scrollCollapse":true,
    "paging":true,
    "pagingType": "simple",
      dom: 'lBfrtip',
        buttons: [
           
            
            { extend: 'excel', className: 'btn btn-primary dtb' }
            
            
        ],

fnRowCallback: function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
$.fn.editable.defaults.send = "always";

$.fn.editable.defaults.mode = 'inline';

$('.testEdit').editable({

      validate: function(value) {
        name=$(this).editable().data('name');
        if(name=="PurchesNo"||name=="Date"||name=="location")
        {
                if($.trim(value) == '') {
                    return 'Value is required.';
                  }
                    }
      
       
         if(name=="Date")
        {

       var regexp = /^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/;             
      if (!regexp.test(value)) {
            return 'This field is not valid';
        }
        
        }
    },
       
    placement: 'right',
    url:'{{URL::to("/")}}/updatePurches',
  
     ajaxOptions: {
     type: 'get',
    sourceCache: 'false',
     dataType: 'json'

   },

       params: function(params) {
            // add additional params from data-attributes of trigger element
            params.name = $(this).editable().data('name');

            // console.log(params);
            return params;
        },
        error: function(response, newValue) {
            if(response.status === 500) {
                return 'This Data Already Exist,Enter Correct Data.';
            } else {
                return response.responseText;
            }
        }
        ,
        success:function(response)
        {
            if(response.status==="You do not have permission.")
             {
              return 'You do not have permission.';
              }
        }


});

}
});

 



   $('.purchase tfoot th').each(function () {



      var title = $('.purchase thead th').eq($(this).index()).text();
               
 if($(this).index()>=1 && $(this).index()<=6)
        {

           $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
}
        

    });
  table.columns().every( function () {
  var that = this;
 $(this.footer()).find('input').on('keyup change', function () {
          that.search(this.value).draw();
            if (that.search(this.value) ) {
               that.search(this.value).draw();
           }
        });
      
    });
$('#warehouseaction').on('change',function(){
 $('#quantity').text('');
  // document.getElementById('quantity').innerHTML="";  
});
   
 });
// $('#activityitems').change(function(){
//   if ($(this).val()==0)
//   {
//   $("#Descr").removeAttr('disabled');
//   $("#Descr").attr("required","required");
//   }
//   else
//   {
//      $("#Descr").attr("disabled","disabled");
//   }
 
 
//     });
$(document).on('change','.checkboxTypes',function(){
    //alert($(this).val());
     data_ids = [];
    if($(".checkboxTypes:checked").length > 0)
    {
      $(".checkboxTypes:checked").each(function() {
        data_ids.push($(this).val());
      });
      $.get("/ads/types",{data_ids},function(data){
        $('.portfolio-item-list').empty();
        $('.portfolio-item-list').html(data);
        console.log(data);
      });
    }
    else
    {
      $.get("/ads/types",{data_ids},function(data){
        $('.portfolio-item-list').empty();
        $('.portfolio-item-list').html(data);
        console.log(data);
      });
    }
  });
  </script>

@stop

@stop
