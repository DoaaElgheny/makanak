@extends('master')
@section('content')
    <!--Pricing-->
    <title> الاماكن</title>
   <style>

.the-fieldset {
        direction: rtl;
    }
.wizard-card .picture {
    width: 195px;
    height: 195px;
    background-color: #999999;
    border: 4px solid #CCCCCC;
    color: #FFFFFF;
   border-radius: 0;
    margin-right: 0px;
    margin-top: 60px;
    overflow: hidden;
    transition: all 0.2s;
    -webkit-transition: all 0.2s;
}
  
    p {
         font-size: 16px;
    font-weight: bold;
    } 
    .the-fieldset {
            margin-bottom: 20px;
    }
</style>
      
      


{!! Form::open(['id'=>'adsForm','class'=>'form-horizontal','method'=>'POST','action'=>['AdvertisesController@store'],'files' => true]) !!}

<div class="col-md-12 ">


  <fieldset class="the-fieldset">
  
                         
       
                            <div class="col-sm-5 ">
                            
                             <div class="col-sm-12 col-sm-offset-1 wizard-card">
                                     <div class="picture-container">
                                          <div class="picture">
                                              <img src="assets/img/default-avatar.png" class="picture-src" id="wizardPicturePreview" title=""/>
                                              <input type="file" name="photo" id="wizard-picture">
                                          </div>

                                         
                                      </div>
                                  </div>     
                            
                            
                            </div> 
 
                            
                            <div class="col-sm-7 ">
                            
                                       
       
        
                         <div class="col-sm-12 ">
      <p>إسم الإعلان</p>
     
        <input type="text" name="name" id="name" data-rule="required" data-msg="Vérifiez votre saisie sur les champs : Le champ 'Société' doit être renseigné."   style="border: 1px #841851 solid;
    width: 290px; margin-bottom: 30px;"/>
                <div class="validation"></div>
      </div>

                 
                           
     

      <div class="col-sm-12 ">
      <p>تفاصيل الإعلان </p>
   
                <textarea name="description" rows="14" data-rule="required" data-msg="Vérifiez votre saisie sur les champs : Le champ 'Message' doit être renseigné." style="border: 1px #841851 solid;
    width: 290px; 
    height: 100px; margin-bottom: 30px;"></textarea>
                <div class="validation"></div>
      </div>
                           
     
                            
                            
                            </div>   
                            
                           
                        </fieldset>


                        
       

</div>
<div>
  <button type="submit" class="btn btn-primary"> Submit</button>
</div>

<div class="row">
    <div class="col-md-4 col-md-offset-4 error">
        <ul>
            @foreach($errors->all() as $error)
                <li>{{$error}}</li>
            @endforeach
        </ul>
    </div>
</div>
{!!form::close()!!}





@stop