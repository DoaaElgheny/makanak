@extends('master')
@section('content')
<!DOCTYPE html>
<meta name="csrf-token" content="{{ csrf_token() }}" />

<section class="panel-body">
     <style type="text/css">
   
       #Mytable tr {
            color: #841851;
        
    }

    #Mytable th {
    text-align: right;
    width: 220px;

}
         .btn-bg {
    border: 1px solid #841851;
    border-radius: 0px;
    color: #fff !important;
    padding: 10px 20px;
    font-weight: bold !important;
    font-size: 14px;
    background-color: #841851;
}
     .btn-bg:hover, .btn-bg:focus{
	border: 1px solid #841851;
	color: white !important;
   background-color:#69053a;
}
         .form-control-feedback.right {
    border-left: 1px solid #841851;
    right: 10px;
    height: 33px;
    top: -7px;
    text-align: center;
}
         
         
</style>
 <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title"  >
            <h2 style="    float: right;
    font-size: 22px; margin-bottom: 10px;">      <i class="fa fa-bars" aria-hidden="true" style="    font-size: 22px; float: right;     margin-left: 5px;
    color: #841851;"></i>   
                        
                       تقرير القاعات </h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"></a>
                      </li>
                     
                      <li><a class="close-link"></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                       <br>
                      <button class="btn btn-bg" id="Export" style="    padding-left: 30px;
    padding-right: 30px;
    margin-bottom: 30px;padding-top: 7px;
    padding-bottom: 7px;
    border-radius: 5px;"
  >Excel</button>
 <fieldset style="    padding: .35em .625em .75em; margin: 0 2px; border: 0px solid silver;" dir="rtl">
 
 


     
                    <div class="form-group">

<div class="col-md-4 col-sm-4 col-xs-12" dir="rtl">
                        <span class=" form-control-feedback right  " aria-hidden="true"  style="    background: #841851;">
    <i style="color:white; margin-top: 9px; margin-right: 1px;" class="fa fa-calendar-o"></i></span>

                          <input type="text" class="form-control has-feedback-right" id="End_Date" name="End_Date" placeholder="الي تاريخ" required>
                        </div>


               
<div class="col-md-4 col-sm-4 col-xs-12" dir="rtl">
                        <span class=" form-control-feedback right  " aria-hidden="true" style="    background: #841851;"> <i style="color:white; margin-top: 9px;margin-right: 1px; " class="fa fa-calendar-o"></i></span>

                          <input type="text" class="form-control has-feedback-right" id="Start_Date" name="Start_Date" placeholder="من تاريخ" required>
                        </div>

                 
    <div class="col-md-4 selectContainer">
    <div class="input-group">
        <span class="input-group-addon" style="background-color:#841851; "><i style="color:white; " class="glyphicon glyphicon-list"></i></span>
 
     {!!Form::select('HallID',([null => 'اختر قاعة'] + $Halls->toArray()) ,null,['class'=>'form-control selectpicker','id' => 'HallID'])!!}


  </div>

   <div >
        <label style="color:red" id="warn"></label> 
     </div>

</div>



                      </div>
  
                    
       <div >
        <label style="color:red" id="warn"></label> 
     </div>
<div class="row" style="margin-top: 20px;" align="center" >
                                <div  >
            <input type="button" class="btn btn-bg btn-lg" onclick="ShowReport();" id="Search" value=" عرض التقرير" title="عرض التقرير">
              
            </div>  
            </div>
</fieldset>
 

                     
<table class="table table-hover table-bordered  dt-responsive nowrap display tasks" id="tasks" cellspacing="0" width="100%">
<!--  <thead>
         
          <th>Invoice No</th>
          <th>Date</th>
          <th>Warehouses Name</th> 
           <th>Item English Name</th> 
           <th>Item Name</th>   
          <th>quantity</th>        
      </thead> -->
                     
      <tbody style='text-align:center;' id="Mytable">
      </tbody>
</table>
  
  </div>
    </div>

  </div>

</section>
  

 @section('other_scripts')
<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>

<script type="text/javascript">
$(function(){
  $('#Start_Date').daterangepicker({
        singleDatePicker: true,
        singleClasses: "picker_4",
         locale: {
            format: 'YYYY-MM-DD'
        }
      }, function(start) {
        console.log(start.toISOString(), 'Hadeel');
      });
  $('#End_Date').daterangepicker({
        singleDatePicker: true,
        singleClasses: "picker_4",
         locale: {
            format: 'YYYY-MM-DD'
        }
      }, function(start) {
        console.log(start.toISOString(), 'Hadeel');
      });

 });
function ShowReport()
{
    if($('#Start_Date').val() <= $('#End_Date').val())
  {
   
   if($('#Start_Date').val()!="" && $('#End_Date').val()!="" )
   {
document.getElementById("warn").innerHTML="";
 $.ajax({
      url: "/ShowReport",
        data:{End_Date:$('#End_Date').val(),Start_Date:$('#Start_Date').val(),HallID:$('#HallID').val()},
      type: "Get",
     success: function(data){
       $("#Mytable").empty(); 
         Nstring="<tr><th>السعر</th><th>اسم الحجز</th><th> اسم القاعة</th> <th>التاريخ</th></tr>";
   $("#Mytable").append(Nstring);

    var Newstring="";
   for (var ke in data) {
       if (data.hasOwnProperty(ke)) {

        Newstring+="<tr>";
        Newstring+='<td rowspan='+data[ke].rowspan+'>'+data[ke].Price+'</td>';
        Newstring+='<td rowspan='+data[ke].rowspan+'>'+data[ke].ResrveName+'</td>';
         Newstring+='<td rowspan='+data[ke].rowspan+'>'+data[ke].Name+'</td>';
         Newstring+='<td rowspan='+data[ke].rowspan+'>'+data[ke].Date_From+'</td>';
      
       
        

}

}
   $("#Mytable").append(Newstring);      
         

      }

    }); 
  
 }
else

document.getElementById("warn").innerHTML="ادخل تاريخ اولا";
}
else
document.getElementById("warn").innerHTML="تاريخ البداية يجب ان يكون  بعد تاريخ النهاية";
 }
 



</script>


<script>
  
      $(function() {
        $("#Export").click(function(){
        $("#tasks").table2excel({
          exclude: ".noExl",
          name: "Excel Document Name",
          filename: "Report",
          fileext: ".xls",
          exclude_img: true,
          exclude_links: true,
          exclude_inputs: true
        });
         });
      });
    </script>
  @stop

@stop
