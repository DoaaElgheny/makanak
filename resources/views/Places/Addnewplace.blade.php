@extends('master')
@section('content')
 <style type="text/css">
  #blah{
  background: url(assets/img/noimage.jpg) no-repeat;
  background-size: cover;
  height: 80px;
  width: 80px;
}
</style>
        <div class="row">
          <div >
            <h4 class="modal-title span7 text-center" id="myModalLabel"><span class="title">Add New Place</span></h4>
          </div>
          <div class="modal-body">
            {!! Form::open(['action' => "PlaceController@createNewPlaces",'method'=>'post','id'=>'profileForm', 'files' => 'true'])  !!}  
              <input type="hidden" name="_token" value="{{ csrf_token() }}">
              <div class="form-group col-md-12 not-existed"> 
                <label  class="control-label col-md-3">Place Name:</label>        
                <div class="col-md-9"> 
                  <input type="text" class="form-control" id="Name" name="Name" placeholder="Enter Place Name" required>
                </div>
              </div>

              <div class="form-group col-md-12 existed" style="display: none;"> 
                <label  class="control-label col-md-3">Place Name:</label>        
                <div class="col-md-9"> 
                  {!! Form::select('place_id', $places->pluck('Name','id'), null, ['class' => 'form-control select2','placeholder'=>'Select Place']) !!}
                </div>
              </div>

              <div class="form-group col-md-12"> 
                <label  class="control-label col-md-3">Existed Place:</label>        
                <div class="col-md-9"> 
                  <input type="hidden" class="form-control" value="0" name="existed" >
                  <input type="checkbox" class="form-control" id="existed_place" value="1" name="existed" >
                </div>
              </div>

              <div class="form-group col-md-12">
                <label for="input_cement_name" class="control-label col-md-3">Hall Name:</label>
                <div class="col-md-9">
                  <input type="text" class="form-control" id="HallName" name="HallName" placeholder="enter Name Hall" required>
                </div>
              </div>
              <div class="form-group col-md-12">
                <label for="input_cement_name" class="control-label col-md-3">Capacity:</label>
                <div class="col-md-9">
                  <input type="number" class="form-control" id="Capacity" name="Capacity" placeholder="enter Capacity" required>
                </div>
              </div>
              <div class="form-group col-md-12">
                <label for="input_cement_name" class="control-label col-md-3">Adress:</label>
                <div class="col-md-9">
                  <input type="text" class="form-control" id="Address" name="Address" placeholder="enter Address" required>
                </div>
              </div>
              <div class="form-group col-md-12">
                <label for="input_cement_name" class="control-label col-md-3">Description:</label>
                <div class="col-md-9">
                  <input type="text" class="form-control" id="Description" name="Description" placeholder="enter Description" required>
                </div>
              </div>
                <div class="form-group col-md-12">
                 <div class="col-md-9" dir="rtl">
 <div id="blah"></div>
            </div>
    <label for="file-upload" class="custom-file-upload" style=" background:#841851;color: white; MARGIN-TOP : 25PX;BORDER: 1px solid #841851;">
    <i class="fa fa-cloud-upload"></i> صورة المكان
</label>
<input id="file-upload"  name="pic" type="file"/>
               
             </div>
               <div class="form-group col-md-12">
                <label for="input_cement_name" class="control-label col-md-3">Price:</label>
                <div class="col-md-9">
                  <input type="number" class="form-control" id="Price" name="Price" placeholder="enter Price" required>
                </div>
              </div>
              <div class="form-group col-md-12">
                <label for="input_cement_name" class="control-label col-md-3">Type:</label>
                <div class="col-md-9">
                  {!!Form::select('Type',([null => 'please chooose'] + $types->toArray()) ,null,['class'=>'form-control','required'=>'required'])!!}
                </div>
              </div>
              <div class="form-group col-md-12">
                <label for="input_cement_name" class="control-label col-md-3">Government:</label>
                <div class="col-md-9">
                  {!!Form::select('Government',([null => 'please chooose'] + $Government->toArray()) ,null,['class'=>'form-control  Government','id' => 'Government','required'=>'required'])!!}
                </div>
              </div>
              <div class="form-group col-md-12">
                <label for="input_cement_name" class="control-label col-md-3">District:</label>
                <div class="col-md-9">
                  <select class="form-control District" name="district" id="prettify" style="border: 1px #841851 solid;
                    width: 290px;">
                    <option></option>
                  </select>
                 {{-- {!!Form::select('district', ([null => 'Select District '] + $District->toArray()) ,null,['class'=>'form-control District','id' => 'prettify','required'=>'required'])!!}--}}
                </div>
              </div>
              <hr/>
              <button type="submit" class="btn btn-primary "style="float:right" name="login_form_submit" value="Save">Save</button>
            {!!Form::close()!!}
          </div>
        </div>
 @section('other_scripts')


<script type="text/javascript">
  $.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  });
</script>
<script type="text/javascript">
  $(document).ready(function()
    {  
             //change imgage    
$("#file-upload").on("change", function()
    {
    
        var files = !!this.files ? this.files : [];

        if (!files.length || !window.FileReader) 
        {
           $("#blah").css("background-image",'url(assets/img/noimage.jpg)');
        } // no file selected, or no FileReader support
 
        else if (/^image/.test( files[0].type)){ // only image file
            var reader = new FileReader(); // instance of the FileReader
            reader.readAsDataURL(files[0]); // read the local file
 
            reader.onloadend = function(){ // set image data as background of div
                $("#blah").css("background-image", "url("+this.result+")");
            }
        }
    });

        $('#profileForm')
        .formValidation({
        framework: 'bootstrap',
        icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
              },
              fields: { 
                        'Price': 
                        {
                          validators: 
                          {
                            numeric:
                            {
                              message: 'The value is not a number'
                            }
                          }
                        },     
                        'Capacity': 
                        {
                          validators: 
                          {
                            numeric:
                            {
                              message: 'The value is not a number'
                            }
                          }
                        },
                        // 'Name':
                        // {
                        //   validators: 
                        //   {
                        //     remote: {
                        //     url: '/checkPlaceName',
                        //     message: 'The  Name is not available',
                        //     type: 'GET'
                        //     },
                        //     notEmpty:
                        //     {
                        //       message:"It is required"
                        //     },
                        //     regexp:
                        //     {
                        //       regexp: /^[\u0600-\u06FFA-Za-z\s]+$/,
                        //       message: 'The Name Place can only consist of Alphabit'
                        //     }
                        //   }
                        // } 
                        // ,
                        'HallName':
                        {
                          validators: 
                          {
                            notEmpty:
                            {
                              message:"It is required"
                            },
                            regexp:
                            {
                              regexp: /^[\u0600-\u06FFA-Za-z\s]+$/,
                              message: 'The Hall Name can only consist of Alphabit'
                            }
                          }
                        } 
                       
              } ,submitHandler: function(form) {
              form.submit();
            }
          })
          .on('success.form.fv', function(e)
          {
            // e.preventDefault();
            var $form = $(e.target);
            // Enable the submit button 
            $form.formValidation('disableSubmitButtons', false);
          });
        });

        $('#existed_place').on('change',function(){
          if(this.checked)
          {
            $('.not-existed').css('display','none');
            $('.existed').css('display','block');
          }
          else
          {
            $('.not-existed').css('display','block');
            $('.existed').css('display','none');
          }
        });

      $('#Government').on('change',function(){
      var id = $(this).val();
      $.ajax({

        url : '/getDists/'+id,
        type : 'GET',
        success : function(data) {   
        var $select = $('#prettify'); 

        $select.find('option').remove();
        data.forEach(function(element)
        {
            console.log(element.id);
            $select.append('<option value='+element.id +'>'+element.Name+'</option>')
          });
        },
        error : function(request,error)
        {
            alert("Request: "+JSON.stringify(request));
        }
      });
    });
          </script>

@stop
@stop