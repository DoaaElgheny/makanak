@extends('master')
@section('content')
 <section class="panel-body">

 <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2 style="padding-left: 80%"> اماكن لم يتم تفعيلها <i class='fa fa-tasks'></i></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"></a>
                      </li>
                     
                      <li><a class="close-link"></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">




 @if(Session::has('flash_message'))
    <div class="alert alert-danger">
        {{ Session::get('flash_message') }}<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    </div>
@endif

  

     
 
                   
                    <table class="table table-hover table-bordered  dt-responsive nowrap  users" cellspacing="0" width="100%" dir="rtl">


  <thead>
  <th>No</th>
          <th>Name</th>
          
           <th>Telephone</th>
      
        <!--   <th>Description</th> -->
          
        <!--   <th>District</th> -->
           <th>Owner Name</th>
          <th>Owner Email</th>
          <th>Owner Telephone</th>
          <th>Facebook</th>
          <th>Address</th>
          <th>Youtube</th>
          <th>website</th>
          <th>UserName</th>
          <td>Password</td>
              <th>Email</th>
          <th></th>

</tfoot>


<tbody style="text-align:center;">
<?php $i=1;?>
     @foreach($places as $place)
     @if($place->IsActive==0)
          <tr style="background-color:#ea82b8 ">
          @else
          <tr style="background-color:#f8d6e8">
    @endif
           <td>{{$i++}}</td>   
          <td>{{$place->Name}}</td>
         
           <td>{{$place->Tele}}</td>
          <td>{{$place->OwnerName}}</td>
          <td>{{$place->EmailOwner}}</td>
          <td>{{$place->TelOwner}}</td>
          <td>{{$place->Facebook}} </td>
           <td>{{$place->Address}}</td>
          <td>{{$place->Youtube}} </td>
          <td>{{$place->website}}</td>
         
            <td>{{$place->UserName}}</td>
           <td>{{$place->Password}}</td>
           <td>{{$place->EmailUser}}</td>

          
           

          <td>
          @if($place->IsActive==0)
            <a href="/Approveplace/{{$place->id}}" class="btn btn-default btn-sm" style="background-color:#841851 ;color: white">تفعيل  </a>
          @else
            <a href="/deactivePlace/{{$place->id}}" class="btn btn-default btn-sm" style="background-color:#841851 ;color: white">تعطيل  </a>
          @endif
<a href="/Approveplace/destroy/{{$place->id}}" class="glyphicon glyphicon-trash"></a>
<a href="placHalls/{{$place->id}}" class="btn btn-default btn-sm" style="background-color:#841851 ;color: white">القاعات</a>
</td>

</td>

         
          </tr>
        @endforeach
</tbody>
</table>

                  </div>
                </div>
              </div>
 
  @section('other_scripts')


<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>



  <script type="text/javascript">


  $(document).ready(function(){


     var table= $('.users').DataTable({
  
    select:true,
    responsive: true,
    "order":[[0,"asc"]],
    'searchable':true,
    "scrollCollapse":true,
    "paging":true,
    "pagingType": "simple",
      dom: 'lBfrtip',
        buttons: [
           
            
            { extend: 'excel', className: 'btn btn-primary dtb' }
            
            
        ],

fnRowCallback: function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
$.fn.editable.defaults.send = "always";

$.fn.editable.defaults.mode = 'inline';

$('.testEdit').editable({

      validate: function(value) {
        name=$(this).editable().data('name');
        if(name=="name"||name=="username"||name=="email"||name=="role")
        {
                if($.trim(value) == '') {
                    return 'Value is required.';
                  }
                    }
        if(name=="name"||name=="username")
        {

      var regexp = /^[a-zA-Z\u0600-\u06FF,-][\sa-zA-Z\u0600-\u06FF,-]+ {1}[a-zA-Z\u0600-\u06FF,-][\sa-zA-Z\u0600-\u06FF,-]/;            if (!regexp.test(value)) {
            return 'This field is not valid';
        }
        
        }
        },

    placement: 'right',
    url:'{{URL::to("/")}}/fupdate',
  
     ajaxOptions: {
     type: 'get',
    sourceCache: 'false',
     dataType: 'json'

   },

       params: function(params) {
            // add additional params from data-attributes of trigger element
            params.name = $(this).editable().data('name');

            // console.log(params);
            return params;
        },
        error: function(response, newValue) {
            if(response.status === 500) {
                return 'This Data Already Exist,Enter Correct Data.';
            } else {
                return response.responseText;
            }
        }


});

}
});

 



   $('.users tfoot th').each(function () {



      var title = $('.users thead th').eq($(this).index()).text();
               
 if($(this).index()>=1 && $(this).index()<=14)
        {

           $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
}
        

    });
  table.columns().every( function () {
  var that = this;
 $(this.footer()).find('input').on('keyup change', function () {
          that.search(this.value).draw();
            if (that.search(this.value) ) {
               that.search(this.value).draw();
           }
        });
      
 
   });


    $('[data-toggle="popover"]').popover({ trigger: "hover" }); 

 

    $('[data-toggle="popover"]').popover({ trigger: "hover" }); 
 });
  </script>
@endsection
@endsection