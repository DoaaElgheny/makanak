@extends('master')
@section('content')
 <section class="panel-body">
 <style type="text/css">
    #blah{
            margin-top: 1px;
  background: url('{{$placedetails->Logo}}') no-repeat;
        background-size: cover;}


        .the-fieldset p {
    margin: 0px;
    font-weight: 500;
    line-height: 2;
    color: black;
    font-weight: bold;
}
     .the-fieldset input {
    border-radius: 0px 5px 5px 0px;
    border: 1px solid #eee;
    width: 75%;
    height: 30px;
    float: left;
    padding: 0px 15px;
}
     .the-fieldset i {
    color: #841851;
}
     
     .the-fieldset .icon-case {
    width: 35px;
    float: left;
    border-radius: 5px 0px 0px 5px;
    background: #eeeeee;
    height: 30px;
    position: relative;
    text-align: center;
    line-height: 30px;
}
     .btn-bg {
    border: 1px solid #841851;
    border-radius: 0px;
    color: #fff !important;
    padding: 10px 20px;
    font-weight: bold !important;
    font-size: 14px;
    background-color: #841851;
}
     .btn-bg:hover, .btn-bg:focus{
	border: 1px solid #841851;
	color: white !important;
   background-color:#69053a;
}
     .wizard-card .picture  {
         
        width: 200px;
    height: 180px;
         border-radius: 50%;
         
     }
         .wizard-card .picture:hover {
  border-color:  #841851;
}

</style>
 <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                   
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"></a>
                      </li>
                     
                      <li><a class="close-link"></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">



<div class="col-md-12 " style="    margin-bottom: 5px;" dir="rtl">
 @if(Session::has('flash_message'))
    <div class="alert alert-success">
        {{ Session::get('flash_message') }}<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    </div>
@endif
  <fieldset class="the-fieldset"  style="margin-bottom: 15px; ">

                            <legend class="the-legend">بيانات المكان</legend>
  {!!Form::open(['route' =>['PlaceData.update',$placedetails->id],'method' => 'put','id'=>'profileForm','files'=>'true']) !!}
           <input type="hidden" name="_token" value="{{ csrf_token() }}">
  <input type="hidden" id="id" name="id" value="{{$placedetails->id}}" required>
<div class="col-sm-5 col-sm-offset-1">
                            
                             <div class="col-sm-12 col-sm-offset-1 wizard-card">
                                   
                                       <div class="picture-container" >
                                          <div class="picture" id="blah" style="    margin-right: 100px;

    margin-top: 0px;
    margin-bottom: 10px;"  >
                                              <input id="file-upload"  name="pic" type="file"/>
                                          </div>  
                                             <h6 class=" hh6" style="  font-size: 17px;
    margin-top: 3px;
    text-align: center;
   width: 120px;
    margin-right: 130px;
                                                                         font-weight: bold;">صورة اللوجو</h6>
                                      </div>
                                  </div>     
                            
                            
                            </div> 
                            <div class="col-sm-5 col-sm-offset-1">
                            
                                       
       
        
                         <div class="col-sm-12 col-sm-offset-1">
      <p>إسم المكان</p>
      <span class="icon-case"><i class="fa fa-home"></i></span>
        <input type="text" name="Name"  id="PlaceName" value="{{$placedetails->Name}}" data-msg="ادخل اسم المكان" required="required" />
                <div class="validation"></div>
      </div>

                      
      <div class="col-sm-12 col-sm-offset-1">
      <p>عنوان المكان </p>
      <span class="icon-case"><i class="fa fa-location-arrow"></i></span>
        <input type="text" name="address"  id="adresse" value="{{$placedetails->Address}}" />
                <div class="validation"></div>
      </div>       
                            
                           
                            
                            
           

      <div class="col-sm-12 col-sm-offset-1">
      <p>عن المكان</p>
      <span class="icon-case"><i class="fa fa-comments-o"></i></span>
            <input name="Description" rows="14" value="{{$placedetails->Description}}">
                <div class="validation"></div>
      </div>
    
                            
                            
                            </div>   
                            
                            
                            
 
                        </fieldset>


                        <fieldset class="the-fieldset" style="margin-bottom: 15px;">
                            <legend class="the-legend"> مسؤول المكان</legend>
                           
            <div class="col-sm-3 col-sm-offset-1">
      <p>الإسم</p>
      <span class="icon-case"><i class="fa fa-user" aria-hidden="true"></i></span>
        <input type="text" name="OwnerName"  id="ville" value="{{$placedetails->OwnerName}}" required="required"/>
                <div class="validation"></div>
      </div>  
                            
               
                           
                            
            
              <div class="col-sm-3 col-sm-offset-1">
      <p>رقم التليفون</p> 
      <span class="icon-case"><i class="fa fa-phone"></i></span>
        <input type="number" name="TelOwner" id="TelOwner" value="{{$placedetails->TelOwner}}"  required="required" />
                <div class="validation"></div>
      </div>
                
                <div class="col-sm-3 col-sm-offset-1">
      <p>البريد الإلكتروني </p> 
      <span class="icon-case"><i class="fa fa-envelope-o"></i></span>
                <input type="email" name="EmailOwner"  id="email" value="{{$placedetails->EmailOwner}}"/>
                <div class="validation"></div>
      </div>  
     
                        </fieldset>
                  
  

   <fieldset class="the-fieldset">
                            <legend class="the-legend"> بيانات التواصل</legend>
                      
                            
                            <div class="col-sm-5 col-sm-offset-1" >

      <div class="col-sm-12 col-sm-offset-1">
      <p>الموقع الالكتروني</p> 
                    <span class="icon-case"><i class="fa fa-facebook"></i></span>
                <input type="text" name="website" id="email" value="{{$placedetails->website}}"/>
                <div class="validation"></div>
      </div>
        
    <div class="col-sm-12 col-sm-offset-1">
      <p> رابط اليوتيوب  </p>  
                    <span class="icon-case"><i class="fa fa-youtube" aria-hidden="true"></i></span>
                <input type="text" name="Youtube" id="email" value="{{$placedetails->Youtube}}"  />
                <div class="validation"></div>
      </div>
        <div class="col-sm-12 col-sm-offset-1">
      <p> رابط  تويتر </p> 
                    <span class="icon-case"><i class="fa fa-twitter" aria-hidden="true"></i></span>
                <input type="text" name="Twitter" id="email"  value="{{$placedetails->Twitter}}"/>
                <div class="validation"></div>
      </div> 
                            
                            
                          
                            
                            
                            </div>
                            
                                <div class="col-sm-5 col-sm-offset-1">
                            
                      
                   <div class="col-sm-12 col-sm-offset-1">
      <p>تليفون 1</p> 
      <span class="icon-case"><i class="fa fa-phone"></i></span>
        <input type="number" name="Tele"  id="phone" value="{{$placedetails->Tele}}" required="required"/>
                <div class="validation"></div>
      </div>

                      <div class="col-sm-12 col-sm-offset-1">
      <p>تليفون 2</p> 
      <span class="icon-case"><i class="fa fa-phone"></i></span>
        <input type="number" name="Tele2" id="phone2" value="{{$placedetails->Tele2}}" />
                
      </div>
                         <div class="col-sm-12 col-sm-offset-1">
      <p>البريد الإلكتروني </p> 
      <span class="icon-case"><i class="fa fa-envelope-o"></i></span>
                <input type="email" name="Email"  id="email" value="{{$placedetails->Email}}"></div>
<div class="col-sm-12 col-sm-offset-1">
      <p> رابط  الفيسبوك </p>  
                    <span class="icon-case"><i class="fa fa-facebook"></i></span>
                <input type="Url" name="Facebook" id="email" value="{{$placedetails->Facebook}}" required="required"/>
                <div class="validation"></div>
      </div>
      </div>      
    </fieldset>

</div>

<div class="col-md-6 col-xs-6 btn-trial3" style="    margin-top: 10px;
    margin-bottom: 15px;">

 </div>

<div class="col-md-6 col-xs-6 btn-trial4" style="    margin-left: 450px;    margin-bottom: 10px;"><!-- 
   <a href="/PlaceData" class="btn btn-bg btn-lg">الغاء </a>  -->
  <button type="submit"  name="submitButton"   class="btn btn-bg btn-lg">تعديل </button> 
     
</div>



             
               
              
          
 

{!!Form::close() !!}
</div>
    
 
                   
                   

                  </div>
                </div>
              </div>
 
  @section('other_scripts')


<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>



    <script type="text/javascript">



  $(document).ready(function(){

    
     //change imgage    
$("#file-upload").on("change", function()
    {
    
        var files = !!this.files ? this.files : [];

        if (!files.length || !window.FileReader) 
        {
           $("#blah").css("background-image",'url(assets/UserFiles/img/noimage.jpg)');
        } // no file selected, or no FileReader support
 
        else if (/^image/.test( files[0].type)){ // only image file
            var reader = new FileReader(); // instance of the FileReader
            reader.readAsDataURL(files[0]); // read the local file
 
            reader.onloadend = function(){ // set image data as background of div
                $("#blah").css("background-image", "url("+this.result+")");
            }
        }
    });
/////////////////////
});
//Form Validation




</script> 
@stop
@stop