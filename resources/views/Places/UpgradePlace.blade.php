@extends('master')
@section('content')
<meta name="csrf-token" content="{{ csrf_token() }}" />
<section class="panel panel-primary" style="    border-color: #841851;
    
    padding-top: 0px;
    height: 200px;
    margin-top: 90px;
                                                margin-left: 20px;
    margin-right: 20px;">
<div class="panel-body" style="    text-align: center;
    margin-top: 50px;
    padding-top: 0px;
    padding-bottom: 0px;
    margin-bottom: 0px;"> 
<div class="flash-message" >
   
      @if(Session::has('message'))

      <p class="alert alert-danger" style="background-color: #841851;">{{ Session::get('message') }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
      @endif
   
  </div>
   {!! Form::open(['action' => "PlaceController@UpgradePlace",'method'=>'post','id'=>'profileForm']) !!} 

      
   


@foreach($result as $Pack1)
<div class="col-md-4">{{$Pack1['Name']}}
        <input name="Packge_id" checked="{{$Pack1['checked']}}" type="radio" value="{{$Pack1['id']}}" >
        </div>   
@endforeach 

<br/><br/><br/>  
   <div class="col-md-4"></div>  
<div class="col-md-4 col-xs-6 btn-trial4" style="">
  <button type="submit"  name="submitButton"   >ترقية </button> 
     

<br/><br/><br/>
<br/><br/><br/>
<br/><br/><br/>
<!-- <button type="submit" style="float: left;background-color:#841851;color:white; " class="col-md-3  btn-trial1"  name="login_form_submit" value="Save">ترقية </button> -->

{!!Form::close()!!}
</div>
    <div class="col-md-4"></div> 
<script type="text/javascript">

  $(document).ready(function(){

     $('#startdate').datepicker({
     language: 'ar',
     format: "yyyy-mm-dd"
});
          $('#Government').change(function(){
        $.get("{{ url('api/newdropdown')}}", 
        { option: $(this).val() }, 
        function(data) {
            $('#district').empty(); 
            $.each(data, function(key, element) {
                $('#district').append("<option value='" + key +"'>" + element + "</option>");
            });
        });
    });
$('.Government').chosen({
                width: '100%',
                no_results_text:'No Results'
            });
                $(".Government").trigger("chosen:updated");
            $(".Government").trigger("change");
 });
        $('#Has_Facebook').change(function(){
            if($(this).val() == "No")
            {
              $("#Facebook_Account").attr("disabled","disabled");
            }
    });
         $('#profileForm')
         .find('[name="Government"]')
        
            .chosen({
                width: '100%',
                inherit_select_classes: true
            })
          .end()
        .formValidation({

            framework: 'bootstrap',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                'Name': {
                         validators: {
              
                  notEmpty:{
                    message:"the contractor name is required"
                  },
                    regexp: {
                        regexp: /^[\u0600-\u06FF\s]+$/,
                        message: 'Name must be arabic'
                    }
                    }
               
                  },
                   'EngName': {
                  
                       validators: {
                
                    regexp: {
                        regexp: /^[a-zA-Z\s]+$/,
                        message: 'The English name can only consist of alphabetical and spaces'
                    }
                    }

                    }
                 
                ,'Government':{
                       validators: {
                  notEmpty:{
                    message:"the Governmentis required"
                  }
                    }

                    }
                    ,"Address": {
                  
                       validators: {
                    regexp: {
                        regexp: /^[\\.\/a-zA-Z\u0600-\u06FF0-9\s]+$/,
                        message: 'The Address can only consist of alphabetical and spaces'
                    }
                    }

                    },'Education': {
                  
                       validators: {
                    regexp: {
                        regexp: /^[a-zA-Z\u0600-\u06FF\s]+$/,
                        message: 'The Education can only consist of alphabetical and spaces'
                    }
                    }

                    }
                    ,'Email':{
                  
                       validators: {
                                        remote: {
                        url: '/cemexmarketring/public/checkContractorEmail',
                         data:function(validator, $field, value)
                        {return{id:validator.getFieldElements('id').val()};},
                        message: 'The email is not available',
                        type: 'GET'
                    },
                  emailAddress:{
                    message:"this Email is not valid"
                  }
                    }

                    }
                    
                    'Birthday':
                   {
                      validators: {
                     date: {
                        message: 'The date is not valid',
                        format: 'YYYY-MM-DD',
                        // min and max options can be strings or Date objects
                        min: '1950-01-01',
                        max: '2001-12-30'
                    }
                    }
                }
                    ,'Job':{
                  
                       validators: {
                 regexp: {
                        regexp: /^[a-zA-Z\u0600-\u06FF\s]+$/,
                        message: 'The Job can only consist of alphabetical and spaces'
                    }
                    }

                    },'Nickname':{
                  
                       validators: {
                 regexp: {
                        regexp:/^[a-zA-Z\u0600-\u06FF\s]+$/,
                        message: 'The Nickname can only consist of alphabetical and spaces'
                    }
                    }

                    }
                   

            } ,submitHandler: function(form) {
        form.submit();
    }
        })
        .on('success.form.fv', function(e) {
   // e.preventDefault();
    var $form = $(e.target);

    // Enable the submit button
   
    $form.formValidation('disableSubmitButtons', false);
});

</script>



</div>
</section>
@stop