<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}" />

    <title>MaKank</title>

<!-- style -->
 <link href="/assets/css/jquerysctipttop.css" rel="stylesheet" type="text/css"> 

<!-- formvalidation -->


 <link href="/assets/css/jquerysctipttop.css" rel="stylesheet" type="text/css"> 
<!-- query-builder -->

    <link rel="stylesheet" type="text/css" href="/assets/css/query-builder.default.min.css">

<!-- Ionicons -->
  <link rel="stylesheet" href="/assets/css/ionicons.min.css">
<!-- Chosen -->
  <link rel="stylesheet" href="/assets/css/chosen.min.css" />

 <!-- datepicker --> 
<link rel="stylesheet" href="/assets/css/datepicker.min.css" />
<link rel="stylesheet" href="/assets/css/datepicker3.min.css"/>
<!-- buttons.jqueryui --> 
<link rel="stylesheet" type="text/css" href="/assets/css/buttons.jqueryui.min.css">
<!-- bootstrap-horizon --> 

 <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap-horizon.css"> 

    <!-- Bootstrap -->
    <link href="/bower_components/gentelella/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="/bower_components/gentelella/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="/bower_components/gentelella/vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="/bower_components/gentelella/vendors/iCheck/skins/flat/green.css" rel="stylesheet">
  
    <!-- bootstrap-progressbar -->
    <link href="/bower_components/gentelella/vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
    <!-- JQVMap -->
    {{--  <link href="/bower_components/gentelella/vendors/jqvmap/dist/jqvmap.min.css" rel="stylesheet"/>  --}}
    <!-- bootstrap-daterangepicker -->
    <link href="/bower_components/gentelella/vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">

<!-- Datatables -->
   <link href="/assets/css/bootstrap-editable.css" rel="stylesheet"/>
    <link rel="stylesheet" type="text/css" href="/assets/css/datatables.min.css"/>
<!-- <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css"/> -->
<link rel="stylesheet" type="text/css" href="/assets/css/dataTables.bootstrap.min.css"/>
<link rel="stylesheet" type="text/css" href="/assets/css/jquery.dataTables.min.css"/>


<link rel="stylesheet prefetch" href="/assets/css/dataTables.responsive.css">
<link rel="stylesheet" type="text/css" href="/assets/css/responsive.bootstrap.min.css"/>
<link rel="stylesheet" href="/assets/formvalidation/css/formValidation.min.css">
   
<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Open+Sans|Candal|Alegreya+Sans">
    <link rel="stylesheet" type="text/css" href="/assets/UserFiles/css/font-awesome.min.css">


 <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Open+Sans|Candal|Alegreya+Sans">

    <link href="https://fonts.googleapis.com/css?family=Cairo" rel="stylesheet">
    <!-- Custom Theme Style -->
    <link href="/bower_components/gentelella/build/css/custom.min.css" rel="stylesheet">
    <style type="text/css">  
     input[type="file"] {
    display: none;
   }
.custom-file-upload {
    border: 1px solid #ccc;
    display: inline-block;
    padding: 6px 12px;
    cursor: pointer;
}


.wizard-card .picture-container {
  position: relative;
  cursor: pointer;
  text-align: center;
}

.wizard-card .picture {
    width: 165px;
    height: 165px;
    background-color: #999999;
    border: 4px solid #CCCCCC;
    color: #FFFFFF;
    border-radius: 50%;
    /* margin: 2px auto; */
    margin-right: 0px;
    margin-top: 60px;
    overflow: hidden;
    transition: all 0.2s;
    -webkit-transition: all 0.2s;
}
.wizard-card .picture:hover {
  border-color:  #26d797;
}
.wizard-card .picture input[type="file"] {
  cursor: pointer;
  display: block;
  height: 100%;
  left: 0;
  opacity: 0 !important;
  position: absolute;
  top: 0;
  width: 100%;
}
.wizard-card .picture-src {
  width: 100%;
}
.hh6 {
    font-size: 19px;
    margin-top: 3px;
    text-align: center;
    margin-left: 230px;
}


  .the-legend {
    border-style: none;
    border-width: 0;
    font-size: 15px;
    color: white;
    line-height: 10px;
    margin-bottom: 0;
    width: auto;
   padding:12px;
    border: 1px solid #e0e0e0;
font-weight: bold;
background-color: #841851;

}
.the-fieldset {
    border: 1px solid #e0e0e0;
    padding: 10px;
    padding-right:40px
}

body {
    color: black;
   
    font-family: 'Cairo', sans-serif;
    font-size: 15px; }
        
        .sidebar-footer a {
                width: 30%;
            background: #2A3F54;
        }

        .sidebar-footer a:hover {
    background: #2A3F54;
}
        .nav.side-menu>li>a {
    color: white;
    font-weight: 500;
            text-align: right;
}
        .panel-body {
    padding: 15px;
    margin-bottom: 82px;
}
        .right_col {
            min-height: 950px;
        }
</style>

     <!-- validator -->
   
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
      

            <!-- sidebar menu -->
           @include('sidebar')

            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
          @include('menu_footer')
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
    @include('header')

        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main" style="border: 2px solid white; min-height: 950px;" >
  @yield('content')
        </div>
        <!-- /page content -->

        <!-- footer content -->
    @include('footer')
        <!-- /footer content -->

      </div>
    </div>

    <!-- jQuery -->
    <script src="/bower_components/gentelella/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
<script type="text/javascript" src="/assets/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="/bower_components/gentelella/vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="/bower_components/gentelella/vendors/nprogress/nprogress.js"></script>
    <!-- Chart.js -->
    <!-- gauge.js -->
    <script src="/bower_components/gentelella/vendors/gauge.js/dist/gauge.min.js"></script>
    <!-- bootstrap-progressbar -->
    <script src="/bower_components/gentelella/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
    <!-- iCheck -->
    <script src="/bower_components/gentelella/vendors/iCheck/icheck.min.js"></script>
    <!-- Skycons -->
    <script src="/bower_components/gentelella/vendors/skycons/skycons.js"></script>
    {{--  <!-- Flot -->
    <script src="/bower_components/gentelella/vendors/Flot/jquery.flot.js"></script>
    <script src="/bower_components/gentelella/vendors/Flot/jquery.flot.pie.js"></script>
    <script src="/bower_components/gentelella/vendors/Flot/jquery.flot.time.js"></script>
    <script src="/bower_components/gentelella/vendors/Flot/jquery.flot.stack.js"></script>
    <script src="/bower_components/gentelella/vendors/Flot/jquery.flot.resize.js"></script>  --}}
    <!-- Flot plugins -->
    {{--  <script src="/bower_components/gentelella/vendors/flot.orderbars/js/jquery.flot.orderBars.js"></script>
    <script src="/bower_components/gentelella/vendors/flot-spline/js/jquery.flot.spline.min.js"></script>
    <script src="/bower_components/gentelella/vendors/flot.curvedlines/curvedLines.js"></script>  --}}
    <!-- DateJS -->
    <script src="/bower_components/gentelella/vendors/DateJS/build/date.js"></script>
    <!-- JQVMap -->
    <script src="/bower_components/gentelella/vendors/jqvmap/dist/jquery.vmap.js"></script>
    <script src="/bower_components/gentelella/vendors/jqvmap/dist/maps/jquery.vmap.world.js"></script>
    <script src="/bower_components/gentelella/vendors/jqvmap/examples/js/jquery.vmap.sampledata.js"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="/bower_components/gentelella/vendors/moment/min/moment.min.js"></script>
    <script src="/bower_components/gentelella/vendors/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- Datatables -->


<script type="text/javascript" src="/assets/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="/assets/js/dataTables.bootstrap.min.js"></script>
<script type="text/javascript" src="/assets/js/dataTables.responsive.min.js"></script>
<script type="text/javascript" src="/assets/js/responsive.bootstrap.min.js"></script>
<script type="text/javascript" src="/assets/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="/assets/js/buttons.print.min.js"></script>
<script type="text/javascript" src="/assets/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="/assets/js/buttons.jqueryui.min.js"></script>
<script type="text/javascript" src="/assets/js/jszip.min.js"></script>
<script type="text/javascript" src="/assets/js/pdfmake.min.js"></script> 
  <script type="text/javascript" src="/bower_components/js/fnReloadAjax.js"></script>
<script src="/assets/js/bootstrap-editable.min.js"></script>
    {{--  <script type="text/javascript" src="/bower_components/gentelella/vendors/datatables.net/js/buttons.jqueryui.min.js"></script>  --}}
<script src="/assets/formvalidation/js/formValidation.min.js"></script>
<script src="/assets/formvalidation/js/framework/bootstrap.min.js"></script>
 
    <!-- Custom Theme Scripts -->
    <script src="/bower_components/gentelella/build/js/custom.js"></script>
   <!--  Validator -->
   <script src="/bower_components/gentelella/vendors/validator/validator.js"></script>
<!-- canvasjs -->

     <script type="text/javascript" src="/assets/js/canvasjs.min.js"></script>

   <!-- datepicker -->

     <script src="/assets/js/bootstrap-datepicker.min.js"></script>
   <!-- extendext -->

     <script src="/assets/js/jQuery.extendext.js"></script>
        <!-- doT -->

<script src="/assets/js/doT.min.js"></script>
   <!-- query-builder -->

<script src="/assets/js/query-builder.min.js"></script>
   <!-- javascript links -->

<script type="text/javascript" src="/assets/js/jquery.validate.js"></script>
<script type="text/javascript" src="/assets/js/additional-methods.js"></script>
<script type="text/javascript" src="/assets/js/fnReloadAjax.js"></script>
<script type="text/javascript" src="/assets/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="/assets/js/additional-methods.min.js"></script>
   <!-- formvalidation -->

<script src="/assets/formvalidation/js/formValidation.min.js"></script>
<script src="/assets/formvalidation/js/framework/bootstrap.min.js"></script>
   <!-- tabletoexcel -->

 <script src="/assets/js/jquery.table2excel.js"></script>
   <!-- chosen -->

      <script src="/assets/js/chosen.jquery.min.js"></script>


 <!--   <script>
    $(document).ready(function() {
      setInterval( function()
 {
      $.ajax({
      url: "/NotificationStock",
      type: "get",
      success: function(data){
    for (var ke in data) {
        if (data.hasOwnProperty(ke)) {
        $('#Notify').text(data[ke].CountNotification);
        $('#headerNotification').text('لديك اصناف قاربت علي الانتهاء ');
         }
       }
     }
   
    });

},5000);
      
 

setInterval( function()
 {

       $.getJSON("/latest", function(data) {

       $count=Object.keys(data).length;

       $('#noti_Counter')
            .css({ opacity: 0 })
            .text($count)              // ADD DYNAMIC VALUE (YOU CAN EXTRACT DATA FROM DATABASE OR XML).
            .css({ top: '-10px' })
            .animate({ top: '-2px', opacity: 1 }, 500);

      

      $("#pnotifications").empty();

     var $notifications =$("#pnotifications");
   
     
     for (var i = 0; i < $count; i++) {
 

                
       $.each(data[i], function(index, value) {
       
        $notifications.append('<div class="item">'
       +' <span class="badge badge-important">'+index+'</span><a> Pormoter  '+value+'  want cement </a>'
       +'<span class="close" data-dismiss="alert" aria-label="Close" id="' +value+'" aria-hidden="true">&times;</span>'
 
       +'</div>');

         });
     }
      });

},5000);

 $('#noti_Button').click(function () {

console.log("noti_Button");
            // TOGGLE (SHOW OR HIDE) NOTIFICATION WINDOW.
            $('#notifications').fadeToggle('fast', 'linear', function () {
                if ($('#notifications').is(':hidden')) {
                    $('#bell').css('color', '#2E467C');
                }
                else $('#bell').css('color', '#fff');        // CHANGE BACKGROUND COLOR OF THE BUTTON.
            });

                         // HIDE THE COUNTER.

            return false;
        });

        // HIDE NOTIFICATIONS WHEN CLICKED ANYWHERE ON THE PAGE.
        $(document).click(function () {
            $('#notifications').hide();


            // CHECK IF NOTIFICATION COUNTER IS HIDDEN.
            if ($('#noti_Counter').is(':hidden')) {
                // CHANGE BACKGROUND COLOR OF THE BUTTON.
                $('#bell').css('color', '#2E467C');
            }
        });

        $('#notifications').click(function () {
            return false;       // DO NOTHING WHEN CONTAINER IS CLICKED.
        });

   

        $("#notifications ").on("click",'span',function(event)
 {

      $id=event.target.id; 
      console.log($id);
     event.preventDefault();
     $(this).parent().remove();

        
  
  

 $.getJSON("/updatelatest/"+$id, function(data) {
  console.log(data);

      $count=Object.keys(data).length;

      console.log($count); 

           $("#myModalLabel").empty();


          $("#modalbode").empty();


       $name='';

       for (var i = 0; i < $count; i++) 
       {


  
      
 
        $name=data[i]['Name'];
          
        $("#modalbode").append('<div class="item ">'
        +'<span>Contractor Code '+data[i]['ContractorCode']+' Want <li>'+data[i]['Products']+'</li> In Date '+data[i]['Date']+'</span>'
        +'</div>');


   

     }

        $("#myModalLabel").append('Pormoter '+$name);

      $('#myModal').modal('show');
             





});

 });
       
    });
    </script> -->
        @yield('other_scripts')
  </body>
</html>
