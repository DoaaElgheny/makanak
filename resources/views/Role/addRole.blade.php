@extends('master')
@section('content')
    @if(session()->has('message'))
        <div class="alert alert-success">
            {{ session()->get('message') }}
        </div>
    @endif
    @if($errors->any())
        <div class="alert alert-danger">
      @foreach($errors->all() as $error)
             {{$error}}</br>
             @endforeach
        </div>
    @endif
    {!! Form::open(['route' => 'addRole', 'class' => 'form-horizontal', 'role' => 'form' ]) !!}
   @include('Role.fields',['action'=>'add'])
    <div style="float: right">
    {{ Form::submit('Add Role ', ['class' => 'btn btn-success btn-xs']) }}
    </div>
    <div class="pull-left">
        {{ link_to_route('roles', 'Cancel', [], ['class' => 'btn btn-danger btn-xs']) }}
    </div><!--pull-left-->
    {!! Form::close() !!}
@endsection

