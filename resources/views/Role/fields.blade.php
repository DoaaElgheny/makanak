<div class="form-group">
    {!! Form::label('name', 'Role Name', ['class' => 'col-lg-2 control-label']) !!}

    <div class="col-lg-10">
        @if($action =='edit')
            {{ Form::text('name', $role->name, ['class' => 'form-control', 'placeholder' => 'Role Name' ,'required']) }}
            @else
        {{ Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Role Name' ,'required']) }}
            @endif
    </div>
</div>

<div class="form-group">
    {!! Form::label('name', 'Attach Permissions', ['class' => ' control-label']) !!}
            </br>
        @foreach($permissions as $label =>$groupedPermissions)
            {!! Form::label('name', "-> ".$label, ['class' => ' control-label']) !!}
        </br>
               @foreach($groupedPermissions as $permission)
                {!! Form::label('name', $permission->name, ['class' => ' control-label']) !!}
                       @if($action =='edit' && in_array($permission->id,$attachedPermissions))
                 {!! Form::checkbox('permission[]',$permission->name,'checked') !!}
                       @else
               {!! Form::checkbox('permission[]',$permission->name) !!}
                       @endif
                   </br>
                 @endforeach
         @endforeach

</div>