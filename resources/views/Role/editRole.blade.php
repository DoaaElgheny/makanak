@extends('master')
@section('content')
    @if(session()->has('message'))
        <div class="alert alert-success">
            {{ session()->get('message') }}
        </div>
    @endif
    @if($errors->any())
        <div class="alert alert-danger">
            @foreach($errors->all() as $error)
            {{$error}}</br>
            @endforeach
        </div>
    @endif
    <form method="post" action="{{route('updateRole', $role->id)}}" class="form-horizontal">
        {{ csrf_field() }}
    @include('Role.fields' , ['action'=>'edit'])
    <div style="float: right">
        {{ Form::submit('Edit Role ', ['class' => 'btn btn-success btn-xs']) }}
        <div class="pull-left">
            {{ link_to_route('roles', 'Cancel', [], ['class' => 'btn btn-danger btn-xs']) }}
        </div><!--pull-left-->
    </div>
    </form>
@endsection

