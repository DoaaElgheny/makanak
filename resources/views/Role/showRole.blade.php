@extends('master')
@section('content')

    <div class="form-group">
        {!! Form::label('name', 'Role Name', ['class' => 'col-lg-2 control-label']) !!}

        <div class="col-lg-10">
         <h3>{{$role->name}}</h3>
        </div>
    </div>

    <div class="form-group">
        {!! Form::label('name', '-> Attached Permissions', ['class' => ' control-label']) !!}
        </br>
@foreach($role->permissions as $permission)
            <h4>{{$permission->name}}</h4>
    @endforeach
    </div>
    <div class="pull-left">
        {{ link_to_route('roles', 'Cancel', [], ['class' => 'btn btn-danger btn-xs']) }}
    </div><!--pull-left-->
@endsection