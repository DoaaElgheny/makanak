@extends('master')
@section('content')
    @if(session()->has('message'))
        <div class="alert alert-success">
            {{ session()->get('message') }}
        </div>
    @endif
    @if($errors->any())
        <div class="alert alert-danger">
            @foreach($errors->all() as $error)
            {{$error}}</br>
            @endforeach
        </div>
    @endif
 <div>
     <h2>Roles</h2>
     </div>
    <h1 class="pull-right">
        <a class="btn btn-primary pull-right" style="margin-top: -10px;margin-bottom: 5px" href="{!! route('addRole') !!}">Add Role</a>
    </h1>
<table class="table table-hover table-bordered  dt-responsive nowrap  users" cellspacing="0" width="100%" dir="ltr">


    <thead>
    <th>Name</th>

    <th>Action</th>

    </thead>

    <tbody style="text-align:center;">

    @foreach($roles as $role)
          <tr>
              <td>
                  {{$role->name}}
              </td>
              <td>
                  {!! Form::open(['route' => ['deleteRole', $role->id]]) !!}
                  <div class='btn-group'>

                      <a href="{{ route('showRole', $role->id) }}" class='btn btn-default btn-xs'>
                          <i class="glyphicon glyphicon-eye-open"></i>
                      </a>
                      <a href="{{ route('editRole', $role->id) }}" class='btn btn-default btn-xs'>
                          <i class="glyphicon glyphicon-edit"></i>
                      </a>
                      {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', [
                          'type' => 'submit',
                          'class' => 'btn btn-danger btn-xs',
                          'onclick' => "return confirm('Are you sure to delete role?')"
                      ]) !!}

                  </div>
                  {!! Form::close() !!}
              </td>

            </tr>
            @endforeach
    </tbody>
</table>

    @endsection