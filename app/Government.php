<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Government extends Model
{
    public $table="governments";
     protected $fillable = [
     	'Name','lat','long',
     ];
    public function GetDistricts()
    {
    	return $this->hasMany('App\District','Government_Id');
    }
}
