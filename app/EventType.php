<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EventType extends Model
{
    //
    public $table="events_types";
    protected $fillable = [
     	'name'
     ];
}
