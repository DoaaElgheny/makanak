<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Advertise extends Model
{
   	public $table="advertises";
    protected $fillable = [
     	'name','photo','description','approval','k_index','created_by',
     ];
    public function user()
    {
    	return $this->belongsTo('App\User','created_by');
    }
}
