<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EventTarget extends Model
{
    //
    public $table="events_targets";
    protected $fillable = [
    'event_id','target_id',
    ];
    public function target()
    {
    	return $this->belongsTo('App\Target','target_id');
    }
    public function event()
    {
    	return $this->belongsTo('App\Event','event_id');
    }
}
