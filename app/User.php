<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;
use Spatie\Permission\Traits\HasPermissions;
class User extends Authenticatable
{
    use HasRoles ;

      /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public $table="users";
    protected $primaryKey='id';
    
protected $fillable = ['Name','Username', 'Email', 'password','active'];


    /*
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token','Email'
    ];
    public function GetSchadules()
    {
    	return $this->hasMany('App\Schadule','id');
    }
    public function GetDistrict()
    {
    	return $this->belongsTo('App\District','District_Id');
    }
    public function GetPlace()
    {
    	return $this->belongsTo('App\Place','Place_Id');
    }
    public function GetRoles()
    {
        return $this->belongsToMany('App\Role','user_has_roles','user_id','role_id');
    }
    public function permissions()
    {
        return $this->belongsToMany('App\Permission','user_has_permissions','user_id','permission_id');

    }
}
