<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Target extends Model
{
    //
     public $table="targets";
     protected $fillable = [
     	'name'
     ];
}
