<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\EventTarget;
class Event extends Model
{
    // 
    public $table="events";
    protected $fillable = [
     	'name','event_type','organizer_id','cover_photo','region_id','description','attendance_type','subscribe_type','price','address','capacity','fb_link','long','lat','date_from','date_to','isActive',
     ];
    public function place()
    {
    	return $this->belongsTo('App\Place','organizer_id');
    }
    public function region()
    {
    	return $this->belongsTo('App\District','region_id');
    }
    public function type()
    {
    	return $this->belongsTo('App\EventType','event_type');
    }
    public function getTargets()
    {
       	$targets= EventTarget::where('event_id','=',$this->id)->get();
    	return $targets;
    }
    public function getAttendanceType($number)
    {
    	if($number == 0)
    	{
    		return "مجانى";
    	}
    	if($number == 1)
    	{
    		return "تكلفة";
    	}
    }
}
