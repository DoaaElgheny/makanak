<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OurUserOffer extends Model
{
    
    public $table="ouruseroffer";
    public $timestamps = false;
}
