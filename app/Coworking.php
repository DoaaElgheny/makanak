<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Coworking extends Model
{
    
    public $table="coworking";
    public $timestamps = false;
    protected $fillable = [
     	'Name','TimeIN','TimeOut','Price','addtional','Active','Hall_id','Place_ID','total_price','paid_price','total_time','calculated_time',
     ];
    public function calculateAccurateTime()
    {
    	$startTime = \Carbon::parse($this->TimeIN);
        $finishTime = \Carbon::parse($this->TimeOut);
    	$totalDuration = $finishTime->diffInSeconds($startTime);
    	return gmdate('H:i:s', $totalDuration);
    }
}
