<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    public $table="roles";
    public function GetUsers()
    {
        return $this->belongsToMany('App\User','role_user','role_id','user_id');
    }
    public function GetPermissions()
    {
        return $this->belongsToMany('App\Permission','role_has_permissions','role_id','permission_id');
    }
}
