<?php
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Methods: GET, POST, OPTIONS");   
 // header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
 header('Access-Control-Allow-Credentials: true');      

use App\User;
use App\District;
use App\Government;


/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::get('/', 'LoginController@Home');


Route::get('/Regest', function () {
    return view('admin/Regest');
});

Route::get('/Welcome', function () {
    return view('admin/welcome');
});

Route::get('/Tools', function () {
    return view('Tools/Tools');
});


Route::get('/Home','LoginController@Home');
Route::resource('/Owner','OwnerController');

Route::post('/storeownerfirst','OwnerController@storeownerfirst');
Route::get('/RegesterTwo','OwnerController@RegesterTwo');
//Doaa Elgheny
Route::get('/ComingSoon','OwnerController@ComingSoon');
Route::get('/Comesoon','LoginController@Comesoon');


Route::post('/SaveData','OwnerController@SaveData');
Route::get('/Offers','LoginController@Offers');

Route::resource('/Owner1','OwneradminController');


Route::get('/Login','LoginController@Login');
Route::get('/ReserveNow','LoginController@ReserveNow');



Route::get('/OwnerCon','OwnerController@NextPageInRegester');
Route::get('/checkEmailUnique','OwnerController@checkEmailUnique');


//Route::any('/showhalldetail/{id}','HallController@showhalldetail');
Route::any('/showhalldetail','HallController@showhalldetail');
Route::group(['middleware' => ['web']], function () {

Route::resource('/Halls','HallController');
Route::post('/changeprice','HallController@changeprice');
Route::post('/changeCapcity','HallController@changeCapcity');
Route::post('/changeDistrict','HallController@changeDistrict');
Route::post('/changeGovernment','HallController@changeGovernment');

Route::post('/changeType','HallController@changeType');
Route::post('/changecont','HallController@changecont');
Route::get('/PlaceDetails/{id}','HallController@PlaceDetails');

Route::POST('/GetEvents','HallController@GetEvents');
Route::POST('/GetEventsUser','HallController@GetEventsUser');
Route::POST('/GetStartOfHall','HallController@getStartOfHall');
Route::POST('/GetEndOfHall','HallController@getEndOfHall');
Route::POST('/GetStartEndOfHall','HallController@GetStartEndOfHall');
Route::POST('/GetAllEvents','HallController@GetAllEvents');
Route::POST('/GetEventsForUser','HallController@GetEventsForUser');
Route::resource('/ContactUS','ContactUsController');
Route::post('/SendContactUs','ContactUsController@SendContactUs');
Route::any('/SaveTypes','HallController@SaveTypes');

Route::post('/changeFacilty','HallController@changeFacilty');
Route::get('/UnRegisterPlace','PlaceController@UnRegisterPlace');
Route::post('/createNewPlaces','PlaceController@createNewPlaces');
Route::get('/ShowPlace','PlaceController@ShowPlace');
Route::get('/ShowPlaceAdmin','PlaceController@ShowPlaceAdmin');

Route::any('/Approveplace/{id}','PlaceController@Approveplace');
Route::any('/deactivePlace/{id}','PlaceController@deactivePlace');
Route::any('/placHalls/{id}','PlaceController@getPlaceHalls');
Route::post('/changeAll','HallController@changeAll');
Route::any('/Approveplace/destroy/{id}','PlaceController@destroy');
Route::any('/AdminDelete/{id}','PlaceController@AdminDelete');
Route::any('/Halladmin/events/{id}','HallController@scheduleForHall');
Route::POST('/getHallEvents','HallController@getHallEvents');


//Places
Route::resource('/PlaceType','PlaceTypeController');
//End Places

//Tools Routes
//Types
Route::resource('/Types','TypesController');
Route::get('/updateTypes','TypesController@updateTypes');
Route::any('/Types/destroy/{id}','TypesController@destroy');
//Facilites Routes 
Route::resource('/Facilites','FacilitesController');
Route::get('/updateFacilites','FacilitesController@updateFacilites');
Route::any('/Facilites/destroy/{id}','FacilitesController@destroy');

//Government
Route::resource('/Government','GovernmentController');
Route::get('/updateGovernments','GovernmentController@updateGovernments');
Route::any('/Government/destroy/{id}','GovernmentController@destroy');
Route::get('/showallDistriGovernment/{id}','DistrictsController@showallDistriGovernment');
//District 
Route::resource('/Districts','DistrictsController');
Route::get('/updateDistricts','DistrictsController@updateDistricts');
Route::any('/Districts/destroy/{id}','DistrictsController@destroy');

//attributes
Route::resource('/Attribute','attributesController');
Route::get('/updateattributes','attributesController@updateattributes');
Route::any('/Attribute/destroy/{id}','attributesController@destroy');
//
//Packages
Route::resource('/Packages','PackgeController');
Route::get('/updatePackages','PackgeController@updatePackages');
Route::any('/Packages/EditPackages/{id}','PackgeController@EditPackages');
//
//Place Data 

Route::any('/updateimage','HallController@updateimage');
Route::resource('/PlaceData','PlaceController');
Route::get('/AllPlace','PlaceController@AllPlace');
Route::get('/showUpgradePlace','PlaceController@showUpgradePlace');
Route::post('/UpgradePlace','PlaceController@UpgradePlace');
//CoWorking
Route::resource('/CoWorking','CoWorkingController');
Route::any('/ShowReportCoworking','CoWorkingController@ShowReportCoworking');
Route::any('/ReportCoworking','CoWorkingController@ReportCoworking');

Route::any('/CoWorking/destroy/{id}','CoWorkingController@destroy');
Route::get('/LogOut/{id}','CoWorkingController@LogOut');
Route::any('/UpdateCoWorking','CoWorkingController@UpdateCoWorking');
 
//{{$Value->id}}
//Admin hall Routes
Route::get('/Showhalladmin','HallController@Showhalladmin');
Route::any('/Halladmin/destroy/{id}','HallController@destroy');
Route::any('/Halladmin/edit/{id}','HallController@edit');
Route::get('/Showaddhall','HallController@Showaddhall');
Route::get('/showgallary/{id}','HallController@showgallary');

//End

//Schedule Routes
Route::resource('/Schadule','SchaduleController');

Route::POST('/Schadule/store', 'SchaduleController@store');
Route::POST('/Schadule/update', 'SchaduleController@update');
Route::POST('/edit','SchaduleController@edit');
Route::any('/destroy/{id}','SchaduleController@destroy');
//End
 

//Report
Route::resource('/Report','ReportController');
Route::resource('/ShowReport','ReportController@ShowReport');


//End

//Events 
Route::resource('/Events','EventsController');
Route::resource('/InsertEvents','EventsController@InsertEvents');
Route::resource('/showEvents','EventsController@showEvents');
Route::get('/editEvent/{id}','EventsController@editEvent');
Route::get('/destroyEvent/{id}','EventsController@destroyEvent');
Route::get('/getDists/{i_id}','NeededEventController@getDists');
Route::get('/getLocation/{i_id}','NeededEventController@getLocation');
Route::get('/getLocationRegion/{i_id}','NeededEventController@getLocationRegion');

Route::post('/update/event/{id}','EventsController@updateEvent');
Route::get('event/modal/{id}','EventsController@getEvent');
Route::get('/events/types','EventsController@getEventsForType');
/*Not Needed now*/
Route::get('/events/regions','EventsController@getEventsForRegions');
Route::get('/events/govs/give','EventsController@getEventsForGovs');
Route::get('/events/attendance','EventsController@getEventsForAttendance');
Route::get('events/betweenDates','EventsController@getEventsBetweenDates');

//advertiser
Route::group(['middleware' => 'auth'], function() {
	Route::resource('/Advertisadmin','AdvertisesController');
	Route::resource('/showAds','AdvertisesController@showAds');
	Route::get('/place/ads','AdvertisesController@showPlaceAds');
	Route::get('/ads/types','AdvertisesController@getAdsForType');
	Route::get('/editAd/{id}','AdvertisesController@editAd');
	Route::get('/edit/PlaceAd/{id}','AdvertisesController@editPlaceAd');
	
	Route::post('/update/ad/{id}','AdvertisesController@updateAd');
	Route::get('/destroyAd/{id}','AdvertisesController@destroyAd');
	Route::post('/place/update/ad/{id}','AdvertisesController@updatePlaceAd');
	Route::get('getHall/{id}','SchaduleController@getHall');
	Route::post('/updateHallData','CoWorkingController@updateHallData');
	Route::get('/getTable/{id}','CoWorkingController@getTableData');
	Route::post('/addCowrking','CoWorkingController@addToCowrking');
});
//Profile Setting
Route::resource('/Profilesetting','LoginController@Profilesetting');

//Role Routes
Route::get('showRoles','RoleController@getAllRole')->name('roles');
Route::get('addRole','RoleController@getAddRole');
Route::post('addRole','RoleController@addRole')->name('addRole');
Route::post('deleteRole/{id}','RoleController@deleteRole')->name('deleteRole');
Route::post('updateRole/{id}','RoleController@updateRole')->name('updateRole');
Route::get('editRole/{id}','RoleController@getEditRole')->name('editRole');
Route::get('showRole/{id}','RoleController@showRole')->name('showRole');
    //User Route
Route::get('addUser','UserController@getAddUser')->name('addUser');
Route::post('addUser','UserController@addUser')->name('saveUser');
Route::get('showUsers','UserController@getAllUsers')->name('users');
Route::get('showUser/{id}','UserController@showUser')->name('showUser');
Route::get('editUser/{id}','UserController@getEditUser')->name('editUser');
Route::post('updateUser/{id}','UserController@updateUser')->name('updateUser');
Route::post('deleteUser/{id}','UserController@deleteUser')->name('deleteUser');

    // Middlewares
    //->middleware('hasPermissions:edit,delete')
    ////->middleware('hasPermission:publish');
    //  //middleware('hasRole:Silver');


});

