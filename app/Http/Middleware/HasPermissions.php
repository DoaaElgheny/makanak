<?php

namespace App\Http\Middleware;

use Illuminate\Support\Facades\Auth;
use Closure;

class HasPermissions
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next, ...$permissions)
    {
        $user = Auth::user();

        if ($user) {
            $result = true;
            foreach ($permissions as $permission) {
                if (!$user->hasPermissionTo($permission)) {
                    $result = false;
                }
            }
            if ($result) {
                return $next($request);
            } else {
                return redirect('/');
            }
        }
    }
}
