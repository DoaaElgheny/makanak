<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use DB;
use App\Http\Controllers\Controller;
use App\Government;
use Session;
use App\District;
use Auth;

class GovernmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Auth::check())
    {
       $Government=Government::get();
      
        return view('Tools.Government',compact('Government'));
    }
    else
    {
        return redirect("/Home");
    }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try
       {
         
            $Government = New Government;
            $Government->Name=Input::get('txtName'); 
            $Government->save();         
        
        return redirect('/Government');
       }
        catch(Exception $e) 
        {
        return redirect('/login');
        }
    }
public function updateGovernments()
{
    try
        {
        $ID = Input::get('pk');  
        $column_name = Input::get('name');
        $column_value = Input::get('value');
        $Government = Government::whereId($ID)->first();
        $Government-> $column_name=$column_value;
        $Government->save();
         if($Government->save())
        {
            return \Response::json(array('status'=>1));
        }

        
    else {
        return \Response::json(array('status'=>0));

      }
           
        }
    catch(Exception $e) 
        {
         return redirect('/login');
        }
}
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {

           $Government=Government::find($id);
        
         $District=District:: where('Government_Id',$id)->first();
        
           if($District==Null || $District==[])
           {
            
           $Government->delete();
            return redirect('/Government');
           }
           else
           {
             Session::flash('flash_message', 'لا يمكن مسح هذا المحافظة');
            return redirect('/Government');
           }


     }//endtry
    catch(Exception $e) 
    {
    return redirect('/login');
    }
   
    }
}
