<?php

namespace App\Http\Controllers;

use Auth;
use App\Http\Requests;
use Illuminate\Http\Request;
use DB;
use App\Http\Controllers\Controller;
use Excel;
use File;
use Illuminate\Support\Facades\Response;
use Validator;
use Illuminate\Support\Facades\Redirect;
use Input;
use App\User;
use App\Place;
use App\Packge;
use App\Government;
use App\District;
use App\Type;
use Mail;
use Session;
use App\Hall;
use App\Facility;

class PlaceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    if (Auth::check())
    {
        $placedetails=User::join('places','places.id','=','users.Place_Id')
        ->where('users.id',Auth::user()->id)->select('*')->first();
         // dd($placedetails);
         return view('Places.EditPlace',compact('placedetails'));
     }
     else{
        return redirect("/Home");
     }
    }
     public function UnRegisterPlace()
    {
        $Government=Government::lists('name', 'id');
        $District=District::lists('name', 'id');
         $types=Type::lists('Name','id');
         $places = Place::all();
        return view('Places.Addnewplace',compact('Government','District','types','places'));
    }
      public function ShowPlace()
    {
        $places=User::join('places','places.id','=','users.Place_Id')->select('*','users.Email as EmailUser','users.Name as UserName')->get();
        return view('Places.showallplace',compact('places'));
    }
    public function ShowPlaceAdmin()
    {
        $places=Place::where('IsRegester',0)->get();
        return view('Places.AdminShowPlace',compact('places'));
    }
    public function createNewPlaces(Request $request)
    {
        $this->validate($request,[
            'existed'=>'required',
            ]);
        $place_id ;
        try
        {
            if($request['existed'] == 0)
            {
                $PlaceData=New Place;
                $PlaceData->Name=Input::get('Name');
                $PlaceData->Description=Input::get('Description');
                $PlaceData->Address=Input::get('Address');
           
                if(Input::file('pic') !=null)
                {
                    $file_img = Input::file('pic');
             
                    $imageName = $file_img->getClientOriginalName();

                    $path=public_path("/assets/dist/img/").$imageName;

                    if (!file_exists($path)) {
                      $file_img->move(public_path("/assets/dist/img"),$imageName);
                      $PlaceData->Logo="/assets/dist/img/".$imageName;
                      
                    }
                else
                    {
                       $random_string = md5(microtime());
                       $file_img->move(public_path("/assets/dist/img"),$random_string.".jpg");
                       
                      $PlaceData->Logo="/assets/dist/img/".$random_string.".jpg";
                    
                    }
                }
            //ENd
                $PlaceData->District_Id=Input::get('district');
                $PlaceData->Type=Input::get('Type');
                $PlaceData->IsActive=0;
                $PlaceData->IsRegester=0;
                $PlaceData->Email = "".rand()."@".rand().".com";
                $PlaceData->save();
                $place_id=$PlaceData->id;
            }
            else
            {
                if($request['place_id'] == null)
                    return redirect()->back();
                else
                    $place_id = $request['place_id'];
            }
            $Hall=New Hall;
            $Hall->Name=Input::get('HallName');;
            $Hall->Price =Input::get('Price');;
            $type=Type::find(Input::get('Type'));
            $Hall->Resv_Type =$type->Name;
            $Hall->Capacity=Input::get('Capacity');
            $Hall->Place_Id= $place_id;
            $Hall->save();

            return redirect('/UnRegisterPlace');
        }
        catch(Exception $e) 
        {
         return redirect('/login');
        }
    }
      
    public function Approveplace($id)
    {
        $place=Place::find($id);
        $place->IsActive=1;
    $userMail=User::where('Place_Id',$id)->select('Email')->first();
// dd($userMail->Email);
    $place->save();

      $data=array('Name'=>$place->OwnerName,'Email'=>$userMail->Email);
        Mail::send('emails.ActiveSucess', $data, function($message) use ($data) {
            $message->to($data['Email']);
            $message->subject('Account Activation');
        });
        
        return redirect('/ShowPlace');
    }
    public function deactivePlace($id)
    {
        $place=Place::find($id);
        $place->IsActive=0;
        $userMail=User::where('Place_Id',$id)->select('Email')->first();
        $place->save();
        $data=array('Name'=>$place->OwnerName,'Email'=>$userMail->Email);
        Mail::send('emails.DeactiveSucess', $data, function($message) use ($data) {
            $message->to($data['Email']);
            $message->subject('Account Deactivated');
        });
        
        return redirect('/ShowPlace');
    }
    public function getPlaceHalls($id)
    {
        $Halls=Hall::where('Place_Id',$id)->get();
        $types=Type::
        where('Type','Hall')
        ->lists('Name', 'id');
       
        $PackID=Place::where('id',$id)->first();
        // $MessagePack=Packge::where('id',$PackID->Packge_id)->first();
        $Facilities=Facility::lists('Name', 'id');
        return view('halls.HallForAdmin',compact('Halls','types','PackID','Facilities'));

    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

   if (Auth::check())
    {
        try
        {
            $PlaceData=Place::find($id);
 

            $PlaceData->Name=Input::get('Name');
            $PlaceData->address=Input::get('address');
            $PlaceData->Tele=Input::get('Tele');
            $PlaceData->Tele2=Input::get('Tele2');
            $PlaceData->Email=Input::get('Email');
            $PlaceData->OwnerName=Input::get('OwnerName');
            $PlaceData->EmailOwner=Input::get('EmailOwner');
            $PlaceData->TelOwner=Input::get('TelOwner');
            $PlaceData->Facebook=Input::get('Facebook');
            $PlaceData->Youtube=Input::get('Youtube');
            $PlaceData->website=Input::get('website');
            $PlaceData->Twitter=Input::get('Twitter');
            $PlaceData->Description=Input::get('Description');

if(Input::file('pic') !=null)
{
     $file_img = Input::file('pic');
         
                $imageName = $file_img->getClientOriginalName();

                $path=public_path("/assets/dist/img/").$imageName;

                if (!file_exists($path)) {
                  $file_img->move(public_path("/assets/dist/img"),$imageName);
                  $PlaceData->Logo="/assets/dist/img/".$imageName;
                  
                }
                else
                {
                   $random_string = md5(microtime());
                   $file_img->move(public_path("/assets/dist/img"),$random_string.".jpg");
                   
                  $PlaceData->Logo="/assets/dist/img/".$random_string.".jpg";
                
                }
}



            $PlaceData->save();
            Session::flash('flash_message', 'تم التعديل بنجاح');
            return redirect('/PlaceData');
        }
        catch(Exception $e) 
    {
    return redirect('/login');
    }
}
else
{
    return redirect("/Home");
    }
}
public function showUpgradePlace()
{
    try{
        $result=[];
        $packageId=Place::where('id',Auth::user()->Place_Id)->select('Packge_id')->pluck('Packge_id')[0];
        $Packages=Packge::all();

        foreach ($Packages as  $value) {
            if($value->id==$packageId)
            {

             array_push($result,array('Name'=>$value->Name,'id'=>$value->id,'checked'=>'checked')); 
            }
            else
            {
                array_push($result,array('Name'=>$value->Name,'id'=>$value->id,'checked'=>'False')); 
            }
        }
   
        return view('Places.UpgradePlace',compact('result'));
       
      

       }
         catch(Exception $e) 
    {
    return redirect('/login');
    }
}
public function AllPlace()
{
    try
    {
         $Places=Place::all();
         // dd($placedetails);
         return view('Places.AllPlaces',compact('Places'));
    }
  catch(Exception $e) 
    {
    return redirect('/login');
    }
}
public function UpgradePlace(request $request)
{
    
    try{

           $updatepackage=DB::table('places')
            ->where('id',Auth::user()->Place_Id)
            ->update(['Packge_id' =>input::get('Packge_id'),'Upgrade_Package'=>0]);
            
        // $request->session()->flash('alert-success', 'User was successful added!');
Redirect::to('/showUpgradePlace')->with('message', 'تم ترقية الحساب بنجاح انتظر التفعيل خلال 24 ساعة');
              return redirect('/showUpgradePlace');

      

       }
         catch(Exception $e) 
    {
    return redirect('/Home');
    }
}
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function AdminDelete($id)
    {
//Hall
        $Hall=Hall::where('Place_Id',$id)->first();
        if($Hall !=Null)
        {
            $Hall->delete();
        }


       $Place=Place::find($id);
        $Place->delete();
        return redirect('/ShowPlaceAdmin');
        

    }
    public function destroy($id)
    {
        try
        {
            $AllHall=Hall::where('Place_Id',$id)->first();
             if($AllHall==Null || $AllHall=="")
           {
                $Place=Place::find($id);

        $Place->delete();
        return redirect('/ShowPlace');
           }
           else
           {
       Session::flash('flash_message', 'لا يمكن مسح المكان مرتبط بقاعات ..امسح القاعات اولا');
        return redirect('/ShowPlace');
           }
  
        }
     catch(Exception $e) 
    {
    return redirect('/Home');
    }
    }
}
