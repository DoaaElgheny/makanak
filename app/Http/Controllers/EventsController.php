<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use App\Target;
use App\EventType;
use App\Government;
use App\District;
use App\Place;
use App\Event;
use App\EventTarget;
use Illuminate\Support\Collection;
use File;
use Carbon;
class EventsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $events = Event::paginate(10);
        $event_types = EventType::all();
        $governments = Government::all();
        return view('Events.EventsShow',compact('events','event_types','governments'));  
    }
    public function InsertEvents()
    {
        $places = Place::all();
        $targets = Target::all();
        $event_types = EventType::all();
        $governments = Government::all();
        $mytime = Carbon::now();
        $dateNow=$mytime->toDateTimeString("Y-m-d H:m:s");
        return view('Events.EventsInsert',compact('places','targets','event_types','governments','dateNow'));  
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.*

          * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request, [
        'file_picture' => 'image|mimes:jpeg,jpg,png|dimensions:width=820,height=312',
        'event_name' => 'required',
        'event_type_id'=>'required',
        'place_id'=>'required',
        'description'=>'required',
        'government_id'=>'required',
        'region'=>'required',
        'address'=>'required',
        'lat'=>'required|numeric',
        'long'=>'required|numeric',
        'date_from'=>'required|date',
        'date_to'=>'required|date',
        'attendance'=>'required',
        'subscribe'=>'required',
        'target_id'=>'required',
         'price' =>'required',
         'capacity'=>'required|numeric',
        ]);
        $date_from = \Carbon\Carbon::parse($request['date_from']);
        $date_to = \Carbon\Carbon::parse($request['date_to']);
        $path1="";
        $event = Event::create(['name'=>$request['event_name'],'event_type'=>$request['event_type_id'],'organizer_id'=>$request['place_id'],'description'=>$request['description'],'price'=>$request['price'],'long'=>$request['long'],'lat'=>$request['lat'],'region_id'=>$request['region'],'address'=>$request['address'],'date_from'=>$date_from,'date_to'=>$date_to,'attendance_type'=>$request['attendance'],'subscribe_type'=>$request['subscribe'],'capacity'=>$request['capacity'],'fb_link'=>$request['fb_link']]);
        if(\Auth::user()->GetPlace()->first()->id == $request['place_id'])
        {
            $event->update(['isActive'=>1]);
        }
        else
        {
             $event->update(['isActive'=>0]);
        }
        if(Input::hasFile('file_picture'))
        {
            $fileIm = Input::file('file_picture');
            $destPath = 'public/images/'.$event->id;
            $imageName = $fileIm->getClientOriginalName();
            $imExten = $fileIm->getClientOriginalExtension();
            $fileIm->move($destPath, $imageName);
            $path1 = $imageName;
            $event->update(['cover_photo'=>$destPath.'/'.$path1]);
        }
        foreach ($request['target_id'] as $key => $target) {
            # code...
            EventTarget::create(['event_id'=>$event->id,'target_id'=>$target]);
        }

        return redirect('/showEvents');

        
    }
    public function showEvents()
    {
        $place = \Auth::user()->GetPlace();
        //dd($place->first()->id);
        $events=Event::where('organizer_id','=',$place->first()->id)->get();
        // $events = Event::all();
        $event=$events->first();
        //dd($event->type->name);
        return view('Events.EventsForAdmin',compact('events'));
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }
    public function editEvent($id)
    {
        //
        $event = Event::find($id);
        $targets = Target::all();
        $target_items =collect();
        $event_types = EventType::all();
        $governments = Government::all();
        foreach ($event->getTargets() as $key => $target) {
            $target_items->push($target->target);
        }
        //$target_items=$target_items->pluck('name','id');
        return view('Events.editEvent',compact('event','targets','target_items','event_types','governments'));
        
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }
    public function updateEvent(Request $request,$id)
    {
             // dd($request->all());
       $this->validate($request, [
        'file_picture' => 'image|mimes:jpeg,jpg,png|dimensions:width=820,height=312',
        'name' => 'required',
        'event_type_id'=>'required',
        'description'=>'required',
        'government_id'=>'required',
        'region'=>'required',
        'address'=>'required',
        'lat'=>'required|numeric',
        'long'=>'required|numeric',
        'date_from'=>'required|date',
        'date_to'=>'required|date',
        'attendance'=>'required',
        'subscribe'=>'required',
        'target_id'=>'required',
        'price' =>'required',
        'capacity'=>'required|numeric',
        ]);    
        $date_from = \Carbon\Carbon::parse($request['date_from']);
        $date_to = \Carbon\Carbon::parse($request['date_to']);
        $path1="";
        $event = Event::find($id);
        $event->update(['name'=>$request['name'],'event_type'=>$request['event_type_id'],'description'=>$request['description'],'price'=>$request['price'],'long'=>$request['long'],'lat'=>$request['lat'],'region_id'=>$request['region'],'address'=>$request['address'],'date_from'=>$date_from,'date_to'=>$date_to,'attendance_type'=>$request['attendance'],'subscribe_type'=>$request['subscribe'],'capacity'=>$request['capacity'],'fb_link'=>$request['fb_link'],'isActive'=>$request['isActive']]);
         if($request['target_id']!= null)
         {
            foreach ($event->getTargets() as $key => $target) {
                      $target->delete();
            }
            foreach ($request['target_id'] as $key => $target) {
            # code...
                EventTarget::create(['event_id'=>$event->id,'target_id'=>$target]);
            }

         }
        if(Input::hasFile('file_picture'))
        {
            $fileIm = Input::file('file_picture');
            $destPath = 'public/images/'.$event->id;
            $imageName = $fileIm->getClientOriginalName();
            $imExten = $fileIm->getClientOriginalExtension();
            $fileIm->move($destPath, $imageName);
            $path1 = $imageName;
            if($event->cover_photo!=null && $event->cover_photo != $destPath.'/'.$path1 )
            {
                unlink( str_replace('\\', '/',base_path().$event->cover_photo));
            }
           
            $event->update(['cover_photo'=>$destPath.'/'.$path1]);
        }
         return redirect('/showEvents');
    }
   
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        
    }
    public function destroyEvent($id)
    {
        //
        $event = Event::find($id);
        $event->delete();
        return redirect()->back();
    }

    public function getDists($id){
        // $government = Government::find($id);
        // $districts = $government->GetDistricts();
        $districts = District::where('Government_Id','=',$id)->get();
        return $districts;
    }

    public function getEvent($id)
    {
        $event= Event::find($id);
        return view('Events.eventDescriptionModal',compact('event'));
    }
    public function getEventsForType(Request $request)
    {
        $gov_id = $request['id'];
        $disId = $request['disId'];
        $attendance = $request['attendance'];
        if($request['attendance']=='on')
                $attendance=1;
        $type_ids=$request['data_ids'];
        $date_from = \Carbon\Carbon::parse($request['date_from']);
        $date_to = \Carbon\Carbon::parse($request['date_to']);
        if(count($type_ids)>0 && $disId=="" && $gov_id=="" && $attendance=="")
        {
            $events = Event::whereIn('event_type',$type_ids)->paginate(10);
            $events->setPath('/Events');
            return view('Events.eventsPartial',compact('events')) ;
        }
        elseif(count($type_ids)>0 && $disId !="" && $attendance=="")
        {
            $events = Event::whereIn('event_type',$type_ids)->where('region_id','=',$disId)->paginate(10);
            $events->setPath('/Events');
            return view('Events.eventsPartial',compact('events')) ;
        }
        elseif(count($type_ids)>0 && $disId =="" && $gov_id !="" && $attendance=="")
        {
            $regions = District::where('Government_Id','=',$request['id'])->get();
            $ids = array();
            foreach ($regions as $key => $region) {
                # code...
                array_push($ids,$region->id);
            }
            $events = Event::whereIn('event_type',$type_ids)->whereIn('region_id',$ids)->paginate(10);
            $events->setPath('/Events');
            return view('Events.eventsPartial',compact('events')) ;
        }
        elseif(count($type_ids)>0 && $disId =="" && $gov_id !="" && $attendance !="")
        {
            $regions = District::where('Government_Id','=',$request['id'])->get();
            $ids = array();
            if($request['attendance']=='on')
                $attendance=1;
            foreach ($regions as $key => $region) {
                # code...
                array_push($ids,$region->id);
            }
            $events = Event::whereIn('event_type',$type_ids)->whereIn('region_id',$ids)->where('attendance_type','=',$attendance)->paginate(10);
            $events->setPath('/Events');
            return view('Events.eventsPartial',compact('events')) ;
        }
        elseif(count($type_ids)>0 && $disId !="" && $attendance !="")
        {
            $events = Event::whereIn('event_type',$type_ids)->where('region_id','=',$disId)->where('attendance_type','=',$attendance)->paginate(10);
            $events->setPath('/Events');
            return view('Events.eventsPartial',compact('events')) ;
        }
         elseif(count($type_ids)>0 && $disId =="" && $attendance !="")
        {
            $events = Event::whereIn('event_type',$type_ids)->where('attendance_type','=',$attendance)->paginate(10);
            $events->setPath('/Events');
            return view('Events.eventsPartial',compact('events')) ;
        }
        elseif(count($type_ids)<1 && $disId !="" && $attendance=="")
        {
            $events = Event::where('region_id','=',$disId)->paginate(10);
            $events->setPath('/Events');    
            return view('Events.eventsPartial',compact('events')) ;
        }
        elseif(count($type_ids)<1 && $disId !="" && $attendance!="")
        {
            $events = Event::where('region_id','=',$disId)->where('attendance_type','=',$attendance)->paginate(10);
            $events->setPath('/Events');    
            return view('Events.eventsPartial',compact('events')) ;
        }
        elseif(count($type_ids)<1 && $disId=="" && $gov_id !="" && $attendance!="")
        {
            $regions = District::where('Government_Id','=',$request['id'])->get();
            $ids = array();
            foreach ($regions as $key => $region) {
                # code...
                array_push($ids,$region->id);
            }
            $events = Event::whereIn('region_id',$ids)->where('attendance_type','=',$attendance)->paginate(10);
            $events->setPath('/Events');
            return view('Events.eventsPartial',compact('events')) ;
        }
        elseif(count($type_ids)<1 && $disId=="" && $gov_id =="" && $attendance!="")
        {
            $events = Event::where('attendance_type','=',$attendance)->paginate(10);
            $events->setPath('/Events');
            return view('Events.eventsPartial',compact('events')) ;
        }
        elseif(count($type_ids)<1 && $disId=="" && $gov_id !="" && $attendance=="")
        {
            $regions = District::where('Government_Id','=',$request['id'])->get();
            $ids = array();
            foreach ($regions as $key => $region) {
                # code...
                array_push($ids,$region->id);
            }
            $events = Event::whereIn('region_id',$ids)->paginate(10);
            $events->setPath('/Events');
            return view('Events.eventsPartial',compact('events')) ;
        }
        else
        {
             $events = Event::paginate(10);
             $events->setPath('/Events');
             return view('Events.eventsPartial',compact('events')) ;    
        }
    }
    /*** 
    * Replaced By one Function 
    public function getEventsForRegions(Request $request)
    {
        $dis_id = $request['disId'];
        $events = Event::where('region_id','=',$dis_id)->paginate(10);
        $events->setPath('/Events');
        return view('Events.eventsPartial',compact('events')) ;
    }
    public function getEventsForGovs(Request $request)
    {
        $regions = District::where('Government_Id','=',$request['id'])->get();
        $ids = array();
        foreach ($regions as $key => $region) {
            # code...
            array_push($ids,$region->id);
        }
        $events = Event::whereIn('region_id',$ids)->paginate(10);
        $events->setPath('/Events');
        return view('Events.eventsPartial',compact('events')) ;

    }
    public function getEventsForAttendance(Request $request)
    {
        $attendance = $request['attendance'];
        if($request['attendance']=='on')
            $attendance=1;
        $events = Event::where('attendance_type','=',$attendance)->paginate(10);
        $events->setPath('/Events');
        return view('Events.eventsPartial',compact('events')) ;
    }

    ***/
    public function getEventsBetweenDates(Request $request)
    {
        $date_from = \Carbon\Carbon::parse($request['date_from']);
        $date_to = \Carbon\Carbon::parse($request['date_to']);
        $events = Event::whereBetween('date_from',[$date_from,$date_to])->whereBetween('date_to',[$date_from,$date_to])->paginate(10);
        $events->setPath('/Events');
        return view('Events.eventsPartial',compact('events')) ;
        

    }
}
