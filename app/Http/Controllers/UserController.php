<?php

namespace App\Http\Controllers;

use App\User;
use Auth;
use App\Http\Requests;
use DB;
use App\Http\Controllers\Controller;
use Request;
use Excel;
use File;
use Illuminate\Support\Facades\Response;
use Validator;
use Illuminate\Support\Facades\Redirect;
use Input;
use App\Http\Requests\AddUserRequest;
class UserController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

  //getting add user view
    public function getAddUser()
    {
        $user = Auth::user();
        $groupedPermissions =  $user->permissions->groupBy('label');

        return view ('Users.addUser')->with('groupedPermissions',$groupedPermissions);
    }

    public function addUser(AddUserRequest $request)
    {
        $inputs = $request->all();
        $user = Auth::user();
        DB::transaction(function () use ($inputs ,$user){
            //add new user
        $newUser = new User();
        $newUser->Name = $inputs['name'];
        $newUser->Username = $inputs['username'];
        $newUser->Tele = $inputs['telephone'];
        $newUser->Email = $inputs['email'];
        $newUser->Password = $inputs['password'];
        // here
       // $newUser->Type = $user->Type;
       // $newUser->Role = $user->Role;
        $newUser->SuperVisor = $user->id;
        $newUser->Place_id = $user->Place_Id;
        $newUser->District_id = $user->District_Id;
        $newUser->save() ;

            //attach role to user
            foreach ($inputs['permissions'] as $permission)
            {
                $newUser->givePermissionTo($permission);
            }

        });
        return redirect()->back()->with ('message', "User added successfully");
    }
    //Get all users that have supervisor = logged in user
    public function getAllUsers(){
        $loggedInUser = Auth::user();
        $users = User::where('Supervisor' , $loggedInUser->id)->get() ;
        return view('Users.showUsers')->with('users', $users);

    }

    //Get edit User view
    public function getEditUser($id)
    {
        $loggedInUser = Auth::user();
        $user = User::find($id);
        $attachedPermissions = $user->permissions;
        $attachedPermissionsArr = array();
        //Format Data
        foreach ($attachedPermissions as $permission) {
            array_push($attachedPermissionsArr, $permission->id);
        }
        $groupedPermissions =  $loggedInUser->permissions->groupBy('label');
        return view('Users.editUser')->with('user', $user)->with('groupedPermissions', $groupedPermissions)->with('attachedPermissionsArr', $attachedPermissionsArr);
    }

    //Update  User
    public function updateUser(AddUserRequest $request ,$id)
    {
        $inputs = $request->all();
        DB::transaction(function () use ($inputs, $id) {
            $user = User::find($id);
            $user->Name = $inputs['name'];
            $user->Username = $inputs['username'];
            $user->Tele = $inputs['telephone'];
            $user->Email = $inputs['email'];
            $user->Password = $inputs['password'];
            $user->save() ;

            if (array_key_exists('permissions', $inputs)) {

                // attash  permissions
                $user->syncPermissions($inputs['permissions']);
            }
        });
        return redirect()->back()->with('message', 'User Updated Successfully');

    }

    //delete exsisting user
    public function deleteUser($id)
    {
        $user = User::find($id);
        if ($user) {
            $user->delete();
            return redirect()->back()->with('message', 'User Deleted Successfully');
        } else {
            return redirect()->back()->withErrors('Role Not Found');
        }

    }

    //Show exsisting user
    public function showUser($id)
    {
        $user = User::find($id);
        if ($user) {
            return view('Users.showUser')->with('user', $user);
        } else {
            return redirect(route('users'))->withErrors('user Not Found');
        }

    }
}
