<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use App\Government;
use App\District;
class NeededEventController extends Controller
{
    //
    public function getDists($id){
        // $government = Government::find($id);
        // $districts = $government->GetDistricts();
        $districts = District::where('Government_Id','=',$id)->get();
        return $districts;
    }
    public function getLocation($id){
        $government = Government::find($id);
        // $districts = $government->GetDistricts();
        //$gov = District::where('Government_Id','=',$id)->get();
        return $government;
    }
    public function getLocationRegion($id)
    {
    	$region = District::find($id);
    	return $region;
    }
}
