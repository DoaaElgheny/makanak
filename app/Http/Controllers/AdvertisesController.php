<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Excel;
use File;
use Illuminate\Support\Facades\Response;
use Validator;
use Illuminate\Support\Facades\Redirect;
use App\Advertise;
use App\Place;
class AdvertisesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('Advertiser.Insertadvertiser');  
      
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
         $this->validate($request, [
            'photo'=>'image|mimes:jpeg,jpg,png',
            'name' => 'required',
            'description'=>'required',
            ]);
         /*
         Here we should check if the creator is an admin then the approval variable should be 1
         */
         $ad = Advertise::create(['name'=>$request['name'],'description'=>$request['description'],'approval'=>0,'created_by'=>\Auth::user()->id]);
        if(Input::hasFile('photo'))
        {
            $fileIm = Input::file('photo');
            $destPath = 'public/images/ads/'.\Auth::user()->GetPlace->id.'/'.$ad->id;
            $imageName = $fileIm->getClientOriginalName();
            $imExten = $fileIm->getClientOriginalExtension();
            $fileIm->move($destPath, $imageName);
            $path1 = $imageName;
            $ad->update(['photo'=>$destPath.'/'.$path1]);
        }
         return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    public function showAds()
    {

        $ads = Advertise::all();
        return view('Advertiser.adsForAdmin',compact('ads'));
    }
    public function showPlaceAds()
    {
        $place_id = \Auth::user()->GetPlace()->first()->id;
        $place = Place::find($place_id); 
        $users = $place->GetUsersplace->lists('id')->toArray();
        $ads = Advertise::whereIn('created_by',$users)->get();
        return view('Advertiser.adsShow',compact('ads'));
    }
    public function getAdsForType(Request $request)
    {
        //dd($request->all());
        $type_ids=$request['data_ids'];
        if(count($type_ids))
        {
            $ads = Advertise::whereIn('approval',$type_ids)->paginate(10);
            $ads->setPath('/ads');
            return view('Advertiser.adsPartial',compact('ads')) ;
        }
        else
        {
            $ads = Advertise::paginate(10);
            $ads->setPath('/ads');
            return view('Advertiser.adsPartial',compact('ads')) ;   
        }
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }
    public function editAd($id)
    {
        $ad = Advertise::find($id); 
        return view('Advertiser.editAd',compact('ad'));
    }
    public function editPlaceAd($id)
    {
        $ad = Advertise::find($id); 
        return view('Advertiser.editPlaceAd',compact('ad'));
    }

    public function updateAd(Request $request, $id)
    {
        //return $request->all();
         $this->validate($request, [
            'photo'=>'image|mimes:jpeg,jpg,png',
            'name' => 'required',
            'description'=>'required',
            'approval' =>'required',
            'index' => 'required',
            ]);
         $ad = Advertise::find($id);
         $ad->update(['name'=>$request['name'],'description'=>$request['description'],'approval'=>$request['approval']]);
        if($request['index'])
        {
            $ads = Advertise::where('k_index','>=',$request['index'])->get();
            if($ads)
            {
                foreach ($ads as $key => $adv) {
                    $adv->update(['k_index'=> ($adv->k_index+1)]);
                    # code...
                }
            }
            $ad->update(['k_index'=>$request['index']]);
        }
        if(Input::hasFile('photo'))
        {
            $fileIm = Input::file('photo');
            $destPath = 'public/images/ads/'.\Auth::user()->GetPlace->id.'/'.$ad->id;
            $imageName = $fileIm->getClientOriginalName();
            $imExten = $fileIm->getClientOriginalExtension();
            $fileIm->move($destPath, $imageName);
            $path1 = $imageName;
            if($ad->photo)
            {
                if($ad->photo != $destPath.'/'.$path1 )
                {
                    unlink(''. str_replace('\\', '/',base_path().'/'.$ad->photo));
                }
            }
           
            $ad->update(['photo'=>$destPath.'/'.$path1]);
        }
         return redirect('/showAds');
    }

    public function updatePlaceAd(Request $request, $id)
    {
        //return $request->all();
         $this->validate($request, [
            'photo'=>'image|mimes:jpeg,jpg,png',
            'name' => 'required',
            'description'=>'required',
            ]);
         $ad = Advertise::find($id);
         
         $ad->name=$request['name'];
         $ad->description=$request['description'];
         $ad->save();
        if(Input::hasFile('photo'))
        {
            $fileIm = Input::file('photo');
            $destPath = 'public/images/ads/'.\Auth::user()->GetPlace->id.'/'.$ad->id;
            $imageName = $fileIm->getClientOriginalName();
            $imExten = $fileIm->getClientOriginalExtension();
            $fileIm->move($destPath, $imageName);
            $path1 = $imageName;
            if($ad->photo)
            {
                if($ad->photo != $destPath.'/'.$path1 )
                {
                    unlink(''. str_replace('\\', '/',base_path().'/'.$ad->photo));
                }
            }
           
            $ad->update(['photo'=>$destPath.'/'.$path1]);
        }
         return redirect('/place/ads');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function destroyAd($id)
    {
        $ad = Advertise::find($id);
        $ad->delete();
        return redirect()->back();
    }
}
