<?php

namespace App\Http\Controllers;

use Auth;
use App\Http\Requests;
use DB;
use App\Http\Controllers\Controller;
use Request;
use Excel;
use File;
use Illuminate\Support\Facades\Response;
use Validator;
use Illuminate\Support\Facades\Redirect;
use Input;
use App\Packge;
use App\Attribute;
class PackgeController extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Auth::check())
    {
       $Packge=Packge::all();
        return view('Tools.Packages',compact('Packge'));
    }
    else
    {
        return redirect("/Home");
    }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try
       {
         
            $Packge = New Packge;
            $Packge->Name=Input::get('txtName'); 
            $Packge->save();         
        
        return redirect('/Packages');
       }
        catch(Exception $e) 
        {
        return redirect('/login');
        }
    }
public function updatePackages()
{
    try
        {
        $ID = Input::get('pk');  
        $column_name = Input::get('name');
        $column_value = Input::get('value');
        $Packge = Packge::whereId($ID)->first();
        $Packge-> $column_name=$column_value;
        $Packge->save();
        return redirect('/Packages');    
        }
    catch(Exception $e) 
        {
         return redirect('/login');
        }
}
public function EditPackages($id)
{
     $Packge=Packge::where('id',$id)->first();
    $groupsss = $Packge->GetAttributes()->select('attributes.Name','attributes.id')->lists('Name','id');
     $groupitem = $Packge->GetAttributes()->select('attributes.Name','attributes.id')->get();

$itemsId=[];

foreach ($groupitem as  $value) {
   array_push($itemsId, $value->id);
}

    $allatrr=Attribute::select('Name','id')->lists('Name','id');

   return view('Tools.EditPackages',compact('Packge','groupsss','allatrr'));
}
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        try{

            $PackageData=Packge::find($id);

            $PackageData->Name=Input::get('Name');
         
            $PackageData->Price=Input::get('Price');
            $PackageData->Description=Input::get('Description');
              

             $PackageData->save();

             $allatrrlass=Input::get('allatrrlass');
        $n=count($allatrrlass);
        $PackageData->GetAttributes()->detach();
       for($i=0;$i<$n-1;$i++)
        {
             $new_item=Attribute::where('id',$allatrrlass[$i])->first();
             $PackageData->GetAttributes()->attach($allatrrlass[$i]);
        }
            
return redirect('/Packages');   

        }
        catch(Exception $e) 
        {
         return redirect('/login');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {

           $Packge=Packge::find($id);
        
        $Packge->delete();
        return redirect('/Packages');

     }//endtry
    catch(Exception $e) 
    {
    return redirect('/login');
    }
   
    }
}
