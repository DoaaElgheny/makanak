<?php

namespace App\Http\Controllers;

use Auth;
use App\Http\Requests;
use DB;
use App\Http\Controllers\Controller;
use Request;
use Excel;
use File;
use Illuminate\Support\Facades\Response;
use Validator;
use Illuminate\Support\Facades\Redirect;
use Input;
use App\Hall;
use App\Place;
use App\Schadule;

class ReportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
     if (Auth::check())
    {
        $Halls=Hall::where('Place_Id',Auth::user()->Place_Id)->lists('Name','id');
        return view('Reports.AccountReport',compact('Halls'));
    }
    else
    {
        return redirect("/Home");
    }

       
    }
    public function ShowReport()
    {
        try
        {
             $range=array( date("Y-m-d",strtotime(Input::get('Start_Date'))), date("Y-m-d",strtotime(Input::get('End_Date'))));

          if(Input::get('Start_Date')==null || Input::get('End_Date')=="")
          {
            $range=array(date('Y-m-d'),date('Y-m-d'));
          }

          if (Input::get('HallID')==null )
          {
           $ReportData=Schadule::join('halles', 'halles.id', '=', 'schadules.Hall_Id')
            ->wherebetween('schadules.Date_From',$range)
          ->where('halles.Place_Id',Auth::user()->Place_Id)->get();
         
        
          }
          else
          {
          $ReportData=Schadule::join('halles', 'halles.id', '=', 'schadules.Hall_Id')
          ->where('halles.Place_Id',Auth::user()->Place_Id)
           ->wherebetween('schadules.Date_From',$range)
          ->where('halles.id',Input::get('HallID'))->select('halles.Name','schadules.Date_From','schadules.Price','schadules.Name as ResrveName')->get();

           
          }
          $result=[];
              foreach ($ReportData as  $rep) 
         {    
     array_push($result,array('Name'=>$rep->Name,'ResrveName'=>$rep->ResrveName,'Date_From'=>$rep->Date_From,'Price'=>$rep->Price)); 
         }
          return $result;

        }
      catch(Exception $e) 
        {
         return redirect('/login');
        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
