<?php

namespace App\Http\Controllers;

use Auth;
use Illuminate\Http\Request;
use App\Http\Requests;
use DB;
use App\Http\Controllers\Controller;
use Excel;
use File;
use Illuminate\Support\Facades\Response;
use Validator;
use Illuminate\Support\Facades\Redirect;
use Input;
use App\User;
use Session;
use App\Packge;
use App\Coworking;
Use App\Hall;
use Carbon;


class CoWorkingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function ReportCoworking()
    {
        try
        {
            return view('CoWorking.ReportDaily');
        }
      catch(Exception $e) 
        {
         return redirect('/Home');
        }
    }

     public function ShowReportCoworking()
    {
        try
        {
            
        $range=array( date("Y-m-d",strtotime(Input::get('Start_Date'))), date("Y-m-d",strtotime(Input::get('End_Date'))));

        if(Input::get('Start_Date')==null || Input::get('End_Date')=="")
          {
            $range=array(date('Y-m-d'),date('Y-m-d'));
          }

           $ReportData=CoWorking::
        where('TimeIN','>=',date("Y-m-d",strtotime(Input::get('Start_Date'))))
      // ->where('TimeIN','<=',date("Y-m-d",strtotime(Input::get('End_Date'))))
          ->where('Place_ID',Auth::user()->Place_Id)->get();
         
        
          $result=[];
              foreach ($ReportData as  $rep) 
{    
     array_push($result,array('Name'=>$rep->Name,'TimeIN'=>$rep->TimeIN,'TimeOut'=>$rep->TimeOut,'Price'=>$rep->Price,'addtional'=>$rep->addtional,'total'=>($rep->Price+$rep->addtional ))); 
         }
          return $result;

        }
      catch(Exception $e) 
        {
         return redirect('/Home');
        }

    }
    public function index()
    {
    if (Auth::check())
    {
        $halls=Hall::join('halles_types','halles_types.Hall_id','=','halles.id')
        ->where('Place_ID',Auth::user()->Place_Id)
        ->where('halles_types.Type_id',5)->lists('halles.Name','halles.id');
        $CoWorking=Coworking::where('Active',1)
        ->where('Place_ID',Auth::user()->Place_Id)
        ->orderBy('TimeOut', 'Asc')->get();
            if(count($halls) == 0)
            {
                Session::flash('flash_message', 'يجب إضافة قاعات أولاً ');
                return view('halls.NoCoworking');
            }
            else
            {
                 return view('CoWorking.CoWorking',compact('CoWorking','halls'));
            }


       
    }
    else
    {
        return redirect("/Home");
    }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function UpdateCoWorking()
    {
 try
        {
             $mytime = Carbon::now();
            $asd= $mytime->toDateTimeString(); 

           $to = Carbon::createFromFormat('Y-m-d H:s:i', $asd);
             $updateData=DB::table('coworking')
            ->where('id', input::get('id'))
            ->update(['TimeOut' => $to,'addtional'=>input::get('addtional'),'Active'=>0]);
                return redirect("/CoWorking");
        }
catch(Exception $e) 
    {
    return redirect('/Home');
    }
    }
    public function LogOut($id){
        try
        {
//سdd("gbgb");
            $CoWorkingdata=CoWorking::where('id',$id)->first();
             
            // $mytime = Carbon::now();


            $new_time = date("Y-m-d H:i:s", strtotime('+2 hours'));
            $to = Carbon::createFromFormat('Y-m-d H:i:s', $new_time);

            $from = Carbon::createFromFormat('Y-m-d H:i:s', $CoWorkingdata->TimeIN);


            $diff_in_hours = ($to)->diffInSeconds($from);
            $result = $diff_in_hours/60; 
            $result=$result/60;
              $output = round($result,0);
            
            $output=($result);

   // dd($from,$to,$diff_in_hours,$output);  
            $totalClock=($CoWorkingdata->Price)*$output;
          //  dd($totalClock);
            $diff_in_hours_rec = $diff_in_hours;

            $diff_in_hours=$output;
            $diff_in_hours_rec=gmdate('H:i:s', $diff_in_hours_rec);
          //  $CoWorkingdata->update(['TimeOut'=>$to,'total_time'=>$diff_in_hours_rec,'calculated_time'=>$diff_in_hours_rec,'total_price'=>$totalClock,'paid_price'=>$totalClock]);
            // return redirect('/CoWorking');
           // dd($totalClock);
            return view('CoWorking.EditCoWorking',compact('CoWorkingdata','diff_in_hours','totalClock'));
        }
        catch(Exception $e) 
        {
            return redirect('/Home');
        }
    }
    

    public function store(Request $request)
    {
        try{
             $CoWorkingdata=new CoWorking();

  $CoWorkingdata->Name=Input::get('Name');

if(Input::get('HallID')!=Null)
{
    $new_time = date("Y-m-d H:i:s", strtotime('+2 hours'));
$HallPrice=Hall::where('id',Input::get('HallID'))->first();

 $CoWorkingdata->TimeIN= $new_time;

  $CoWorkingdata->Price=$HallPrice->Price; 
  $CoWorkingdata->Hall_id=Input::get('HallID');   
$CoWorkingdata->Place_ID=Auth::user()->Place_Id;
  
 $CoWorkingdata->save();
 return redirect('/CoWorking');
}
else
{
     Session::flash('flash_message', 'يجب اضفة قاعة اولا مساحة عمل مشتركة');
    return redirect('/CoWorking');
}

   

         }
        catch(Exception $e) 
    {
      return redirect('/Home'); 
    }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try
        {
     $CoWorking=Coworking::find($id);
     $CoWorking->delete();
     return redirect('/CoWorking');
        }
        catch(Exception $e) 
    {
    return redirect('/Home');
    }

    }

    public function updateHallData(Request $request)
    {
        $this->validate($request, [
            'HallID'=>'required',
            'h_capacity'=>'required|numeric',
            'h_price'=>'required|numeric',
            ]);
        //dd($request->all());
        $hall = Hall::find($request['HallID']);
        $hall->update(['Capacity'=>$request['h_capacity'],'Price'=>$request['h_price']]);
        return $hall;   
    }
    public function getTableData($id)
    {
        $CoWorking = Coworking::where('Hall_id','=',$id)->orderBy('Active')->get();

        return view('CoWorking.cowrkPartial',compact('CoWorking'));
    }
    public function addToCowrking(Request $request)
    {
        //dd($request->all());
        $this->validate($request,[
            'h_id'=>'required',
            'name'=> 'required',
            ]);
        $h_id = $request['h_id'];
        $hall = Hall::find($h_id);
        $CoWorkingdata = new CoWorking();
        $CoWorkingdata->Name = $request['name'];
        $new_time = date("Y-m-d H:i:s", strtotime('+2 hours'));

        $CoWorkingdata->TimeIN= $new_time;

        $CoWorkingdata->Price=$hall->Price; 
        $CoWorkingdata->Hall_id=$hall->id;   
        $CoWorkingdata->Place_ID=Auth::user()->Place_Id;
        $CoWorkingdata->save();
        $CoWorking = Coworking::where('Hall_id','=',$h_id)->orderBy('Active')->get();
        return view('CoWorking.cowrkPartial',compact('CoWorking'));
    }
}


