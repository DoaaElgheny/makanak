<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use DB;
use App\Http\Controllers\Controller;
use App\District;
use App\Government;
use App\User;
use Auth;
use Session;

class DistrictsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Auth::check())
    {
        $showadditem=1;
        Session::put('Government_id',null);
        $districts = District::all();
        $Government=Government::select('Name','id')->lists('Name','id');
    
        return view('Tools.District',compact('districts','Government','showadditem'));
    }
    else
    {
        return redirect ("/Home");
    }
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
     try
       {
         
            $District = New District;
            $District->Name=Input::get('txtName');
            $District->Government_Id=Input::get('Government');  
            $District->save();         
        
        return redirect('/Districts');
       }
        catch(Exception $e) 
        {
        return redirect('/login');
        }
    }
    public function updateDistricts()
{
    try
        {
        $ID = Input::get('pk');  
        $column_name = Input::get('name');
        $column_value = Input::get('value');
        $District = District::whereId($ID)->first();
        $District-> $column_name=$column_value;
        $District->save();
         
        if($District->save())
        {
            return \Response::json(array('status'=>1));
        }

        
    else {
        return \Response::json(array('status'=>0));

      }  
        }
    catch(Exception $e) 
        {
         return redirect('/login');
        }
}

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {

           $District=District::find($id);
         $DistrictUser=User::where('District_Id',$id)->first();

           if($DistrictUser==Null || $DistrictUser==[])
           {
           
         $District->delete();
        return redirect('/Districts');
           }
           else
           {
             Session::flash('flash_message', 'لا يمكن مسح هذا النوع');
            return redirect('/Districts');
           }
       

     }//endtry
    catch(Exception $e) 
    {
    return redirect('/login');
    }
   
    
    }
}
