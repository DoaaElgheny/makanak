<?php
namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Auth;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Flash;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\RoleRequest;

class RoleController extends Controller
{


    //Get add role view
    public function getAddRole()
    {
        $permissions = Permission::all()->groupBy('label');
        return view('/Role/addRole')->with('permissions', $permissions);
    }

    //Get All roles view
    public function getAllRole()
    {
        $roles = Role::all();
        return view('/Role/showRoles')->with('roles', $roles);
    }

    //Get edit role view
    public function getEditRole($id)
    {
        $role = Role::find($id);
        $attachedPermissions = $role->permissions;
        $attachedPermissionsArr = array();
        //Format Data
        foreach ($attachedPermissions as $permission) {
            array_push($attachedPermissionsArr, $permission->id);
        }
        $permissions = Permission::all()->groupBy('label');
        return view('/Role/editRole')->with('role', $role)->with('permissions', $permissions)->with('attachedPermissions', $attachedPermissionsArr);
    }

    //Create new role and assign permission to it
    public function addRole(RoleRequest $request)
    {

        $inputs = $request->all();
        DB::transaction(function () use ($inputs) {
            $role = Role::create(['name' => $inputs['name']]);
            if (array_key_exists('permission', $inputs)) {
                foreach ($inputs['permission'] as $permission) {
                    //attach permission into Role
                    $role->givePermissionTo($permission);
                }
            }
        });
        return redirect()->back()->with('message', 'Role Saved Successfully');
    }

    //update exsisting role and assign permission to it
    public function updateRole(RoleRequest $request, $id)
    {

        $inputs = $request->all();
        DB::transaction(function () use ($inputs, $id) {
            $role = Role::find($id);
            $role->name = $inputs['name'];
            $role->save();
            if (array_key_exists('permission', $inputs)) {

                // attash  permissions
                    $role->syncPermissions($inputs['permission']);

            }
        });
        return redirect()->back()->with('message', 'Role Updated Successfully');
    }

    //Show exsisting role
    public function showRole($id)
    {
        $role = Role::find($id);
        if ($role) {
            return view('Role.showRole')->with('role', $role);
        } else {
            return redirect(route('roles'))->withErrors('Role Not Found');
        }

    }

    //delete exsisting role
    public function deleteRole($id)
    {
        $role = Role::find($id);
        if ($role) {
            $role->delete();
            return redirect()->back()->with('message', 'Role Deleted Successfully');
        } else {
            return redirect()->back()->withErrors('Role Not Found');
        }

    }


}