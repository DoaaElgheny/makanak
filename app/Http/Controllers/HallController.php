<?php

namespace App\Http\Controllers;

use Auth;
//use App\Http\Requests;
use DB;
use App\Http\Controllers\Controller;
//use Request;
use Illuminate\Http\Request;
use Excel;
use File;
use Illuminate\Support\Facades\Response;
use Validator;
use Illuminate\Support\Facades\Redirect;
use Input;
use App\Hall;
use App\Type;
use App\Government;
use App\District;
use App\Facility;
use App\Schadule;
use Session;
use View;
use App\Place;
use App\Packge;
use App\Image;
use Carbon;
use App\Coworking;
class HallController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function scheduleForHall($id)
    {
            $hall=Hall::where('id',$id)->get();

            if(count($hall) == 0)
            {
                
                Session::flash('flash_message', 'يجب إضافة قاعات أولاً ');

                return view('Schadule.NoHalls');
            }
              
            return view('Schadule.eventHallSchadule',compact('hall'));
    }

    public function getHallEvents(Request $request)
    {
         $query=Schadule::where('Hall_id',$request['hall_id'])->get();
            
            $events = array();
            foreach ($query as $e)
            {
                $result = array();
                $result['id'] = $e['id'];
                $result['title'] =$e['Event_Name'];
                $result['start'] = $e['Date_From'];
                $result['end'] = $e['Date_To'];
                $result['allDay'] = false;
                $result['color'] = '#841851';
                $result['className'] = $e['Hall_Id'];

                // Merge the event array into the return array
                array_push($events,$result);

            }
        
            return json_encode($events);
    }


    public function Showhalladmin()
    {
        if (Auth::check())
    {
        try
        {

        $Halls=Hall::where('Place_Id',Auth::user()->Place_Id)->get();
        $types=Type::
        where('Type','Hall')
        ->lists('Name', 'id');
       
        $PackID=Place::where('id',Auth::user()->Place_Id)->first();
        // $MessagePack=Packge::where('id',$PackID->Packge_id)->first();
        $Facilities=Facility::lists('Name', 'id');
        return view('halls.HallForAdmin',compact('Halls','types','PackID','Facilities'));

        }
          catch(Exception $e) 
          {
         return redirect('/login');
         }
     }
     else
     {
        return redirect ("/Home");
     }
    }
    public function showgallary($id)
    {
        $hallImages=Image::where('places_id',Auth::user()->Place_Id)->get();
        
       return view ("halls.GallaryHall",compact('hallImages'))  ;
    }
    public function Showaddhall()
    {
        if (Auth::check())
    {
        try
        {    
            $types=Type::$types=Type::where('id','!=','5')
            ->where('Type','Hall')
            ->lists('Name', 'Name');
            $PackID=Place::where('id',Auth::user()->Place_Id)->first();
 
             $MessagePack=Packge::where('id',$PackID->Packge_id)->first();
          
            $Facilities=Facility::lists('Name', 'id');
            return view('halls.AddNemHall',compact('types','Facilities','MessagePack'));
        }
       catch(Exception $e) 
          {
         return redirect('/login');
         }

      }
      else
      {
        return redirect("/Home");
      }

    }
    public function index()
    {
       
        $stats=Hall::simplePaginate(5);
        $halles=Hall::all();
        $pagecount=ceil(count($halles)/5);
        $types=Type::where('id','!=','5')
        ->where('Type','Hall')
        ->lists('Name', 'Name');
        $price=Hall::orderBy('Price', 'desc')->first();
        $price1=Hall::orderBy('Price', 'asc')->first();
        $Capacity=Hall::orderBy('Capacity', 'desc')->first();
        $Capacity1=Hall::orderBy('Capacity', 'asc')->first();
        $Facilities=Facility::all();
        $MaxPrice=$price->Price;
        $MinPrice=$price1->Price;
        $MaxCapcity=$Capacity->Capacity;
        $MinCapcity=$Capacity1->Capacity;
        $Government=Government::lists('name', 'id');
        $District=District::lists('name', 'id');
        $mytime = Carbon::now();
        $dateNow=$mytime->toDateTimeString("Y-m-d H:m:s");

        return view('halls.index',compact('stats','types','MaxPrice','MinPrice','MaxCapcity','MinCapcity','Government','District','pagecount','Facilities','dateNow')); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function changeAll()
    {
       
        $Price=Input::get('Price');
        $Capcity=Input::get('Capcity');
        $fromdate=Input::get('fromdate');
        $todate=Input::get('todate');
        $District=[];
        $Government=[];

        if(Input::get('District')==null)
        {
            $DistrictAll=District::select('id')->get();
            
            foreach ($DistrictAll as  $value) {
            array_push($District, $value->id);
            }

        }
        else
        {

             for ($i=0; $i < 1; $i++) { 
                  array_push($Government, Input::get('District'));
             }
         
            
           
        }
       
       if(Input::get('Government')==null)
        {
            $GovernmentAll=Government::select('id')->get();
            
            foreach ($GovernmentAll as  $value) {
            array_push($Government, $value->id);
            }

        }
        else
        {

             for ($i=0; $i < 1; $i++) { 
                  array_push($Government, Input::get('Government'));
             }
         
            
           
        }
        $Type=Input::get('Type');
        $mytime = Carbon::now();
       
        $datetimefromm = new Carbon(Input::get('fromdate'));
        $datetimetoo = new Carbon(Input::get('todate'));
        $datetimefrom=$datetimefromm->toDateString("Y-m-d");
        $datetimeto=$datetimetoo->toDateString("Y-m-d");
        $price1=Hall::orderBy('Price', 'asc')->first();
        $MinPrice=$price1->Price;
        $Capacity1=Hall::orderBy('Capacity', 'asc')->first();
        $MinCapcity=$Capacity1->Capacity;

        if($datetimefrom==$mytime->toDateString("Y-m-d") && $datetimeto==$mytime->toDateString("Y-m-d") && $District=="" && $Government=="" && $Type=="")
        {
            $stats=Hall::whereBetween('Capacity',[$MinCapcity,$Capcity])
            ->whereBetween('halles.Price',[$MinPrice,$Price])
            ->simplePaginate(5);

            $halles=Hall::whereBetween('Capacity',[$MinCapcity,$Capcity])
            ->whereBetween('halles.Price',[$MinPrice,$Price])
            ->get();
           
        }
        elseif ($datetimefrom!=$mytime->toDateString("Y-m-d") && $datetimeto==$mytime->toDateString("Y-m-d") && $District=="" && $Government=="" && $Type=="") {
            
            $stats=Hall::join('schadules','schadules.Hall_Id','=','halles.id')
            ->whereBetween('Capacity',[$MinCapcity,$Capcity])
            ->whereBetween('halles.Price',[$MinPrice,$Price])
            ->whereRaw('? Not between Date_From and Date_To', [$fromdate])
            ->select('halles.*')
            ->simplePaginate(5);
            $halles=Hall::join('schadules','schadules.Hall_Id','=','halles.id')
            ->whereBetween('Capacity',[$MinCapcity,$Capcity])
            ->whereBetween('halles.Price',[$MinPrice,$Price])
            ->whereRaw('? Not between Date_From and Date_To', [$fromdate])
            ->select('halles.*')
            ->get();
            
        }
        elseif ($datetimefrom==$mytime->toDateString("Y-m-d") && $datetimeto!=$mytime->toDateString("Y-m-d") && $District=="" && $Government=="" && $Type=="") {
           $stats=Hall::join('schadules','schadules.Hall_Id','=','halles.id')
           ->whereBetween('Capacity',[$MinCapcity,$Capcity])
                ->whereBetween('halles.Price',[$MinPrice,$Price])
                ->whereRaw('? Not between Date_From and Date_To', [$todate])
                ->select('halles.*')
                ->simplePaginate(5);
            $halles=Hall::join('schadules','schadules.Hall_Id','=','halles.id')
            ->whereBetween('Capacity',[$MinCapcity,$Capcity])
                ->whereBetween('halles.Price',[$MinPrice,$Price])
                ->whereRaw('? Not between Date_From and Date_To', [$todate])
                ->select('halles.*')
                ->get();
            
        }
        elseif ($datetimefrom==$mytime->toDateString("Y-m-d") && $datetimeto==$mytime->toDateString("Y-m-d") && $District!="" && $Government=="" && $Type=="") {
            $stats=Hall::join('places','places.id','=','halles.Place_Id')
                ->whereIN('District_Id',$District)
                ->whereBetween('Capacity',[$MinCapcity,$Capcity])
                ->whereBetween('halles.Price',[$MinPrice,$Price])
                ->select('halles.*')
                ->simplePaginate(5);
            $halles=Hall::join('places','places.id','=','halles.Place_Id')
                ->whereIN('District_Id',$District)
                ->whereBetween('Capacity',[$MinCapcity,$Capcity])
                ->whereBetween('halles.Price',[$MinPrice,$Price])
                ->select('halles.*')
                ->get();
                
        }
        elseif ($datetimefrom==$mytime->toDateString("Y-m-d") && $datetimeto==$mytime->toDateString("Y-m-d") && $District=="" && $Government!="" && $Type=="") {
            $stats=Hall::join('places','places.id','=','halles.Place_Id')
                ->join('districts','districts.id','=','places.District_Id')
                ->whereIn('Government_Id',$Government)
                ->whereBetween('Capacity',[$MinCapcity,$Capcity])
                ->whereBetween('halles.Price',[$MinPrice,$Price])
                ->select('halles.*')
                ->simplePaginate(5);
            $halles=Hall::join('places','places.id','=','halles.Place_Id')
                ->join('districts','districts.id','=','places.District_Id')
                ->whereIN('Government_Id',$Government)
                ->whereBetween('Capacity',[$MinCapcity,$Capcity])
                ->whereBetween('halles.Price',[$MinPrice,$Price])
                ->select('halles.*')
                ->get();

        }
        elseif ($datetimefrom==$mytime->toDateString("Y-m-d") && $datetimeto==$mytime->toDateString("Y-m-d") && $District=="" && $Government=="" && $Type!="") {
                $stats=Hall::where('Resv_Type',$Type)
                    ->whereBetween('Capacity',[$MinCapcity,$Capcity])
                    ->whereBetween('halles.Price',[$MinPrice,$Price])
                    ->simplePaginate(5);
                $halles=Hall::where('Resv_Type',$Type)
                    ->whereBetween('Capacity',[$MinCapcity,$Capcity])
                    ->whereBetween('halles.Price',[$MinPrice,$Price])
                    ->get();
        }
        elseif ($datetimefrom!=$mytime->toDateString("Y-m-d") && $datetimeto!=$mytime->toDateString("Y-m-d") && $District=="" && $Government=="" && $Type=="") {
            $stats=Hall::join('schadules','schadules.Hall_Id','=','halles.id')
            ->whereBetween('Capacity',[$MinCapcity,$Capcity])
                ->whereBetween('halles.Price',[$MinPrice,$Price])
                ->whereRaw('? Not between Date_From and Date_To', [$fromdate])
                ->whereRaw('? Not between Date_From and Date_To', [$todate])
                ->select('halles.*')
                ->simplePaginate(5);
               
            $halles=Hall::join('schadules','schadules.Hall_Id','=','halles.id')
            ->whereBetween('Capacity',[$MinCapcity,$Capcity])
                ->whereBetween('halles.Price',[$MinPrice,$Price])
                ->whereRaw('? Not between Date_From and Date_To', [$fromdate])
                ->whereRaw('? Not between Date_From and Date_To', [$todate])
                ->select('halles.*')
                ->get();
        }
        elseif ($datetimefrom!=$mytime->toDateString("Y-m-d") && $datetimeto==$mytime->toDateString("Y-m-d") && $District!="" && $Government=="" && $Type=="") {
            $stats=Hall::join('places','places.id','=','halles.Place_Id')
                ->join('schadules','schadules.Hall_Id','=','halles.id')
                ->whereIN('District_Id',$District)
                ->whereRaw('? Not between Date_From and Date_To', [$fromdate])
                ->whereBetween('Capacity',[$MinCapcity,$Capcity])
                ->whereBetween('halles.Price',[$MinPrice,$Price])
                ->select('halles.*')
                ->simplePaginate(5);
             $halles=Hall::join('places','places.id','=','halles.Place_Id')
                ->join('schadules','schadules.Hall_Id','=','halles.id')
                ->whereIN('District_Id',$District)
                ->whereRaw('? Not between Date_From and Date_To', [$fromdate])
                ->whereBetween('Capacity',[$MinCapcity,$Capcity])
                ->whereBetween('halles.Price',[$MinPrice,$Price])
                ->select('halles.*')
                ->get();
        }
        elseif ($datetimefrom!=$mytime->toDateString("Y-m-d") && $datetimeto==$mytime->toDateString("Y-m-d") && $District=="" && $Government!="" && $Type=="") {
           $stats=Hall::join('places','places.id','=','halles.Place_Id')
                ->join('districts','districts.id','=','places.District_Id')
                ->join('schadules','schadules.Hall_Id','=','halles.id')
                ->whereIN('Government_Id',$Government)
                ->whereRaw('? Not between Date_From and Date_To', [$fromdate])
                ->whereBetween('Capacity',[$MinCapcity,$Capcity])
                ->whereBetween('halles.Price',[$MinPrice,$Price])
                ->select('halles.*')
                ->simplePaginate(5);
            $halles=Hall::join('places','places.id','=','halles.Place_Id')
                ->join('districts','districts.id','=','places.District_Id')
                ->join('schadules','schadules.Hall_Id','=','halles.id')
                ->whereIN('Government_Id',$Government)
                ->whereRaw('? Not between Date_From and Date_To', [$fromdate])
                ->whereBetween('Capacity',[$MinCapcity,$Capcity])
                ->whereBetween('halles.Price',[$MinPrice,$Price])
                ->select('halles.*')
                ->get();
        }
        elseif ($datetimefrom!=$mytime->toDateString("Y-m-d") && $datetimeto==$mytime->toDateString("Y-m-d") && $District=="" && $Government=="" && $Type !="") {
            $stats=Hall::join('schadules','schadules.Hall_Id','=','halles.id')
            ->where('Resv_Type',$Type)
                ->whereRaw('? Not between Date_From and Date_To', [$fromdate])
                ->whereBetween('Capacity',[$MinCapcity,$Capcity])
                ->whereBetween('halles.Price',[$MinPrice,$Price])
                ->select('halles.*')
                ->simplePaginate(5);
             $halles=Hall::join('schadules','schadules.Hall_Id','=','halles.id')
             ->where('Resv_Type',$Type)
                ->whereRaw('? Not between Date_From and Date_To', [$fromdate])
                ->whereBetween('Capacity',[$MinCapcity,$Capcity])
                ->whereBetween('halles.Price',[$MinPrice,$Price])
                ->select('halles.*')
                ->get();
        }
        elseif ($datetimefrom==$mytime->toDateString("Y-m-d") && $datetimeto!=$mytime->toDateString("Y-m-d") && $District!="" && $Government=="" && $Type=="") {
            $stats=Hall::join('places','places.id','=','halles.Place_Id')
            ->join('schadules','schadules.Hall_Id','=','halles.id')
                ->whereIN('District_Id',$District)
                ->whereRaw('? Not between Date_From and Date_To', [$todate])
                ->whereBetween('Capacity',[$MinCapcity,$Capcity])
                ->whereBetween('halles.Price',[$MinPrice,$Price])
                ->select('halles.*')
                ->simplePaginate(5);
            $halles=Hall::join('places','places.id','=','halles.Place_Id')
            ->join('schadules','schadules.Hall_Id','=','halles.id')
                ->whereIN('District_Id',$District)
                ->whereRaw('? Not between Date_From and Date_To', [$todate])
                ->whereBetween('Capacity',[$MinCapcity,$Capcity])
                ->whereBetween('halles.Price',[$MinPrice,$Price])
                ->select('halles.*')
                ->get();
        }
        elseif ($datetimefrom==$mytime->toDateString("Y-m-d") && $datetimeto!=$mytime->toDateString("Y-m-d") && $District=="" && $Government!="" && $Type=="") {
           $stats=Hall::join('places','places.id','=','halles.Place_Id')
                ->join('districts','districts.id','=','places.District_Id')
                ->join('schadules','schadules.Hall_Id','=','halles.id')
                ->whereIN('Government_Id',$Government)
                ->whereRaw('? Not between Date_From and Date_To', [$todate])
                ->whereBetween('Capacity',[$MinCapcity,$Capcity])
                ->whereBetween('halles.Price',[$MinPrice,$Price])
                ->select('halles.*')
                ->simplePaginate(5);
            $halles=Hall::join('places','places.id','=','halles.Place_Id')
                ->join('districts','districts.id','=','places.District_Id')
                ->join('schadules','schadules.Hall_Id','=','halles.id')
                ->whereIN('Government_Id',$Government)
                ->whereRaw('? Not between Date_From and Date_To', [$todate])
                ->whereBetween('Capacity',[$MinCapcity,$Capcity])
                ->whereBetween('halles.Price',[$MinPrice,$Price])
                ->select('halles.*')
                ->get();
        }
        elseif ($datetimefrom==$mytime->toDateString("Y-m-d") && $datetimeto!=$mytime->toDateString("Y-m-d") && $District=="" && $Government=="" && $Type !="") {
            $stats=Hall::join('schadules','schadules.Hall_Id','=','halles.id')
            ->where('Resv_Type',$Type)
                ->whereRaw('? Not between Date_From and Date_To', [$todate])
                ->whereBetween('Capacity',[$MinCapcity,$Capcity])
                ->whereBetween('halles.Price',[$MinPrice,$Price])
                ->select('halles.*')
                ->simplePaginate(5);
            $halles=Hall::join('schadules','schadules.Hall_Id','=','halles.id')
            ->where('Resv_Type',$Type)
                ->whereRaw('? Not between Date_From and Date_To', [$todate])
                ->whereBetween('Capacity',[$MinCapcity,$Capcity])
                ->whereBetween('halles.Price',[$MinPrice,$Price])
                ->select('halles.*')
                ->get();
        }
        elseif ($datetimefrom==$mytime->toDateString("Y-m-d") && $datetimeto==$mytime->toDateString("Y-m-d") && $District!="" && $Government!="" && $Type=="") {
           $stats=Hall::join('places','places.id','=','halles.Place_Id')
                ->join('districts','districts.id','=','places.District_Id')
                ->whereIN('Government_Id',$Government)
                ->whereIN('District_Id',$District)
                ->whereBetween('Capacity',[$MinCapcity,$Capcity])
                ->whereBetween('halles.Price',[$MinPrice,$Price])
                ->select('halles.*')
                ->simplePaginate(5);
            $halles=Hall::join('places','places.id','=','halles.Place_Id')
                ->join('districts','districts.id','=','places.District_Id')
                ->whereIN('Government_Id',$Government)
                ->whereIN('District_Id',$District)
                ->whereBetween('Capacity',[$MinCapcity,$Capcity])
                ->whereBetween('halles.Price',[$MinPrice,$Price])
                ->select('halles.*')
                ->get();
        }
        elseif ($datetimefrom==$mytime->toDateString("Y-m-d") && $datetimeto==$mytime->toDateString("Y-m-d") && $District!="" && $Government=="" && $Type !="") {
            $stats=Hall::join('places','places.id','=','halles.Place_Id')
                ->where('Resv_Type',$Type)
                ->whereIN('District_Id',$District)
                ->whereBetween('Capacity',[$MinCapcity,$Capcity])
                ->whereBetween('halles.Price',[$MinPrice,$Price])
                ->simplePaginate(5);
            $halles=Hall::join('places','places.id','=','halles.Place_Id')
                ->where('Resv_Type',$Type)
                ->whereIN('District_Id',$District)
                ->whereBetween('Capacity',[$MinCapcity,$Capcity])
                ->whereBetween('halles.Price',[$MinPrice,$Price])
                ->get();
        }
        elseif ($datetimefrom==$mytime->toDateString("Y-m-d") && $datetimeto==$mytime->toDateString("Y-m-d") && $District=="" && $Government!="" && $Type!="") {
           $stats=Hall::join('places','places.id','=','halles.Place_Id')
                ->join('districts','districts.id','=','places.District_Id')
                ->where('Resv_Type',$Type)
                ->whereIN('Government_Id',$Government)
                ->whereBetween('Capacity',[$MinCapcity,$Capcity])
                ->whereBetween('halles.Price',[$MinPrice,$Price])
                ->select('halles.*')
                ->simplePaginate(5);
            $halles=Hall::join('places','places.id','=','halles.Place_Id')
                ->join('districts','districts.id','=','places.District_Id')
                ->where('Resv_Type',$Type)
                ->whereIN('Government_Id',$Government)
                ->whereBetween('Capacity',[$MinCapcity,$Capcity])
                ->whereBetween('halles.Price',[$MinPrice,$Price])
                ->select('halles.*')
                ->get();
        }
        elseif ($datetimefrom!=$mytime->toDateString("Y-m-d") && $datetimeto!=$mytime->toDateString("Y-m-d") && $District!="" && $Government=="" && $Type=="") {
           $stats=Hall::join('places','places.id','=','halles.Place_Id')
           ->join('schadules','schadules.Hall_Id','=','halles.id')
                ->whereIN('District_Id',$District)
                ->whereRaw('? Not between Date_From and Date_To', [$fromdate])
                ->whereRaw('? Not between Date_From and Date_To', [$todate])
                ->whereBetween('Capacity',[$MinCapcity,$Capcity])
                ->whereBetween('halles.Price',[$MinPrice,$Price])
                ->select('halles.*')
                ->simplePaginate(5);
            $halles=Hall::join('places','places.id','=','halles.Place_Id')
            ->join('schadules','schadules.Hall_Id','=','halles.id')
                ->whereIN('District_Id',$District)
                ->whereRaw('? Not between Date_From and Date_To', [$fromdate])
                ->whereRaw('? Not between Date_From and Date_To', [$todate])
                ->whereBetween('Capacity',[$MinCapcity,$Capcity])
                ->whereBetween('halles.Price',[$MinPrice,$Price])
                ->select('halles.*')
                ->get();
        }
        elseif ($datetimefrom!=$mytime->toDateString("Y-m-d") && $datetimeto!=$mytime->toDateString("Y-m-d") && $District=="" && $Government!="" && $Type=="") {
           $stats=Hall::join('places','places.id','=','halles.Place_Id')
                ->join('districts','districts.id','=','places.District_Id')
                ->join('schadules','schadules.Hall_Id','=','halles.id')
                ->whereIN('Government_Id',$Government)
                ->whereRaw('? Not between Date_From and Date_To', [$fromdate])
                ->whereRaw('? Not between Date_From and Date_To', [$todate])
                ->whereBetween('Capacity',[$MinCapcity,$Capcity])
                ->whereBetween('halles.Price',[$MinPrice,$Price])
                ->select('halles.*')
                ->simplePaginate(5);
            $halles=Hall::join('places','places.id','=','halles.Place_Id')
                ->join('districts','districts.id','=','places.District_Id')
                ->join('schadules','schadules.Hall_Id','=','halles.id')
                ->whereIN('Government_Id',$Government)
                ->whereRaw('? Not between Date_From and Date_To', [$fromdate])
                ->whereRaw('? Not between Date_From and Date_To', [$todate])
                ->whereBetween('Capacity',[$MinCapcity,$Capcity])
                ->whereBetween('halles.Price',[$MinPrice,$Price])
                ->select('halles.*')
                ->get();
        }
        elseif ($datetimefrom!=$mytime->toDateString("Y-m-d") && $datetimeto!=$mytime->toDateString("Y-m-d") && $District=="" && $Government=="" && $Type !="") {
           $stats=Hall::join('schadules','schadules.Hall_Id','=','halles.id')
                ->where('Resv_Type',$Type)
                ->whereRaw('? Not between Date_From and Date_To', [$fromdate])
                ->whereRaw('? Not between Date_From and Date_To', [$todate])
                ->whereBetween('Capacity',[$MinCapcity,$Capcity])
                ->whereBetween('halles.Price',[$MinPrice,$Price])
                ->select('halles.*')
                ->simplePaginate(5);

            $halles=Hall::join('schadules','schadules.Hall_Id','=','halles.id')
            ->where('Resv_Type',$Type)
                ->whereRaw('? Not between Date_From and Date_To', [$fromdate])
                ->whereRaw('? Not between Date_From and Date_To', [$todate])
                ->whereBetween('Capacity',[$MinCapcity,$Capcity])
                ->whereBetween('halles.Price',[$MinPrice,$Price])
                ->select('halles.*')
                ->get();
        }
        elseif ($datetimefrom!=$mytime->toDateString("Y-m-d") && $datetimeto!=$mytime->toDateString("Y-m-d") && $District!="" && $Government!="" && $Type =="") {
           $stats=Hall::join('places','places.id','=','halles.Place_Id')
                ->join('districts','districts.id','=','places.District_Id')
                ->join('schadules','schadules.Hall_Id','=','halles.id')
                ->whereIN('Government_Id',$Government)
                ->whereIN('District_Id',$District)
                ->whereRaw('? Not between Date_From and Date_To', [$fromdate])
                ->whereRaw('? Not between Date_From and Date_To', [$todate])
                ->whereBetween('Capacity',[$MinCapcity,$Capcity])
                ->whereBetween('halles.Price',[$MinPrice,$Price])
                ->select('halles.*')
                ->simplePaginate(5);
            $halles=Hall::join('places','places.id','=','halles.Place_Id')
                ->join('districts','districts.id','=','places.District_Id')
                ->join('schadules','schadules.Hall_Id','=','halles.id')
                ->whereIN('Government_Id',$Government)
                ->whereIN('District_Id',$District)
                ->whereRaw('? Not between Date_From and Date_To', [$fromdate])
                ->whereRaw('? Not between Date_From and Date_To', [$todate])
                ->whereBetween('Capacity',[$MinCapcity,$Capcity])
                ->whereBetween('halles.Price',[$MinPrice,$Price])
                ->select('halles.*')
                ->get();
        }
        elseif ($datetimefrom!=$mytime->toDateString("Y-m-d") && $datetimeto!=$mytime->toDateString("Y-m-d") && $District!="" && $Government=="" && $Type !="") {
               $stats=Hall::join('places','places.id','=','halles.Place_Id')
               ->join('schadules','schadules.Hall_Id','=','halles.id')
                ->whereIN('District_Id',$District)
                ->where('Resv_Type',$Type)
                ->whereRaw('? Not between Date_From and Date_To', [$fromdate])
                ->whereRaw('? Not between Date_From and Date_To', [$todate])
                ->whereBetween('Capacity',[$MinCapcity,$Capcity])
                ->whereBetween('halles.Price',[$MinPrice,$Price])
                ->select('halles.*')
                ->simplePaginate(5);
                $halles=Hall::join('places','places.id','=','halles.Place_Id')
                ->join('schadules','schadules.Hall_Id','=','halles.id')
                ->whereIN('District_Id',$District)
                ->where('Resv_Type',$Type)
                ->whereRaw('? Not between Date_From and Date_To', [$fromdate])
                ->whereRaw('? Not between Date_From and Date_To', [$todate])
                ->whereBetween('Capacity',[$MinCapcity,$Capcity])
                ->whereBetween('halles.Price',[$MinPrice,$Price])
                ->select('halles.*')
                ->get();
        }
        elseif ($datetimefrom!=$mytime->toDateString("Y-m-d") && $datetimeto!=$mytime->toDateString("Y-m-d") && $District!="" && $Government!="" && $Type !="") {
               $stats=Hall::join('places','places.id','=','halles.Place_Id')
                ->join('districts','districts.id','=','places.District_Id')
                ->join('schadules','schadules.Hall_Id','=','halles.id')
                ->whereIN('Government_Id',$Government)
                ->whereIN('District_Id',$District)
                ->whereRaw('? Not between Date_From and Date_To', [$fromdate])
                ->whereRaw('? Not between Date_From and Date_To', [$todate])
                ->where('Resv_Type',$Type)
                ->whereBetween('Capacity',[$MinCapcity,$Capcity])
                ->whereBetween('halles.Price',[$MinPrice,$Price])
                ->select('halles.*')
                ->simplePaginate(5);
            $halles=Hall::join('places','places.id','=','halles.Place_Id')
                ->join('districts','districts.id','=','places.District_Id')
                ->join('schadules','schadules.Hall_Id','=','halles.id')
                ->whereIN('Government_Id',$Government)
                ->whereIN('District_Id',$District)
                ->whereRaw('? Not between Date_From and Date_To', [$fromdate])
                ->whereRaw('? Not between Date_From and Date_To', [$todate])
                ->where('Resv_Type',$Type)
                ->whereBetween('Capacity',[$MinCapcity,$Capcity])
                ->whereBetween('halles.Price',[$MinPrice,$Price])
                ->select('halles.*')
                ->get();
        }
       
         elseif ($datetimefrom==$mytime->toDateString("Y-m-d") && $datetimeto!=$mytime->toDateString("Y-m-d") && $District!="" && $Government!="" && $Type =="") {
               $stats=Hall::join('places','places.id','=','halles.Place_Id')
                ->join('districts','districts.id','=','places.District_Id')
                ->join('schadules','schadules.Hall_Id','=','halles.id')
                ->whereIN('Government_Id',$Government)
                ->whereIN('District_Id',$District)
                ->whereRaw('? Not between Date_From and Date_To', [$todate])
                ->whereBetween('Capacity',[$MinCapcity,$Capcity])
                ->whereBetween('halles.Price',[$MinPrice,$Price])
                ->select('halles.*')
                ->simplePaginate(5);
            $halles=Hall::join('places','places.id','=','halles.Place_Id')
                ->join('districts','districts.id','=','places.District_Id')
                ->join('schadules','schadules.Hall_Id','=','halles.id')
                ->whereIN('Government_Id',$Government)
                ->whereIN('District_Id',$District)
                ->whereRaw('? Not between Date_From and Date_To', [$todate])
                ->whereBetween('Capacity',[$MinCapcity,$Capcity])
                ->whereBetween('halles.Price',[$MinPrice,$Price])
                ->select('halles.*')
                ->get();
        }
         elseif ($datetimefrom==$mytime->toDateString("Y-m-d") && $datetimeto!=$mytime->toDateString("Y-m-d") && $District!="" && $Government=="" && $Type !="") {
               $stats=Hall::join('places','places.id','=','halles.Place_Id')
                ->join('districts','districts.id','=','places.District_Id')
                ->join('schadules','schadules.Hall_Id','=','halles.id')
                ->whereIN('District_Id',$District)
                ->whereRaw('? Not between Date_From and Date_To', [$todate])
                ->where('Resv_Type',$Type)
                ->whereBetween('Capacity',[$MinCapcity,$Capcity])
                ->whereBetween('halles.Price',[$MinPrice,$Price])
                ->select('halles.*')
                ->simplePaginate(5);
            $halles=Hall::join('places','places.id','=','halles.Place_Id')
                ->join('districts','districts.id','=','places.District_Id')
                ->join('schadules','schadules.Hall_Id','=','halles.id')
                ->whereIN('District_Id',$District)
                ->whereRaw('? Not between Date_From and Date_To', [$todate])
                ->where('Resv_Type',$Type)
                ->whereBetween('Capacity',[$MinCapcity,$Capcity])
                ->whereBetween('halles.Price',[$MinPrice,$Price])
                ->select('halles.*')
                ->get();
        }
        elseif ($datetimefrom!=$mytime->toDateString("Y-m-d") && $datetimeto!=$mytime->toDateString("Y-m-d") && $District=="" && $Government!="" && $Type !="") {
               $stats=Hall::join('places','places.id','=','halles.Place_Id')
                ->join('districts','districts.id','=','places.District_Id')
                ->join('schadules','schadules.Hall_Id','=','halles.id')
                ->whereIN('Government_Id',$Government)
                ->whereRaw('? Not between Date_From and Date_To', [$fromdate])
                ->whereRaw('? Not between Date_From and Date_To', [$todate])
                ->where('Resv_Type',$Type)
                ->whereBetween('Capacity',[$MinCapcity,$Capcity])
                ->whereBetween('halles.Price',[$MinPrice,$Price])
                ->select('halles.*')
                ->simplePaginate(5);
            $halles=Hall::join('places','places.id','=','halles.Place_Id')
                ->join('districts','districts.id','=','places.District_Id')
                ->join('schadules','schadules.Hall_Id','=','halles.id')
                ->whereIN('Government_Id',$Government)
                ->whereRaw('? Not between Date_From and Date_To', [$fromdate])
                ->whereRaw('? Not between Date_From and Date_To', [$todate])
                ->where('Resv_Type',$Type)
                ->whereBetween('Capacity',[$MinCapcity,$Capcity])
                ->whereBetween('halles.Price',[$MinPrice,$Price])
                ->select('halles.*')
                ->get();
        }
         elseif ($datetimefrom==$mytime->toDateString("Y-m-d") && $datetimeto!=$mytime->toDateString("Y-m-d") && $District!="" && $Government!="" && $Type !="") {
               $stats=Hall::join('places','places.id','=','halles.Place_Id')
                ->join('districts','districts.id','=','places.District_Id')
                ->join('schadules','schadules.Hall_Id','=','halles.id')
                ->whereIN('Government_Id',$Government)
                ->whereIN('District_Id',$District)
                ->whereRaw('? Not between Date_From and Date_To', [$todate])
                ->where('Resv_Type',$Type)
                ->whereBetween('Capacity',[$MinCapcity,$Capcity])
                ->whereBetween('halles.Price',[$MinPrice,$Price])
                ->select('halles.*')
                ->simplePaginate(5);
            $halles=Hall::join('places','places.id','=','halles.Place_Id')
                ->join('districts','districts.id','=','places.District_Id')
                ->join('schadules','schadules.Hall_Id','=','halles.id')
                ->whereIN('Government_Id',$Government)
                ->whereIN('District_Id',$District)
                ->whereRaw('? Not between Date_From and Date_To', [$todate])
                ->where('Resv_Type',$Type)
                ->whereBetween('Capacity',[$MinCapcity,$Capcity])
                ->whereBetween('halles.Price',[$MinPrice,$Price])
                ->select('halles.*')
                ->get();
        }
         elseif ($datetimefrom==$mytime->toDateString("Y-m-d") && $datetimeto==$mytime->toDateString("Y-m-d") && $District!="" && $Government!="" && $Type !="") {
               $stats=Hall::join('places','places.id','=','halles.Place_Id')
                ->join('districts','districts.id','=','places.District_Id')
                ->whereIN('Government_Id',$Government)
                ->whereIN('District_Id',$District)
                ->where('Resv_Type',$Type)
                ->whereBetween('Capacity',[$MinCapcity,$Capcity])
                ->whereBetween('halles.Price',[$MinPrice,$Price])
                ->select('halles.*')
                ->simplePaginate(5);
                $halles=Hall::join('places','places.id','=','halles.Place_Id')
                ->join('districts','districts.id','=','places.District_Id')
                ->whereIN('Government_Id',$Government)
                ->whereIN('District_Id',$District)
                ->where('Resv_Type',$Type)
                ->whereBetween('Capacity',[$MinCapcity,$Capcity])
                ->whereBetween('halles.Price',[$MinPrice,$Price])
                ->select('halles.*')
                ->get();
        }
        elseif ($datetimefrom ==$mytime->toDateString("Y-m-d") && $datetimeto!=$mytime->toDateString("Y-m-d") && $District=="" && $Government!="" && $Type !="") {
               $stats=Hall::join('places','places.id','=','halles.Place_Id')
                ->join('districts','districts.id','=','places.District_Id')
                ->join('schadules','schadules.Hall_Id','=','halles.id')
                ->whereIN('Government_Id',$Government)
                ->whereRaw('? Not between Date_From and Date_To', [$todate])
                ->where('Resv_Type',$Type)
                ->whereBetween('Capacity',[$MinCapcity,$Capcity])
                ->whereBetween('halles.Price',[$MinPrice,$Price])
                ->select('halles.*')
                ->simplePaginate(5);
                $halles=Hall::join('places','places.id','=','halles.Place_Id')
                ->join('districts','districts.id','=','places.District_Id')
                ->join('schadules','schadules.Hall_Id','=','halles.id')
                ->whereIN('Government_Id',$Government)
                ->whereRaw('? Not between Date_From and Date_To', [$todate])
                ->where('Resv_Type',$Type)
                ->whereBetween('Capacity',[$MinCapcity,$Capcity])
                ->whereBetween('halles.Price',[$MinPrice,$Price])
                ->select('halles.*')
                ->get();
        }
        $pagecount=ceil(count($halles)/5);
        $html = View::make('halls.particalview', compact('stats','pagecount'))->render();
        return Response::json(['html' => $html]);
    }
    
    public function changecont()
    {
        

        $Price=Input::get('Price');
        $Capcity=Input::get('Capcity');
        $fromdate=Input::get('fromdate');
        $todate=Input::get('todate');
        $District=Input::get('District');
        $Government=Input::get('Government');
        $Type=Input::get('Type');
        $count=Input::get('count');
        $start=5*($count-1);
        $mytime = Carbon::now();
        $datetimefromm = new Carbon(Input::get('fromdate'));
        $datetimetoo = new Carbon(Input::get('todate'));
        $datetimefrom=$datetimefromm->toDateString("Y-m-d");
        $datetimeto=$datetimetoo->toDateString("Y-m-d");
        $price1=Hall::orderBy('halles.Price', 'asc')->first();
        $MinPrice=$price1->Price;
        $Capacity1=Hall::orderBy('Capacity', 'asc')->first();
        $MinCapcity=$Capacity1->Capacity;

         if($datetimefrom==$mytime->toDateString("Y-m-d") && $datetimeto==$mytime->toDateString("Y-m-d") && $District=="" && $Government=="" && $Type=="")
        {
            $stats=Hall::whereBetween('Capacity',[$MinCapcity,$Capcity])
            ->whereBetween('halles.Price',[$MinPrice,$Price])
            ->skip($start)->take(5)->get();

            $halles=Hall::whereBetween('Capacity',[$MinCapcity,$Capcity])
            ->whereBetween('halles.Price',[$MinPrice,$Price])
            ->get();
           
        }
        elseif ($datetimefrom!=$mytime->toDateString("Y-m-d") && $datetimeto==$mytime->toDateString("Y-m-d") && $District=="" && $Government=="" && $Type=="") {
            $stats=Hall::join('schadules','schadules.Hall_Id','=','halles.id')
            ->whereBetween('Capacity',[$MinCapcity,$Capcity])
            ->whereBetween('halles.Price',[$MinPrice,$Price])
            ->whereRaw('? Not between Date_From and Date_To', [$fromdate])
            ->select('halles.*')
            ->skip($start)->take(5)->get();
            $halles=Hall::join('schadules','schadules.Hall_Id','=','halles.id')
            ->whereBetween('Capacity',[$MinCapcity,$Capcity])
            ->whereBetween('halles.Price',[$MinPrice,$Price])
            ->whereRaw('? Not between Date_From and Date_To', [$fromdate])
            ->select('halles.*')
            ->get();
            
        }
        elseif ($datetimefrom==$mytime->toDateString("Y-m-d") && $datetimeto!=$mytime->toDateString("Y-m-d") && $District=="" && $Government=="" && $Type=="") {
           $stats=Hall::join('schadules','schadules.Hall_Id','=','halles.id')
           ->whereBetween('Capacity',[$MinCapcity,$Capcity])
                ->whereBetween('halles.Price',[$MinPrice,$Price])
                ->whereRaw('? Not between Date_From and Date_To', [$todate])
                ->select('halles.*')
                ->skip($start)->take(5)->get();
            $halles=Hall::join('schadules','schadules.Hall_Id','=','halles.id')
            ->whereBetween('Capacity',[$MinCapcity,$Capcity])
                ->whereBetween('halles.Price',[$MinPrice,$Price])
                ->whereRaw('? Not between Date_From and Date_To', [$todate])
                ->select('halles.*')
                ->get();
            
        }
        elseif ($datetimefrom==$mytime->toDateString("Y-m-d") && $datetimeto==$mytime->toDateString("Y-m-d") && $District!="" && $Government=="" && $Type=="") {
            $stats=Hall::join('places','places.id','=','halles.Place_Id')
                ->whereIN('District_Id',$District)
                ->whereBetween('Capacity',[$MinCapcity,$Capcity])
                ->whereBetween('halles.Price',[$MinPrice,$Price])
                ->select('halles.*')
                ->skip($start)->take(5)->get();
            $halles=Hall::join('places','places.id','=','halles.Place_Id')
                ->whereIN('District_Id',$District)
                ->whereBetween('Capacity',[$MinCapcity,$Capcity])
                ->whereBetween('halles.Price',[$MinPrice,$Price])
                ->select('halles.*')
                ->get();
                
        }
        elseif ($datetimefrom==$mytime->toDateString("Y-m-d") && $datetimeto==$mytime->toDateString("Y-m-d") && $District=="" && $Government!="" && $Type=="") {
            $stats=Hall::join('places','places.id','=','halles.Place_Id')
                ->join('districts','districts.id','=','places.District_Id')
                ->where('Government_Id',$Government)
                ->whereBetween('Capacity',[$MinCapcity,$Capcity])
                ->whereBetween('halles.Price',[$MinPrice,$Price])
                ->select('halles.*')
                ->skip($start)->take(5)->get();
            $halles=Hall::join('places','places.id','=','halles.Place_Id')
                ->join('districts','districts.id','=','places.District_Id')
                ->where('Government_Id',$Government)
                ->whereBetween('Capacity',[$MinCapcity,$Capcity])
                ->whereBetween('halles.Price',[$MinPrice,$Price])
                ->select('halles.*')
                ->get();

        }
        elseif ($datetimefrom==$mytime->toDateString("Y-m-d") && $datetimeto==$mytime->toDateString("Y-m-d") && $District=="" && $Government=="" && $Type!="") {
                $stats=Hall::where('Resv_Type',$Type)
                    ->whereBetween('Capacity',[$MinCapcity,$Capcity])
                    ->whereBetween('halles.Price',[$MinPrice,$Price])
                    ->skip($start)->take(5)->get();
                $halles=Hall::where('Resv_Type',$Type)
                    ->whereBetween('Capacity',[$MinCapcity,$Capcity])
                    ->whereBetween('halles.Price',[$MinPrice,$Price])
                    ->get();
        }
        elseif ($datetimefrom!=$mytime->toDateString("Y-m-d") && $datetimeto!=$mytime->toDateString("Y-m-d") && $District=="" && $Government=="" && $Type=="") {
            $stats=Hall::join('schadules','schadules.Hall_Id','=','halles.id')
            ->whereBetween('Capacity',[$MinCapcity,$Capcity])
                ->whereBetween('halles.Price',[$MinPrice,$Price])
                ->whereRaw('? Not between Date_From and Date_To', [$fromdate])
                ->whereRaw('? Not between Date_From and Date_To', [$todate])
                ->select('halles.*')
                ->skip($start)->take(5)->get();
            $halles=Hall::join('schadules','schadules.Hall_Id','=','halles.id')
            ->whereBetween('Capacity',[$MinCapcity,$Capcity])
                ->whereBetween('halles.Price',[$MinPrice,$Price])
                ->whereRaw('? Not between Date_From and Date_To', [$fromdate])
                ->whereRaw('? Not between Date_From and Date_To', [$todate])
                ->select('halles.*')
                ->get();
        }
        elseif ($datetimefrom!=$mytime->toDateString("Y-m-d") && $datetimeto==$mytime->toDateString("Y-m-d") && $District!="" && $Government=="" && $Type=="") {
            $stats=Hall::join('places','places.id','=','halles.Place_Id')
                ->join('schadules','schadules.Hall_Id','=','halles.id')
                ->whereIN('District_Id',$District)
                ->whereRaw('? Not between Date_From and Date_To', [$fromdate])
                ->whereBetween('Capacity',[$MinCapcity,$Capcity])
                ->whereBetween('halles.Price',[$MinPrice,$Price])
                ->select('halles.*')
                ->skip($start)->take(5)->get();
             $halles=Hall::join('places','places.id','=','halles.Place_Id')
                ->join('schadules','schadules.Hall_Id','=','halles.id')
                ->whereIN('District_Id',$District)
                ->whereRaw('? Not between Date_From and Date_To', [$fromdate])
                ->whereBetween('Capacity',[$MinCapcity,$Capcity])
                ->whereBetween('halles.Price',[$MinPrice,$Price])
                ->select('halles.*')
                ->get();
        }
        elseif ($datetimefrom!=$mytime->toDateString("Y-m-d") && $datetimeto==$mytime->toDateString("Y-m-d") && $District=="" && $Government!="" && $Type=="") {
           $stats=Hall::join('places','places.id','=','halles.Place_Id')
                ->join('districts','districts.id','=','places.District_Id')
                ->join('schadules','schadules.Hall_Id','=','halles.id')
                ->where('Government_Id',$Government)
                ->whereRaw('? Not between Date_From and Date_To', [$fromdate])
                ->whereBetween('Capacity',[$MinCapcity,$Capcity])
                ->whereBetween('halles.Price',[$MinPrice,$Price])
                ->select('halles.*')
                ->skip($start)->take(5)->get();
            $halles=Hall::join('places','places.id','=','halles.Place_Id')
                ->join('districts','districts.id','=','places.District_Id')
                ->join('schadules','schadules.Hall_Id','=','halles.id')
                ->where('Government_Id',$Government)
                ->whereRaw('? Not between Date_From and Date_To', [$fromdate])
                ->whereBetween('Capacity',[$MinCapcity,$Capcity])
                ->whereBetween('halles.Price',[$MinPrice,$Price])
                ->select('halles.*')
                ->get();
        }
        elseif ($datetimefrom!=$mytime->toDateString("Y-m-d") && $datetimeto==$mytime->toDateString("Y-m-d") && $District=="" && $Government=="" && $Type !="") {
            $stats=Hall::join('schadules','schadules.Hall_Id','=','halles.id')
            ->where('Resv_Type',$Type)
                ->whereRaw('? Not between Date_From and Date_To', [$fromdate])
                ->whereBetween('Capacity',[$MinCapcity,$Capcity])
                ->whereBetween('halles.Price',[$MinPrice,$Price])
                ->select('halles.*')
                ->skip($start)->take(5)->get();
             $halles=Hall::join('schadules','schadules.Hall_Id','=','halles.id')
             ->where('Resv_Type',$Type)
                ->whereRaw('? Not between Date_From and Date_To', [$fromdate])
                ->whereBetween('Capacity',[$MinCapcity,$Capcity])
                ->whereBetween('halles.Price',[$MinPrice,$Price])
                ->select('halles.*')
                ->get();
        }
        elseif ($datetimefrom==$mytime->toDateString("Y-m-d") && $datetimeto!=$mytime->toDateString("Y-m-d") && $District!="" && $Government=="" && $Type=="") {
            $stats=Hall::join('places','places.id','=','halles.Place_Id')
            ->join('schadules','schadules.Hall_Id','=','halles.id')
                ->whereIN('District_Id',$District)
                ->whereRaw('? Not between Date_From and Date_To', [$todate])
                ->whereBetween('Capacity',[$MinCapcity,$Capcity])
                ->whereBetween('halles.Price',[$MinPrice,$Price])
                ->select('halles.*')
                ->skip($start)->take(5)->get();
            $halles=Hall::join('places','places.id','=','halles.Place_Id')
            ->join('schadules','schadules.Hall_Id','=','halles.id')
                ->whereIN('District_Id',$District)
                ->whereRaw('? Not between Date_From and Date_To', [$todate])
                ->whereBetween('Capacity',[$MinCapcity,$Capcity])
                ->whereBetween('halles.Price',[$MinPrice,$Price])
                ->select('halles.*')
                ->get();
        }
        elseif ($datetimefrom==$mytime->toDateString("Y-m-d") && $datetimeto!=$mytime->toDateString("Y-m-d") && $District=="" && $Government!="" && $Type=="") {
           $stats=Hall::join('places','places.id','=','halles.Place_Id')
                ->join('districts','districts.id','=','places.District_Id')
                ->join('schadules','schadules.Hall_Id','=','halles.id')
                ->where('Government_Id',$Government)
                ->whereRaw('? Not between Date_From and Date_To', [$todate])
                ->whereBetween('Capacity',[$MinCapcity,$Capcity])
                ->whereBetween('halles.Price',[$MinPrice,$Price])
                ->select('halles.*')
                ->skip($start)->take(5)->get();
            $halles=Hall::join('places','places.id','=','halles.Place_Id')
                ->join('districts','districts.id','=','places.District_Id')
                ->join('schadules','schadules.Hall_Id','=','halles.id')
                ->where('Government_Id',$Government)
                ->whereRaw('? Not between Date_From and Date_To', [$todate])
                ->whereBetween('Capacity',[$MinCapcity,$Capcity])
                ->whereBetween('halles.Price',[$MinPrice,$Price])
                ->select('halles.*')
                ->get();
        }
        elseif ($datetimefrom==$mytime->toDateString("Y-m-d") && $datetimeto!=$mytime->toDateString("Y-m-d") && $District=="" && $Government=="" && $Type !="") {
            $stats=Hall::join('schadules','schadules.Hall_Id','=','halles.id')
            ->where('Resv_Type',$Type)
                ->whereRaw('? Not between Date_From and Date_To', [$todate])
                ->whereBetween('Capacity',[$MinCapcity,$Capcity])
                ->whereBetween('halles.Price',[$MinPrice,$Price])
                ->select('halles.*')
                ->skip($start)->take(5)->get();
            $halles=Hall::join('schadules','schadules.Hall_Id','=','halles.id')
            ->where('Resv_Type',$Type)
                ->whereRaw('? Not between Date_From and Date_To', [$todate])
                ->whereBetween('Capacity',[$MinCapcity,$Capcity])
                ->whereBetween('halles.Price',[$MinPrice,$Price])
                ->select('halles.*')
                ->get();
        }
        elseif ($datetimefrom==$mytime->toDateString("Y-m-d") && $datetimeto==$mytime->toDateString("Y-m-d") && $District!="" && $Government!="" && $Type=="") {
           $stats=Hall::join('places','places.id','=','halles.Place_Id')
                ->join('districts','districts.id','=','places.District_Id')
                ->where('Government_Id',$Government)
                ->whereIN('District_Id',$District)
                ->whereBetween('Capacity',[$MinCapcity,$Capcity])
                ->whereBetween('halles.Price',[$MinPrice,$Price])
                ->select('halles.*')
                ->skip($start)->take(5)->get();
            $halles=Hall::join('places','places.id','=','halles.Place_Id')
                ->join('districts','districts.id','=','places.District_Id')
                ->where('Government_Id',$Government)
                ->whereIN('District_Id',$District)
                ->whereBetween('Capacity',[$MinCapcity,$Capcity])
                ->whereBetween('halles.Price',[$MinPrice,$Price])
                ->select('halles.*')
                ->get();
        }
        elseif ($datetimefrom==$mytime->toDateString("Y-m-d") && $datetimeto==$mytime->toDateString("Y-m-d") && $District!="" && $Government=="" && $Type !="") {
            $stats=Hall::join('places','places.id','=','halles.Place_Id')
                ->where('Resv_Type',$Type)
                ->whereIN('District_Id',$District)
                ->whereBetween('Capacity',[$MinCapcity,$Capcity])
                ->whereBetween('halles.Price',[$MinPrice,$Price])
                ->skip($start)->take(5)->get();
            $halles=Hall::join('places','places.id','=','halles.Place_Id')
                ->where('Resv_Type',$Type)
                ->whereIN('District_Id',$District)
                ->whereBetween('Capacity',[$MinCapcity,$Capcity])
                ->whereBetween('halles.Price',[$MinPrice,$Price])
                ->get();
        }
        elseif ($datetimefrom==$mytime->toDateString("Y-m-d") && $datetimeto==$mytime->toDateString("Y-m-d") && $District=="" && $Government!="" && $Type!="") {
           $stats=Hall::join('places','places.id','=','halles.Place_Id')
                ->join('districts','districts.id','=','places.District_Id')
                ->where('Resv_Type',$Type)
                ->where('Government_Id',$Government)
                ->whereBetween('Capacity',[$MinCapcity,$Capcity])
                ->whereBetween('halles.Price',[$MinPrice,$Price])
                ->select('halles.*')
                ->skip($start)->take(5)->get();
            $halles=Hall::join('places','places.id','=','halles.Place_Id')
                ->join('districts','districts.id','=','places.District_Id')
                ->where('Resv_Type',$Type)
                ->where('Government_Id',$Government)
                ->whereBetween('Capacity',[$MinCapcity,$Capcity])
                ->whereBetween('halles.Price',[$MinPrice,$Price])
                ->select('halles.*')
                ->get();
        }
        elseif ($datetimefrom!=$mytime->toDateString("Y-m-d") && $datetimeto!=$mytime->toDateString("Y-m-d") && $District!="" && $Government=="" && $Type=="") {
           $stats=Hall::join('places','places.id','=','halles.Place_Id')
           ->join('schadules','schadules.Hall_Id','=','halles.id')
                ->whereIN('District_Id',$District)
                ->whereRaw('? Not between Date_From and Date_To', [$fromdate])
                ->whereRaw('? Not between Date_From and Date_To', [$todate])
                ->whereBetween('Capacity',[$MinCapcity,$Capcity])
                ->whereBetween('halles.Price',[$MinPrice,$Price])
                ->select('halles.*')
                ->skip($start)->take(5)->get();
            $halles=Hall::join('places','places.id','=','halles.Place_Id')
            ->join('schadules','schadules.Hall_Id','=','halles.id')
                ->whereIN('District_Id',$District)
                ->whereRaw('? Not between Date_From and Date_To', [$fromdate])
                ->whereRaw('? Not between Date_From and Date_To', [$todate])
                ->whereBetween('Capacity',[$MinCapcity,$Capcity])
                ->whereBetween('halles.Price',[$MinPrice,$Price])
                ->select('halles.*')
                ->get();
        }
        elseif ($datetimefrom!=$mytime->toDateString("Y-m-d") && $datetimeto!=$mytime->toDateString("Y-m-d") && $District=="" && $Government!="" && $Type=="") {
           $stats=Hall::join('places','places.id','=','halles.Place_Id')
                ->join('districts','districts.id','=','places.District_Id')
                ->join('schadules','schadules.Hall_Id','=','halles.id')
                ->where('Government_Id',$Government)
                ->whereRaw('? Not between Date_From and Date_To', [$fromdate])
                ->whereRaw('? Not between Date_From and Date_To', [$todate])
                ->whereBetween('Capacity',[$MinCapcity,$Capcity])
                ->whereBetween('halles.Price',[$MinPrice,$Price])
                ->select('halles.*')
                ->skip($start)->take(5)->get();
            $halles=Hall::join('places','places.id','=','halles.Place_Id')
                ->join('districts','districts.id','=','places.District_Id')
                ->join('schadules','schadules.Hall_Id','=','halles.id')
                ->where('Government_Id',$Government)
                ->whereRaw('? Not between Date_From and Date_To', [$fromdate])
                ->whereRaw('? Not between Date_From and Date_To', [$todate])
                ->whereBetween('Capacity',[$MinCapcity,$Capcity])
                ->whereBetween('halles.Price',[$MinPrice,$Price])
                ->select('halles.*')
                ->get();
        }
        elseif ($datetimefrom!=$mytime->toDateString("Y-m-d") && $datetimeto!=$mytime->toDateString("Y-m-d") && $District=="" && $Government=="" && $Type !="") {
           $stats=Hall::join('schadules','schadules.Hall_Id','=','halles.id')
           ->where('Resv_Type',$Type)
                ->whereRaw('? Not between Date_From and Date_To', [$fromdate])
                ->whereRaw('? Not between Date_From and Date_To', [$todate])
                ->whereBetween('Capacity',[$MinCapcity,$Capcity])
                ->whereBetween('halles.Price',[$MinPrice,$Price])
                ->select('halles.*')
                ->skip($start)->take(5)->get();
            $halles=Hall::join('schadules','schadules.Hall_Id','=','halles.id')
            ->where('Resv_Type',$Type)
                ->whereRaw('? Not between Date_From and Date_To', [$fromdate])
                ->whereRaw('? Not between Date_From and Date_To', [$todate])
                ->whereBetween('Capacity',[$MinCapcity,$Capcity])
                ->whereBetween('halles.Price',[$MinPrice,$Price])
                ->select('halles.*')
                ->get();
        }
        elseif ($datetimefrom!=$mytime->toDateString("Y-m-d") && $datetimeto!=$mytime->toDateString("Y-m-d") && $District!="" && $Government!="" && $Type =="") {
           $stats=Hall::join('places','places.id','=','halles.Place_Id')
                ->join('districts','districts.id','=','places.District_Id')
                ->join('schadules','schadules.Hall_Id','=','halles.id')
                ->where('Government_Id',$Government)
                ->whereIN('District_Id',$District)
                ->whereRaw('? Not between Date_From and Date_To', [$fromdate])
                ->whereRaw('? Not between Date_From and Date_To', [$todate])
                ->whereBetween('Capacity',[$MinCapcity,$Capcity])
                ->whereBetween('halles.Price',[$MinPrice,$Price])
                ->select('halles.*')
                ->skip($start)->take(5)->get();
            $halles=Hall::join('places','places.id','=','halles.Place_Id')
                ->join('districts','districts.id','=','places.District_Id')
                ->join('schadules','schadules.Hall_Id','=','halles.id')
                ->where('Government_Id',$Government)
                ->whereIN('District_Id',$District)
                ->whereRaw('? Not between Date_From and Date_To', [$fromdate])
                ->whereRaw('? Not between Date_From and Date_To', [$todate])
                ->whereBetween('Capacity',[$MinCapcity,$Capcity])
                ->whereBetween('halles.Price',[$MinPrice,$Price])
                ->select('halles.*')
                ->get();
        }
        elseif ($datetimefrom!=$mytime->toDateString("Y-m-d") && $datetimeto!=$mytime->toDateString("Y-m-d") && $District!="" && $Government=="" && $Type !="") {
               $stats=Hall::join('places','places.id','=','halles.Place_Id')
               ->join('schadules','schadules.Hall_Id','=','halles.id')
                ->whereIN('District_Id',$District)
                ->where('Resv_Type',$Type)
                ->whereRaw('? Not between Date_From and Date_To', [$fromdate])
                ->whereRaw('? Not between Date_From and Date_To', [$todate])
                ->whereBetween('Capacity',[$MinCapcity,$Capcity])
                ->whereBetween('halles.Price',[$MinPrice,$Price])
                ->select('halles.*')
                ->skip($start)->take(5)->get();
                $halles=Hall::join('places','places.id','=','halles.Place_Id')
                ->join('schadules','schadules.Hall_Id','=','halles.id')
                ->whereIN('District_Id',$District)
                ->where('Resv_Type',$Type)
                ->whereRaw('? Not between Date_From and Date_To', [$fromdate])
                ->whereRaw('? Not between Date_From and Date_To', [$todate])
                ->whereBetween('Capacity',[$MinCapcity,$Capcity])
                ->whereBetween('halles.Price',[$MinPrice,$Price])
                ->select('halles.*')
                ->get();
        }
        elseif ($datetimefrom!=$mytime->toDateString("Y-m-d") && $datetimeto!=$mytime->toDateString("Y-m-d") && $District!="" && $Government!="" && $Type !="") {
               $stats=Hall::join('places','places.id','=','halles.Place_Id')
                ->join('districts','districts.id','=','places.District_Id')
                ->join('schadules','schadules.Hall_Id','=','halles.id')
                ->where('Government_Id',$Government)
                ->whereIN('District_Id',$District)
                ->whereRaw('? Not between Date_From and Date_To', [$fromdate])
                ->whereRaw('? Not between Date_From and Date_To', [$todate])
                ->where('Resv_Type',$Type)
                ->whereBetween('Capacity',[$MinCapcity,$Capcity])
                ->whereBetween('halles.Price',[$MinPrice,$Price])
                ->select('halles.*')
                ->skip($start)->take(5)->get();
            $halles=Hall::join('places','places.id','=','halles.Place_Id')
                ->join('districts','districts.id','=','places.District_Id')
                ->join('schadules','schadules.Hall_Id','=','halles.id')
                ->where('Government_Id',$Government)
                ->whereIN('District_Id',$District)
                ->whereRaw('? Not between Date_From and Date_To', [$fromdate])
                ->whereRaw('? Not between Date_From and Date_To', [$todate])
                ->where('Resv_Type',$Type)
                ->whereBetween('Capacity',[$MinCapcity,$Capcity])
                ->whereBetween('halles.Price',[$MinPrice,$Price])
                ->select('halles.*')
                ->get();
        }
        elseif ($datetimefrom!=$mytime->toDateString("Y-m-d") && $datetimeto!=$mytime->toDateString("Y-m-d") && $District=="" && $Government!="" && $Type !="") {
               $stats=Hall::join('places','places.id','=','halles.Place_Id')
                ->join('districts','districts.id','=','places.District_Id')
                ->join('schadules','schadules.Hall_Id','=','halles.id')
                ->where('Government_Id',$Government)
                ->whereRaw('? Not between Date_From and Date_To', [$fromdate])
                ->whereRaw('? Not between Date_From and Date_To', [$todate])
                ->where('Resv_Type',$Type)
                ->whereBetween('Capacity',[$MinCapcity,$Capcity])
                ->whereBetween('halles.Price',[$MinPrice,$Price])
                ->select('halles.*')
                ->skip($start)->take(5)->get();
            $halles=Hall::join('places','places.id','=','halles.Place_Id')
                ->join('districts','districts.id','=','places.District_Id')
                ->join('schadules','schadules.Hall_Id','=','halles.id')
                ->where('Government_Id',$Government)
                ->whereRaw('? Not between Date_From and Date_To', [$fromdate])
                ->whereRaw('? Not between Date_From and Date_To', [$todate])
                ->where('Resv_Type',$Type)
                ->whereBetween('Capacity',[$MinCapcity,$Capcity])
                ->whereBetween('halles.Price',[$MinPrice,$Price])
                ->select('halles.*')
                ->get();
        }
         elseif ($datetimefrom==$mytime->toDateString("Y-m-d") && $datetimeto!=$mytime->toDateString("Y-m-d") && $District!="" && $Government!="" && $Type =="") {
               $stats=Hall::join('places','places.id','=','halles.Place_Id')
                ->join('districts','districts.id','=','places.District_Id')
                ->join('schadules','schadules.Hall_Id','=','halles.id')
                ->where('Government_Id',$Government)
                ->whereIN('District_Id',$District)
                ->whereRaw('? Not between Date_From and Date_To', [$todate])
                ->whereBetween('Capacity',[$MinCapcity,$Capcity])
                ->whereBetween('halles.Price',[$MinPrice,$Price])
                ->select('halles.*')
                ->skip($start)->take(5)->get();
            $halles=Hall::join('places','places.id','=','halles.Place_Id')
                ->join('districts','districts.id','=','places.District_Id')
                ->join('schadules','schadules.Hall_Id','=','halles.id')
                ->where('Government_Id',$Government)
                ->whereIN('District_Id',$District)
                ->whereRaw('? Not between Date_From and Date_To', [$todate])
                ->whereBetween('Capacity',[$MinCapcity,$Capcity])
                ->whereBetween('halles.Price',[$MinPrice,$Price])
                ->select('halles.*')
                ->get();
        }
         elseif ($datetimefrom==$mytime->toDateString("Y-m-d") && $datetimeto!=$mytime->toDateString("Y-m-d") && $District!="" && $Government=="" && $Type !="") {
               $stats=Hall::join('places','places.id','=','halles.Place_Id')
                ->join('districts','districts.id','=','places.District_Id')
                ->join('schadules','schadules.Hall_Id','=','halles.id')
                ->whereIN('District_Id',$District)
                ->whereRaw('? Not between Date_From and Date_To', [$todate])
                ->where('Resv_Type',$Type)
                ->whereBetween('Capacity',[$MinCapcity,$Capcity])
                ->whereBetween('halles.Price',[$MinPrice,$Price])
                ->select('halles.*')
                ->skip($start)->take(5)->get();
            $halles=Hall::join('places','places.id','=','halles.Place_Id')
                ->join('districts','districts.id','=','places.District_Id')
                ->join('schadules','schadules.Hall_Id','=','halles.id')
                ->whereIN('District_Id',$District)
                ->whereRaw('? Not between Date_From and Date_To', [$todate])
                ->where('Resv_Type',$Type)
                ->whereBetween('Capacity',[$MinCapcity,$Capcity])
                ->whereBetween('halles.Price',[$MinPrice,$Price])
                ->select('halles.*')
                ->get();
        }
         elseif ($datetimefrom==$mytime->toDateString("Y-m-d") && $datetimeto!=$mytime->toDateString("Y-m-d") && $District!="" && $Government!="" && $Type !="") {
               $stats=Hall::join('places','places.id','=','halles.Place_Id')
                ->join('districts','districts.id','=','places.District_Id')
                ->join('schadules','schadules.Hall_Id','=','halles.id')
                ->where('Government_Id',$Government)
                ->whereIN('District_Id',$District)
                ->whereRaw('? Not between Date_From and Date_To', [$todate])
                ->where('Resv_Type',$Type)
                ->whereBetween('Capacity',[$MinCapcity,$Capcity])
                ->whereBetween('halles.Price',[$MinPrice,$Price])
                ->select('halles.*')
                ->skip($start)->take(5)->get();
            $halles=Hall::join('places','places.id','=','halles.Place_Id')
                ->join('districts','districts.id','=','places.District_Id')
                ->join('schadules','schadules.Hall_Id','=','halles.id')
                ->where('Government_Id',$Government)
                ->whereIN('District_Id',$District)
                ->whereRaw('? Not between Date_From and Date_To', [$todate])
                ->where('Resv_Type',$Type)
                ->whereBetween('Capacity',[$MinCapcity,$Capcity])
                ->whereBetween('halles.Price',[$MinPrice,$Price])
                ->select('halles.*')
                ->get();
        }
         elseif ($datetimefrom==$mytime->toDateString("Y-m-d") && $datetimeto==$mytime->toDateString("Y-m-d") && $District!="" && $Government!="" && $Type !="") {
               $stats=Hall::join('places','places.id','=','halles.Place_Id')
                ->join('districts','districts.id','=','places.District_Id')
                ->where('Government_Id',$Government)
                ->whereIN('District_Id',$District)
                ->where('Resv_Type',$Type)
                ->whereBetween('Capacity',[$MinCapcity,$Capcity])
                ->whereBetween('halles.Price',[$MinPrice,$Price])
                ->select('halles.*')
                ->skip($start)->take(5)->get();
                $halles=Hall::join('places','places.id','=','halles.Place_Id')
                ->join('districts','districts.id','=','places.District_Id')
                ->where('Government_Id',$Government)
                ->whereIN('District_Id',$District)
                ->where('Resv_Type',$Type)
                ->whereBetween('Capacity',[$MinCapcity,$Capcity])
                ->whereBetween('halles.Price',[$MinPrice,$Price])
                ->select('halles.*')
                ->get();
        }
        elseif ($datetimefrom==$mytime->toDateString("Y-m-d") && $datetimeto!=$mytime->toDateString("Y-m-d") && $District=="" && $Government!="" && $Type !="") {
               $stats=Hall::join('places','places.id','=','halles.Place_Id')
                ->join('districts','districts.id','=','places.District_Id')
                ->join('schadules','schadules.Hall_Id','=','halles.id')
                ->where('Government_Id',$Government)
                ->whereRaw('? Not between Date_From and Date_To', [$todate])
                ->where('Resv_Type',$Type)
                ->whereBetween('Capacity',[$MinCapcity,$Capcity])
                ->whereBetween('halles.Price',[$MinPrice,$Price])
                ->select('halles.*')
                ->skip($start)->take(5)->get();
                $halles=Hall::join('places','places.id','=','halles.Place_Id')
                ->join('districts','districts.id','=','places.District_Id')
                ->join('schadules','schadules.Hall_Id','=','halles.id')
                ->where('Government_Id',$Government)
                ->whereRaw('? Not between Date_From and Date_To', [$todate])
                ->where('Resv_Type',$Type)
                ->whereBetween('Capacity',[$MinCapcity,$Capcity])
                ->whereBetween('halles.Price',[$MinPrice,$Price])
                ->select('halles.*')
                ->get();
        }
        $pagecount=ceil(count($halles)/5);
        $html = View::make('halls.particalview', compact('stats','pagecount'))->render();
        return Response::json(['html' => $html]);
    }
     public function changeFacilty()
    {

      
        $Price=Input::get('Price');
        $Capcity=Input::get('Capcity');
        if(Input::get('id')==null)
        {
            $Facility_id=Facility::select('id')->get();
            $id=[];
            foreach ($Facility_id as  $value) {
            array_push($id, $value->id);
            }
        }
        else
        {
            $id=Input::get('id');
        }
        
        $price1=Hall::orderBy('halles.Price', 'asc')->first();
        $MinPrice=$price1->Price;
        $Capacity1=Hall::orderBy('Capacity', 'asc')->first();
        $MinCapcity=$Capacity1->Capacity;

            $stats=Hall::join('halles_facilites','halles_facilites.Hall_id','=','halles.id')
            ->whereBetween('Capacity',[$MinCapcity,$Capcity])
            ->whereBetween('Price',[$MinPrice,$Price])
            ->whereIn('halles_facilites.Facility_id',$id)
            ->groupBy('halles_facilites.Hall_id')
            ->simplePaginate(5);
         

            $halles=Hall::join('halles_facilites','halles_facilites.Hall_id','=','halles.id')
            ->whereBetween('Capacity',[$MinCapcity,$Capcity])
            ->whereBetween('Price',[$MinPrice,$Price])
            ->whereIn('halles_facilites.Facility_id',$id)
            ->groupBy('halles_facilites.Hall_id')
            ->get();
        $pagecount=ceil(count($halles)/5);
        $html = View::make('halls.particalview', compact('stats','pagecount'))->render();
        return Response::json(['html' => $html]);
    }

    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function updateimage()
    {
        $halldata=Hall::find(Input::get('idd'));
      
       if(Input::file('pic') !=null)
{

     $file_img = Input::file('pic');
         
                $imageName = $file_img->getClientOriginalName();

                $path=public_path("/assets/dist/img/").$imageName;



                if (!file_exists($path)) {

                    // $img = \Image::make($path)->resize(200,200);
                  $file_img->move(public_path("/assets/dist/img"),$imageName);

                  $halldata->Image="/assets/dist/img/".$imageName;
                  
                }
                else
                {
                    
                   $random_string = md5(microtime());
                   // $img = \Image::make($path->getRealPath())->resize(200,200);
                   $file_img->move(public_path("/assets/dist/img"),$random_string.".jpg");
                   
                  $halldata->Image="/assets/dist/img/".$random_string.".jpg";
                
                }
}


   $halldata->save();
   return redirect ('/Halladmin/edit/'.Input::get('idd'));

    }
    public function SaveTypes()
    {
       try{
      

  $halldata=Hall::find(Input::get('HallID'));
 $halldata->Name=Input::get('Name'); 
 $halldata->Price=Input::get('Price'); 
$halldata->Place_Id=Auth::user()->Place_Id; 
 $halldata->Capacity=Input::get('Capacity');
  $halldata->WorkStart="00:00:00";
   $halldata->WorkEnd="23:00:00";  
 




if(Input::file('pic') !=null)
{

     $file_img = Input::file('pic');
         
                $imageName = $file_img->getClientOriginalName();

                $path=public_path("/assets/dist/img/").$imageName;



                if (!file_exists($path)) {

                    // $img = \Image::make($path)->resize(200,200);
                  $file_img->move(public_path("/assets/dist/img"),$imageName);

                  $halldata->Image="/assets/dist/img/".$imageName;
                  
                }
                else
                {
                    
                   $random_string = md5(microtime());
                   // $img = \Image::make($path->getRealPath())->resize(200,200);
                   $file_img->move(public_path("/assets/dist/img"),$random_string.".jpg");
                   
                  $halldata->Image="/assets/dist/img/".$random_string.".jpg";
                
                }
}








   



   $halldata->save(); 
  //Add Facilites
  $d=count(Input::get('id')); //typeid
  $n=count(Input::get('Faci'));
  $asd=DB::table('halles_facilites')
 
           ->where('Hall_id',$halldata->id)
           ->delete();
            $Exsistasd=DB::table('halles_types')
           
           ->where('Hall_id',$halldata->id)
           ->delete();
 
   for($i=0;$i<$n;$i++)
        {

$Facilitesitem=Facility::where('id',Input::get('Faci')[$i])->first();

            $Facilitesitem->GetHalls()->attach($halldata->id);
         

        }
        for($i=0;$i<$d;$i++)
        {

            $Typeitem=Type::where('id',Input::get('id')[$i])->first();
         
            $Typeitem->GetHallsType()->attach($halldata->id);
          
           
             
        }
 
        return Response::json('/Showhalladmin');
       }
        catch(Exception $e) 
          {
         return redirect('/Home');
         }
    }
    public function store(Request $request)
    {
        try{
    
        $halldata=new Hall;

        $halldata->Name=Input::get('Name'); 
        $halldata->Price=0; 
        $halldata->Place_Id=Auth::user()->Place_Id; 
        $halldata->Capacity=Input::get('Capacity'); 
        $halldata->WorkStart="00:00:00";
        $halldata->WorkEnd="23:00:00"; 
   

if(Input::file('pic') !=null)
{
     $file_img = Input::file('pic');
         
                $imageName = $file_img->getClientOriginalName();

                $path=public_path("/assets/dist/img/").$imageName;

                if (!file_exists($path)) {
                  $file_img->move(public_path("/assets/dist/img"),$imageName);
                  $halldata->Image="/assets/dist/img/".$imageName;
                  
                }
                else
                {
                   $random_string = md5(microtime());
                   $file_img->move(public_path("/assets/dist/img"),$random_string.".jpg");
                   
                  $halldata->Image="/assets/dist/img/".$random_string.".jpg";
                
                }
}


     
     $halldata->save(); 
  //Add Facilites
  $n=count(Input::get('Facilities')); //typeid
  $d=count(Input::get('types'));
 
   for($i=0;$i<$n;$i++)
        {

$Facilitesitem=Facility::where('id',Input::get('Facilities')[$i])->first();
   $Facilitesitem->GetHalls()->attach($halldata->id);
        }
        for($m=0;$m<$d;$m++)
        {

            $Typeitem=Type::where('id',Input::get('types')[$m])->first();
            $Typeitem->GetHallsType()->attach($halldata->id);
        }

// insert into hall range 
$fromcount=count(Input::get('From'));
$from=(Input::get('From'));
$to=(Input::get('To'));
$Price=(Input::get('Price'));
for($f=0;$f<$fromcount-1;$f++)
{
    $QuestionInsert=\DB::table('hall_range')->insert(
        array(
               'Hall_Id' =>$halldata->id, 
               'From'    =>$from[$f],
               'to'      =>$to[$f],
               'price'   =>$Price[$f],
        )
   );
    
}

        return redirect('/Showhalladmin');
       }
        catch(Exception $e) 
          {
         return redirect('/login');
         }
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function PlaceDetails($id)
    {


       $hallall=Hall::where('Place_Id',$id)->get();
       $PlaceData=Place::where('id',$id)->first();
     

    return view('halls.hall',compact('hall','hallall','PlaceData'));

    }
    public function show($id)
    {

        $hall=Hall::find($id);
        
        if($hall==null)
        {
            $PlaceId=$id;
        }
        else
        {
            $PlaceId=$hall->Place_Id;
        }

       $hallall=Hall::where('Place_Id',$PlaceId)->get();
       $PlaceData=Place::where('id',$PlaceId)->first();
      

        return view('halls.hall',compact('hall','hallall','PlaceData'));
    }

    public function GetAllEvents (Request $request)
    {
       if (Auth::check())
        {
            $query=Schadule::where('User_Id',Auth::user()->id)->get();
            
            $events = array();
            foreach ($query as $e)
            {
                $result = array();
                $result['id'] = $e['id'];
                $result['title'] =$e['Event_Name'];
                $result['start'] = $e['Date_From'];
                $result['end'] = $e['Date_To'];
                $result['allDay'] = false;
                $result['color'] = '#841851';
                $result['className'] = $e['Hall_Id'];

                // Merge the event array into the return array
                array_push($events,$result);

            }
        }
        return json_encode($events);
    }
    
     public function GetEventsForUser(Request $request)
    {
       $query=Schadule::where(['Hall_Id' => $request->id])->get();
       $q=Hall::where('id',$request->id)->get();

       $events = array();
       foreach ($query as $e)
       {
           $result = array();

           $stringStart = $e['Date_From'] . ' ' . $e['Time_From'];
            $dateStart = new \DateTime($stringStart);
             
            $stringEnd = $e['Date_To'] . ' ' . $e['Time_To'];
            $dateEnd = new \DateTime($stringEnd);

            $result['id'] = $e['id'];
            //$result['title'] = $e['Event_Name'];
             // $result['allDay'] = true;
            $result['start'] = $e['Date_From'];
            $result['end'] = $e['Date_To'];
            $result['color'] = '#841851';
            $result['className'] = $e['Hall_Id'];

            // Merge the event array into the return array
            array_push($events,$result);

        }

       return json_encode($events);

    }
    public function asd(Request $request)
    {
        $query=Schadule::where(['Hall_Id' => $request->id])->get();
       $q=Hall::where('id',$request->id)->get();

       $events = array();
       foreach ($query as $e)
       {
           $result = array();

           $stringStart = $e['Date_From'] . ' ' . $e['Time_From'];
            $dateStart = new \DateTime($stringStart);
             
            $stringEnd = $e['Date_To'] . ' ' . $e['Time_To'];
            $dateEnd = new \DateTime($stringEnd);

            $result['id'] = $e['id'];
            // $result['title'] = $e['Event_Name'];
            $result['start'] = $e['Date_From'];
            $result['end'] = $e['Date_To'];
            $result['allDay'] = false;
            $result['color'] = '#841851';
            $result['className'] = $e['Hall_Id'];
            $result['minTime'] = '1:00:00';
            $result['maxTime'] = '5:00:00';

            // Merge the event array into the return array
            array_push($events,$result);

        }

       return json_encode($events);
    }

    public function GetEvents(Request $request)
    {
       $query=Schadule::where(['Hall_Id' => $request->id])->get();
       $q=Hall::where('id',$request->id)->get();

       $events = array();
       foreach ($query as $e)
       {
           $result = array();

           $stringStart = $e['Date_From'] . ' ' . $e['Time_From'];
            $dateStart = new \DateTime($stringStart);
             
            $stringEnd = $e['Date_To'] . ' ' . $e['Time_To'];
            $dateEnd = new \DateTime($stringEnd);

            $result['id'] = $e['id'];
            $result['title'] = $e['Event_Name'];
            $result['start'] = $e['Date_From'];
            $result['end'] = $e['Date_To'];
            $result['allDay'] = false;
            $result['color'] = '#841851';
            $result['className'] = $e['Hall_Id'];
            $result['minTime'] = '1:00:00';
            $result['maxTime'] = '5:00:00';

            // Merge the event array into the return array
            array_push($events,$result);

        }

       return json_encode($events);

    }

   public function GetStartEndOfHall (Request $request)
    {
        $q=Hall::where('id',$request->id)->get();
       return json_encode($q);
    }

    public function dd($id)
    {
        return view('halls.hall');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       $HallData=Hall::find($id);
       $Facility=Facility::all();
       $types=Type::
        where('Type','Hall')
        ->get();
        $tyeWithchecked=[];
        $Facilitychecked=[];
        $typesHall=DB::table('halles_types')->where('Hall_id',$id)->get();
        $FaciHall=DB::table('halles_facilites')->where('Hall_id',$id)->get();       
        foreach ($types as $value) {
             $Checked='';
            foreach ($typesHall as $tt) {
                if($value->id==$tt->Type_id)
                {
                 $Checked='checked';
                 }
              
                
            }
array_push($tyeWithchecked,array('Name'=>$value->Name,'id'=>$value->id,'Checked'=>$Checked)); 
  
        } 

         foreach ($Facility as $value) {
            $Checked='';
            foreach ($FaciHall as $tt) {
                
                if($value->id==$tt->Facility_id)
                {
                   
                  $Checked='checked';
                }
               
                
            }

array_push($Facilitychecked,array('Name'=>$value->Name,'id'=>$value->id,'Checked'=>$Checked)); 
  
        } 
     
return view('halls.AddNemHall',compact('HallData','Facilitychecked','tyeWithchecked'));


    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {


        try {

         $schadules=Schadule::where('Hall_Id',$id) ->first();
         $Coworking=Coworking::where('Hall_Id',$id)->first();
        if($schadules==Null || $schadules=="" )
           {
            if($Coworking==Null || $Coworking=="")
            {
               $HallData=Hall::find($id);
             $HallData->delete();
        return redirect('/Showhalladmin'); 
          }
         else{Session::flash('flash_message', 'لا يمكن مسح القاعه مرتبطة بمواعيد امسح المواعيد اولا');
            return redirect('/Showhalladmin');
        }
             
           }
           else
           {
            Session::flash('flash_message', 'لا يمكن مسح القاعه مرتبطة بمواعيد امسح المواعيد اولا');
            return redirect('/Showhalladmin');
           } 
    }
       
            
         
        catch (Exception $e) {
        }

}
}
