<?php

namespace App\Http\Controllers;

use Auth;
use Illuminate\Http\Request;
use DB;
use App\Http\Controllers\Controller;
use Excel;
use File;
use Illuminate\Support\Facades\Response;
use Validator;
use Illuminate\Support\Facades\Redirect;
use Input;
use App\Hall;
use App\Schadule;
use Illuminate\Support\Facades\Session;


class SchaduleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Auth::check())
        {
            $hall=Hall::where('Place_Id',Auth::user()->Place_Id)->get();

            if(count($hall) == 0)
            {
                
                Session::flash('flash_message', 'يجب إضافة قاعات أولاً ');

                return view('Schadule.NoHalls');
            }
              
            return view('Schadule.eventSchadule',compact('hall'));
        }
        else
        {
            return redirect('/Home');
        }
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

     
    public function create()
    {
        $hall=Hall::where('Place_Id',Auth::user()->Place_Id)->get();
        
        if($hall->count())
        {
            return view('Schadule.create',compact('hall'));
        }
        else
        {
            Session::flash('flash_message', 'يجب اضافة القاعات المتاحة أولا !');

            return redirect('/Schadule');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {

        $rules = array(
            'Price' => 'required|numeric',
            'Date_From' => 'required|date_format:"Y/m/d H:i"',
            'Date_To' => 'required|date_format:"Y/m/d H:i"',
            'Event_Name' => 'required',

        );

            if($request->input('AddOrUpdate') != 'add')
            {
                //dd($request->all());
                $request['Hall_Id']=$request['Hall_IdU'];
            }
            $input = $request->all();
            
     //       dd($request->all());
            $event = new Schadule;
            
            $this->validate($request, $rules);
            if($request->input('IsEvent') == "")
            {
                $event->IsEvent = 0;
            }
            if($request->input('IsEvent') == "on")
            {
                 $event->IsEvent = 1;
            }
            $event->Name = $request->input('Name');
            $event->Price = $request->input('Price');
            
            $event->Date_From = $request->input('Date_From');
            $event->Date_To = $request->input('Date_To');
            
            $event->Notes = $request->input('Notes');
            $event->Event_Name = $request->input('Event_Name');
            $event->Tele = $request->input('Tele');
            $event->Hall_Id = $request->input('Hall_Id');
            $event->User_Id = Auth::user()->id;
            //dd($request->all());
            if(($event->Date_To < $event->Date_From) || ($event->Date_To == $event->Date_From))
            {
                Session::flash('flash_message', 'صيغة التاريخ غير صحيحة');

                return redirect('/Schadule');
            }
        
            $query1 = Schadule::where([
                ['Hall_Id','=', $event->Hall_Id]])->get();
            
            if ($query1->count())
            {
                if($request->input('AddOrUpdate') == 'add')
                {
                    foreach($query1 as $q)
                    {    
                        $time = strtotime($q->Date_From);
                        $time = $time + 60;
                        $qDateFromPlusMinute = date("Y/m/d H:i:s", $time);

                        $time = strtotime($q->Date_To);
                        $time = $time - 60;
                        $qDateToMinusMinute = date("Y/m/d H:i:s", $time);

                        if( (($qDateFromPlusMinute >= $event->Date_From) && ($qDateToMinusMinute <= $event->Date_To))
                            || (($qDateFromPlusMinute <= $event->Date_From) && ($qDateToMinusMinute >= $event->Date_To))
                            || (($qDateFromPlusMinute >= $event->Date_From) && ($qDateToMinusMinute >= $event->Date_To)
                                    && ($qDateFromPlusMinute <= $event->Date_To))
                            || (($qDateFromPlusMinute <= $event->Date_From) && ($qDateToMinusMinute <= $event->Date_To)
                                    && ($qDateToMinusMinute >= $event->Date_From)))
                        {
                            Session::flash('flash_message', 'القاعة محجوزة في الوقت والتاريخ الذي تم اختياره .. من فضلك اختر قاعة او وقت اخر');

                            return redirect('/Schadule');
                        
                        }
                    }
                
                    $event->save();

                    Session::flash('flash_message', 'تم اضافة الايفينت بنجاح !');

                    return redirect('/Schadule');
                }
                else
                {
                    foreach($query1 as $q)
                    {    
                        if ($request->input('AddOrUpdate') == $q->id) 
                        {
                            continue;
                        }
                        $time = strtotime($q->Date_From);
                        $time = $time + 60;
                        $qDateFromPlusMinute = date("Y/m/d H:i:s", $time);

                        $time = strtotime($q->Date_To);
                        $time = $time - 60;
                        $qDateToMinusMinute = date("Y/m/d H:i:s", $time);

                        if( (($qDateFromPlusMinute >= $event->Date_From) && ($qDateToMinusMinute <= $event->Date_To))
                            || (($qDateFromPlusMinute <= $event->Date_From) && ($qDateToMinusMinute >= $event->Date_To))
                            || (($qDateFromPlusMinute >= $event->Date_From) && ($qDateToMinusMinute >= $event->Date_To)
                                    && ($qDateFromPlusMinute <= $event->Date_To))
                            || (($qDateFromPlusMinute <= $event->Date_From) && ($qDateToMinusMinute <= $event->Date_To)
                                    && ($qDateToMinusMinute >= $event->Date_From)))

                        {
                            Session::flash('flash_message', 'القاعة محجوزة في الوقت والتاريخ الذي تم اختياره .. من فضلك اختر قاعة او وقت اخر');

                            return redirect('/Schadule');
                        
                        }
                    }
                
                    //$event->save();
                    $updatedEvent = Schadule::where('id',$request->input('AddOrUpdate'))
                                                ->update(['Name' => $event->Name, 'Price' => $event->Price,
                                                        'IsEvent' => $event->IsEvent, 'Date_From' => $event->Date_From,
                                                        'Date_To' => $event->Date_To, 'Notes' => $event->Notes,
                                                        'Event_Name' => $event->Event_Name, 'Tele' => $event->Tele,
                                                        'Hall_Id' => $event->Hall_Id]);

                    Session::flash('flash_message', 'تم تعديل الايفنت بنجاح');

                    return redirect('/Schadule');
                }
                
            }
            else
            {
                $event->save();

                Session::flash('flash_message', 'تم إضافة الايفينت بنجاح');

                return redirect('/Schadule');
            }
            
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        $query=Schadule::where('id',$request->id)->get();

       $events = array();
       foreach ($query as $e)
       {
            $result = array();

            $result['Name'] = $e['Name'];
            $result['Price'] = $e['Price'];
            $result['Date_From'] = $e['Date_From'];
            $result['Date_To'] = $e['Date_To'];
            $result['Notes'] = $e['Notes'];
            $result['IsEvent'] = $e['IsEvent'];
            $result['Event_Name'] = $e['Event_Name'];
            $result['Tele'] = $e['Tele'];
            $result['Hall_Id'] = $e['Hall_Id'];

            // Merge the event array into the return array
            array_push($events,$result);

        }

       return json_encode($events);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $event=Schadule::find($id);

         if ($event->count())
         {
             $event->delete();
             Session::flash('flash_message', 'تم حذف الايفينت بنجاح');
         }
        
        return redirect('/Schadule');
    }
    public function getHall($id)
    {
        $hall = Hall::find($id);
        return $hall;
    }
}