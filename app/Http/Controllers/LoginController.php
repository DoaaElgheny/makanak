<?php

namespace App\Http\Controllers;

use Auth;
use App\Http\Requests;
use DB;
use App\Http\Controllers\Controller;
use Request;
use Excel;
use File;
use Illuminate\Support\Facades\Response;
use Validator;
use Illuminate\Support\Facades\Redirect;
use Input;
use App\User;
use Session;
use App\Packge;
use App\OurUserOffer;
use Mail;
use App\Advertise;


class LoginController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function Profilesetting()
    {
        return view('Auth.Profilesetting');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
  

    public function Offers(Request $request)

    {

        try {
            $OurUserOffer = New OurUserOffer;
            $OurUserOffer->Name = Input::get('txtName');
            $OurUserOffer->Email = Input::get('EMAIL');
            $OurUserOffer->save();
            $data = array('Name' => Input::get('txtName'), 'Email' => Input::get('EMAIL'));
            Mail::send('emails.OfferMail', $data, function ($message) use ($data) {
                $message->to($data['Email']);
                $message->subject('Subscribe');
            });
            return redirect('/Home');
        } catch (Exception $e) {
            return redirect('/login');
        }

    }

    public function Home()
    {

        try {
            // $packages=Packge::all();
            $ads = Advertise::where('approval','=',1)->orderBy('k_index', 'DESC')->get();
            return view('admin.home',compact('ads'));
            //return view('admin.home');
            // return view('Ola.OlaPage' );
        } catch (Exception $e) {
            return redirect('/Home');

        try
        {
             // $packages=Packge::all();
            $ads = Advertise::where('approval','=',1)->orderBy('k_index', 'DESC')->get();
              return view('admin.home',compact('ads'));
             // return view('Ola.OlaPage' );
        }
         catch(Exception $e)
        {
        return redirect('/Home');

        }
    }}

    public function ReserveNow()
    {

        $data = array('Name' => Input::get('txtName'), 'Email' => Input::get('EmailRes'));
        Mail::send('emails.ReserveNow', $data, function ($message) use ($data) {
            $message->to($data['Email']);
            $message->subject('Resserve Online');
            //Email To Our Mail
        });


        $data = array('Name' => Input::get('Name'), 'EmailRes' => Input::get('EmailRes'), 'Details' => Input::get('Details'), 'Tele' => Input::get('Tele'));


        Mail::send('emails.AdminReserveNow', $data, function ($message) use ($data) {
            $message->to('makanka2019@gmail.com');
            $message->subject('Resserve Online');
            //Email To Our Mail
        });


        return Response::json("/Home");

    }

    public function Login()
    {


        if (Input::get("Password") == "" || Input::get("Username") == "") {
            return Response::json('2');
        } else {
            $logintrue = User::join('places', 'places.id', '=', 'users.Place_Id')->where('users.Email', Input::get("Username"))
                ->where('users.Password', Input::get("Password"))
                ->where('places.IsActive', 1)
                ->select('*', 'users.id as usersid')
                ->first();


            if ($logintrue != null) {
                $userdata = array(
                    'userid' => $logintrue->id
                );

                Session::put('user', $logintrue);
                Session::put('username', $logintrue->Name);
                Session::put('Logo', $logintrue->Logo);
                Session::put('role', $logintrue->Role);
                session(['user' => $logintrue]);
                Auth::loginUsingId($logintrue->usersid);
                return Response::json('/Schadule');

            } else {


                $logintrue = User::join('places', 'places.id', '=', 'users.Place_Id')->where('users.Email', Input::get("Username"))
                    ->where('places.IsActive', 1)
                    ->select('*', 'users.id as usersid')
                    ->first();

                $loginNotactive = User::join('places', 'places.id', '=', 'users.Place_Id')->where('users.Email', Input::get("Username"))
                    ->select('*', 'users.id as usersid')
                    ->first();

                if ($logintrue != null) {
                    return Response::json(1);
                } else if ($loginNotactive != Null || $loginNotactive != []) {

                    return Response::json(4);
                } else {
                    return Response::json(3);
                }

            }
        }


    }

    public function update(Request $request, $id)
    {


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
