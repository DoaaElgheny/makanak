<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use DB;
use App\Http\Controllers\Controller;
use App\Attribute;

class attributesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

       $Attribute=Attribute::all();
        return view('Tools.arrtibutes',compact('Attribute'));
    }


       public function showallDistriGovernment($id)
    {
        
try {

         Session::put('Hallis', $id);
        
        $districts = District::where('Government_Id','=',$id)->get(); 
         $governments=Government::select('Name','id')->lists('Name','id');
        return view('Tools.District',compact('districts','governments','showadditem'));
    


}//endtry
catch(Exception $e) 
    {
    return redirect('/login');
    }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try
       {
         
            $Attribute = New Attribute;
            $Attribute->Name=Input::get('txtName'); 
            $Attribute->save();         
        
        return redirect('/Attribute');
       }
        catch(Exception $e) 
        {
        return redirect('/login');
        }
    }
public function updateAttributes()
{
    try
        {
        $ID = Input::get('pk');  
        $column_name = Input::get('name');
        $column_value = Input::get('value');
        $Attribute = Attribute::whereId($ID)->first();
        $Attribute-> $column_name=$column_value;
        $Attribute->save();
        return redirect('/Attribute');    
        }
    catch(Exception $e) 
        {
         return redirect('/login');
        }
}
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {

           $Attribute=Attribute::find($id);
        
        $Attribute->delete();
        return redirect('/Attribute');

     }//endtry
    catch(Exception $e) 
    {
    return redirect('/login');
    }
   
    }
}
