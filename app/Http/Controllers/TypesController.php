<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use DB;
use App\Http\Controllers\Controller;
use App\Type;
use Session;
use Auth;

class TypesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Auth::check())
    {
        $types=Type::all();
        return view('Tools.Types',compact('types'));
    }
    else
    {
        return redirect("/Home");
    }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function updateTypes()
    {
        if (Auth::check())
    {
        try
        {
       $ID = Input::get('pk');  
        $column_name = Input::get('name');
        $column_value = Input::get('value');
    
        $TypeData = Type::whereId($ID)->first();
        $TypeData-> $column_name=$column_value;

        $TypeData->save();
      
        if($TypeData->save())
        {
            return \Response::json(array('status'=>1));
        }

        
    else {
        return \Response::json(array('status'=>0));

      }   
    }
       catch(Exception $e) 
          {
         return redirect('/login');
         }
     }
     else
     {
        return redirect ("/Home");
     }
     }
    public function store(Request $request)
    {
        if (Auth::check())
    {
         try {
          
            $TypesDtat = New Type;
            $TypesDtat->Name=Input::get('txtName');
            $TypesDtat->Type=Input::get('TypePlace'); 

             
            $TypesDtat->save();         
        
        return redirect('/Types');
            }
            catch(Exception $e) 
          {
         return redirect('/login');
         }
     }
     else
     {
        return redirect("/Home");
     }
      
       
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Auth::check())
    {
              try {

           $TypeData=Type::find($id);
           $HallTypes=DB::table('halles_types')->where('Type_id',$id)->get();
           if($HallTypes==Null || $HallTypes=="")
           {
           
        $TypeData->delete();
        return redirect('/Types');
           }
           else
           {
             Session::flash('flash_message', 'لا يمكن مسح هذا النوع');
            return redirect('/Types');
           }

        

}//endtry
catch(Exception $e) 
    {
    return redirect('/login');
    }
  } 
   else
   {
    return redirect("/Home");
   }

    }
}
